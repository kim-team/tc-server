INSERT INTO modules (module_id, is_published, priority) VALUES
('qanda',             true,     100),
('cafeteria',         true,     200),
('organizer',         true,     300),
('semesterfee',       true,     400),
('comingsoon',        true,     500),
('businesscards',     false,    600),
('bulletinboard',     true,     700),
('events',            true,     800),
('feedback',          true,     900),
('thinkbig',          false,   1000);
