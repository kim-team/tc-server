/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelinePin;
import de.thm.kim.tc.app.timeline.data.repository.TimelinePinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class TimelinePinService {

    private final TimelinePinRepository timelinePinRepository;

    @Autowired
    public TimelinePinService(TimelinePinRepository timelinePinRepository) {
        this.timelinePinRepository = timelinePinRepository;
    }

    public Collection<TimelineEntry> getAllTimelinePins() {
        return timelinePinRepository.findAll()
            .stream()
            .map(TimelinePin::getTimelineEntry)
            .collect(Collectors.toList());
    }

    public Collection<TimelineEntry> getAllTimelinePinsByUser(User user) {
        return timelinePinRepository.findAllByUser(user)
            .stream()
            .map(TimelinePin::getTimelineEntry)
            .collect(Collectors.toList());
    }

    public TimelinePin pinTimelineEntry(TimelineEntry entry) {
        return timelinePinRepository.save(new TimelinePin(entry));
    }

    public void unpinTimelineEntry(TimelineEntry entry) {
        timelinePinRepository.deleteByTimelineEntry(entry);
    }
}
