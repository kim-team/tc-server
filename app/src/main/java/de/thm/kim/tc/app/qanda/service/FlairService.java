/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.qanda.data.entity.Flair;
import de.thm.kim.tc.app.qanda.data.entity.UserFlair;
import de.thm.kim.tc.app.qanda.data.repository.FlairRepository;
import de.thm.kim.tc.app.qanda.data.repository.UserFlairRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlairService implements HealthIndicator {

    private final FlairRepository flairRepository;
    private final UserFlairRepository userFlairRepository;

    @Autowired
    public FlairService(FlairRepository flairRepository, UserFlairRepository userFlairRepository) {
        this.flairRepository = flairRepository;
        this.userFlairRepository = userFlairRepository;
    }

    @Override
    public Health health() {
        if (flairRepository.count() <= 0)
            return Health.down().build();

        return Health.up().build();
    }

    public Collection<Flair> getAllFlairs() {
        return flairRepository.findAll();
    }

    public Flair getFlairById(Long id) {
        return flairRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No Flair found for ID: " + id));
    }

    public List<Flair> getAllFlairsForUser(User user) {
        return userFlairRepository.findAllByUser(user).stream()
            .map(UserFlair::getFlair)
            .collect(Collectors.toList());
    }

    public void setFlairs(List<Long> flairIds, User user) {
        userFlairRepository.deleteAllByUser(user);

        if (flairIds.isEmpty()) {
            return;
        }

        List<UserFlair> userFlairs = new ArrayList<>();
        for (Long id : flairIds) {
            val flair = getFlairById(id);
            userFlairs.add(new UserFlair(flair, user));
        }

        userFlairRepository.saveAll(userFlairs);
    }
}
