/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.ReportedAnswer;
import de.thm.kim.tc.app.qanda.data.entity.ReportedAnswerId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ReportedAnswerRepository extends CrudRepository<ReportedAnswer, ReportedAnswerId> {

    boolean existsByReportedItemAndReportingUser(Answer answer, User user);

    void deleteByReportedItemAndReportingUser(Answer answer, User user);

    void deleteAllByReportedItemId(Long id);

    void deleteAllByReportedItem(Answer answer);

    Collection<ReportedAnswer> findAll();

    List<ReportedAnswer> findAllByOrderByReportedItemDesc();

    void deleteAllByReportingUser(User user);
}
