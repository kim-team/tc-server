UPDATE sources SET name = 'timeline.sources.editorial_dept.title'     WHERE source_id = 1;
UPDATE sources SET name = 'timeline.sources.editorial_dept.general'   WHERE source_id = 2;
UPDATE sources SET name = 'timeline.sources.editorial_dept.important' WHERE source_id = 3;
UPDATE sources SET name = 'timeline.sources.editorial_dept.events'    WHERE source_id = 4;

UPDATE sources SET name = 'timeline.sources.qanda.title'              WHERE source_id = 5;

UPDATE sources SET name = 'timeline.sources.thinkbig.title'           WHERE source_id = 6;
UPDATE sources SET name = 'timeline.sources.thinkbig_comments'        WHERE source_id = 7;

UPDATE sources SET description = 'timeline.sources.editorial_dept.description' WHERE source_id = 1;
UPDATE sources SET description = 'timeline.sources.qanda.description'          WHERE source_id = 5;
UPDATE sources SET description = 'timeline.sources.thinkbig.description'       WHERE source_id = 6;
