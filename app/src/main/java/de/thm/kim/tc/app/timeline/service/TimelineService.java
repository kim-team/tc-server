/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.qanda.data.repository.TimelineQuestionRepository;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelineLike;
import de.thm.kim.tc.app.timeline.data.repository.TimelineEntryRepository;
import de.thm.kim.tc.app.timeline.data.repository.TimelineLikeRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TimelineService {

    private final BlockService blockService;
    private final SourceService sourceService;
    private final TimelineEntryRepository timelineEntryRepository;
    private final TimelinePinService timelinePinService;
    private final TimelineQuestionRepository timelineQuestionRepository;
    private final UserTimelineFaveService userTimelineFaveService;
    private final UserTimelineHideService userTimelineHideService;
    private final UserTimelineUnpinService userTimelineUnpinService;
    private final TimelineLikeRepository timelineLikeRepository;
    @Lazy
    @Autowired
    private TimelineReportService timelineReportService;

    @Autowired
    public TimelineService(BlockService blockService,
                           SourceService sourceService,
                           TimelineEntryRepository timelineEntryRepository,
                           TimelinePinService timelinePinService,
                           TimelineQuestionRepository timelineQuestionRepository,
                           UserTimelineFaveService userTimelineFaveService,
                           UserTimelineHideService userTimelineHideService,
                           UserTimelineUnpinService userTimelineUnpinService,
                           TimelineLikeRepository timelineLikeRepository) {
        this.blockService = blockService;
        this.sourceService = sourceService;
        this.timelineEntryRepository = timelineEntryRepository;
        this.timelinePinService = timelinePinService;
        this.timelineQuestionRepository = timelineQuestionRepository;
        this.userTimelineFaveService = userTimelineFaveService;
        this.userTimelineHideService = userTimelineHideService;
        this.userTimelineUnpinService = userTimelineUnpinService;
        this.timelineLikeRepository = timelineLikeRepository;
    }

    public Page<TimelineEntry> getAllTimelineEntries(Pageable pageable) {
        Collection<Long> blacklist = timelinePinService.getAllTimelinePins().stream().map(TimelineEntry::getId).collect(Collectors.toList());
        return timelineEntryRepository.findAllByIdIsNotIn(blacklist, pageable);
    }

    public List<TimelineEntry> getAllTimelineEntriesAsList() {
        return timelineEntryRepository.findAllByOrderByIdDesc();
    }

    public Page<TimelineEntry> getAllTimelineEntriesByUser(User user, Pageable pageable) {
        Collection<Source> sources = sourceService.getAllSourcesByUser(user);
        Collection<Long> blacklistTimelineEntries = Stream.of(
            userTimelineHideService.getAllHiddenTimelineEntriesByUser(user)
                .stream()
                .map(TimelineEntry::getId),
            timelinePinService.getAllTimelinePinsByUser(user)
                .stream()
                .map(TimelineEntry::getId),
            getTimelineEntryIdsForBlockedQuestionAuthors(user))
            .flatMap(Function.identity())
            .collect(Collectors.toSet());

        if (blacklistTimelineEntries.isEmpty())
            return timelineEntryRepository.findAllBySourceIn(sources, pageable);
        else
            return timelineEntryRepository.findAllBySourceInAndIdIsNotIn(sources, blacklistTimelineEntries, pageable);
    }

    private Stream<Long> getTimelineEntryIdsForBlockedQuestionAuthors(User user) {
        val blockedUsers = blockService.getBlockedUsers(user);

        return timelineQuestionRepository.findAllByQuestion_AuthorIn(blockedUsers)
            .stream()
            .map(tle -> tle.getTimelineEntry().getId());
    }

    public TimelineEntry getTimelineEntryById(Long timelineEntryId) {
        return timelineEntryRepository.findById(timelineEntryId)
            .orElseThrow(() -> new ResourceNotFoundException("No TimelineEntry found for ID: " + timelineEntryId));
    }

    public TimelineEntry createNewTimelineEntry(Source source, String text, String contentArgs) {
        return timelineEntryRepository.save(new TimelineEntry(source, text, contentArgs));
    }

    public TimelineEntry updateTimelineEntry(TimelineEntry entry) {
        return timelineEntryRepository.save(entry);
    }

    @Transactional
    public void deleteTimelineEntry(TimelineEntry entry) {
        // For lack of proper cascading in the database, all references to
        // timeline entries need to be deleted before the entry can be removed.

        // Reports
        timelineReportService.removeAllReports(entry);

        // User-unpinned and pinned
        userTimelineUnpinService.deleteUnpinTimelineEntry(entry);
        timelinePinService.unpinTimelineEntry(entry);

        // Hidden
        userTimelineHideService.restoreTimelineEntryForAllUsers(entry);

        // Faves
        userTimelineFaveService.deleteTimelineFave(entry);

        // Linked Question
        timelineQuestionRepository.deleteByTimelineEntry(entry);

        // Timeline likes
        timelineLikeRepository.deleteAllByTimelineEntry(entry);

        // Finally delete TimelineEntry
        timelineEntryRepository.delete(entry);
    }

    public Collection<TimelineLike> getTimelineLikesForTimelineEntry(TimelineEntry entry) {
        return timelineLikeRepository.findAllByTimelineEntry(entry);
    }

    public TimelineLike createNewTimelineLike(TimelineEntry entry, User user) {
        return timelineLikeRepository.findByTimelineEntryAndUser(entry, user)
            .orElse(timelineLikeRepository.save(new TimelineLike(entry, user)));
    }

    public TimelineLike removeTimelineLike(TimelineEntry entry, User user) {
        TimelineLike timelineLike = timelineLikeRepository.findByTimelineEntryAndUser(entry, user)
            .orElseThrow(() -> new ResourceNotFoundException("No TimelineLike found for TimelineEntry ID: " + entry.getId() + " and User ID: " + user.getId()));

        timelineLikeRepository.delete(timelineLike);
        return timelineLike;
    }
}
