/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.praxis.data.entity.PpUnit;
import de.thm.kim.tc.app.praxis.data.repository.PpUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PpUnitService {

    private final PpEntryService entryService;
    private final PpMediaService mediaService;
    private final PpQuestionService questionService;
    private final PpUnitRepository ppUnitRepository;

    @Autowired
    public PpUnitService(PpEntryService entryService,
                         PpMediaService mediaService,
                         PpQuestionService questionService,
                         PpUnitRepository ppUnitRepository) {
        this.entryService = entryService;
        this.mediaService = mediaService;
        this.questionService = questionService;
        this.ppUnitRepository = ppUnitRepository;
    }

    public List<PpUnit> getUnitsForGroup(Long groupId) {
        return ppUnitRepository.findAllByGroupIdOrderByOrder(groupId);
    }

    public List<PpUnit> getAllUnits() {
        return ppUnitRepository.findAllByOrderByOrder();
    }

    public Optional<PpUnit> getUnitById(Long unitId) {
        return ppUnitRepository.findById(unitId);
    }

    public PpUnit createOrUpdateUnit(PpUnit unit) {
        return ppUnitRepository.save(unit);
    }

    @Transactional
    public void deleteUnitById(Long unitId) {
        entryService.getEntriesForUnit(unitId)
            .forEach(e -> entryService.deleteEntryById(e.getEntryId()));
        mediaService.getMediaForUnit(unitId)
            .forEach(m -> mediaService.deleteMediaById(m.getMediaId()));
        questionService.getQuestionsForUnit(unitId)
            .forEach(q -> questionService.deleteQuestionById(q.getQuestionId()));
        ppUnitRepository.deleteById(unitId);
    }
}
