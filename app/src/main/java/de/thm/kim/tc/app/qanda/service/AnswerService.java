/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.data.entity.QuestionCategory;
import de.thm.kim.tc.app.qanda.data.repository.AnswerRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static de.thm.kim.tc.app.qanda.constants.Constants.ACTIVITY_TIMEFRAME;

@Service
public class AnswerService {

    private final AnswerRepository answerRepository;
    private final BlockService blockService;
    private final LikeService likeService;
    private final ReportService<Answer> reportService;

    @Autowired
    public AnswerService(AnswerRepository answerRepository,
                         BlockService blockService,
                         LikeService likeService,
                         ReportService<Answer> reportService) {
        this.answerRepository = answerRepository;
        this.blockService = blockService;
        this.likeService = likeService;
        this.reportService = reportService;
    }

    public List<Answer> getAnswersForQuestion(Question question, Optional<User> user) {
        val blockedUsers = user
            .map(blockService::getBlockedUsers)
            .orElse(Collections.emptySet());

        return answerRepository.findAllByQuestionOrderByDateDesc(question)
            .stream()
            .filter(a -> user.isPresent() && a.getAuthor().equals(user.get())
                || !blockedUsers.contains(a.getAuthor()))
            .collect(Collectors.toList());
    }

    public int countAnswersForQuestion(Question question) {
        return answerRepository.countAllByQuestion(question);
    }

    public Answer createOrUpdateAnswer(Answer answer) {
        return answerRepository.save(answer);
    }

    public Answer getAnswerById(Long id) {
        return answerRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No Answer found for ID: " + id));
    }

    public Long getAnswerCountByUser(User user) {
        return answerRepository.countAllByAuthor(user);
    }

    public List<Answer> getAllAnswers() {
        return answerRepository.findAllByOrderByDateDesc();
    }

    public List<Answer> getAllAnswersByUser(User user) {
        return answerRepository.findAllByAuthorOrderByDateDesc(user);
    }

    @Transactional
    public void deleteAnswer(Answer answer) {
        reportService.removeAllReports(answer);
        likeService.deleteAllAnswerLikesForAnswer(answer);
        answerRepository.delete(answer);
    }

    public int getAnswerActivitiesForCategory(QuestionCategory questionCategory) {
        Date date = new Date(System.currentTimeMillis() - ACTIVITY_TIMEFRAME);
        return answerRepository.countByQuestion_CategoryIdAndDateAfter(questionCategory.getId(), date);
    }

    public int getReplyActivityForUser(User user) {
        Date date = new Date(System.currentTimeMillis() - ACTIVITY_TIMEFRAME);
        return answerRepository.countByQuestion_AuthorAndAuthorNotAndDateAfter(user, user, date);
    }
}
