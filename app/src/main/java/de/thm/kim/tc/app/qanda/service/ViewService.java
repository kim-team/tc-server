/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.data.entity.QuestionView;
import de.thm.kim.tc.app.qanda.data.repository.QuestionViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ViewService {

    private final QuestionViewRepository repository;

    @Autowired
    public ViewService(QuestionViewRepository repository) {
        this.repository = repository;
    }

    public QuestionView registerView(Question question, User user) {
        return repository.findByQuestionAndUser(question, user)
            .orElse(repository.save(new QuestionView(question, user)));
    }

    public int getViewCountForQuestion(Question question) {
        return repository.countByQuestion(question);
    }

    public void deleteAllQuestionViewsForQuestion(Question question) {
        repository.deleteAllByQuestion(question);
    }

    @Transactional
    public void deleteAllByUser(User user) {
        repository.deleteAllByUser(user);
    }
}
