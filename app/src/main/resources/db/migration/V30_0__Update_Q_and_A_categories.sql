UPDATE categories SET title = 'Feedback tinyCampus' WHERE category_id = 1;
UPDATE categories SET title = 'Allgemein THM'       WHERE category_id = 6;
UPDATE categories SET title = 'Campus THM'          WHERE category_id = 8;
UPDATE categories SET title = 'Smalltalk'           WHERE category_id = 10; -- 'Sonstiges'

-- Add 'Events' and 'Lost and Found' categories
INSERT INTO categories (category_id, parent_id, text, logo, title) VALUES
(176, null, '', '', 'Events'),
(177, null, '', '', 'Lost and Found'),
(178, 177,  '', '', 'Lost'),
(179, 177,  '', '', 'Found');

DELETE FROM categories WHERE category_id = 11; -- 'Fehler in der App melden'
DELETE FROM categories WHERE category_id = 12; -- 'Kontakt zum Team'
