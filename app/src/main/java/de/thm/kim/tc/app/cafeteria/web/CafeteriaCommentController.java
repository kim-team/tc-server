/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaCommentLike;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaCommentService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaLikeService;
import de.thm.kim.tc.app.common.reports.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Api(description = "Everything about cafeteria comments", tags = {"Cafeteria - Comments"})
@RestController
@RequestMapping(path = "/cafeteria")
public class CafeteriaCommentController {

    final private CafeteriaCommentService cafeteriaCommentService;
    final private ReportService<CafeteriaComment> reportService;
    final private CafeteriaLikeService cafeteriaLikeService;
    final private UserService userService;

    @Autowired
    public CafeteriaCommentController(CafeteriaCommentService cafeteriaCommentService,
                                      ReportService<CafeteriaComment> reportService,
                                      CafeteriaLikeService cafeteriaLikeService,
                                      UserService userService) {
        this.cafeteriaCommentService = cafeteriaCommentService;
        this.reportService = reportService;
        this.cafeteriaLikeService = cafeteriaLikeService;
        this.userService = userService;
    }

    @ApiOperation(value = "Get all comments for cafeteria item")
    @GetMapping(path = "/{cafeteriaItem}/comments")
    public List<CafeteriaCommentOutputDto> getCommentsForCafeteriaItem(Principal principal, @PathVariable("cafeteriaItem") String cafeteriaItem) {
        Optional<User> user = userService.getUserByPrincipal(principal);
        List<CafeteriaComment> cafeteriaComments = cafeteriaCommentService.getCafeteriaCommentsForCafeteriaItem(cafeteriaItem, user);

        return cafeteriaComments.stream()
            .map(c -> {
                Collection<CafeteriaCommentLike> likes = cafeteriaLikeService.getCafeteriaCommentLikesForCafetriaComment(c);
                boolean isLiked = (user.isPresent() &&
                    likes.stream().anyMatch(l -> l.getUser().equals(user.get())));
                boolean canEdit = (user.isPresent() && c.getAuthor().equals(user.get()));
                boolean isReported = reportService.isReportedByUser(c, user);

                return new CafeteriaCommentOutputDto(c, likes.size(), isLiked, canEdit, isReported);
            })
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Create a new comment for a cafeteria item")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/{cafeteriaItem}/comments")
    public CafeteriaCommentOutputDto createComment(Principal principal,
                                                   @Valid @RequestBody CafeteriaCommentInputDto input,
                                                   @PathVariable("cafeteriaItem") String cafeteriaItem) {
        User user = userService.getUserByUsername(principal.getName());
        CafeteriaComment cafeteriaComment = cafeteriaCommentService.createNewCafeteriaComment(cafeteriaItem, user, input.getText());

        return new CafeteriaCommentOutputDto(cafeteriaComment);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Report cafeteria comment by ID")
    @PostMapping(path = "/comments/{commentId}/report")
    @ResponseStatus(HttpStatus.CREATED)
    public void reportComment(@PathVariable("commentId") Long commentId,
                              Principal principal) {
        CafeteriaComment comment = cafeteriaCommentService.getCafeteriaCommentById(commentId);
        User user = userService.getUserByUsername(principal.getName());

        reportService.report(comment, user);
    }

    @Secured("ROLE_USER")
    @Transactional
    @ApiOperation(value = "Remove cafeteria comment by comment ID (only the comment's author can remove)")
    @ResponseStatus(HttpStatus.CREATED)
    @DeleteMapping(path = "/comments/{commentId}")
    public ResponseEntity<?> deleteComment(Principal principal,
                                           @PathVariable("commentId") Long commentId) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        CafeteriaComment comment = cafeteriaCommentService.getCafeteriaCommentById(commentId);

        if (!comment.getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        cafeteriaCommentService.deleteComment(comment);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
