/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.service.AuthUserService;
import de.thm.kim.tc.app.auth.service.AuthUserVerificationService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaLikeService;
import de.thm.kim.tc.app.modules.service.ModuleService;
import de.thm.kim.tc.app.qanda.data.entity.Badge;
import de.thm.kim.tc.app.qanda.data.entity.UserBadge;
import de.thm.kim.tc.app.qanda.service.AnswerService;
import de.thm.kim.tc.app.qanda.service.BadgeService;
import de.thm.kim.tc.app.qanda.service.LikeService;
import de.thm.kim.tc.app.qanda.service.QuestionService;
import de.thm.kim.tc.app.thinkbig.service.ThinkBigLikeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Api(description = "Everything about user profiles", tags = {"Profile"})
@RestController
public class ProfileController {

    private final AnswerService answerService;
    private final AuthUserService authUserService;
    private final AuthUserVerificationService authUserVerificationService;
    private final BadgeService badgeService;
    private final CafeteriaLikeService cafeteriaLikeService;
    private final LikeService likeService;
    private final ModuleService moduleService;
    private final QuestionService questionService;
    private final ThinkBigLikeService thinkBigLikeService;
    private final UserService userService;

    @Autowired
    public ProfileController(AnswerService answerService,
                             AuthUserService authUserService,
                             BadgeService badgeService,
                             CafeteriaLikeService cafeteriaLikeService,
                             LikeService likeService,
                             ModuleService moduleService,
                             QuestionService questionService,
                             ThinkBigLikeService thinkBigLikeService,
                             UserService userService,
                             AuthUserVerificationService authUserVerificationService) {
        this.answerService = answerService;
        this.authUserService = authUserService;
        this.authUserVerificationService = authUserVerificationService;
        this.badgeService = badgeService;
        this.cafeteriaLikeService = cafeteriaLikeService;
        this.likeService = likeService;
        this.moduleService = moduleService;
        this.questionService = questionService;
        this.thinkBigLikeService = thinkBigLikeService;
        this.userService = userService;
    }

    @ApiOperation(value = "Find user profile by user ID")
    @GetMapping(path = "/profile")
    public ProfileDto profile(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);
        val emailSent = user
            .map(u -> authUserVerificationService.checkMailStatus(u.getUsername()))
            .orElse(false);
        val isVerified = user
            .map(u -> authUserService.getAuthUser(u.getUsername()).isVerified())
            .orElse(false);
        val level = user.map(badgeService::getUserLevel).orElse(1L);

        Map<String, Long> hearts = getHeartsForUser(user);
        Long totalHearts = hearts.values().stream().mapToLong(Long::longValue).sum();
        List<BadgeDto> badges = prepareBadgeDtos(user, totalHearts);

        return user.map(u -> new ProfileDto(u, isVerified, emailSent, level, totalHearts, hearts, badges))
            .orElseThrow(() -> new UsernameNotFoundException("There is no user with that username!"));
    }

    private Map<String, Long> getHeartsForUser(Optional<User> user) {
        val result = new HashMap<String, Long>();

        // TODO i18n
        if (moduleService.isQAndAPublished())
            result.put("Fragen und Antworten",
                user.map(u ->
                    (long) (likeService.getAnswerLikesForAnswerAuthor(u).size()
                        + likeService.getQuestionLikesForQuestionAuthor(u).size()))
                    .orElse(0L));

        // TODO i18n
        if (moduleService.isThinkBigPublished())
            result.put("Think Big",
                user.map(u ->
                    (long) thinkBigLikeService.getCommentLikesForUser(u).size())
                    .orElse(0L));

        // TODO i18n
        if (moduleService.isCafeteriaPublished())
            result.put("Mensa",
                user.map(cafeteriaLikeService::getCafeteriaLikesForUser)
                    .orElse(0L));

        return result;
    }

    private List<BadgeDto> prepareBadgeDtos(Optional<User> user, Long hearts) {
        Collection<Badge> availableBadges = badgeService.getAllBadges();

        long answers = user.map(answerService::getAnswerCountByUser).orElse(0L);
        long questions = user.map(questionService::getQuestionCountByUser).orElse(0L);
        long level = user.map(badgeService::getUserLevel).orElse(0L);

        List<BadgeDto> badges = new ArrayList<>();

        if (user.isPresent()) {
            Collection<UserBadge> earnedBadges = badgeService.getAllUserBadgesForUser(user.get());

            // Remove earned badges from available badges
            availableBadges.removeAll(earnedBadges.stream()
                .map(UserBadge::getBadge)
                .collect(Collectors.toList()));

            // Merge earned and unearned badges
            badges.addAll(earnedBadges.stream()
                .map(b -> new BadgeDto(b, getBadgeConditions(b.getBadge(), answers, questions, hearts, level)))
                .collect(Collectors.toList()));
        }

        badges.addAll(availableBadges.stream()
            .map(b -> new BadgeDto(b, getBadgeConditions(b, answers, questions, hearts, level)))
            .collect(Collectors.toList()));

        badges.sort(Comparator.comparing(BadgeDto::getBadgeId));

        return badges;
    }

    private List<BadgeCondition> getBadgeConditions(Badge badge, Long answers, Long questions, Long hearts, Long level) {
        List<BadgeCondition> conditions = new ArrayList<>();

        switch (badge.getId().intValue()) {
            // Questions
            case 1:
                // Ask one question
                conditions.add(new BadgeCondition(questions, 1L));
                break;
            case 2:
                // Ask five questions
                conditions.add(new BadgeCondition(questions, 5L));
                break;
            case 3:
                // Ask 25 questions
                conditions.add(new BadgeCondition(questions, 25L));
                break;

            // Answers
            case 4:
                // Post one answer
                conditions.add(new BadgeCondition(answers, 1L));
                break;
            case 5:
                // Post five answers
                conditions.add(new BadgeCondition(answers, 5L));
                break;
            case 6:
                // Post 25 answers
                conditions.add(new BadgeCondition(answers, 25L));
                break;

            // Hearts
            case 7:
                // Receive five hearts
                conditions.add(new BadgeCondition(hearts, 5L));
                break;
            case 8:
                // Receive 25 hearts
                conditions.add(new BadgeCondition(hearts, 25L));
                break;
            case 9:
                // Receive a hundred hearts
                conditions.add(new BadgeCondition(hearts, 100L));
                break;

            // Levels
            case 10:
                // Gain level 3
                conditions.add(new BadgeCondition(level, 3L));
                break;
            case 11:
                // Gain level 6
                conditions.add(new BadgeCondition(level, 6L));
                break;
            case 12:
                // Gain level 11
                conditions.add(new BadgeCondition(level, 11L));
                break;

            default:
                throw new IllegalArgumentException("No Badge found for ID: " + badge.getId());
        }

        return conditions;
    }

    @Secured("ROLE_USER")
    @PostMapping("/change/name/{displayName}")
    public void changeDisplayName(Principal principal, @PathVariable String displayName) {
        userService.changeDisplayName(principal.getName(), displayName);
    }
}
