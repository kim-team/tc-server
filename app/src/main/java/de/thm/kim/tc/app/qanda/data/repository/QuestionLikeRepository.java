/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.data.entity.QuestionLike;
import de.thm.kim.tc.app.qanda.data.entity.QuestionLikeId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface QuestionLikeRepository extends CrudRepository<QuestionLike, QuestionLikeId> {
    Collection<QuestionLike> findAllByQuestion(Question question);

    Collection<QuestionLike> findAllByQuestion_Author(User user);

    Optional<QuestionLike> findByQuestionAndUser(Question question, User user);

    void deleteAllByQuestion(Question question);

    void deleteAllByUser(User user);
}
