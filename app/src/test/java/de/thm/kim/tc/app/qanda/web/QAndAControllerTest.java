/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaLikeService;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.service.*;
import de.thm.kim.tc.app.thinkbig.service.ThinkBigLikeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class QAndAControllerTest {

    private static final String URL = "/questions/";
    private static final String ANSWER_TEXT = "äh, das hab ich jetzt akustisch nicht verstanden";
    private static final Long ANSWER_ID = 5L;
    private static final String QUESTION_TEXT = "Wie bitte?";
    private static final Long QUESTION_ID = 6L;
    private static final String VALID_USERNAME = "blocked";

    @Autowired
    private WebApplicationContext context;

    @SpyBean
    private QuestionService questionService;
    @SpyBean
    private AnswerService answerService;
    @SpyBean
    private LikeService likeService;
    @SpyBean
    private CafeteriaLikeService cafeteriaLikeService;
    @SpyBean
    private HelpfulAnswerService helpfulAnswerService;
    @SpyBean
    private UserService userService;
    @SpyBean
    private QuestionCategoryService questionCategoryService;
    @SpyBean
    private BadgeService badgeService;
    @SpyBean
    private ViewService viewService;
    @SpyBean
    private ReportService<Answer> answerReportService;
    @SpyBean
    private ReportService<Question> questionReportService;
    @SpyBean
    private ThinkBigLikeService thinkBigLikeService;

    private MockMvc mockMvc;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Transactional
    @WithMockUser(VALID_USERNAME)
    @Test
    public void get_all_questions() throws Exception {
        // Expected number of questions
        final int n = 6;

        mockMvc.perform(get(URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(n)));

        verify(questionService).getAllQuestionsForUser(any());
        verify(likeService, times(n)).getQuestionLikesForQuestion(any());
        verify(viewService, times(n)).getViewCountForQuestion(any());
        verify(answerService, times(n)).countAnswersForQuestion(any());
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void post_new_answer_to_question() throws Exception {
        AnswerInputDto answer = new AnswerInputDto(ANSWER_TEXT);

        // FIXME Workaround to avoid PK constraint violation on Postgres
        answerService.getAllAnswers().forEach(a -> answerService.deleteAnswer(a));

        mockMvc.perform(post(URL + QUESTION_ID + "/answers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonMapper.writeValueAsString(answer)))
            .andExpect(status().isCreated())
            // ID will be 1 on Postgres and higher on H2 and due to workaround above
            .andExpect(jsonPath("$.answerId", is(oneOf(1, 13))))
            .andExpect(jsonPath("$.author.authorId", is(6)))
            .andExpect(jsonPath("$.author.displayName", is("Blocked User")))
            .andExpect(jsonPath("$.author.flairs", is(empty())))
            .andExpect(jsonPath("$.text", is(ANSWER_TEXT)))
            .andExpect(jsonPath("$.likes", is(0)))
            .andExpect(jsonPath("$.canEdit", is(true)))
            .andExpect(jsonPath("$.liked", is(false)))
            .andExpect(jsonPath("$.helpful", is(false)));

        verify(userService).getUserByUsername(VALID_USERNAME);
        verify(questionService).getQuestionById(QUESTION_ID);
        verify(answerService).createOrUpdateAnswer(any(Answer.class));
        verify(answerService).getAnswerCountByUser(any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void edit_existing_answer() throws Exception {
        AnswerInputDto answer = new AnswerInputDto(ANSWER_TEXT);

        mockMvc.perform(patch(URL + "answers/" + ANSWER_ID)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonMapper.writeValueAsString(answer)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.answerId", is(ANSWER_ID.intValue())))
            .andExpect(jsonPath("$.author.authorId", is(6)))
            .andExpect(jsonPath("$.author.displayName", is("Blocked User")))
            .andExpect(jsonPath("$.author.flairs", is(empty())))
            .andExpect(jsonPath("$.text", is(ANSWER_TEXT)))
            .andExpect(jsonPath("$.likes", is(0)))
            .andExpect(jsonPath("$.canEdit", is(true)))
            .andExpect(jsonPath("$.liked", is(false)))
            .andExpect(jsonPath("$.helpful", is(false)));

        verify(userService).getUserByPrincipal(any(Principal.class));
        verify(answerService).getAnswerById(ANSWER_ID);
        verify(answerService).createOrUpdateAnswer(any(Answer.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void edit_existing_question() throws Exception {
        QuestionUpdateDto question = new QuestionUpdateDto(QUESTION_TEXT);

        mockMvc.perform(patch(URL + QUESTION_ID)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonMapper.writeValueAsString(question)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is(QUESTION_ID.intValue())))
            .andExpect(jsonPath("$.category", is("Feedback tinyCampus")))
            .andExpect(jsonPath("$.author.authorId", is(6)))
            .andExpect(jsonPath("$.author.displayName", is("Blocked User")))
            .andExpect(jsonPath("$.author.flairs", is(empty())))
            .andExpect(jsonPath("$.text", is(QUESTION_TEXT)))
            .andExpect(jsonPath("$.numberOfAnswers", is(1)))
            .andExpect(jsonPath("$.helpfulAnswer", nullValue()))
            .andExpect(jsonPath("$.answers", empty()))
            .andExpect(jsonPath("$.views", is(0)))
            .andExpect(jsonPath("$.likes", is(0)))
            .andExpect(jsonPath("$.canEdit", is(true)))
            .andExpect(jsonPath("$.liked", is(false)))
            .andExpect(jsonPath("$.reported", is(false)));

        verify(userService).getUserByPrincipal(any(Principal.class));
        verify(questionService).getQuestionById(QUESTION_ID);
        verify(questionService).createOrUpdateQuestion(any(Question.class));
    }
}
