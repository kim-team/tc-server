/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpPhase;
import de.thm.kim.tc.app.praxis.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis phases", tags = {"Praxis - Admin - Phases"})
@RestController
@RequestMapping(path = "/praxis")
class PhaseController {

    private final PpPhaseService ppPhaseService;
    private final PpPersonService ppPersonService;
    private final PpAddressService ppAddressService;
    private final PpGroupService ppGroupService;
    private final PpUnitService ppUnitService;
    private final PpMediaService ppMediaService;
    private final PpEntryService ppEntryService;
    private final PpQuestionService ppQuestionService;
    private final PpAnswerService ppAnswerService;

    @Autowired
    PhaseController(PpPhaseService ppPhaseService,
                    PpPersonService ppPersonService,
                    PpAddressService ppAddressService,
                    PpGroupService ppGroupService,
                    PpUnitService ppUnitService,
                    PpMediaService ppMediaService,
                    PpEntryService ppEntryService,
                    PpQuestionService ppQuestionService,
                    PpAnswerService ppAnswerService) {
        this.ppPhaseService = ppPhaseService;
        this.ppPersonService = ppPersonService;
        this.ppAddressService = ppAddressService;
        this.ppGroupService = ppGroupService;
        this.ppUnitService = ppUnitService;
        this.ppMediaService = ppMediaService;
        this.ppEntryService = ppEntryService;
        this.ppQuestionService = ppQuestionService;
        this.ppAnswerService = ppAnswerService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all praxis phases", tags = {"Praxis - Admin - Phases"})
    @GetMapping(path = "/phases")
    public List<PhaseDto> getAllPhases() {
        return ppPhaseService.getAllPhases().stream()
            .map(p -> new PhaseDto(p,
                GroupController.getGroupDtosForPhase(
                    ppGroupService,
                    ppUnitService,
                    ppMediaService,
                    ppEntryService,
                    ppQuestionService,
                    ppAnswerService,
                    p),
                PersonController.getPersonDtosForPhase(
                    ppPersonService,
                    ppAddressService,
                    p)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Full content for specified praxis phase", tags = {"Praxis - App"})
    @GetMapping(path = "/phases/{phaseId}")
    public PhaseDto getPhase(@PathVariable("phaseId") Long phaseId) {
        return ppPhaseService.getPhaseById(phaseId)
            .map(p -> new PhaseDto(p,
                GroupController.getGroupDtosForPhase(
                    ppGroupService,
                    ppUnitService,
                    ppMediaService,
                    ppEntryService,
                    ppQuestionService,
                    ppAnswerService,
                    p),
                PersonController.getPersonDtosForPhase(
                    ppPersonService,
                    ppAddressService,
                    p)))
            .orElseThrow(() -> new ResourceNotFoundException("No Phase found for ID: " + phaseId));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new praxis phase", tags = {"Praxis - Admin - Phases"})
    @PostMapping(path = "/phases")
    @ResponseStatus(HttpStatus.CREATED)
    public PhaseDto savePhase(@RequestBody PpPhase phase) {
        return new PhaseDto(ppPhaseService.createOrUpdatePhase(phase),
            new ArrayList<>(), new ArrayList<>());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a praxis phase", tags = {"Praxis - Admin - Phases"})
    @PutMapping(path = "/phases/{phaseId}")
    public PhaseDto updatePhase(@PathVariable("phaseId") Long phaseId, @RequestBody PpPhase phase) {
        if (phase.getPhaseId() == null || !phase.getPhaseId().equals(phaseId))
            throw new IllegalArgumentException("Invalid phaseId in body");

        ppPhaseService.getPhaseById(phaseId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpPhase for ID: " + phaseId));

        PpPhase updatedPhase = ppPhaseService.createOrUpdatePhase(phase);

        return new PhaseDto(updatedPhase,
            GroupController.getGroupDtosForPhase(
                ppGroupService,
                ppUnitService,
                ppMediaService,
                ppEntryService,
                ppQuestionService,
                ppAnswerService,
                updatedPhase),
            PersonController.getPersonDtosForPhase(
                ppPersonService,
                ppAddressService,
                updatedPhase
            ));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a praxis phase", tags = {"Praxis - Admin - Phases"})
    @DeleteMapping(path = "/phases/{phaseId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePhase(@PathVariable("phaseId") Long phaseId) {
        ppPhaseService.getPhaseById(phaseId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpPhase for ID: " + phaseId));

        ppPhaseService.deletePhaseById(phaseId);
    }

    /**
     * This DTO is for the admin API, see
     * {@link de.thm.kim.tc.app.praxis.web.PraxisController.VisiblePhaseDto}
     * for the app DTO.
     */
    @Value
    static class PhaseDto {

        @JsonUnwrapped
        PraxisController.VisiblePhaseDto phase;
        Boolean visible;

        PhaseDto(PpPhase phase, List<GroupDto> groups, List<PersonDto> persons) {
            this.phase = new PraxisController.VisiblePhaseDto(phase, groups, persons);
            this.visible = phase.isVisible();
        }
    }
}
