/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpQuestion;
import de.thm.kim.tc.app.praxis.data.entity.PpUnit;
import de.thm.kim.tc.app.praxis.service.PpAnswerService;
import de.thm.kim.tc.app.praxis.service.PpQuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis questions", tags = {"Praxis - Admin - Questions"})
@RestController
@RequestMapping(path = "/praxis")
class QuestionController {

    private final PpQuestionService ppQuestionService;
    private final PpAnswerService ppAnswerService;

    @Autowired
    QuestionController(PpQuestionService ppQuestionService,
                       PpAnswerService ppAnswerService) {
        this.ppQuestionService = ppQuestionService;
        this.ppAnswerService = ppAnswerService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all questions for unit", tags = {"Praxis - Admin - Questions"})
    @GetMapping(path = "/units/{unitId}/questions")
    public List<QuestionDto> getQuestionsForUnit(@PathVariable("unitId") Long unitId) {
        return ppQuestionService.getQuestionsForUnit(unitId).stream()
            .map(q -> new QuestionDto(q,
                AnswerController.getAnswerDtosForQuestion(
                    ppAnswerService,
                    q)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all questions", tags = {"Praxis - Admin - Questions"})
    @GetMapping(path = "/questions")
    public List<QuestionDto> getAllQuestions() {
        return ppQuestionService.getAllQuestions().stream()
            .map(q -> new QuestionDto(q,
                AnswerController.getAnswerDtosForQuestion(
                    ppAnswerService,
                    q)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new question", tags = {"Praxis - Admin - Questions"})
    @PostMapping(path = "/questions")
    @ResponseStatus(HttpStatus.CREATED)
    public QuestionDto saveQuestion(@RequestBody PpQuestion question) {
        PpQuestion q = ppQuestionService.createOrUpdateQuestion(question);

        return new QuestionDto(q,
            AnswerController.getAnswerDtosForQuestion(
                ppAnswerService,
                q));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a question", tags = {"Praxis - Admin - Questions"})
    @PutMapping(path = "/questions/{questionId}")
    public QuestionDto updateQuestion(@PathVariable("questionId") Long questionId, @RequestBody PpQuestion question) {
        if (question.getQuestionId() == null || !question.getQuestionId().equals(questionId))
            throw new IllegalArgumentException("Invalid questionId in body");

        ppQuestionService.getQuestionById(questionId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpQuestion for ID: " + questionId));

        PpQuestion q = ppQuestionService.createOrUpdateQuestion(question);

        return new QuestionDto(q,
            AnswerController.getAnswerDtosForQuestion(
                ppAnswerService,
                q));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a question", tags = {"Praxis - Admin - Questions"})
    @DeleteMapping(path = "/questions/{questionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteQuestion(@PathVariable("questionId") Long questionId) {
        ppQuestionService.getQuestionById(questionId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpQuestion for ID: " + questionId));

        ppQuestionService.deleteQuestionById(questionId);
    }

    public static List<QuestionDto> getQuestionDtosForUnit(PpQuestionService questionService,
                                                           PpAnswerService answerService,
                                                           PpUnit unit) {
        return questionService.getQuestionsForUnit(unit.getUnitId()).stream()
            .map(q -> new QuestionDto(q,
                AnswerController.getAnswerDtosForQuestion(answerService, q)))
            .collect(Collectors.toList());
    }
}
