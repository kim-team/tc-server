/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.dashboard;

import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.service.AuthUserService;
import de.thm.kim.tc.app.auth.service.UserDeleteService;
import de.thm.kim.tc.app.qanda.data.entity.Flair;
import de.thm.kim.tc.app.qanda.service.FlairService;
import lombok.extern.java.Log;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log
@Controller
@RequestMapping("/participants")
public class UserDashboardController {

    private final AuthUserService authUserService;
    private final FlairService flairService;
    private final UserService userService;
    private final UserDeleteService userDeleteService;

    @Autowired
    public UserDashboardController(AuthUserService authUserService,
                                   FlairService flairService,
                                   UserService userService,
                                   UserDeleteService userDeleteService) {
        this.authUserService = authUserService;
        this.flairService = flairService;
        this.userService = userService;
        this.userDeleteService = userDeleteService;
    }

    @GetMapping
    public String showParticipants(Model model) {
        val emails = authUserService.getAll().stream()
            .collect(Collectors.toMap(AuthUser::getUsername, AuthUser::getEmail));

        model.addAttribute("title", "Users");
        model.addAttribute("emails", emails);
        model.addAttribute("users", userService.getAll());
        model.addAttribute("classActiveUsers", "active");

        return "participants";
    }

    @Transactional
    @PostMapping
    public String deleteUser(@RequestParam String confirmedUserName,
                             @RequestParam String userName,
                             RedirectAttributes attributes) {
        val user = userService.getUserByUsername(userName);
        var message = "";
        var alertType = "";

        if (confirmedUserName.equals(userName)) {
            userDeleteService.deleteUser(user);

            message = String.format("User '%s' deleted successfully", userName);
            alertType = "alert-success";

            log.info(message);
        } else {
            message = "Error: Input did not match username";
            alertType = "alert-danger";
        }

        attributes.addFlashAttribute("message", message);
        attributes.addFlashAttribute("alertType", alertType);

        return "redirect:/participants";
    }

    @GetMapping("/{userId}")
    public String showParticipants(@PathVariable("userId") Long userId, Model model) {
        val user = userService.getUserById(userId);
        val flairs = flairService.getAllFlairs();
        val userFlairs = flairService.getAllFlairsForUser(user).stream()
            .map(Flair::getId)
            .collect(Collectors.toList());

        model.addAttribute("title", "User - " + user.getName());
        model.addAttribute("user", user);
        model.addAttribute("flairs", flairs);
        model.addAttribute("userFlairs", userFlairs);
        model.addAttribute("classActiveUsers", "active");

        return "user";
    }

    @Transactional
    @PostMapping("/{userId}")
    public String showParticipants(@PathVariable("userId") Long userId,
                                   @RequestParam(value = "ids", required = false) List<Long> ids) {
        val user = userService.getUserById(userId);

        if (ids == null)
            ids = new ArrayList<>();

        flairService.setFlairs(ids, user);

        return "redirect:/participants/" + userId;
    }
}
