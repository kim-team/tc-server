/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineHide;
import de.thm.kim.tc.app.timeline.data.repository.UserTimelineHideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserTimelineHideService {

    private final UserTimelineHideRepository userTimelineHideRepository;

    @Autowired
    public UserTimelineHideService(UserTimelineHideRepository userTimelineHideRepository) {
        this.userTimelineHideRepository = userTimelineHideRepository;
    }

    public Collection<TimelineEntry> getAllHiddenTimelineEntriesByUser(User user) {
        return userTimelineHideRepository.findAllByUser(user).stream()
            .map(UserTimelineHide::getTimelineEntry).collect(Collectors.toList());
    }

    public void addHideTimelineEntry(TimelineEntry timelineEntry, User user) {
        if (userTimelineHideRepository.findByTimelineEntryAndUser(timelineEntry, user).isEmpty())
            userTimelineHideRepository.save(new UserTimelineHide(timelineEntry, user));
    }

    @Transactional
    public void restoreAllHiddenTimelineEntriesForUser(User user) {
        userTimelineHideRepository.deleteAllByUser(user);
    }

    public void restoreTimelineEntryForAllUsers(TimelineEntry entry) {
        userTimelineHideRepository.deleteAllByTimelineEntry(entry);
    }
}
