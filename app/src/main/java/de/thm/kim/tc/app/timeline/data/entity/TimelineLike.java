/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.entity;

import de.thm.kim.tc.app.account.data.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "timeline_likes")
public class TimelineLike {

    @EmbeddedId
    private TimelineLikeId id;

    @ManyToOne
    @MapsId("timelineEntryId")
    @JoinColumn(name = "timeline_entry_id")
    private TimelineEntry timelineEntry;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "USER_ID")
    private User user;

    @CreatedDate
    @Column(name = "DATE")
    private Date date;

    public TimelineLike(TimelineEntry entry, User user) {
        this.id = new TimelineLikeId(entry.getId(), user.getId());
        this.timelineEntry = entry;
        this.user = user;
        this.date = new Date();
    }
}
