/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpPerson;
import de.thm.kim.tc.app.praxis.data.entity.PpPhase;
import de.thm.kim.tc.app.praxis.service.PpAddressService;
import de.thm.kim.tc.app.praxis.service.PpPersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about contact persons", tags = {"Praxis - Admin - Persons"})
@RestController
@RequestMapping(path = "/praxis")
class PersonController {

    private final PpAddressService ppAddressService;
    private final PpPersonService ppPersonService;

    @Autowired
    PersonController(PpAddressService ppAddressService,
                     PpPersonService ppPersonService) {
        this.ppAddressService = ppAddressService;
        this.ppPersonService = ppPersonService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all persons for phase", tags = {"Praxis - Admin - Persons"})
    @GetMapping(path = "/phases/{phaseId}/persons")
    public List<PersonDto> getPersonsForPhase(@PathVariable("phaseId") Long phaseId) {
        return ppPersonService.getPersonsForPhaseId(phaseId).stream()
            .map(p -> new PersonDto(p,
                AddressController.getAddressDtosForPerson(ppAddressService, p)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all persons", tags = {"Praxis - Admin - Persons"})
    @GetMapping(path = "/persons")
    public List<PersonDto> getAllPersons() {
        return ppPersonService.getAllPersons().stream()
            .map(p -> new PersonDto(p,
                AddressController.getAddressDtosForPerson(ppAddressService, p)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Full content for specified person including all addresses", tags = {"Praxis - Admin - Persons"})
    @GetMapping(path = "/persons/{personId}")
    public PersonDto getPerson(@PathVariable("personId") Long personId) {
        return ppPersonService.getPersonById(personId)
            .map(p -> new PersonDto(p,
                AddressController.getAddressDtosForPerson(ppAddressService, p)))
            .orElseThrow(() -> new ResourceNotFoundException("No Person found for ID: " + personId));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new person", tags = {"Praxis - Admin - Persons"})
    @PostMapping(path = "/persons")
    @ResponseStatus(HttpStatus.CREATED)
    public PersonDto savePerson(@RequestBody PpPerson person) {
        PpPerson p = ppPersonService.createOrUpdatePerson(person);

        return new PersonDto(p,
            AddressController.getAddressDtosForPerson(
                ppAddressService,
                p));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a person", tags = {"Praxis - Admin - Persons"})
    @PutMapping(path = "/persons/{personId}")
    public PersonDto updatePerson(@PathVariable("personId") Long personId, @RequestBody PpPerson person) {
        if (person.getPersonId() == null || !person.getPersonId().equals(personId))
            throw new IllegalArgumentException("Invalid personId in body");

        ppPersonService.getPersonById(personId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpPerson for ID: " + personId));

        PpPerson p = ppPersonService.createOrUpdatePerson(person);

        return new PersonDto(p,
            AddressController.getAddressDtosForPerson(
                ppAddressService,
                p));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a person", tags = {"Praxis - Admin - Persons"})
    @DeleteMapping(path = "/persons/{personId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePerson(@PathVariable("personId") Long personId) {
        ppPersonService.getPersonById(personId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpPerson for ID: " + personId));

        ppPersonService.deletePersonById(personId);
    }


    public static List<PersonDto> getPersonDtosForPhase(PpPersonService personService,
                                                        PpAddressService addressService,
                                                        PpPhase phase) {
        return personService.getPersonsForPhaseId(phase.getPhaseId()).stream()
            .map(p -> new PersonDto(p,
                AddressController.getAddressDtosForPerson(addressService, p)))
            .collect(Collectors.toList());
    }
}
