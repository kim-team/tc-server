/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpAnswer;
import de.thm.kim.tc.app.praxis.data.entity.PpQuestion;
import de.thm.kim.tc.app.praxis.service.PpAnswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis answers", tags = {"Praxis - Admin - Answers"})
@RestController
@RequestMapping(path = "/praxis")
class AnswerController {

    private final PpAnswerService ppAnswerService;

    @Autowired
    AnswerController(PpAnswerService ppAnswerService) {
        this.ppAnswerService = ppAnswerService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all answers for question", tags = {"Praxis - Admin - Answers"})
    @GetMapping(path = "/questions/{questionId}/answers")
    public List<AnswerDto> getAnswersForQuestion(@PathVariable("questionId") Long questionId) {
        return ppAnswerService.getAnswersForQuestion(questionId).stream()
            .map(AnswerDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all answers", tags = {"Praxis - Admin - Answers"})
    @GetMapping(path = "/answers")
    public List<AnswerDto> getAllAnswers() {
        return ppAnswerService.getAllAnswers().stream()
            .map(AnswerDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new answer", tags = {"Praxis - Admin - Answers"})
    @PostMapping(path = "/answers")
    @ResponseStatus(HttpStatus.CREATED)
    public AnswerDto saveAnswer(@RequestBody PpAnswer answer) {
        PpAnswer a = ppAnswerService.createOrUpdateAnswer(answer);

        return new AnswerDto(a);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update an answer", tags = {"Praxis - Admin - Answers"})
    @PutMapping(path = "/answers/{answerId}")
    public AnswerDto updateAnswer(@PathVariable("answerId") Long answerId, @RequestBody PpAnswer answer) {
        ppAnswerService.getAnswerById(answerId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpAnswer for ID: " + answerId));

        PpAnswer a = ppAnswerService.createOrUpdateAnswer(answer);

        return new AnswerDto(a);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete an answer", tags = {"Praxis - Admin - Answers"})
    @DeleteMapping(path = "/answers/{answerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAnswer(@PathVariable("answerId") Long answerId) {
        ppAnswerService.getAnswerById(answerId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpAnswer for ID: " + answerId));

        ppAnswerService.deleteAnswerById(answerId);
    }

    public static List<AnswerDto> getAnswerDtosForQuestion(PpAnswerService service, PpQuestion question) {
        return service.getAnswersForQuestion(question.getQuestionId()).stream()
            .map(AnswerDto::new)
            .collect(Collectors.toList());
    }
}
