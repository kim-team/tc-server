/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.account.data.entity.User;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Value
class ProfileDto {
    String displayName;

    String userName;

    boolean verified;

    boolean emailSent;

    Long level;

    Long totalHearts;

    Map<String, Long> hearts;

    List<BadgeDto> badges;

    public ProfileDto(User user, boolean verified, boolean emailSent, Long level, Long totalHearts, Map<String, Long> hearts, List<BadgeDto> badges) {
        this(user.getName(), user.getUsername(), verified, emailSent, level, totalHearts, hearts, badges);
    }
}
