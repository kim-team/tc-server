/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TimelineEntryRepositoryTest {

    private static final int PAGE_NUMBER = 0;
    private static final int PAGE_SIZE = 20;

    private final Pageable pageRequest = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);

    @Autowired
    private TimelineEntryRepository repository;

    @Autowired
    private SourceRepository sourceRepository;

    @Test
    public void find_all() {
        Page<TimelineEntry> timelineEntries = repository.findAll(pageRequest);
        assertFalse(timelineEntries.isEmpty());
    }

    @Test
    public void find_all_by_source_in_and_id_is_not_in() {
        Collection<Source> sources = sourceRepository.findAll();
        Collection<Long> hideIds = Set.of(-8211198101114116L); // FIXME
        Page<TimelineEntry> timelineEntries = repository.findAllBySourceInAndIdIsNotIn(sources, hideIds, pageRequest);
        assertFalse(timelineEntries.isEmpty());
    }
}
