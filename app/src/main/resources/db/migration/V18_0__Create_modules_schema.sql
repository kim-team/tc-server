CREATE TABLE modules (
    module_id    VARCHAR(255) NOT NULL,
    is_published BOOLEAN      NOT NULL DEFAULT FALSE,
    priority     INT          NOT NULL,
    PRIMARY KEY (module_id)
);

-- Shown/hidden modules and module order for users
CREATE TABLE user_modules (
    user_id    BIGINT       NOT NULL,
    module_id  VARCHAR(255) NOT NULL,
    is_visible BOOLEAN      NOT NULL DEFAULT TRUE,
    priority   INT          NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (module_id) REFERENCES modules (module_id),
    PRIMARY KEY (user_id, module_id)
);
