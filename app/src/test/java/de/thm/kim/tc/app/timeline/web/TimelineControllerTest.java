/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import de.thm.kim.tc.app.timeline.service.UserTimelineFaveService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TimelineControllerTest {

    private static final String TIMELINE_URL = "/timeline";
    private static final String LIKE_URL = "/like";
    private static final String UNLIKE_URL = "/unlike";

    private static final Long USER_ID = 4L;
    private static final Long TIMELINE_ENTRY_ID = 1L;
    private static final Long INVALID_USER_ID = 999L;
    private static final String VALID_USERNAME = "jane_doe";

    private static final int PAGE_NUMBER = 0;
    private static final int PAGE_SIZE = 2;
    private static final Sort.Direction PAGE_SORT_DIRECTION = Sort.Direction.ASC;
    private static final String PAGE_SORT_PROPERTY = "id";
    private static final Sort PAGE_SORT = Sort.by(
        new Sort.Order(PAGE_SORT_DIRECTION, PAGE_SORT_PROPERTY)
    );
    private static final String TIMELINE_URL_SORT_QUERY = "" +
        "?page=" + PAGE_NUMBER +
        "&size=" + PAGE_SIZE +
        "&sort=" + PAGE_SORT_PROPERTY + "," + PAGE_SORT_DIRECTION;
    private final Pageable pageRequest = PageRequest.of(PAGE_NUMBER, PAGE_SIZE, PAGE_SORT);

    @SpyBean
    private TimelineService timelineService;

    @SpyBean
    private UserService userService;

    @SpyBean
    private UserTimelineFaveService userTimelineFaveService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void get_all_timeline_entries_without_principal() throws Exception {
        // Expected number of timeline entries
        final int n = 51;

        // Expected number of pages
        final int nPages = n / PAGE_SIZE + 1;

        mockMvc.perform(get(TIMELINE_URL + TIMELINE_URL_SORT_QUERY))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.content[0].faved", is(false)))
            .andExpect(jsonPath("$.empty", is(false)))
            .andExpect(jsonPath("$.first", is(true)))
            .andExpect(jsonPath("$.last", is(false)))
            .andExpect(jsonPath("$.number", is(PAGE_NUMBER)))
            .andExpect(jsonPath("$.numberOfElements", is(PAGE_SIZE)))
            .andExpect(jsonPath("$.size", is(PAGE_SIZE)))
            .andExpect(jsonPath("$.sort.empty", is(false)))
            .andExpect(jsonPath("$.sort.sorted", is(true)))
            .andExpect(jsonPath("$.sort.unsorted", is(false)))
            .andExpect(jsonPath("$.totalElements", is(n)))
            .andExpect(jsonPath("$.totalPages", is(nPages)));

//        verify(timelineService).getAllTimelineEntries(pageRequest);
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void get_all_timeline_entries_with_principal() throws Exception {
        User user = userService.getUserById(USER_ID);
        // Expected number of timeline entries
        final int n = 51;

        mockMvc.perform(get(TIMELINE_URL + TIMELINE_URL_SORT_QUERY))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.content[0].faved", is(false)))
            .andExpect(jsonPath("$.empty", is(false)))
            .andExpect(jsonPath("$.first", is(true)))
            .andExpect(jsonPath("$.last", is(false)))
            .andExpect(jsonPath("$.number", is(PAGE_NUMBER)))
            .andExpect(jsonPath("$.numberOfElements", is(PAGE_SIZE)))
            .andExpect(jsonPath("$.size", is(PAGE_SIZE)))
            .andExpect(jsonPath("$.sort.empty", is(false)))
            .andExpect(jsonPath("$.sort.sorted", is(true)))
            .andExpect(jsonPath("$.sort.unsorted", is(false)))
            .andExpect(jsonPath("$.totalElements", is(n)))
            .andExpect(jsonPath("$.totalPages", is(((int) Math.ceil((double) n / PAGE_SIZE)))));

        verify(timelineService).getAllTimelineEntriesByUser(user, pageRequest);
        timelineService.getAllTimelineEntriesByUser(user, pageRequest).getContent().forEach(timelineEntry ->
            verify(timelineService).getTimelineLikesForTimelineEntry(timelineEntry)
        );
    }

    @Test
    public void should_fail_to_get_timeline_entries_for_user_not_exists() throws Exception {
        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            User user = userService.getUserById(INVALID_USER_ID);

            when(timelineService.getAllTimelineEntriesByUser(eq(user), any(Pageable.class))).thenThrow(ResourceNotFoundException.class);

            mockMvc.perform(get(TIMELINE_URL + TIMELINE_URL_SORT_QUERY))
                .andExpect(status().isNotFound());

            verify(timelineService).getAllTimelineEntriesByUser(user, pageRequest);
        });
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void like_timeline_entry() throws Exception {
        mockMvc.perform(post(TIMELINE_URL + "/" + TIMELINE_ENTRY_ID + LIKE_URL))
            .andExpect(status().isOk());

        verify(timelineService).getTimelineEntryById(TIMELINE_ENTRY_ID);
        verify(userService).getUserByUsername(VALID_USERNAME);

        TimelineEntry timelineEntry = timelineService.getTimelineEntryById(TIMELINE_ENTRY_ID);
        User user = userService.getUserByUsername(VALID_USERNAME);

        verify(timelineService).createNewTimelineLike(timelineEntry, user);
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void unlike_timeline_entry() throws Exception {
        mockMvc.perform(post(TIMELINE_URL + "/" + TIMELINE_ENTRY_ID + UNLIKE_URL))
            .andExpect(status().isOk());

        verify(timelineService).getTimelineEntryById(TIMELINE_ENTRY_ID);
        verify(userService).getUserByUsername(VALID_USERNAME);

        TimelineEntry timelineEntry = timelineService.getTimelineEntryById(TIMELINE_ENTRY_ID);
        User user = userService.getUserByUsername(VALID_USERNAME);

        verify(timelineService).removeTimelineLike(timelineEntry, user);
    }
}
