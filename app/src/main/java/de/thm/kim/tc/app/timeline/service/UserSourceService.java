/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.UserSource;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import de.thm.kim.tc.app.timeline.data.repository.UserSourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserSourceService {

    private final UserSourceRepository userSourceRepository;

    @Autowired
    public UserSourceService(UserSourceRepository userSourceRepository) {
        this.userSourceRepository = userSourceRepository;
    }

    /**
     * Try to find an UserSource, if there is no UserSource, they will throw an IllegalArgumentException.
     *
     * @param source   source
     * @param user     user
     * @return existing UserSource
     * @throws IllegalArgumentException if UserSource doesn't exists
     */
    private UserSource getUserSourceBySourceAndUser(Source source, User user) {
        return userSourceRepository.findBySourceAndUser(source, user)
            .orElseThrow(() -> new ResourceNotFoundException("No UserSource found for Source ID: " + source.getId() + " and User ID: " + user.getId()));
    }

    /**
     * Try to find an UserSource, if there is no UserSource, they will create a new UserSource
     *
     * @param source   source
     * @param user     user
     * @return existing UserSource or new UserSource
     */
    private UserSource getUserSourceBySourceAndUserOrCreate(Source source, User user) {
        return userSourceRepository.findBySourceAndUser(source, user)
            .orElse(new UserSource(source, user, SubscriptionTypes.UNSUBSCRIBED));
    }

    /**
     * Update existing UserSource if exists (Toggle UserSource), if UserSource doesn't exists, they will create a new one (Toggle default value).
     *
     * @param source   source
     * @param user     user
     * @return existing UserSource or new UserSource
     */
    public UserSource createOrToggleUserSource(Source source, User user) {
        UserSource userSource = getUserSourceBySourceAndUserOrCreate(source, user);
        userSource.setSubscriptionType(
            (userSource.getSubscriptionType() == SubscriptionTypes.UNSUBSCRIBED)
                ? SubscriptionTypes.SUBSCRIBED
                : SubscriptionTypes.UNSUBSCRIBED
        );
        return userSourceRepository.save(userSource);
    }

    /**
     * Update existing UserSource if exists, if UserSource doesn't exists, they will create a new one.
     *
     * @param source           source
     * @param user             user
     * @param subscriptionType subscription type
     * @return existing UserSource or new UserSource
     */
    public UserSource createOrUpdateUserSource(Source source, User user, SubscriptionTypes subscriptionType) {
        UserSource userSource = getUserSourceBySourceAndUserOrCreate(source, user);
        userSource.setSubscriptionType(subscriptionType);
        return userSourceRepository.save(userSource);
    }

    /**
     * Try to find the UserSource and delete it.
     *
     * @param source   source
     * @param user     user
     */
    public void deleteUserSource(Source source, User user) {
        userSourceRepository.delete(getUserSourceBySourceAndUser(source, user));
    }

    @Transactional
    public void deleteAllByUser(User user) {
        userSourceRepository.deleteAllByUser(user);
    }
}
