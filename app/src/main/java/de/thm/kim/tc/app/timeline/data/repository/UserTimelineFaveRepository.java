/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineFave;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface UserTimelineFaveRepository extends PagingAndSortingRepository<UserTimelineFave, Long> {
    Page<UserTimelineFave> findAllByUser(User user, Pageable pageable);

    Page<UserTimelineFave> findAllByUserAndTimelineEntryIdIsNotIn(User user, Collection<Long> hideIds, Pageable pageable);

    Optional<UserTimelineFave> findByTimelineEntryAndUser(TimelineEntry timelineEntry, User user);

    Collection<UserTimelineFave> findByTimelineEntryInAndUser(Collection<TimelineEntry> timelineEntries, User user);

    Collection<UserTimelineFave> findByTimelineEntryInAndUserAndTimelineEntryIdIsNotIn(Collection<TimelineEntry> timelineEntries, User user, Collection<Long> hideIds);

    void deleteAllByTimelineEntry(TimelineEntry timelineEntry);

    void deleteAllByUser(User user);
}
