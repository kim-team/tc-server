ALTER TABLE flairs ADD COLUMN default_title VARCHAR(255) NOT NULL DEFAULT '';

UPDATE flairs SET default_title = 'tinyCampus-Redaktion'      WHERE flair_id = 1;
UPDATE flairs SET default_title = 'tinyCampus-Team'           WHERE flair_id = 2;
UPDATE flairs SET default_title = 'tinyCampus-Moderation'     WHERE flair_id = 3;
UPDATE flairs SET default_title = 'My Prof is a Gamer'        WHERE flair_id = 4;
UPDATE flairs SET default_title = 'KiM Mitarbeiter*in'        WHERE flair_id = 5;
UPDATE flairs SET default_title = 'THM Mitarbeiter*in'        WHERE flair_id = 6;
UPDATE flairs SET default_title = 'Dozent*in'                 WHERE flair_id = 7;
UPDATE flairs SET default_title = 'Zentrale Studienberatung'  WHERE flair_id = 8;
UPDATE flairs SET default_title = 'Studentenwerk'             WHERE flair_id = 9;
UPDATE flairs SET default_title = 'THM Hochschulsport'        WHERE flair_id = 10;
UPDATE flairs SET default_title = 'THM Info'                  WHERE flair_id = 11;
UPDATE flairs SET default_title = 'THM Öffentlichkeitsarbeit' WHERE flair_id = 12;
UPDATE flairs SET default_title = 'ZQE'                       WHERE flair_id = 13;
UPDATE flairs SET default_title = 'ZekoLL'                    WHERE flair_id = 14;
