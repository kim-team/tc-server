/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpPhase;
import de.thm.kim.tc.app.praxis.data.repository.PpPhaseRepository;
import de.thm.kim.tc.app.qanda.service.QuestionCategoryService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PpPhaseService {

    private final PpCompletionService completionService;
    private final PpGroupService groupService;
    private final PpPersonService personService;
    private final PpPhaseRepository ppPhaseRepository;
    private final QuestionCategoryService questionCategoryService;

    @Autowired
    public PpPhaseService(PpCompletionService completionService,
                          PpGroupService groupService,
                          PpPersonService personService,
                          PpPhaseRepository ppPhaseRepository,
                          QuestionCategoryService questionCategoryService) {
        this.completionService = completionService;
        this.groupService = groupService;
        this.personService = personService;
        this.ppPhaseRepository = ppPhaseRepository;
        this.questionCategoryService = questionCategoryService;
    }

    public List<PpPhase> getAllPhases() {
        return ppPhaseRepository.findAllByOrderByTitleDe();
    }

    public List<PpPhase> getAllVisiblePhases() {
        return ppPhaseRepository.findAllByVisibleTrueOrderByTitleDe();
    }

    public Optional<PpPhase> getVisiblePhaseById(Long id) {
        return ppPhaseRepository.findByPhaseIdAndVisibleTrue(id);
    }

    public Optional<PpPhase> getPhaseById(Long id) {
        return ppPhaseRepository.findById(id);
    }

    @Transactional
    public PpPhase createOrUpdatePhase(PpPhase phase) {
        if (phase.getCategoryId() == null) {
            if (phase.isVisible()) {
                val category = questionCategoryService.createPraxisQuestionCategory(phase.getTitleDe());
                phase.setCategoryId(category.getId());
            }
        } else {
            questionCategoryService.updateCategoryTitle(phase.getCategoryId(), phase.getTitleDe());
        }

        return ppPhaseRepository.save(phase);
    }

    @Transactional
    public void deletePhaseById(Long phaseId) {
        ppPhaseRepository.findById(phaseId)
            .ifPresentOrElse(
                p -> {
                    completionService.getCompletionsByPhaseId(phaseId)
                        .forEach(c -> completionService.deleteCompletionById(c.getCompletionId()));
                    groupService.getGroupsForPhase(phaseId)
                        .forEach(g -> groupService.deleteGroupById(g.getGroupId()));
                    personService.getPersonsForPhaseId(phaseId)
                        .forEach(person -> personService.deletePersonById(person.getPersonId()));
                    ppPhaseRepository.delete(p);

                    if (p.getCategoryId() != null)
                        questionCategoryService.deleteCategoryById(p.getCategoryId());

                },
                () -> {
                    throw new ResourceNotFoundException("No Phase found for ID: " + phaseId);
                }
            );
    }
}
