-- Dummy app users
INSERT INTO users (username, name, program_id, campus_id) VALUES
('admin',                  'Powerful User',     12,  1), -- Informatik BSc.
('user',                   'Fellow User',       12,  1), -- Informatik BSc.
('anonymous',              'Mysterious User',   12,  1), -- Informatik BSc.
('jane_doe',               'Friendly User',     12,  1), -- Informatik BSc.
('otto_normalverbraucher', 'Happy User',        390, 2); -- WI MSc.

-- Homescreen for Dummy User
INSERT INTO homescreens (user_id, cards_order) VALUES (1, '[1,2,3]');

-- Rewards for Dummy User
INSERT INTO rewards (user_id, value, comment) VALUES
(1, 35, 'Donec vitae sapien ut libero venenatis faucibus.'),
(1, 15, 'Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt.');
