ALTER TABLE answers   ADD COLUMN updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE questions ADD COLUMN updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

UPDATE answers   SET updated_at = date WHERE updated_at IS NULL;
UPDATE questions SET updated_at = date WHERE updated_at IS NULL;

ALTER TABLE answers   ALTER COLUMN updated_at SET NOT NULL;
ALTER TABLE questions ALTER COLUMN updated_at SET NOT NULL;
