/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.web;

import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import lombok.Value;

import java.util.Date;

@Value
class CafeteriaCommentOutputDto {

    Long id;
    Long authorId;
    String author;
    String text;
    Date createdAt;
    int likes;
    boolean isLiked;
    boolean canEdit;
    boolean isReported;

    CafeteriaCommentOutputDto(CafeteriaComment cafeteriaComment) {
        this(cafeteriaComment, 0, false, true, false);
    }

    CafeteriaCommentOutputDto(CafeteriaComment cafeteriaComment, int likes, boolean isLiked, boolean canEdit, boolean isReported) {
        this.id = cafeteriaComment.getId();
        this.authorId = cafeteriaComment.getAuthor().getId();
        this.author = cafeteriaComment.getAuthor().getName();
        this.text = cafeteriaComment.getText();
        this.createdAt = cafeteriaComment.getCreatedAt();
        this.likes = likes;
        this.isLiked = isLiked;
        this.canEdit = canEdit;
        this.isReported = isReported;
    }
}
