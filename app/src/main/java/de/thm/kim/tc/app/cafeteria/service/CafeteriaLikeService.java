/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaCommentLike;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaCommentLikeRepository;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class CafeteriaLikeService {

    private final CafeteriaCommentLikeRepository cafeteriaCommentLikeRepository;

    @Autowired
    public CafeteriaLikeService(CafeteriaCommentLikeRepository cafeteriaCommentLikeRepository) {
        this.cafeteriaCommentLikeRepository = cafeteriaCommentLikeRepository;
    }

    public Collection<CafeteriaCommentLike> getCafeteriaCommentLikesForCafetriaComment(CafeteriaComment cafeteriaComment) {
        return cafeteriaCommentLikeRepository.findAllByCafeteriaComment(cafeteriaComment);
    }

    public Long getCafeteriaLikesForUser(User user) {
        return cafeteriaCommentLikeRepository.countAllByCafeteriaComment_Author(user);
    }

    public CafeteriaCommentLike createNewCafeteriaCommentLike(CafeteriaComment cafeteriaComment, User user) {
        return cafeteriaCommentLikeRepository.findByCafeteriaCommentAndUser(cafeteriaComment, user)
            .orElse(cafeteriaCommentLikeRepository.save(new CafeteriaCommentLike(cafeteriaComment, user)));
    }

    public CafeteriaCommentLike removeCafeteriaCommentLike(CafeteriaComment cafeteriaComment, User user) {
        CafeteriaCommentLike cafeteriaCommentLike = cafeteriaCommentLikeRepository.findByCafeteriaCommentAndUser(cafeteriaComment, user)
            .orElseThrow(() -> new ResourceNotFoundException("No CafeteriaCommentLike found for CafeteriaComment ID: " + cafeteriaComment.getId() + " and User ID: " + user.getId()));

        cafeteriaCommentLikeRepository.delete(cafeteriaCommentLike);
        return cafeteriaCommentLike;
    }

    @Transactional
    public void deleteAllCafeteriaCommentLikesForCafeteriaComment(CafeteriaComment cafeteriaComment) {
        cafeteriaCommentLikeRepository.deleteAllByCafeteriaComment(cafeteriaComment);
    }

    @Transactional
    public void deleteAllCafeteriaCommentLikesForUser(User user) {
        cafeteriaCommentLikeRepository.deleteAllByUser(user);
    }
}
