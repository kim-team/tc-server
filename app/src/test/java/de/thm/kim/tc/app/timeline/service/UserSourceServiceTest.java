/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.UserSource;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import de.thm.kim.tc.app.timeline.data.repository.UserSourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UserSourceServiceTest {

    private UserSourceService service;

    @Mock
    private UserSourceRepository userSourceRepositoryMock;

    @Mock
    private User userMock;
    @Mock
    private Source sourceMock;
    @Mock
    private UserSource userSourceMock;
    @Mock
    private UserSource userSourceUpdatedMock;

    @BeforeEach
    public void setUp() {
        service = new UserSourceService(userSourceRepositoryMock);

        when(userSourceRepositoryMock.findBySourceAndUser(sourceMock, userMock))
            .thenReturn(Optional.of(userSourceMock));
        when(userSourceRepositoryMock.findBySourceAndUser(sourceMock, userMock))
            .thenReturn(Optional.of(userSourceMock));
    }

    @Test
    public void create_or_toggle_user_source() {
        when(userSourceRepositoryMock.save(userSourceMock))
            .thenReturn(userSourceUpdatedMock);
        when(userSourceRepositoryMock.save(userSourceMock))
            .thenReturn(userSourceUpdatedMock);

        assertThat(service.createOrToggleUserSource(sourceMock, userMock), is(userSourceUpdatedMock));
    }

    @Test
    public void create_or_update_user_source() {
        when(userSourceRepositoryMock.save(userSourceMock))
            .thenReturn(userSourceUpdatedMock);
        when(userSourceRepositoryMock.save(userSourceMock))
            .thenReturn(userSourceUpdatedMock);

        assertThat(service.createOrUpdateUserSource(sourceMock, userMock, SubscriptionTypes.SUBSCRIBED),
            is(userSourceUpdatedMock));
    }

    @Test
    public void delete_user_source() {
        service.deleteUserSource(sourceMock, userMock);

        verify(userSourceRepositoryMock).delete(userSourceMock);
    }
}
