/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.praxis.data.entity.PpAnswer;
import de.thm.kim.tc.app.praxis.data.repository.PpAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PpAnswerService {

    private final PpAnswerRepository ppAnswerRepository;

    @Autowired
    public PpAnswerService(PpAnswerRepository ppAnswerRepository) {
        this.ppAnswerRepository = ppAnswerRepository;
    }

    public List<PpAnswer> getAnswersForQuestion(Long questionId) {
        return ppAnswerRepository.findAllByQuestionIdOrderByAnswerId(questionId);
    }

    public List<PpAnswer> getAllAnswers() {
        return ppAnswerRepository.findAllByOrderByAnswerId();
    }

    public Optional<PpAnswer> getAnswerById(Long answerId) {
        return ppAnswerRepository.findById(answerId);
    }

    public PpAnswer createOrUpdateAnswer(PpAnswer question) {
        return ppAnswerRepository.save(question);
    }

    @Transactional
    public void deleteAnswerById(Long questionId) {
        ppAnswerRepository.deleteById(questionId);
    }
}
