/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.entity;

import de.thm.kim.tc.app.common.reports.Reportable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "timeline_entries")
public class TimelineEntry implements Reportable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timeline_entry_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "source_id")
    private Source source;

    @Column(name = "text")
    private String text;

    @Column(name = "content_args")
    private String contentArgs;

    @Column(name = "created_at")
    @CreatedDate
    private Date created;

    @Column(name = "updated_at")
    @LastModifiedDate
    private Date modified;

    public TimelineEntry(Source source, String text, String contentArgs) {
        this.source = source;
        this.text = text;
        this.contentArgs = contentArgs;
        this.created = new Date();
        this.modified = new Date();
    }
}
