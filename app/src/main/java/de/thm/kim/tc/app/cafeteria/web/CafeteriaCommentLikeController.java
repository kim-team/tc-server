/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaCommentService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaLikeService;
import de.thm.kim.tc.app.qanda.data.entity.Badge;
import de.thm.kim.tc.app.qanda.service.BadgeService;
import de.thm.kim.tc.app.qanda.service.LikeService;
import de.thm.kim.tc.app.thinkbig.service.ThinkBigLikeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Api(description = "Everything about cafeteria comment likes", tags = {"Cafeteria - Comment likes"})
@RestController
@RequestMapping(path = "/cafeteria/comments")
public class CafeteriaCommentLikeController {

    final private CafeteriaCommentService cafeteriaCommentService;
    final private CafeteriaLikeService cafeteriaLikeService;
    final private BadgeService badgeService;
    final private LikeService likeService;
    final private ThinkBigLikeService thinkBigLikeService;
    final private UserService userService;

    public CafeteriaCommentLikeController(CafeteriaCommentService cafeteriaCommentService,
                                          CafeteriaLikeService cafeteriaLikeService,
                                          BadgeService badgeService,
                                          LikeService likeService,
                                          ThinkBigLikeService thinkBigLikeService,
                                          UserService userService) {
        this.cafeteriaCommentService = cafeteriaCommentService;
        this.cafeteriaLikeService = cafeteriaLikeService;
        this.badgeService = badgeService;
        this.likeService = likeService;
        this.thinkBigLikeService = thinkBigLikeService;
        this.userService = userService;
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Like cafeteria comment by ID")
    @PostMapping(value = "/{commentId}/like")
    public void likeCafeteriaComment(@PathVariable("commentId") Long commentId, Principal principal) {
        CafeteriaComment cafeteriaComment = cafeteriaCommentService.getCafeteriaCommentById(commentId);
        User user = userService.getUserByUsername(principal.getName());

        cafeteriaLikeService.createNewCafeteriaCommentLike(cafeteriaComment, user);

        Badge badge;
        int likeCount = checkUserLikeCount(user);

        if (likeCount >= 100) {
            badge = badgeService.getBadgeById(9L);
            badgeService.awardBadgeToUser(badge, cafeteriaComment.getAuthor());
        } else if (likeCount >= 25) {
            badge = badgeService.getBadgeById(8L);
            badgeService.awardBadgeToUser(badge, cafeteriaComment.getAuthor());
        } else if (likeCount >= 5) {
            badge = badgeService.getBadgeById(7L);
            badgeService.awardBadgeToUser(badge, cafeteriaComment.getAuthor());
        }
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unlike cafeteria comment by ID")
    @PostMapping(value = "/{commentId}/unlike")
    public void unlikeCafeteriaComment(@PathVariable("commentId") Long commentId, Principal principal) {
        CafeteriaComment cafeteriaComment = cafeteriaCommentService.getCafeteriaCommentById(commentId);
        User user = userService.getUserByUsername(principal.getName());

        cafeteriaLikeService.removeCafeteriaCommentLike(cafeteriaComment, user);
    }

    // TODO Copied from QAndAController, move to own service
    private int checkUserLikeCount(User user) {
        // FIXME ProfileController has a redundant method now
        return likeService.getAnswerLikesForAnswerAuthor(user).size()
            + likeService.getQuestionLikesForQuestionAuthor(user).size()
            + thinkBigLikeService.getCommentLikesForUser(user).size()
            + cafeteriaLikeService.getCafeteriaLikesForUser(user).intValue();
    }
}
