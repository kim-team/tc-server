/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.data.repository;

import de.thm.kim.tc.app.auth.data.entity.AuthUserVerification;
import de.thm.kim.tc.app.auth.data.entity.AuthUserVerificationId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AuthUserVerificationRepository extends JpaRepository<AuthUserVerification, AuthUserVerificationId> {
    List<AuthUserVerification> findAllByIdEmail(String email);

    Optional<AuthUserVerification> findByIdUsernameAndIdEmail(String username, String email);

    Optional<AuthUserVerification> findByIdEmailAndToken(String email, String token);

    boolean existsByIdUsername(String username);

    boolean existsByToken(String token);

    long deleteAllByCreatedAtBefore(Date date);

    void deleteAllByIdUsername(String username);
}
