/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/phases',
      name: 'Phases',
      component: () => import('../views/Phases.vue'),
    },
    {
      path: '/phases/:phaseId/groups',
      name: 'Phase - Groups',
      component: () => import('../views/Groups.vue'),
    },
    {
      path: '/phases/:phaseId/groups/:groupId/units',
      name: 'Phase - Groups - Units',
      component: () => import('../views/Units.vue'),
    },
    {
      path: '/phases/:phaseId/groups/:groupId/units/:unitId/medias',
      name: 'Phase - Groups - Units - Medias',
      component: () => import('../views/Medias.vue'),
    },
    {
      path: '/phases/:phaseId/groups/:groupId/units/:unitId/entries',
      name: 'Phase - Groups - Units - Entries',
      component: () => import('../views/Entries.vue'),
    },
    {
      path: '/phases/:phaseId/groups/:groupId/units/:unitId/questions',
      name: 'Phase - Groups - Units - Questions',
      component: () => import('../views/Questions.vue'),
    },
    {
      path: '/phases/:phaseId/groups/:groupId/units/:unitId/questions/:questionId/answers',
      name: 'Phase - Groups - Units - Questions - Answers',
      component: () => import('../views/Answers.vue'),
    },
    {
      path: '/phases/:phaseId/persons',
      name: 'Phase - Persons',
      component: () => import('../views/Persons.vue'),
    },
    {
      path: '/phases/:phaseId/persons/:personId/addresses',
      name: 'Phase - Persons - Addresses',
      component: () => import('../views/Addresses.vue'),
    },
    {
      path: '/completions',
      name: 'Completed quizzes',
      component: () => import('../views/Completions.vue'),
    },
  ]
})
