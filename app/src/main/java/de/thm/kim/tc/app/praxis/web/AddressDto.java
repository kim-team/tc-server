/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.praxis.data.entity.PpAddress;
import lombok.Value;

// TODO identical to PpAddress
@Value
class AddressDto {

    Long addressId;
    Long personId;
    String street;
    String city;
    String postalCode;
    String phone;
    String email;
    String latitude;
    String longitude;

    public AddressDto(PpAddress address) {
        this.addressId = address.getAddressId();
        this.personId = address.getPersonId();
        this.street = address.getStreet();
        this.city = address.getCity();
        this.postalCode = address.getPostalCode();
        this.phone = address.getPhone();
        this.email = address.getEmail();
        this.latitude = address.getLatitude();
        this.longitude = address.getLongitude();
    }
}
