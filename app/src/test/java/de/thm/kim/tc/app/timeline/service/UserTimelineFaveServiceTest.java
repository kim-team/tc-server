/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineFave;
import de.thm.kim.tc.app.timeline.data.repository.UserTimelineFaveRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UserTimelineFaveServiceTest {

    private static final Collection<TimelineEntry> TIMELINE_ENTRIES = Set.of();

    private static final int PAGE_NUMBER = 0;
    private static final int PAGE_SIZE = 2;

    private UserTimelineFaveService service;

    @Mock
    private UserTimelineHideService userTimelineHideService;
    @Mock
    private UserTimelineFaveRepository userTimelineFaveRepositoryMock;

    @Mock
    private UserTimelineFave userTimelineFaveMock;
    @Mock
    private UserTimelineFave userTimelineFaveUpdateMock;
    @Mock
    private TimelineEntry timelineEntryMock;
    @Mock
    private User userMock;
    @Mock
    private Collection<UserTimelineFave> userTimelineFavesMock;
    @Mock
    private Page<UserTimelineFave> userTimelineFavePageMock;

    private final Pageable pageRequest = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);

    @BeforeEach
    public void setUp() {
        service = new UserTimelineFaveService(userTimelineFaveRepositoryMock, userTimelineHideService);
    }

    @Test
    public void get_timeline_fave_by_timeline_entries_and_user_id() {
        when(userTimelineHideService.getAllHiddenTimelineEntriesByUser(any()))
            .thenReturn(Collections.emptySet());
        when(userTimelineFaveRepositoryMock.findByTimelineEntryInAndUser(TIMELINE_ENTRIES, userMock))
            .thenReturn(userTimelineFavesMock);

        assertThat(service.getTimelineFaveByTimelineEntriesAndUser(TIMELINE_ENTRIES, userMock),
            is(userTimelineFavesMock.stream()
                .map(UserTimelineFave::getTimelineEntry)
                .collect(Collectors.toSet())));
        verify(userTimelineFaveRepositoryMock).findByTimelineEntryInAndUser(anyCollection(), any());
    }

    @Test
    public void get_faves_for_user_as_page() {
        when(userTimelineFaveRepositoryMock.findAllByUser(eq(userMock), any(Pageable.class)))
            .thenReturn(userTimelineFavePageMock);

        assertThat(service.getFavesForUserAsPage(userMock, pageRequest),
            is(userTimelineFavePageMock.map(UserTimelineFave::getTimelineEntry)));
    }

    @Test
    public void add_timeline_fave() {
        when(userTimelineFaveRepositoryMock.findByTimelineEntryAndUser(timelineEntryMock, userMock))
            .thenReturn(Optional.of(userTimelineFaveMock));
        when(userTimelineFaveRepositoryMock.save(userTimelineFaveMock))
            .thenReturn(userTimelineFaveUpdateMock);

        service.addTimelineFave(timelineEntryMock, userMock);

        verify(userTimelineFaveRepositoryMock).save(userTimelineFaveMock);
    }

    @Test
    public void delete_timeline_fave() {
        when(userTimelineFaveRepositoryMock.findByTimelineEntryAndUser(timelineEntryMock, userMock))
            .thenReturn(Optional.of(userTimelineFaveMock));

        service.deleteTimelineFaveForUser(timelineEntryMock, userMock);

        verify(userTimelineFaveRepositoryMock).delete(userTimelineFaveMock);
    }
}
