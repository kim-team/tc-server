/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelineLike;
import de.thm.kim.tc.app.timeline.service.TimelinePinService;
import de.thm.kim.tc.app.timeline.service.TimelineReportService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import de.thm.kim.tc.app.timeline.service.UserTimelineFaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Api(description = "Everything about faved timeline entries", tags = {"Timeline - Faved"})
@RestController
@RequestMapping(value = "/timeline/faved")
public class FavedEntryController {

    private final UserService userService;
    private final TimelineService timelineService;
    private final TimelinePinService timelinePinService;
    private final TimelineReportService timelineReportService;
    private final UserTimelineFaveService userTimelineFaveService;

    @Autowired
    public FavedEntryController(UserService userService,
                                TimelineService timelineService,
                                TimelinePinService timelinePinService,
                                TimelineReportService timelineReportService, UserTimelineFaveService userTimelineFaveService) {
        this.userService = userService;
        this.timelineService = timelineService;
        this.timelinePinService = timelinePinService;
        this.timelineReportService = timelineReportService;
        this.userTimelineFaveService = userTimelineFaveService;
    }

    @ApiOperation(value = "Find faved timeline entries for current user. Returns no elements for guests.")
    @GetMapping
    public Page<TimelineEntryDto> favesByUser(Principal principal,
                                              @SortDefault(
                                                  sort = "timelineEntry.id",
                                                  direction = Sort.Direction.DESC)
                                                  Pageable pageable) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new PageImpl<>(Collections.emptyList(), pageable, 0);

        Page<TimelineEntry> page = userTimelineFaveService.getFavesForUserAsPage(user.get(), pageable);
        Collection<TimelineEntry> pins = timelinePinService.getAllTimelinePinsByUser(user.get());

        List<TimelineEntryDto> timelineEntryDtos = page.getContent()
            .stream()
            .map(timelineEntry -> {
                Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                return new TimelineEntryDto(timelineEntry,
                    true,
                    pins.contains(timelineEntry),
                    likes.size(),
                    likes.stream().anyMatch(value -> value.getUser().equals(user.get())),
                    timelineReportService.isReportedByUser(timelineEntry, user));
            })
            .collect(Collectors.toList());

        return new PageImpl<>(timelineEntryDtos, pageable, page.getTotalElements());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Fave a timeline entry for current user")
    @PostMapping(path = "/{entryId}")
    public void addTimelineFaves(Principal principal,
                                 @PathVariable("entryId") Long entryId) {
        User user = userService.getUserByUsername(principal.getName());
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);

        userTimelineFaveService.addTimelineFave(entry, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Un-fave a timeline entry for current user")
    @DeleteMapping(path = "/{entryId}")
    public void deleteTimelineFaves(Principal principal,
                                    @PathVariable("entryId") Long entryId) {
        User user = userService.getUserByUsername(principal.getName());
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);

        userTimelineFaveService.deleteTimelineFaveForUser(entry, user);
    }
}
