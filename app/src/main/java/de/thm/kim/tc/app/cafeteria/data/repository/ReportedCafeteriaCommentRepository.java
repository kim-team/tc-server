/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.data.entity.ReportedCafeteriaComment;
import de.thm.kim.tc.app.cafeteria.data.entity.ReportedCafeteriaCommentId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ReportedCafeteriaCommentRepository extends CrudRepository<ReportedCafeteriaComment, ReportedCafeteriaCommentId> {

    boolean existsByReportedItemAndReportingUser(CafeteriaComment comment, User user);

    void deleteByReportedItemAndReportingUser(CafeteriaComment comment, User user);

    void deleteAllByReportedItemId(Long id);

    void deleteAllByReportedItem(CafeteriaComment comment);

    Collection<ReportedCafeteriaComment> findAll();

    List<ReportedCafeteriaComment> findAllByOrderByReportedItemDesc();

    void deleteAllByReportingUser(User user);
}
