-- Fix PK sequence for categories after manual INSERT with hard-coded PKs
SELECT pg_catalog.setval(pg_get_serial_sequence('categories', 'category_id'),
                         (SELECT MAX(category_id) FROM categories));
