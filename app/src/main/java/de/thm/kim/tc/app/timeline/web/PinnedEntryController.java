/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelineLike;
import de.thm.kim.tc.app.timeline.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Api(description = "Everything about pinned timeline entries", tags = {"Timeline - Pinned entries"})
@RestController
@RequestMapping(value = "/timeline/pinned")
public class PinnedEntryController {

    private final UserService userService;
    private final TimelineService timelineService;
    private final TimelinePinService timelinePinService;
    private final TimelineReportService timelineReportService;
    private final UserTimelineFaveService userTimelineFaveService;
    private final UserTimelineUnpinService userTimelineUnpinService;

    @Autowired
    public PinnedEntryController(UserService userService,
                                 TimelineService timelineService,
                                 TimelinePinService timelinePinService,
                                 TimelineReportService timelineReportService,
                                 UserTimelineFaveService userTimelineFaveService,
                                 UserTimelineUnpinService userTimelineUnpinService) {
        this.userService = userService;
        this.timelineService = timelineService;
        this.timelinePinService = timelinePinService;
        this.timelineReportService = timelineReportService;
        this.userTimelineFaveService = userTimelineFaveService;
        this.userTimelineUnpinService = userTimelineUnpinService;
    }

    @Deprecated
    @ApiOperation(value = "DEPRECATED - Get all pinned timeline entries")
    @GetMapping(path = "/all")
    public Collection<TimelineEntryDto> getAllTimelinePins() {
        return timelinePinService.getAllTimelinePins()
            .stream()
            .map(timelineEntry -> {
                Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                return new TimelineEntryDto(timelineEntry, false, true, likes.size(), false, false);
            })
            .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get pinned timeline entries for a user")
    @GetMapping
    public Collection<TimelineEntryDto> getAllTimelinePinsByUser(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty()) {
            return timelinePinService.getAllTimelinePins()
                .stream()
                .map(timelineEntry -> {
                    Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                    return new TimelineEntryDto(timelineEntry, false, true, likes.size(), false, false);
                })
                .collect(Collectors.toList());

        }

        Collection<TimelineEntry> timelineEntries = timelinePinService.getAllTimelinePinsByUser(user.get());
        Collection<TimelineEntry> faves = userTimelineFaveService.getTimelineFaveByTimelineEntriesAndUser(timelineEntries, user.get());

        return timelineEntries
            .stream()
            .map(timelineEntry -> {
                Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                return new TimelineEntryDto(timelineEntry,
                    faves.contains(timelineEntry),
                    true,
                    likes.size(),
                    likes.stream().anyMatch(value -> value.getUser().equals(user.get())),
                    timelineReportService.isReportedByUser(timelineEntry, user));
            })
            .collect(Collectors.toList());
    }

    @Deprecated
    @Secured("ROLE_USER")
    @ApiOperation(value = "DEPRECATED - Get unpinned timeline entries for a user")
    @GetMapping(path = "/unpin")
    public Collection<TimelineEntryDto> getAllUnpinnedTimelineEntriesByUser(Principal principal) {

        User user = userService.getUserByUsername(principal.getName());

        Collection<TimelineEntry> timelineEntries = userTimelineUnpinService.getAllUnpinnedTimelineEntriesByUser(user);
        Collection<TimelineEntry> faves = userTimelineFaveService.getTimelineFaveByTimelineEntriesAndUser(timelineEntries, user);

        return timelineEntries
            .stream()
            .map(timelineEntry -> {
                Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                return new TimelineEntryDto(timelineEntry,
                    faves.contains(timelineEntry),
                    false,
                    likes.size(),
                    likes.stream().anyMatch(value -> value.getUser().equals(user)),
                    timelineReportService.isReportedByUser(timelineEntry, Optional.of(user)));
            })
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unpin a timeline entry for a user")
    @PostMapping(path = "/unpin/{entryId}")
    public void unpinTimelineEntry(Principal principal,
                                   @PathVariable("entryId") Long entryId) {
        User user = userService.getUserByUsername(principal.getName());
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);

        userTimelineUnpinService.addUnpinTimelineEntry(entry, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Reset an unpinned timeline entry for a user")
    @DeleteMapping(path = "/unpin/{entryId}")
    public void deleteUnpinTimelineEntry(Principal principal,
                                         @PathVariable("entryId") Long entryId) {
        User user = userService.getUserByUsername(principal.getName());
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);

        userTimelineUnpinService.deleteUnpinTimelineEntryForUser(entry, user);
    }
}
