/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.praxis.data.entity.PpGroup;
import de.thm.kim.tc.app.praxis.data.repository.PpGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PpGroupService {

    private final PpUnitService unitService;
    private final PpGroupRepository ppGroupRepository;

    @Autowired
    public PpGroupService(PpUnitService unitService,
                          PpGroupRepository ppGroupRepository) {
        this.unitService = unitService;
        this.ppGroupRepository = ppGroupRepository;
    }

    public List<PpGroup> getGroupsForPhase(Long phaseId) {
        return ppGroupRepository.findAllByPhaseIdOrderByOrder(phaseId);
    }

    public List<PpGroup> getAllGroups() {
        return ppGroupRepository.findAllByOrderByOrder();
    }

    public Optional<PpGroup> getGroupById(Long groupId) {
        return ppGroupRepository.findById(groupId);
    }

    public PpGroup createOrUpdateGroup(PpGroup group) {
        return ppGroupRepository.save(group);
    }

    @Transactional
    public void deleteGroupById(Long groupId) {
        unitService.getUnitsForGroup(groupId)
            .forEach(u -> unitService.deleteUnitById(u.getUnitId()));
        ppGroupRepository.deleteById(groupId);
    }
}
