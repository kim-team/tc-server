/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.entity.Permission;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class TinyCampusUserPrincipal implements UserDetails {

    private final AuthUser authUser;
    private final List<Permission> permissions;

    public TinyCampusUserPrincipal(AuthUser authUser, List<Permission> permissions) {
        super();
        this.authUser = authUser;
        this.permissions = permissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(permissions == null)
            return Collections.emptySet();

        Set<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();

        permissions.forEach(role ->
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRole()))
        );

        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return authUser.getPassword();
    }

    @Override
    public String getUsername() {
        return authUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
