/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth;

import de.thm.kim.tc.app.auth.service.TinyCampusUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;

import java.util.Arrays;
import java.util.Properties;
import java.util.stream.Stream;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final TinyCampusUserDetailsService userDetailsService;
    private final CorsConfig corsConfig;
    private final Environment env;

    @Autowired
    public SecurityConfig(TinyCampusUserDetailsService userDetailsService, CorsConfig corsConfig, Environment env) {
        this.userDetailsService = userDetailsService;
        this.corsConfig = corsConfig;
        this.env = env;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(passwordEncoder());
        provider.setAuthoritiesMapper(authoritiesMapper());
        return provider;
    }

    @Bean
    public GrantedAuthoritiesMapper authoritiesMapper() {
        SimpleAuthorityMapper authorityMapper = new SimpleAuthorityMapper();
        authorityMapper.setConvertToUpperCase(true);
        return authorityMapper;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth
            .authenticationEventPublisher(authenticationEventPublisher())
            .authenticationProvider(authenticationProvider());
    }

    @Bean
    public DefaultAuthenticationEventPublisher authenticationEventPublisher() {
        DefaultAuthenticationEventPublisher authenticationEventPublisher = new DefaultAuthenticationEventPublisher();
        Properties additionalExceptions = new Properties();
        additionalExceptions.put(
            InvalidGrantException.class.getName(), AuthenticationFailureBadCredentialsEvent.class.getName()
        );
        authenticationEventPublisher.setAdditionalExceptionMappings(additionalExceptions);
        return authenticationEventPublisher;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Disable certain routes in production
        String[] adminPattern = SecurityPatterns.ADMIN_PATTERN;

        // Add dev routes
        if (Arrays.stream(env.getActiveProfiles()).anyMatch("dev"::equalsIgnoreCase)) {
            adminPattern = Stream.concat(
                Stream.of(adminPattern),
                Stream.of(SecurityPatterns.DEV_PATTERN)
            ).toArray(String[]::new);
        }

        // Add Swagger routes
        if (Arrays.stream(env.getActiveProfiles()).anyMatch("swagger"::equalsIgnoreCase)) {
            adminPattern = Stream.concat(
                Stream.of(adminPattern),
                Stream.of(SecurityPatterns.SWAGGER_PATTERN)
            ).toArray(String[]::new);
        }

        http
            .authorizeRequests()
            .antMatchers(SecurityPatterns.PERMIT_ALL_PATTERN).permitAll()
            .antMatchers(SecurityPatterns.USER_PATTERN).hasRole("USER")
            .antMatchers(adminPattern).hasRole("ADMIN")
            .anyRequest().denyAll()
            .and()
            .httpBasic();

        http.headers().frameOptions().disable();
        http.csrf().disable();

        // Enable CORS for the local practice frontend server. In production,
        // the frontend runs on the same host as the server.
        if (Arrays.stream(env.getActiveProfiles()).anyMatch("dev"::equalsIgnoreCase)) {
            http.cors().configurationSource(corsConfig::getCorsConfig);
        }
    }

}
