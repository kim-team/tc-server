/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.thinkbig.data.entity.Comment;
import de.thm.kim.tc.app.thinkbig.data.entity.CommentLike;
import de.thm.kim.tc.app.thinkbig.data.entity.Event;
import de.thm.kim.tc.app.thinkbig.data.entity.EventLike;
import de.thm.kim.tc.app.thinkbig.data.repository.CommentLikeRepository;
import de.thm.kim.tc.app.thinkbig.data.repository.EventLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ThinkBigLikeService {

    private final EventLikeRepository eventLikeRepository;
    private final CommentLikeRepository commentLikeRepository;

    @Autowired
    public ThinkBigLikeService(EventLikeRepository eventLikeRepository, CommentLikeRepository commentLikeRepository) {
        this.eventLikeRepository = eventLikeRepository;
        this.commentLikeRepository = commentLikeRepository;
    }

    // Event Likes

    public Collection<EventLike> getEventLikesForEvent(Event event) {
        return eventLikeRepository.findAllByEvent(event);
    }

    public EventLike createNewEventLike(Event event, User user) {
        // FIXME findByEventAndUser() always returns null
        return eventLikeRepository.findByEventAndUser(event, user)
            .orElse(eventLikeRepository.save(new EventLike(event, user)));
    }

    public EventLike removeEventLike(Event event, User user) {
        // FIXME findByEventAndUser() always returns null
        EventLike eventLike = eventLikeRepository.findByEventAndUser(event, user)
            .orElseThrow(() -> new ResourceNotFoundException("No EventLike found for Event ID: " + event.getId() + " and User ID: " + user.getId()));

        eventLikeRepository.delete(eventLike);
        return eventLike;
    }

    // Comment Likes

    public Collection<CommentLike> getCommentLikesForComment(Comment comment) {
        return commentLikeRepository.findAllByComment(comment);
    }

    public Collection<CommentLike> getCommentLikesForUser(User user) {
        return commentLikeRepository.findAllByUser(user);
    }

    public CommentLike createNewCommentLike(Comment comment, User user) {
        // FIXME findByCommentAndUser() always returns null
        return commentLikeRepository.findByCommentAndUser(comment, user)
            .orElse(commentLikeRepository.save(new CommentLike(comment, user)));
    }

    public CommentLike removeCommentLike(Comment comment, User user) {
        // FIXME findByCommentAndUser() always returns null
        CommentLike commentLike = commentLikeRepository.findByCommentAndUser(comment, user)
            .orElseThrow(() -> new ResourceNotFoundException("No QuestionLike found for Question ID: " + comment.getId() + " and User ID: " + user.getId()));

        commentLikeRepository.delete(commentLike);
        return commentLike;
    }

    public void deleteAllCommentLikesForComment(Comment comment) {
        commentLikeRepository.deleteAllByComment(comment);
    }
}
