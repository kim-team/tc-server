/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.modules.data.entity;

import de.thm.kim.tc.app.account.data.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user_modules")
public class UserModule {

    @EmbeddedId
    private UserModuleId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("moduleId")
    @JoinColumn(name = "module_id")
    private Module module;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "is_visible")
    private boolean isVisible;

    @Column(name = "priority")
    private int priority;

    public UserModule(Module module, User user, boolean isVisible, int priority) {
        this.id = new UserModuleId(module.getId(), user.getId());
        this.module = module;
        this.user = user;
        this.isVisible = isVisible;
        this.priority = priority;
    }

    public UserModule(Module module, User user, boolean isVisible) {
        this(module, user, isVisible, module.getPriority());
    }

}
