# tinyCampus Server
[![pipeline status](https://gitlab.com/tinycampus/server/badges/master/pipeline.svg)](https://gitlab.com/tinycampus/server/commits/master)
Server for the tinyCampus app. Built using Spring Boot and Spring Security.

## Build Instructions

### Build from source and run using Gradle wrapper (recommended)

1. Clone git repository and build from source  
    `$ git clone git@gitlab.com:tinycampus/server.git`
2. Run using Gradle wrapper  
     **GNU/Linux:**  
     `$ ./gradlew bootRun`  
     
     **Windows:**  
     `C:\Users\YourUserName\server>gradlew.bat bootRun`

### Run using runnable JAR

1. Download latest 'build' artifact JAR via the 'Download' button right next to the address bar or [here](https://gitlab.com/tinycampus/server/-/jobs/artifacts/master/download?job=build)
2. Extract archive
3. Run JAR with `java -jar tc-app-0.0.1-SNAPSHOT.jar`

### Build Docker image using the [Jib](https://github.com/GoogleContainerTools/jib) plugin for Gradle

- Build Docker image and push to registry (requires `CI_REGISTRY`, `CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD` environment variables)  
    `$ ./gradlew jib`
- Build Docker image as tarball  
    `$ ./gradlew jibBuildTar`

## Deployment

Example `docker-compose.yml` files for the production and staging environments
can be found in the `docker/` directory. You will need to build the images using
Jib as described in the previous step first and adjust the image URL in the
`docker-compose.yml` files first.

## Usage

### Default port

The default port is 8080, e.g. `http://localhost:8080/`. Specify a custom port:

- Using Gradle Wrapper  
  `$ ./gradlew bootRun --args="--server.port=3333"`
- Using the runnable JAR  
  `$ java -jar -Dserver.port=3333 tc-app-0.0.1-SNAPSHOT.jar`

### Login credentials

At the moment, there are three user accounts available for testing purposes. All routes requiring a user ID (you can use ID `1` for testing) can be used with either account.

| User name   | Password    | Role                              |
| ----------- | ----------- | --------------------------------- |
| `user`      | `user`      | Regular authenticated user        |
| `admin`     | `admin`     | User with admin privileges        |
| `anonymous` | `anonymous` | Anynomous user without privileges |

## API documentation

Interactive API documentation is provided via Swagger UI and can be accessed
under http://localhost:8080/swagger-ui.html using the credentials `admin`/`admin`.  

## Licence

tinyCampus is licenced under the [EUPL](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

The name "tinyCampus" and the tinyCapmus logo are trademarked by [Technische Hochschule Mittelhessen](https://thm.de).
