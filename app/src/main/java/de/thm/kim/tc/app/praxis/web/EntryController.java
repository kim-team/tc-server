/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpEntry;
import de.thm.kim.tc.app.praxis.data.entity.PpUnit;
import de.thm.kim.tc.app.praxis.service.PpEntryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis entries", tags = {"Praxis - Admin - Entries"})
@RestController
@RequestMapping(path = "/praxis")
class EntryController {

    private final PpEntryService ppEntryService;

    @Autowired
    EntryController(PpEntryService ppEntryService) {
        this.ppEntryService = ppEntryService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all entries for unit", tags = {"Praxis - Admin - Entries"})
    @GetMapping(path = "/units/{unitId}/entries")
    public List<EntryDto> getEntriesForUnit(@PathVariable("unitId") Long unitId) {
        return ppEntryService.getEntriesForUnit(unitId).stream()
            .map(EntryDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all entries", tags = {"Praxis - Admin - Entries"})
    @GetMapping(path = "/entries")
    public List<EntryDto> getAllEntries() {
        return ppEntryService.getAllEntries().stream()
            .map(EntryDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new entry", tags = {"Praxis - Admin - Entries"})
    @PostMapping(path = "/entries")
    @ResponseStatus(HttpStatus.CREATED)
    public EntryDto saveEntry(@RequestBody PpEntry entry) {
        PpEntry e = ppEntryService.createOrUpdateEntry(entry);

        return new EntryDto(e);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update an entry", tags = {"Praxis - Admin - Entries"})
    @PutMapping(path = "/entries/{entryId}")
    public EntryDto updateEntry(@PathVariable("entryId") Long entryId, @RequestBody PpEntry entry) {
        if (entry.getEntryId() == null || !entry.getEntryId().equals(entryId))
            throw new IllegalArgumentException("Invalid entryId in body");

        ppEntryService.getEntryById(entryId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpEntry for ID: " + entryId));

        PpEntry e = ppEntryService.createOrUpdateEntry(entry);

        return new EntryDto(e);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete an entry", tags = {"Praxis - Admin - Entries"})
    @DeleteMapping(path = "/entries/{entryId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEntry(@PathVariable("entryId") Long entryId) {
        ppEntryService.getEntryById(entryId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpEntry for ID: " + entryId));

        ppEntryService.deleteEntryById(entryId);
    }

    public static List<EntryDto> getEntryDtosForUnit(PpEntryService service, PpUnit unit) {
        return service.getEntriesForUnit(unit.getUnitId()).stream()
            .map(EntryDto::new)
            .collect(Collectors.toList());
    }
}
