/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.repository.SourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class SourceService implements HealthIndicator {

    private final SourceRepository sourceRepository;

    @Autowired
    public SourceService(SourceRepository sourceRepository) {
        this.sourceRepository = sourceRepository;
    }

    @Override
    public Health health() {
        if (sourceRepository.count() <= 0)
            return Health.down().build();

        return Health.up().build();
    }

    /**
     * Get all Sources in the repository
     *
     * @return all Sources
     */
    public Collection<Source> getAllSources() {
        return sourceRepository.findAll();
    }

    public Collection<Source> getAllSourcesByUser(User user) {
        return sourceRepository.findAllByUser(user);
    }

    /**
     * Get all top-level Sources
     * <p>
     * Sources without a parent Source are considered top-level.
     *
     * @return all top-level Sources
     */
    public Collection<Source> getAllTopLevelSources() {
        return sourceRepository.findAllByParentIsNull();
    }

    /**
     * Get an entire list of Sources, depending on the User.
     * The user can black- and whitelist Sources and the
     *
     * @param user user
     * @return all default subscribed Sources, depending on the user (whitelist, blacklist, campus and department)
     */
    // TODO: There is a duplicate entry, see getAllSourcesByUserId
    public Collection<Source> getAllSubscribedSourcesByUser(User user) {
        Collection<Source> sources = sourceRepository.findAllByUser(user);

        // Dirty workaround to make sure no sources further down the tree are included
        // in case the user does not subscribe to one of their parents
        return sources.stream()
            .filter(source -> source.getParent() == null || sources.containsAll(source.getAllParents()))
            .collect(Collectors.toList());
    }

    /**
     * Try to find {@link Source} for the specified ID or throw {@link ResourceNotFoundException}.
     *
     * @param sourceId Source id
     * @return existing Source
     * @throws ResourceNotFoundException if Source doesn't exists
     */
    public Source getSourceById(Long sourceId) {
        return sourceRepository.findById(sourceId)
            .orElseThrow(() -> new ResourceNotFoundException("No Source found for ID: " + sourceId));
    }

    /**
     * Try to find {@link Source} for the specified name or throw {@link ResourceNotFoundException}.
     *
     * @param name Source name
     * @return existing Source
     * @throws ResourceNotFoundException if Source doesn't exists
     */
    public Source getSourceByName(String name) {
        return sourceRepository.findByName(name)
            .orElseThrow(() -> new ResourceNotFoundException("No Source found for name: " + name));
    }
}
