/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.data.entity;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.reports.Reportable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "questions")
public class Question implements Reportable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "question_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private QuestionCategory category;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

    @Column(name = "text")
    private String text;

    @CreatedDate
    @Column(name = "date")
    private Date date;

    @LastModifiedDate
    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date modifiedAt;

    @OneToMany(mappedBy = "question")
    private List<Answer> answers;

    public Question(QuestionCategory questionCategory, User user, String text) {
        this.category = questionCategory;
        this.author = user;
        this.text = text;
        Date date = new Date();
        this.date = date;
        this.modifiedAt = date;
    }
}
