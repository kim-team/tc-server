/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users_sources")
public class UserSource {
    @JsonIgnore
    @EmbeddedId
    private UserSourceId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("sourceId")
    @JoinColumn(name = "source_id")
    private Source source;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "subscription")
    @Enumerated(EnumType.STRING)
    private SubscriptionTypes subscriptionType;

    public UserSource(Source source, User user, SubscriptionTypes subscriptionType) {
        this.id = new UserSourceId(source.getId(), user.getId());
        this.source = source;
        this.user = user;
        this.subscriptionType = subscriptionType;
    }
}
