/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.qanda.data.entity.Badge;
import de.thm.kim.tc.app.qanda.data.entity.UserBadge;
import de.thm.kim.tc.app.qanda.data.repository.BadgeRepository;
import de.thm.kim.tc.app.qanda.data.repository.UserBadgeRepository;
import lombok.extern.java.Log;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Log
@Service
public class BadgeService implements HealthIndicator {

    private final BadgeRepository badgeRepository;
    private final UserBadgeRepository userBadgeRepository;

    @Autowired
    public BadgeService(BadgeRepository badgeRepository, UserBadgeRepository userBadgeRepository) {
        this.badgeRepository = badgeRepository;
        this.userBadgeRepository = userBadgeRepository;
    }

    @Override
    public Health health() {
        if (badgeRepository.count() <= 0)
            return Health.down().build();

        return Health.up().build();
    }

    public Collection<Badge> getAllBadges() {
        return badgeRepository.findAll();
    }

    public Badge getBadgeById(Long id) {
        return badgeRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No Badge found for ID: " + id));
    }

    public void awardBadgeToUser(Badge badge, User user) {
        awardBadgeIfNotUnlocked(badge, user);

        val level = getUserLevel(user);
        if (level >= 11) {
            awardBadgeIfNotUnlocked(getBadgeById(12L), user);
        } else if (level >= 6) {
            awardBadgeIfNotUnlocked(getBadgeById(11L), user);
        } else if (level >= 3) {
            awardBadgeIfNotUnlocked(getBadgeById(10L), user);
        }
    }

    public Collection<UserBadge> getAllUserBadgesForUser(User user) {
        return userBadgeRepository.findAllByUser(user);
    }

    private void awardBadgeIfNotUnlocked(Badge badge, User user) {
        if (userBadgeRepository.findByBadgeAndUser(badge, user).isEmpty()) {
            userBadgeRepository.save(new UserBadge(badge, user));

            log.info(String.format("Awarded Badge %d to User %d",
                badge.getId(), user.getId()));
        }
    }

    /**
     * Returns the current level for the specified {@link User}. Users start
     * at level 1. The level is determined by the number of unlocked
     * {@link Badge}s + 1.
     *
     * @param user the user
     * @return the user's level
     */
    public long getUserLevel(User user) {
        return 1L + userBadgeRepository.countAllByUser(user);
    }

    @Transactional
    public void deleteBadgesByUser(User user) {
        userBadgeRepository.deleteAllByUser(user);
    }
}
