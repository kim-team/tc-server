/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.event;

import de.thm.kim.tc.app.auth.service.AuthUserVerificationService;
import de.thm.kim.tc.app.common.events.SendMailUtils;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Component
public class UserMailVerificationListener implements ApplicationListener<OnUserMailVerificationEvent> {

    private final AuthUserVerificationService service;
    private final SimpleMailMessage emailTemplate;
    private final JavaMailSender mailSender;

    @Autowired
    public UserMailVerificationListener(SimpleMailMessage emailTemplate,
                                        JavaMailSender mailSender,
                                        AuthUserVerificationService service) {
        this.emailTemplate = emailTemplate;
        this.mailSender = mailSender;
        this.service = service;
    }

    @Override
    public void onApplicationEvent(OnUserMailVerificationEvent event) {
        String username = event.getUsername();
        String displayName = event.getDisplayName();
        String email = event.getEmail();
        String token = service.createVerificationToken(username, email);
        String urlEncodedEmail = URLEncoder.encode(email, StandardCharsets.UTF_8);
        String link = SendMailUtils.getAppUrl() + "/oauth/verify?email=" + urlEncodedEmail + "&token=" + token;

        val subject = "tinyCampus - E-Mail-Adresse bestätigen";
        val text = "Willkommen bei tinyCampus, " + displayName + "\n\n" +
            "Jemand hat mit dieser E-Mail-Adresse einen Account bei tinyCampus erstellt. " +
            "Bitte verifiziere deinen Account, indem du auf folgenden Link klickst:\n" +
            "\n" +
            link + "\n" +
            "\n" +
            "Wenn du dich nicht bei tinyCampus registriert hast kannst du kannst diese E-Mail ignorieren. " +
            "Missbrauch bitte an uns melden unter info@tinycampus.de.\n" +
            "\n" +
            "Viele Grüße\n" +
            "dein tinyCampus-Team\n" +
                "\n\n" +
            "Welcome to tinyCampus, " + displayName + "\n\n" +
            "Someone created an account for tinyCampus using this email address. " +
            "Please verify your account by clicking on the following link:\n" +
            "\n" +
            link + "\n" +
            "\n" +
            "You can safely ignore this email if you did not sign up for tinyCampus. " +
            "Please report abuse to info@tinycampus.de.\n" +
            "\n" +
            "Best regards\n" +
            "your tinyCampus team\n";

        SendMailUtils.sendMail(emailTemplate, mailSender, email, subject, text);
    }
}
