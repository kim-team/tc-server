/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.UserRepository;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineFave;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UserTimelineFaveRepositoryTest {

    private static final Long USER_ID = 1L;
    private static final Long USER2_ID = 2L;
    private static final Long TIMELINE_ENTRY_ID = 8L;

    private static final int PAGE_NUMBER = 0;
    private static final int PAGE_SIZE = 2;

    @Autowired
    private UserTimelineFaveRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TimelineEntryRepository timelineEntryRepository;

    private final Pageable pageRequest = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);

    @Test
    public void find_all_by_user() {
        User user = userRepository.findById(USER_ID).orElseThrow();

        Page<UserTimelineFave> userTimelineFaves = repository.findAllByUser(user, pageRequest);
        assertFalse(userTimelineFaves.isEmpty());

        User user2 = userRepository.findById(USER2_ID).orElseThrow();
        Page<UserTimelineFave> user2TimelineFaves = repository.findAllByUser(user2, pageRequest);

        assertNotEquals(userTimelineFaves, user2TimelineFaves);
    }

    @Test
    public void find_by_timeline_entry_and_user() {
        User user = userRepository.findById(USER_ID).orElseThrow();
        TimelineEntry timelineEntry = timelineEntryRepository.findById(TIMELINE_ENTRY_ID).orElseThrow();

        Optional<UserTimelineFave> userTimelineFave = repository.findByTimelineEntryAndUser(timelineEntry, user);
        assertFalse(userTimelineFave.isEmpty());

        User user2 = userRepository.findById(USER2_ID).orElseThrow();
        Optional<UserTimelineFave> user2TimelineFave = repository.findByTimelineEntryAndUser(timelineEntry, user2);

        assertNotEquals(userTimelineFave, user2TimelineFave);
    }

    @Test
    public void find_by_timeline_entry_in_and_user() {
        User user = userRepository.findById(USER_ID).orElseThrow();
        TimelineEntry timelineEntry = timelineEntryRepository.findById(TIMELINE_ENTRY_ID).orElseThrow();

        Collection<UserTimelineFave> userTimelineFaves = repository.findByTimelineEntryInAndUser(List.of(timelineEntry), user);
        assertFalse(userTimelineFaves.isEmpty());

        User user2 = userRepository.findById(USER2_ID).orElseThrow();
        Collection<UserTimelineFave> user2TimelineFaves = repository.findByTimelineEntryInAndUser(List.of(timelineEntry), user2);

        assertNotEquals(userTimelineFaves, user2TimelineFaves);
    }
}
