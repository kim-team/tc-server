/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.praxis.data.entity.PpMedia;
import de.thm.kim.tc.app.praxis.data.repository.PpMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PpMediaService {

    private final PpMediaRepository ppMediaRepository;

    @Autowired
    public PpMediaService(PpMediaRepository ppMediaRepository) {
        this.ppMediaRepository = ppMediaRepository;
    }

    public List<PpMedia> getMediaForUnit(Long unitId) {
        return ppMediaRepository.findAllByUnitIdOrderByOrder(unitId);
    }

    public List<PpMedia> getAllMedia() {
        return ppMediaRepository.findAllByOrderByOrder();
    }

    public Optional<PpMedia> getMediaById(Long mediaId) {
        return ppMediaRepository.findById(mediaId);
    }

    public PpMedia createOrUpdateMedia(PpMedia media) {
        return ppMediaRepository.save(media);
    }

    @Transactional
    public void deleteMediaById(Long mediaId) {
        ppMediaRepository.deleteById(mediaId);
    }
}
