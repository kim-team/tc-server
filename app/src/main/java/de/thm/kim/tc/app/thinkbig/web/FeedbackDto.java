/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.web;

import de.thm.kim.tc.app.thinkbig.data.entity.Feedback;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Data
class FeedbackDto {

    @NotNull
    private Long eventId;

    @Min(1)
    @Max(5)
    @ApiModelProperty(allowableValues = "range[1, 5]")
    private int rating;

    @NotNull
    @Size(max = 255)
    private String comment;

    FeedbackDto(Feedback feedback) {
        this.eventId = feedback.getEvent().getId();
        this.rating = feedback.getRating();
        this.comment = feedback.getComment();
    }
}
