/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaCommentRepository;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.common.reports.ReportService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CafeteriaCommentService {

    private final BlockService blockService;
    private final CafeteriaCommentRepository cafeteriaCommentRepository;
    private final CafeteriaLikeService cafeteriaLikeService;
    private final ReportService<CafeteriaComment> reportService;

    @Autowired
    public CafeteriaCommentService(BlockService blockService,
                                   CafeteriaCommentRepository cafeteriaCommentRepository,
                                   CafeteriaLikeService cafeteriaLikeService,
                                   ReportService<CafeteriaComment> reportService) {
        this.blockService = blockService;
        this.cafeteriaCommentRepository = cafeteriaCommentRepository;
        this.cafeteriaLikeService = cafeteriaLikeService;
        this.reportService = reportService;
    }

    public List<CafeteriaComment> getCafeteriaCommentsForCafeteriaItem(String cafeteriaItem, Optional<User> user) {
        val blockedUsers = user
            .map(blockService::getBlockedUsers)
            .orElse(Collections.emptySet());

        return cafeteriaCommentRepository.findAllByCafeteriaItemOrderByCreatedAtDesc(cafeteriaItem)
            .stream()
            .filter(comment -> user.isPresent() && comment.getAuthor().equals(user.get())
                || !blockedUsers.contains(comment.getAuthor()))
            .collect(Collectors.toList());
    }

    public CafeteriaComment createNewCafeteriaComment(String cafeteriaItem, User user, String text) {
        return cafeteriaCommentRepository.save(new CafeteriaComment(cafeteriaItem, user, text));
    }

    public CafeteriaComment getCafeteriaCommentById(Long cafeteriaCommentId) {
        return cafeteriaCommentRepository.findById(cafeteriaCommentId)
            .orElseThrow(() -> new ResourceNotFoundException("No Comment found for ID: " + cafeteriaCommentId));
    }

    public int getNumberOfCafeteriaCommentsForCafeteriaItem(String cafeteriaItem) {
        return cafeteriaCommentRepository.countByCafeteriaItem(cafeteriaItem);
    }

    public List<CafeteriaComment> getAllCafeteriaComments() {
        return cafeteriaCommentRepository.findAllByOrderByCreatedAtDesc();
    }

    public Collection<CafeteriaComment> getAllCafeteriaCommentsByAuthor(User author) {
        return cafeteriaCommentRepository.findAllByAuthor(author);
    }

    @Transactional
    public void deleteComment(CafeteriaComment cafeteriaComment) {
        reportService.removeAllReports(cafeteriaComment);
        cafeteriaLikeService.deleteAllCafeteriaCommentLikesForCafeteriaComment(cafeteriaComment);
        cafeteriaCommentRepository.delete(cafeteriaComment);
    }
}
