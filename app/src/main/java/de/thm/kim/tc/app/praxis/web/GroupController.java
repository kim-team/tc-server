/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpGroup;
import de.thm.kim.tc.app.praxis.data.entity.PpPhase;
import de.thm.kim.tc.app.praxis.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis groups", tags = {"Praxis - Admin - Groups"})
@RestController
@RequestMapping(path = "/praxis")
class GroupController {

    private final PpGroupService ppGroupService;
    private final PpUnitService ppUnitService;
    private final PpMediaService ppMediaService;
    private final PpEntryService ppEntryService;
    private final PpQuestionService ppQuestionService;
    private final PpAnswerService ppAnswerService;

    @Autowired
    GroupController(PpGroupService ppGroupService,
                    PpUnitService ppUnitService,
                    PpMediaService ppMediaService,
                    PpEntryService ppEntryService,
                    PpQuestionService ppQuestionService,
                    PpAnswerService ppAnswerService) {
        this.ppGroupService = ppGroupService;
        this.ppUnitService = ppUnitService;
        this.ppMediaService = ppMediaService;
        this.ppEntryService = ppEntryService;
        this.ppQuestionService = ppQuestionService;
        this.ppAnswerService = ppAnswerService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all groups for phase", tags = {"Praxis - Admin - Groups"})
    @GetMapping(path = "/phases/{phaseId}/groups")
    public List<GroupDto> getGroupsForPhase(@PathVariable("phaseId") Long phaseId) {
        return ppGroupService.getGroupsForPhase(phaseId).stream()
            .map(g -> new GroupDto(g,
                UnitController.getUnitDtosForGroup(
                    ppUnitService,
                    ppMediaService,
                    ppEntryService,
                    ppQuestionService,
                    ppAnswerService,
                    g)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all groups", tags = {"Praxis - Admin - Groups"})
    @GetMapping(path = "/groups")
    public List<GroupDto> getAllGroups() {
        return ppGroupService.getAllGroups().stream()
            .map(g -> new GroupDto(g,
                UnitController.getUnitDtosForGroup(
                    ppUnitService,
                    ppMediaService,
                    ppEntryService,
                    ppQuestionService,
                    ppAnswerService,
                    g)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Full content for specified praxis group", tags = {"Praxis - Admin - Groups"})
    @GetMapping(path = "/groups/{groupId}")
    public GroupDto getGroup(@PathVariable("groupId") Long groupId) {
        return ppGroupService.getGroupById(groupId)
            .map(g -> new GroupDto(g,
                UnitController.getUnitDtosForGroup(
                    ppUnitService,
                    ppMediaService,
                    ppEntryService,
                    ppQuestionService,
                    ppAnswerService,
                    g)))
            .orElseThrow(() -> new ResourceNotFoundException("No Group found for ID: " + groupId));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new group", tags = {"Praxis - Admin - Groups"})
    @PostMapping(path = "/groups")
    @ResponseStatus(HttpStatus.CREATED)
    public GroupDto saveGroup(@RequestBody PpGroup group) {
        PpGroup g = ppGroupService.createOrUpdateGroup(group);

        return new GroupDto(g,
            UnitController.getUnitDtosForGroup(
                ppUnitService,
                ppMediaService,
                ppEntryService,
                ppQuestionService,
                ppAnswerService,
                g));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a group", tags = {"Praxis - Admin - Groups"})
    @PutMapping(path = "/groups/{groupId}")
    public GroupDto updateGroup(@PathVariable("groupId") Long groupId, @RequestBody PpGroup group) {
        if (group.getGroupId() == null || !group.getGroupId().equals(groupId))
            throw new IllegalArgumentException("Invalid groupId in body");

        ppGroupService.getGroupById(groupId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpGroup for ID: " + groupId));

        PpGroup g = ppGroupService.createOrUpdateGroup(group);

        return new GroupDto(g,
            UnitController.getUnitDtosForGroup(
                ppUnitService,
                ppMediaService,
                ppEntryService,
                ppQuestionService,
                ppAnswerService,
                g));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a group", tags = {"Praxis - Admin - Groups"})
    @DeleteMapping(path = "/groups/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGroup(@PathVariable("groupId") Long groupId) {
        ppGroupService.getGroupById(groupId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpGroup for ID: " + groupId));

        ppGroupService.deleteGroupById(groupId);
    }

    public static List<GroupDto> getGroupDtosForPhase(PpGroupService groupService,
                                                      PpUnitService unitService,
                                                      PpMediaService mediaService,
                                                      PpEntryService entryService,
                                                      PpQuestionService questionService,
                                                      PpAnswerService answerService,
                                                      PpPhase phase) {
        return groupService.getGroupsForPhase(phase.getPhaseId()).stream()
            .map(g -> new GroupDto(g,
                UnitController.getUnitDtosForGroup(unitService, mediaService, entryService, questionService, answerService, g)))
            .collect(Collectors.toList());
    }
}
