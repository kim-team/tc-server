ALTER TABLE categories ADD parent_id BIGINT;
ALTER TABLE categories ADD logo VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE categories ADD FOREIGN KEY (parent_id) REFERENCES categories (category_id);

INSERT INTO categories (category_id, parent_id, title, text) VALUES
(7,  1, 'Wo ist was?',      'Fragen zu Locations etc.'),
(8,  5, 'Lost',             'Vermisstes'),
(9,  5, 'Found',            'Fundsachen'),
(10, 6, 'Schreibwerkstatt', 'Fragen rund um wissenschaftliches Schreiben');

CREATE TABLE users_categories (
    user_id         BIGINT NOT NULL,
    category_id     BIGINT NOT NULL,
    FOREIGN KEY (category_id) REFERENCES categories (category_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (category_id, user_id)
);
