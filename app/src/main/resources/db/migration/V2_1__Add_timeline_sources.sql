INSERT INTO sources (source_id, parent_id, campus_id, department_id, name, description, logo, subscription, content_root) VALUES
(1,  NULL, NULL, NULL, 'Redaktion',                    'Informationskanal der tinyCampus-Redaktion',    'tinycampus/logo_with_padding.png', 'SUBSCRIBED', '/home'),
(2,  1,    NULL, NULL, 'Allgemein',                    '',                                              'tinycampus/logo_with_padding.png', 'SUBSCRIBED', '/home'),
(3,  1,    NULL, NULL, 'Wichtige Mitteilungen',        '',                                              'tinycampus/logo_with_padding.png', 'SUBSCRIBED', '/home'),
(4,  1,    NULL, NULL, 'Veranstaltungen',              '',                                              'tinycampus/logo_with_padding.png', 'SUBSCRIBED', '/home'),
(5,  NULL, NULL, NULL, 'Fragen & Antworten',           'Beiträge aus dem Modul "Fragen und Antworten"', 'homescreen/module-qanda.png',      'SUBSCRIBED', '/qanda/question_view'),
(6,  NULL, NULL, NULL, 'Think Big',                    'Alles zur "Think Big - Build Small"-Konferenz', 'thinkbig/thinkbig_banner.jpg',     'SUBSCRIBED', '/tb'),
(7,  6,    NULL, NULL, 'Think Big - Kommentare',       '',                                              'thinkbig/thinkbig_banner.jpg',     'SUBSCRIBED', '/tb/event/comments');
