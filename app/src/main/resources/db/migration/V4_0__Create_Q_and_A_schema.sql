CREATE TABLE categories (
    category_id BIGINT GENERATED BY DEFAULT AS IDENTITY,
    title       VARCHAR(255) NOT NULL,
    text        VARCHAR(255) NOT NULL,
    PRIMARY KEY (category_id)
);

CREATE TABLE questions (
    question_id BIGINT GENERATED BY DEFAULT AS IDENTITY,
    category_id BIGINT       NOT NULL,
    user_id     BIGINT       NOT NULL,
    text        VARCHAR(255) NOT NULL,
    date        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (category_id) REFERENCES categories (category_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (question_id)
);

CREATE TABLE answers (
    answer_id   BIGINT GENERATED BY DEFAULT AS IDENTITY,
    question_id BIGINT       NOT NULL,
    user_id     BIGINT       NOT NULL,
    text        VARCHAR(255) NOT NULL,
    date        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (answer_id)
);

CREATE TABLE answer_likes (
    answer_id BIGINT    NOT NULL,
    user_id   BIGINT    NOT NULL,
    date      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (answer_id) REFERENCES answers (answer_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (answer_id, user_id)
);

CREATE TABLE question_likes (
    question_id BIGINT    NOT NULL,
    user_id     BIGINT    NOT NULL,
    date        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (question_id, user_id)
);

CREATE TABLE question_views (
    question_id BIGINT NOT NULL,
    user_id     BIGINT NOT NULL,
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (question_id, user_id)
);

CREATE TABLE badges (
    badge_id    BIGINT GENERATED BY DEFAULT AS IDENTITY,
    title       VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    asset       VARCHAR(255) NOT NULL,
    PRIMARY KEY (badge_id)
);

CREATE TABLE user_badges (
    badge_id BIGINT    NOT NULL,
    user_id  BIGINT    NOT NULL,
    date     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (badge_id) REFERENCES badges (badge_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (badge_id, user_id)
);
