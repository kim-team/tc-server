/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import de.thm.kim.tc.app.timeline.service.SourceService;
import de.thm.kim.tc.app.timeline.service.UserSourceService;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.Objects;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class SourceControllerTest {

    private static final String URL = "/timeline/sources";
    private static final String VALID_USERNAME = "jane_doe";
    private static final String INVALID_USERNAME = "i_dont_exist";
    private static final Long SOURCE_ID = 1L;
    private static final Long INVALID_SOURCE_ID = 999L;

    @SpyBean
    private SourceService sourceService;

    @SpyBean
    private UserService userService;

    @SpyBean
    private UserSourceService userSourceService;

    @Autowired
    private MockMvc mockMvc;

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void get_public_timeline_settings_for_invalid_user() throws Exception {
        mockMvc.perform(get(URL + "/settings"))
            .andExpect(status().isOk());

        verify(sourceService, never())
            .getAllSubscribedSourcesByUser(any(User.class));

        verify(sourceService).getAllTopLevelSources();
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void get_timeline_settings_for_valid_user() throws Exception {
        User user = userService.getUserByUsername(VALID_USERNAME);

        mockMvc.perform(get(URL + "/settings"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 5)));

        verify(sourceService).getAllTopLevelSources();
        verify(sourceService).getAllSubscribedSourcesByUser(user);
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void subscribe_user_to_valid_source() throws Exception {
        User user = userService.getUserByUsername(VALID_USERNAME);
        Source source = sourceService.getSourceById(SOURCE_ID);

        mockMvc.perform(put(URL + "/" + SOURCE_ID + "/SUBSCRIBED"))
            .andExpect(status().isOk());
        verify(userSourceService)
            .createOrUpdateUserSource(source, user, SubscriptionTypes.SUBSCRIBED);

        mockMvc.perform(put(URL + "/" + SOURCE_ID + "/UNSUBSCRIBED"))
            .andExpect(status().isOk());
        verify(userSourceService)
            .createOrUpdateUserSource(source, user, SubscriptionTypes.UNSUBSCRIBED);
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_subscribe_user_to_source_for_invalid_source() throws Exception {
        mockMvc.perform(put(URL + "/" + INVALID_SOURCE_ID + "/SUBSCRIBED"))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No Source found for ID: " + INVALID_SOURCE_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .createOrUpdateUserSource(
                any(Source.class), any(User.class), any(SubscriptionTypes.class));

        verify(userService).getUserByUsername(VALID_USERNAME);
        verify(sourceService).getSourceById(INVALID_SOURCE_ID);
    }

    @WithMockUser(INVALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_subscribe_user_to_source_for_invalid_user() throws Exception {
        mockMvc.perform(put(URL + "/" + SOURCE_ID + "/SUBSCRIBED"))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(sourceService, never())
            .getSourceById(SOURCE_ID);
        verify(userSourceService, never())
            .createOrUpdateUserSource(
                any(Source.class), any(User.class), any(SubscriptionTypes.class));

        verify(userService).getUserByUsername(INVALID_USERNAME);
    }

    @WithMockUser(INVALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_subscribe_user_to_source_for_invalid_source_and_user() throws Exception {
        mockMvc.perform(put(URL + "/" + INVALID_SOURCE_ID + "/SUBSCRIBED"))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(sourceService, never())
            .getSourceById(INVALID_SOURCE_ID);
        verify(userSourceService, never())
            .createOrUpdateUserSource(
                any(Source.class), any(User.class), any(SubscriptionTypes.class));

        verify(userService).getUserByUsername(INVALID_USERNAME);
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void unsubscribe_user_from_valid_source() throws Exception {
        User user = userService.getUserByUsername(VALID_USERNAME);
        Source source = sourceService.getSourceById(SOURCE_ID);

        mockMvc.perform(delete(URL + "/" + SOURCE_ID))
            .andExpect(status().isNotFound());
        verify(userSourceService)
            .deleteUserSource(any(Source.class), any(User.class));

        mockMvc.perform(put(URL + "/" + SOURCE_ID + "/SUBSCRIBED"))
            .andExpect(status().isOk());
        verify(userSourceService)
            .createOrUpdateUserSource(source, user, SubscriptionTypes.SUBSCRIBED);

        mockMvc.perform(delete(URL + "/" + SOURCE_ID))
            .andExpect(status().isOk());
        verify(userSourceService, times(2))
            .deleteUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_unsubscribe_user_from_invalid_source() throws Exception {
        mockMvc.perform(delete(URL + "/" + INVALID_SOURCE_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No Source found for ID: " + INVALID_SOURCE_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .deleteUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_unsubscribe_invalid_user() throws Exception {
        mockMvc.perform(delete(URL + "/" + SOURCE_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .deleteUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_unsubscribe_invalid_user_from_invalid_source() throws Exception {
        mockMvc.perform(delete(URL + "/" + INVALID_SOURCE_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No Source found for ID: " + INVALID_SOURCE_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .deleteUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void toggle_user_subscription_for_valid_source_and_user() throws Exception {
        mockMvc.perform(put(URL + "/" + SOURCE_ID))
            .andExpect(status().isOk());

        verify(userSourceService)
            .createOrToggleUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_toggle_subscription_for_invalid_source() throws Exception {
        mockMvc.perform(put(URL + "/" + INVALID_SOURCE_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No Source found for ID: " + INVALID_SOURCE_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .createOrToggleUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_toggle_subscription_for_invalid_user() throws Exception {
        mockMvc.perform(put(URL + "/" + SOURCE_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .createOrToggleUserSource(any(Source.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_toggle_subscription_for_invalid_source_and_user() throws Exception {
        mockMvc.perform(put(URL + "/" + INVALID_SOURCE_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userSourceService, never())
            .createOrToggleUserSource(any(Source.class), any(User.class));
    }
}
