CREATE TABLE cafeteria_hours (
    cafeteria     VARCHAR(255) NOT NULL,
    day_of_week   INTEGER NOT NULL,
    open          TIME NOT NULL,
    close         TIME NOT NULL,
    PRIMARY KEY (cafeteria, day_of_week)
);

CREATE TABLE cafeteria_closures (
    cafeteria_closure_id  BIGINT GENERATED BY DEFAULT AS IDENTITY,
    cafeteria             VARCHAR(255) NOT NULL,
    closure_from          DATE         NOT NULL,
    closure_to            DATE         NOT NULL,
    PRIMARY KEY (cafeteria_closure_id)
);
