/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.common.events;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SendMailUtils {

    public static void sendMail(SimpleMailMessage emailTemplate,
                                JavaMailSender mailSender,
                                String email,
                                String subject,
                                String text) {

        var mail = new SimpleMailMessage(emailTemplate);
        mail.setTo(email);
        mail.setSubject(subject);
        mail.setText(text);
        mailSender.send(mail);
    }

    public static String getAppUrl() {
        final String host = ServletUriComponentsBuilder.fromCurrentContextPath()
            .build()
            .getHost();

        if ("localhost".equalsIgnoreCase(host) || "127.0.0.1".equals(host))
            return ServletUriComponentsBuilder.fromCurrentContextPath()
                .build()
                .toUriString();

        return ServletUriComponentsBuilder.fromCurrentContextPath()
            .scheme("https")
            .build()
            .toUriString();
    }
}
