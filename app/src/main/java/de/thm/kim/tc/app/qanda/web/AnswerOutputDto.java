/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.web;

import de.thm.kim.tc.app.qanda.data.entity.Answer;
import lombok.Value;

import java.util.Date;

@Value
class AnswerOutputDto {

    Long answerId;

    Long questionId;

    AuthorDto author;

    String text;

    Date createdAt;

    Date modifiedAt;

    int likes;

    boolean isLiked;

    boolean canEdit;

    boolean isHelpful;

    boolean isReported;

    /**
     * Constructor for newly created {@link Answer} objects with no likes
     *
     * <p>N.b.: {@code canEdit} is set to {@code true}. Only use this
     * constructor in POST requests that create new answers.
     *
     * @param answer {@link Answer} in question
     * @param author answer author and {@link de.thm.kim.tc.app.qanda.data.entity.Flair}s
     */
    AnswerOutputDto(Answer answer, AuthorDto author) {
        this(answer, author, 0, false, true, false, false);
    }

    /**
     * Constructor for individual {@link Answer} objects with likes
     *
     * @param answer    {@link Answer} in question
     * @param author    answer author and {@link de.thm.kim.tc.app.qanda.data.entity.Flair}s
     * @param likes     number of likes
     * @param isLiked   {@code true} if the current user has liked the {@link Answer}
     * @param canEdit   {@code true} if the current user is allowed to edit the {@link Answer}
     * @param isHelpful {@code true} if the {@link Answer} is the helpful {@link Answer} for the {@link de.thm.kim.tc.app.qanda.data.entity.Question}
     */
    AnswerOutputDto(Answer answer, AuthorDto author, int likes, boolean isLiked, boolean canEdit, boolean isHelpful, boolean isReported) {
        this.answerId = answer.getId();
        this.questionId = answer.getQuestion().getId();
        this.author = author;
        this.text = answer.getText();
        this.createdAt = answer.getDate();
        this.modifiedAt = answer.getModifiedAt();
        this.likes = likes;
        this.isLiked = isLiked;
        this.canEdit = canEdit;
        this.isHelpful = isHelpful;
        this.isReported = isReported;
    }
}
