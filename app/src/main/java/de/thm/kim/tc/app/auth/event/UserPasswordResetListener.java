/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.event;

import de.thm.kim.tc.app.auth.service.AuthUserPasswordResetService;
import de.thm.kim.tc.app.common.events.SendMailUtils;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Component
public class UserPasswordResetListener implements ApplicationListener<OnUserPasswordResetEvent> {

    private final SimpleMailMessage emailTemplate;
    private final JavaMailSender mailSender;
    private final AuthUserPasswordResetService service;

    @Autowired
    public UserPasswordResetListener(final SimpleMailMessage emailTemplate,
                                     final JavaMailSender mailSender,
                                     final AuthUserPasswordResetService service) {
        this.emailTemplate = emailTemplate;
        this.mailSender = mailSender;
        this.service = service;
    }

    @Override
    public void onApplicationEvent(OnUserPasswordResetEvent event) {
        String displayName = event.getDisplayName();
        String email = event.getEmail();
        String token = service.createPasswordResetToken(email);
        String urlEncodedEmail = URLEncoder.encode(email, StandardCharsets.UTF_8);
        String link = SendMailUtils.getAppUrl() + "/reset/index.html?email=" + urlEncodedEmail + "&token=" + token;

        val subject = "tinyCampus - Zurücksetzen des Passworts";
        val text = "Jemand hat das Zurücksetzen des Passworts für folgendes Benutzerkonto angefordert:\n" +
            "\n" +
            "Name:   " + displayName + "\n" +
            "E-Mail: " + email + "\n" +
            "\n" +
            "Falls das nicht beabsichtigt war, ignoriere diese E-Mail einfach. Es wird dann nichts passieren.\n" +
            "Um dein Passwort zurückzusetzen, besuche folgende Adresse:\n" +
            "\n" +
            link + "\n" +
            "\n" +
            "Falls du Fragen hast, schreib uns doch einfach unter info@tinycampus.de.\n" +
            "\n" +
            "Viele Grüße\n" +
            "das tinyCampus Team"; // TODO English text

        SendMailUtils.sendMail(emailTemplate, mailSender, email, subject, text);
    }
}
