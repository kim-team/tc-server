/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;

import java.util.Date;
import java.util.Optional;

@NoArgsConstructor
@Data
public class TimelineEntryDto {

    private Long id;
    private String author;
    private String parentSource;
    private String avatar;
    private String text;
    private String route;
    private String routeArgs;
    private Date date;
    private boolean faved;
    private boolean pinned;
    private int likes;
    private boolean liked;
    private HeartDto heart;
    private boolean isReported;

    public TimelineEntryDto(TimelineEntry timelineEntry) {
        this(timelineEntry, false, false, 0, false, false);
    }

    TimelineEntryDto(TimelineEntry timelineEntry, boolean faved, boolean pinned, int likes, boolean liked, boolean isReported) {
        val source = timelineEntry.getSource();

        id = timelineEntry.getId();
        author = source.getName();
        parentSource = (source.getParent() != null
            ? source.getParent().getName()
            : source.getName());
        avatar = source.getLogo();
        text = timelineEntry.getText();
        route = source.getContentRoot();
        routeArgs = timelineEntry.getContentArgs();
        date = Optional.ofNullable(timelineEntry.getModified()).orElse(timelineEntry.getCreated());
        this.faved = faved;
        this.pinned = pinned;
        this.likes = likes;
        this.liked = liked;
        this.heart = new HeartDto(likes, liked);
        this.isReported = isReported;
    }
}
