/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpCompletion;
import de.thm.kim.tc.app.praxis.service.PpCompletionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Everything about completions", tags = {"Praxis - Admin - Completions"})
@RestController
@RequestMapping(path = "/praxis/completions")
public class CompletionController {

    private final PpCompletionService ppCompletionService;

    @Autowired
    public CompletionController(PpCompletionService ppCompletionService) {
        this.ppCompletionService = ppCompletionService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get completed praxis phases", tags = {"Praxis - Admin - Completions"})
    @GetMapping
    public List<PpCompletion> getCompletions() {
        return ppCompletionService.getAllCompletions();
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a completed praxis phase", tags = {"Praxis - Admin - Completions"})
    @PutMapping(path = "/{completionId}")
    public PpCompletion updateCompletions(@PathVariable("completionId") Long completionId,
                                          @RequestBody PpCompletion input) {
        if (!input.getCompletionId().equals(completionId))
            throw new IllegalArgumentException(
                String.format("CompletionId in body (%d) does not match ID in path (%d)",
                    input.getCompletionId(), completionId));

        return ppCompletionService.saveOrUpdateCompletion(input);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a completion", tags = {"Praxis - Admin - Completions"})
    @DeleteMapping(path = "/{completionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompletion(@PathVariable("completionId") Long completionId) {
        ppCompletionService.getCompletionById(completionId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpCompletion for ID: " + completionId));

        ppCompletionService.deleteCompletionById(completionId);
    }
}
