/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpGroup;
import de.thm.kim.tc.app.praxis.data.entity.PpUnit;
import de.thm.kim.tc.app.praxis.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis units", tags = {"Praxis - Admin - Units"})
@RestController
@RequestMapping(path = "/praxis")
class UnitController {

    private final PpUnitService ppUnitService;
    private final PpMediaService ppMediaService;
    private final PpEntryService ppEntryService;
    private final PpQuestionService ppQuestionService;
    private final PpAnswerService ppAnswerService;

    @Autowired
    UnitController(PpUnitService ppUnitService,
                   PpMediaService ppMediaService,
                   PpEntryService ppEntryService,
                   PpQuestionService ppQuestionService,
                   PpAnswerService ppAnswerService) {
        this.ppUnitService = ppUnitService;
        this.ppMediaService = ppMediaService;
        this.ppEntryService = ppEntryService;
        this.ppQuestionService = ppQuestionService;
        this.ppAnswerService = ppAnswerService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all units for group", tags = {"Praxis - Admin - Units"})
    @GetMapping(path = "/groups/{groupId}/units")
    public List<UnitDto> getUnitsForGroup(@PathVariable("groupId") Long groupId) {
        return ppUnitService.getUnitsForGroup(groupId).stream()
            .map(u -> new UnitDto(u,
                MediaController.getMediaDtosForUnit(
                    ppMediaService,
                    u),
                EntryController.getEntryDtosForUnit(
                    ppEntryService,
                    u),
                QuestionController.getQuestionDtosForUnit(
                    ppQuestionService,
                    ppAnswerService,
                    u)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all units", tags = {"Praxis - Admin - Units"})
    @GetMapping(path = "/units")
    public List<UnitDto> getAllUnits() {
        return ppUnitService.getAllUnits().stream()
            .map(u -> new UnitDto(u,
                MediaController.getMediaDtosForUnit(
                    ppMediaService,
                    u),
                EntryController.getEntryDtosForUnit(
                    ppEntryService,
                    u),
                QuestionController.getQuestionDtosForUnit(
                    ppQuestionService,
                    ppAnswerService,
                    u)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new unit", tags = {"Praxis - Admin - Units"})
    @PostMapping(path = "/units")
    @ResponseStatus(HttpStatus.CREATED)
    public UnitDto saveUnit(@RequestBody PpUnit unit) {
        PpUnit u = ppUnitService.createOrUpdateUnit(unit);

        return new UnitDto(u,
            MediaController.getMediaDtosForUnit(
                ppMediaService,
                u),
            EntryController.getEntryDtosForUnit(
                ppEntryService,
                u),
            QuestionController.getQuestionDtosForUnit(
                ppQuestionService,
                ppAnswerService,
                u));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a unit", tags = {"Praxis - Admin - Units"})
    @PutMapping(path = "/units/{unitId}")
    public UnitDto updateUnit(@PathVariable("unitId") Long unitId, @RequestBody PpUnit unit) {
        if (unit.getUnitId() == null || !unit.getUnitId().equals(unitId))
            throw new IllegalArgumentException("Invalid unitId in body");

        ppUnitService.getUnitById(unitId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpUnit for ID: " + unitId));

        PpUnit u = ppUnitService.createOrUpdateUnit(unit);

        return new UnitDto(u,
            MediaController.getMediaDtosForUnit(
                ppMediaService,
                u),
            EntryController.getEntryDtosForUnit(
                ppEntryService,
                u),
            QuestionController.getQuestionDtosForUnit(
                ppQuestionService,
                ppAnswerService,
                u));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a unit", tags = {"Praxis - Admin - Units"})
    @DeleteMapping(path = "/units/{unitId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUnit(@PathVariable("unitId") Long unitId) {
        ppUnitService.getUnitById(unitId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpUnit for ID: " + unitId));

        ppUnitService.deleteUnitById(unitId);
    }

    public static List<UnitDto> getUnitDtosForGroup(PpUnitService unitService,
                                                    PpMediaService mediaService,
                                                    PpEntryService entryService,
                                                    PpQuestionService questionService,
                                                    PpAnswerService answerService,
                                                    PpGroup group) {
        return unitService.getUnitsForGroup(group.getGroupId()).stream()
            .map(u -> new UnitDto(u,
                MediaController.getMediaDtosForUnit(mediaService, u),
                EntryController.getEntryDtosForUnit(entryService, u),
                QuestionController.getQuestionDtosForUnit(questionService, answerService, u)))
            .collect(Collectors.toList());
    }
}
