/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@Table(name = "pp_person")
@Entity
public class PpPerson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long personId;
    private Long phaseId;

    private String fullNameDe;
    private String fullNameEn;
    private String image;
    private String imageAltDe;
    private String imageAltEn;
    private String descriptionDe;
    private String descriptionEn;

    @JsonIgnore
    public Map<String, String> getFullNameMap() {
        Map<String, String> fullName = new HashMap<>();

        if (!this.getFullNameDe().isBlank())
            fullName.put("de", this.getFullNameDe());

        if (!this.getFullNameEn().isBlank())
            fullName.put("en", this.getFullNameEn());

        return fullName;
    }

    @JsonIgnore
    public Map<String, String> getImageAltMap() {
        Map<String, String> imageAlt = new HashMap<>();

        if (!this.getImageAltDe().isBlank())
            imageAlt.put("de", this.getImageAltDe());

        if (!this.getImageAltEn().isBlank())
            imageAlt.put("en", this.getImageAltEn());

        return imageAlt;
    }

    @JsonIgnore
    public Map<String, String> getDescriptionMap() {
        Map<String, String> description = new HashMap<>();

        if (!this.getDescriptionDe().isBlank())
            description.put("de", this.getDescriptionDe());

        if (!this.getDescriptionEn().isBlank())
            description.put("en", this.getDescriptionEn());

        return description;
    }
}
