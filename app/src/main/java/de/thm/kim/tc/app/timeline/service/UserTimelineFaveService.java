/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineFave;
import de.thm.kim.tc.app.timeline.data.repository.UserTimelineFaveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserTimelineFaveService {

    private final UserTimelineFaveRepository userTimelineFaveRepository;
    private final UserTimelineHideService userTimelineHideService;

    @Autowired
    public UserTimelineFaveService(UserTimelineFaveRepository userTimelineFaveRepository,
                                   UserTimelineHideService userTimelineHideService) {
        this.userTimelineFaveRepository = userTimelineFaveRepository;
        this.userTimelineHideService = userTimelineHideService;
    }

    public Collection<TimelineEntry> getTimelineFaveByTimelineEntriesAndUser(Collection<TimelineEntry> timelineEntries, User user) {
        Collection<Long> blacklistTimelineEntries = userTimelineHideService.getAllHiddenTimelineEntriesByUser(user)
            .stream()
            .map(TimelineEntry::getId)
            .collect(Collectors.toSet());

        if (blacklistTimelineEntries.isEmpty())
            return userTimelineFaveRepository.findByTimelineEntryInAndUser(timelineEntries, user)
                .stream()
                .map(UserTimelineFave::getTimelineEntry)
                .collect(Collectors.toSet());
        else
            return userTimelineFaveRepository.findByTimelineEntryInAndUserAndTimelineEntryIdIsNotIn(timelineEntries, user, blacklistTimelineEntries)
                .stream()
                .map(UserTimelineFave::getTimelineEntry)
                .collect(Collectors.toSet());
    }

    private UserTimelineFave getTimelineFaveByTimelineAndUserOrCreate(TimelineEntry timelineEntry, User user) {
        return userTimelineFaveRepository.findByTimelineEntryAndUser(timelineEntry, user)
            .orElse(new UserTimelineFave(timelineEntry, user));
    }

    public Page<TimelineEntry> getFavesForUserAsPage(User user, Pageable pageable) {
        Collection<Long> blacklistTimelineEntries = userTimelineHideService.getAllHiddenTimelineEntriesByUser(user)
            .stream()
            .map(TimelineEntry::getId)
            .collect(Collectors.toSet());

        if (blacklistTimelineEntries.isEmpty())
            return userTimelineFaveRepository.findAllByUser(user, pageable)
                .map(UserTimelineFave::getTimelineEntry);
        else
            return userTimelineFaveRepository.findAllByUserAndTimelineEntryIdIsNotIn(user, blacklistTimelineEntries, pageable)
                .map(UserTimelineFave::getTimelineEntry);
    }

    public void addTimelineFave(TimelineEntry timelineEntry, User user) {
        userTimelineFaveRepository.save(getTimelineFaveByTimelineAndUserOrCreate(timelineEntry, user));
    }

    public void deleteTimelineFaveForUser(TimelineEntry timelineEntry, User user) {
        userTimelineFaveRepository.findByTimelineEntryAndUser(timelineEntry, user)
            .ifPresentOrElse(userTimelineFaveRepository::delete,
                () -> {
                    throw new ResourceNotFoundException("No UserTimelineFave found for TimelineEntry ID: " + timelineEntry.getId() + " and User ID: " + user.getId());
                });
    }

    public void deleteTimelineFave(TimelineEntry timelineEntry) {
        userTimelineFaveRepository.deleteAllByTimelineEntry(timelineEntry);
    }

    @Transactional
    public void deleteAllByUser(User user) {
        userTimelineFaveRepository.deleteAllByUser(user);
    }
}
