ALTER TABLE badges
ADD animation_level VARCHAR(255);

UPDATE badges SET asset = 'badge_qanda_answers.flr', animation_level = 'level1' WHERE badge_id = 1;
UPDATE badges SET asset = 'badge_qanda_answers.flr', animation_level = 'level2' WHERE badge_id = 2;
UPDATE badges SET asset = 'badge_qanda_answers.flr', animation_level = 'level3' WHERE badge_id = 3;
UPDATE badges SET asset = 'badge_qanda_answers.flr', animation_level = 'level1' WHERE badge_id = 4;
UPDATE badges SET asset = 'badge_qanda_answers.flr', animation_level = 'level2' WHERE badge_id = 5;
UPDATE badges SET asset = 'badge_qanda_answers.flr', animation_level = 'level3' WHERE badge_id = 6;
UPDATE badges SET asset = 'badge_heart.flr',         animation_level = 'level1' WHERE badge_id = 7;
UPDATE badges SET asset = 'badge_heart.flr',         animation_level = 'level2' WHERE badge_id = 8;
UPDATE badges SET asset = 'badge_heart.flr',         animation_level = 'level3' WHERE badge_id = 9;
UPDATE badges SET asset = 'badge_example.flr',       animation_level = 'level1' WHERE badge_id = 10;
UPDATE badges SET asset = 'badge_example.flr',       animation_level = 'level2' WHERE badge_id = 11;
UPDATE badges SET asset = 'badge_example.flr',       animation_level = 'level3' WHERE badge_id = 12;
