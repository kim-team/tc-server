/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.thinkbig.data.entity.Comment;
import de.thm.kim.tc.app.thinkbig.data.entity.CommentLike;
import de.thm.kim.tc.app.thinkbig.data.entity.Event;
import de.thm.kim.tc.app.thinkbig.data.entity.EventLike;
import de.thm.kim.tc.app.thinkbig.data.entity.enums.EventTopic;
import de.thm.kim.tc.app.thinkbig.data.entity.enums.EventType;
import de.thm.kim.tc.app.thinkbig.service.CommentService;
import de.thm.kim.tc.app.thinkbig.service.EventService;
import de.thm.kim.tc.app.thinkbig.service.ThinkBigLikeService;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.service.SourceService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/events")
public class EventController {

    private final EventService eventService;
    private final CommentService commentService;
    private final UserService userService;
    private final SourceService sourceService;
    private final ThinkBigLikeService thinkBigLikeService;
    private final TimelineService timelineService;

    @Autowired
    public EventController(EventService eventService, CommentService commentService, UserService userService, SourceService sourceService, ThinkBigLikeService thinkBigLikeService, TimelineService timelineService) {
        this.eventService = eventService;
        this.commentService = commentService;
        this.userService = userService;
        this.sourceService = sourceService;
        this.thinkBigLikeService = thinkBigLikeService;
        this.timelineService = timelineService;
    }

    // Events

    @ApiOperation(value = "Get all events", tags = {"Think Big - Events"})
    @GetMapping
    public List<EventDto> getAllEvents(Principal principal) {
        return convertToDtos(eventService.getAllEvents(), principal);
    }

    @ApiOperation(value = "Get event for ID", tags = {"Think Big - Events"})
    @GetMapping(path = "/{eventId}")
    public EventDto getEventForId(Principal principal,
                                  @PathVariable("eventId") Long eventId) {
        return convertToDto(eventService.getEvenBytId(eventId), principal);
    }

    @ApiOperation(value = "Get all events for topic", tags = {"Think Big - Events"})
    @GetMapping(path = "/topic/{topic}")
    public Collection<EventDto> getEventsForTopic(Principal principal,
                                                  @PathVariable("topic") EventTopic topic) {
        return convertToDtos(eventService.getEventsPerTopic(topic), principal);
    }

    private List<EventDto> convertToDtos(Collection<Event> events, Principal principal) {
        return events
            .stream()
            .map(e -> convertToDto(e, principal))
            .collect(Collectors.toList());
    }

    private EventDto convertToDto(Event event, Principal principal) {
        Collection<EventLike> likes = thinkBigLikeService.getEventLikesForEvent(event);
        int comments = commentService.getNumberOfCommentsForEvent(event);
        float avgRating = eventService.getAverageRatingForEvent(event);
        int ratings = eventService.getNumberOfRatingsForEvent(event);

        return new EventDto(event,
            comments,
            likes.size(),
            ((principal != null) && likes.stream()
                .anyMatch(l -> l.getUser().getUsername().equals(principal.getName()))),
            avgRating,
            ratings);
    }

    @ApiOperation(value = "Get overview over the different tracks", tags = {"Think Big - Events"})
    @GetMapping(path = "/overview")
    public EventOverviewDto getOverview() {
        Long generalWorkshops = eventService.countEventsByTopicAndType(EventTopic.GENERAL, EventType.WORKSHOP);
        Long generalTalks = eventService.countEventsByTopicAndType(EventTopic.GENERAL, EventType.VORTRAG);
        Long programmingWorkshops = eventService.countEventsByTopicAndType(EventTopic.PROGRAMMING, EventType.WORKSHOP);
        Long programmingTalks = eventService.countEventsByTopicAndType(EventTopic.PROGRAMMING, EventType.VORTRAG);
        Long artWorkshops = eventService.countEventsByTopicAndType(EventTopic.ART_DESIGN, EventType.WORKSHOP);
        Long artTalks = eventService.countEventsByTopicAndType(EventTopic.ART_DESIGN, EventType.VORTRAG);
        Long pitchWorkshops = eventService.countEventsByTopicAndType(EventTopic.PITCH_CONCEPT, EventType.WORKSHOP);
        Long pitchTalks = eventService.countEventsByTopicAndType(EventTopic.PITCH_CONCEPT, EventType.VORTRAG);
        Long producingWorkshops = eventService.countEventsByTopicAndType(EventTopic.PRODUCING, EventType.WORKSHOP);
        Long producingTalks = eventService.countEventsByTopicAndType(EventTopic.PRODUCING, EventType.VORTRAG);
        Long miscWorkshops = eventService.countEventsByTopicAndType(EventTopic.MISC, EventType.WORKSHOP);
        Long miscTalks = eventService.countEventsByTopicAndType(EventTopic.MISC, EventType.VORTRAG);

        return EventOverviewDto.builder()
            .general(new EventOverviewDto.EventTrack(generalWorkshops, generalTalks))
            .programming(new EventOverviewDto.EventTrack(programmingWorkshops, programmingTalks))
            .art(new EventOverviewDto.EventTrack(artWorkshops, artTalks))
            .pitch(new EventOverviewDto.EventTrack(pitchWorkshops, pitchTalks))
            .producing(new EventOverviewDto.EventTrack(producingWorkshops, producingTalks))
            .misc(new EventOverviewDto.EventTrack(miscWorkshops, miscTalks))
            .build();
    }


    // Comments

    @ApiOperation(value = "Get all comments for event", tags = {"Think Big - Comments"})
    @GetMapping(path = "/{eventId}/comments")
    public List<CommentOutputDto> showCommentsForEvent(Principal principal,
                                                       @PathVariable("eventId") Long eventId) {
        Event event = eventService.getEvenBytId(eventId);
        Collection<Comment> comments = commentService.getCommentsForEvent(event);

        return comments.stream()
            .map(c -> {
                Collection<CommentLike> likes = thinkBigLikeService.getCommentLikesForComment(c);

                return new CommentOutputDto(c,
                    likes.size(),
                    ((principal != null) && likes.stream()
                        .anyMatch(l -> l.getUser().getUsername().equals(principal.getName())))
                );
            })
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Create a new comment", tags = {"Think Big - Comments"})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/{eventId}/comments")
    public CommentOutputDto createComment(Principal principal,
                                          @Valid @RequestBody CommentInputDto input,
                                          @PathVariable("eventId") Long eventId) {
        Event event = eventService.getEvenBytId(eventId);
        User user = userService.getUserByUsername(principal.getName());

        // Create timeline entry for comment
        // FIXME Individual sources for events
        Source source = sourceService.getSourceByName("timeline.sources.thinkbig_comments");
        String contentArgs = eventId.toString();
        // TODO i18n
        String commentAuthor = user.getName() + " kommentierte: ";
        timelineService.createNewTimelineEntry(source, commentAuthor + input.getText(), contentArgs);

        Comment comment = commentService.createNewComment(event, user, input.getText());

        return new CommentOutputDto(comment, 0, false);
    }


    // Likes - Events

    @Secured("ROLE_USER")
    @ApiOperation(value = "Like an event", tags = {"Think Big - Likes"})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/{eventId}/like")
    public void likeEvent(Principal principal,
                          @PathVariable("eventId") Long eventId) {
        Event event = eventService.getEvenBytId(eventId);
        User user = userService.getUserByUsername(principal.getName());

        thinkBigLikeService.createNewEventLike(event, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unlike an event", tags = {"Think Big - Likes"})
    @PostMapping(path = "/{eventId}/unlike")
    public void unlikeEvent(Principal principal,
                            @PathVariable("eventId") Long eventId) {
        Event event = eventService.getEvenBytId(eventId);
        User user = userService.getUserByUsername(principal.getName());

        thinkBigLikeService.removeEventLike(event, user);
    }


    // Likes - Comments

    @Secured("ROLE_USER")
    @ApiOperation(value = "Like a comment", tags = {"Think Big - Likes"})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/comments/{commentId}/like")
    public void likeComment(Principal principal,
                            @PathVariable("commentId") Long commentId) {
        Comment comment = commentService.getCommentById(commentId);
        User user = userService.getUserByUsername(principal.getName());

        thinkBigLikeService.createNewCommentLike(comment, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unlike a comment", tags = {"Think Big - Likes"})
    @PostMapping(path = "/comments/{commentId}/unlike")
    public void unlikeComment(Principal principal,
                              @PathVariable("commentId") Long commentId) {
        Comment comment = commentService.getCommentById(commentId);
        User user = userService.getUserByUsername(principal.getName());

        thinkBigLikeService.removeCommentLike(comment, user);
    }


    // Feedback

    @ApiOperation(value = "Create new feedback", tags = {"Think Big - Feedback"})
    @PostMapping(path = "/feedback")
    public void postFeedback(@ApiParam("Rating must be between 1 and 5 (inclusive), comment length must be 255 characters or less")
                             @Validated @RequestBody FeedbackDto input) {
        Event event = eventService.getEvenBytId(input.getEventId());
        eventService.createNewFeedback(event, input.getRating(), input.getComment());
    }
}
