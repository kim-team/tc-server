# "[WI]r können Praxis" frontend

## Build instructions

### Local development

1. Install npm dependencies
   ```sh
   $ npm install
   ```
2. Create local `.env.development.local` file based on example
   ```sh
   $ cp .env.development.local.example .env.development.local
   ```
3. Start local backend
   ```sh
   $ ../gradlew bootRun
   ```
4. Acquire OAuth2 access token
   ```sh
   $ curl --location --request POST 'localhost:8080/oauth/token' \
          --header 'Content-Type: application/x-www-form-urlencoded' \
          --header 'Authorization: Basic Y2xpZW50OnNlY3JldA==' \
          --data-urlencode 'grant_type=password' \
          --data-urlencode 'username=admin' \
          --data-urlencode 'password=admin'
   ```
5. Add `access_token` to `VUE_APP_TOKEN` variable in `.env.development.local`
6. Start local dev server
   ```sh
   $ npm run serve
   ```
7. (Optional) Run linter
   ```sh
   $ npm run lint
   ```

### Production

1. Install dependencies
   ```sh
   $ npm install
   ```
2. Run production build   
   ```sh
   $ npm run build
   ```
3. Create directory for static files and copy files from `dist`
   ```sh
   $ mkdir -p ../app/src/main/resources/static/dashboard/praxis
   $ cp -r dist/* ../app/src/main/resources/static/dashboard/praxis
   ```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
