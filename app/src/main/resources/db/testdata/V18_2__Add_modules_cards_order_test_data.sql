INSERT INTO user_modules (user_id, module_id, is_visible, priority) VALUES
(1, 'semesterfee', true,  10),
(1, 'organizer',   false, 100),
(1, 'qanda',       false, 50),
(1, 'feedback',    true,  0);
