/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.entity.AuthUserRecoveryCode;
import de.thm.kim.tc.app.auth.data.entity.Permission;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRecoveryCodeRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.data.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RegistrationService {

    private final AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository;
    private final AuthUserRepository authUserRepository;
    private final PermissionRepository permissionRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    @Autowired
    public RegistrationService(AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository,
                               AuthUserRepository authUserRepository,
                               PermissionRepository permissionRepository,
                               PasswordEncoder passwordEncoder,
                               UserService userService) {
        this.authUserRecoveryCodeRepository = authUserRecoveryCodeRepository;
        this.authUserRepository = authUserRepository;
        this.permissionRepository = permissionRepository;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    public Optional<User> registerNewUser(String username, String displayName, String hashedRecoveryCode) {
        if (authUserRepository.findByUsername(username).isPresent())
            return Optional.empty();

        AuthUser user = authUserRepository.save(new AuthUser(username, hashedRecoveryCode));

        permissionRepository.save(new Permission(user.getUsername(), "USER"));
        User newUser = userService.createNewUser(user.getUsername(), displayName);

        authUserRecoveryCodeRepository.save(new AuthUserRecoveryCode(user.getUsername(), hashedRecoveryCode));

        return Optional.of(newUser);
    }

    public Optional<AuthUser> registerNewAdminUser(String username, String displayName, String email, String password) {
        if (authUserRepository.findByUsername(username).isPresent())
            return Optional.empty();

        String hashedPassword = passwordEncoder.encode(password);
        AuthUser user = authUserRepository.save(new AuthUser(username, email, hashedPassword));

        permissionRepository.save(new Permission(user.getUsername(), "USER"));
        permissionRepository.save(new Permission(user.getUsername(), "VERIFIED"));
        permissionRepository.save(new Permission(user.getUsername(), "ADMIN"));
        userService.createNewUser(user.getUsername(), displayName);

        return Optional.of(user);
    }
}
