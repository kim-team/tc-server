/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.web;

import de.thm.kim.tc.app.cafeteria.data.entity.Cafeteria;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaHoursService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Api(description = "Everything about cafeteria hours", tags = {"Cafeteria - Hours"})
@RestController
@RequestMapping("/cafeteria/hours")
public class CafeteriaHoursController {

    private final CafeteriaService cafeteriaService;
    private final CafeteriaHoursService cafeteriaHoursService;

    @Autowired
    public CafeteriaHoursController(CafeteriaService cafeteriaService, CafeteriaHoursService cafeteriaHoursService) {
        this.cafeteriaService = cafeteriaService;
        this.cafeteriaHoursService = cafeteriaHoursService;
    }

    @GetMapping
    public Iterable<CafeteriaHoursOutputDto> getAllHours() {
        return convertToDomain(cafeteriaService.getAllCafeterias())
            .stream()
            .sorted(Comparator.comparing(CafeteriaHoursOutputDto::getCafeteriaId))
            .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @GetMapping("/{cafeteriaId}")
    public CafeteriaHoursOutputDto getHours(@PathVariable Long cafeteriaId) {
        return convertToDomain(cafeteriaService.getAllCafeterias(cafeteriaId));
    }

    private Set<CafeteriaHoursOutputDto> convertToDomain(Iterable<Cafeteria> cafeterias) {
        Set<CafeteriaHoursOutputDto> cafeteriaHours = new HashSet<>();
        for (Cafeteria cafeteria : cafeterias) {
            cafeteriaHours.add(convertToDomain(cafeteria));
        }
        return cafeteriaHours;
    }

    private CafeteriaHoursOutputDto convertToDomain(Cafeteria cafeteria) {
        CafeteriaHoursOutputDto cafeteriaHoursOutputDto = new CafeteriaHoursOutputDto(cafeteria);
        cafeteriaHoursService.getCafeteriaOpeningHours(cafeteria.getId())
            .forEach(e -> cafeteriaHoursOutputDto.addOpeningHours(e.getDayOfWeek(), e.getOpen(), e.getClose()));
        cafeteriaHoursService.getCafeteriaEatingHours(cafeteria.getId())
            .forEach(e -> cafeteriaHoursOutputDto.addEatingHours(e.getDayOfWeek(), e.getOpen(), e.getClose()));
        cafeteriaHoursService.getAllCafeteriaClosures(cafeteria.getId())
            .forEach(c -> cafeteriaHoursOutputDto.addClosure(c.getFrom(), c.getTo()));
        return cafeteriaHoursOutputDto;
    }
}
