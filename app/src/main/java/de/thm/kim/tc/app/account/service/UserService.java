/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.service;

import de.thm.kim.tc.app.account.data.entity.Campus;
import de.thm.kim.tc.app.account.data.entity.Program;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.CampusRepository;
import de.thm.kim.tc.app.account.data.repository.ProgramRepository;
import de.thm.kim.tc.app.account.data.repository.UserRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.Optional;

@Service
public class UserService {

    private final AuthUserRepository authUserRepository;
    private final CampusRepository campusRepository;
    private final UserRepository userRepository;
    private final ProgramRepository programRepository;

    @Autowired
    public UserService(AuthUserRepository authUserRepository,
                       CampusRepository campusRepository,
                       UserRepository userRepository,
                       ProgramRepository programRepository) {
        this.authUserRepository = authUserRepository;
        this.campusRepository = campusRepository;
        this.userRepository = userRepository;
        this.programRepository = programRepository;
    }

    /**
     * Try to find an User, if there is no User, they will throw an IllegalArgumentException.
     *
     * @param id user id
     * @return existing User
     * @throws IllegalArgumentException if User doesn't exists
     */
    public User getUserById(Long id) {
        return userRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No User found for ID: " + id));
    }

    public Optional<User> getUserByPrincipal(Principal principal) {
        if (principal == null)
            return Optional.empty();

        return userRepository.findByUsername(principal.getName());
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
            .orElseThrow(() -> new ResourceNotFoundException("No User found for username: " + username));
    }

    public User createNewUser(String username, String name) {
        long defaultProgramId = 69;
        long defaultCampusId = 2;

        Program program = programRepository.findById(defaultProgramId)
            .orElseThrow(() -> new ResourceNotFoundException("No Program found for Program ID: " + defaultProgramId));
        Campus campus = campusRepository.findById(defaultCampusId)
            .orElseThrow(() -> new ResourceNotFoundException("No Campus found for Campus ID: " + defaultCampusId));

        return userRepository.save(new User(username, name, program, campus));
    }

    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @Transactional
    public void deleteUserByUsername(String username) {
        userRepository.deleteByUsername(username);
    }

    public void changeDisplayName(String username, String newDisplayName) {
        // TODO: Change the username
    }
}
