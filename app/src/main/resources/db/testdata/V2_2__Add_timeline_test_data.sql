-- Timeline

INSERT INTO users_sources (user_id, source_id, subscription) VALUES
(1, 4, 'SUBSCRIBED'),
(1, 3, 'SUBSCRIBED');

INSERT INTO timeline_entries (source_id, text, content_args) VALUES
(4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', ''),
(1, 'Aenean commodo ligula eget dolor', ''),
(4, 'Aenean massa', ''),
(1, 'Cum sociis natoque penatibus et magnis dis parturient montes, nasce', ''),
(4, 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem', ''),
(3, 'Nulla consequat massa quis enim', ''),
(4, 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu' , ''),
(2, 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo', ''),
(5, 'Nullam dictum felis eu pede mollis pretium', '1'),
(3, 'At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles.', ''),
(4, 'Cras dapibus', ''),
(2, 'Vivamus elementum semper nisi', ''),
(3, 'Aenean vulputate eleifend tellus', ''),
(1, 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim', ''),
(2, 'Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus', ''),
(2, 'Phasellus viverra nulla ut metus varius laoreet', ''),
(2, 'Quisque rutrum', ''),
(2, 'Aenean imperdiet', ''),
(4, 'Etiam ultricies nisi vel augue', ''),
(5, 'Curabitur ullamcorper ultricies nisi', '1'),
(3, 'Nam eget dui', ''),
(2, 'Etiam rhoncus', ''),
(1, 'Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper', ''),
(3, 'Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem', ''),
(4, 'Maecenas nec odio et ante tincidunt tempus', ''),
(2, 'Donec vitae sapien ut libero venenatis faucibus', ''),
(5, 'Nullam quis ante', '1'),
(4, 'Etiam sit amet orci eget eros faucibus tincidunt', ''),
(2, 'Duis leo', ''),
(3, 'Sed fringilla mauris sit amet nibh', ''),
(1, 'Donec sodales sagittis magna', ''),
(1, 'Sed consequat, leo eget bibendum sodales, augue velit cursus nunc', ''),
(3, 'Li Europan lingues es membres del sam familie', ''),
(4, 'Lor separat existentie es un myth', ''),
(3, 'Por scientie, musica, sport etc, litot Europa usa li sam vocabular', ''),
(2, 'Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules', ''),
(4, 'Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar cust', ''),
(3, 'At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles', ''),
(3, 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regular', ''),
(3, 'Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues', ''),
(3, 'It va esser tam simplic quam Occidental in fact, it va esser Occidental', ''),
(3, 'A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit m', ''),
(4, 'Lor separat existentie es un myth', ''),
(2, 'Por scientie, musica, sport etc, litot Europa usa li sam vocabular', ''),
(2, 'Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules', ''),
(1, 'Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar cust', ''),
(5, 'At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles.', '1'),
(4, 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu', ''),
(3, 'Aenean commodo ligula eget dolor', ''),
(2, 'Aenean massa', ''),
(1, 'Cum sociis natoque penatibus et magnis dis parturient montes, nasce', ''),
(1, 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem', ''),
(2, 'Nulla consequat massa quis enim', ''),
(1, 'Lorem ipsum Testfrage', ''),
(5, 'Donec pede Testantwort 1', '1'),
(5, 'Aenean leo Testantwort 2', '1'),
(5, 'Aliquam lorem ante Testantwort 3', '1');

INSERT INTO timeline_pins (timeline_entry_id) VALUES
(1),
(4),
(5),
(35),
(52);

INSERT INTO users_timeline_entries_faves (user_id, timeline_entry_id) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 8),
(1, 9);

INSERT INTO users_timeline_entries_hides (user_id, timeline_entry_id) VALUES (1, 6);

INSERT INTO users_timeline_entries_unpins (user_id, timeline_entry_id) VALUES (1, 4);
