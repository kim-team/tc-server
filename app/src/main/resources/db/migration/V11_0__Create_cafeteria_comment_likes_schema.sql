CREATE TABLE cafeteria_comment_likes (
    cafeteria_comment_id    BIGINT    NOT NULL,
    user_id                 BIGINT    NOT NULL,
    date                    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (cafeteria_comment_id) REFERENCES cafeteria_comment (cafeteria_comment_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (cafeteria_comment_id, user_id)
);
