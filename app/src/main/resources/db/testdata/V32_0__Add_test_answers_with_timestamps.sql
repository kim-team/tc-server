INSERT INTO answers (answer_id, question_id, user_id, text, date) VALUES
(8,  1, 1, 'sed eiusmod tempor incidunt ut labore Testantwort 3', '2020-10-17 13:36:07'),
(9,  3, 1, 'Ut enim ad minim veniam Testantwort 1',               '2020-10-14 00:36:07'),
(10, 3, 1, 'quis nostrud exercitation ullamco Testantwort 2',     '2020-10-14 13:36:07'),
(11, 2, 1, 'Quis aute iure reprehenderit Tetsantwort 5',          '2020-10-19 14:12:06');

INSERT INTO answers (answer_id, question_id, user_id, text) VALUES
(12, 2, 1, 'esse cillum dolore eu fugiat nulla pariatur Testantwort 6');


UPDATE answers
SET text = 'Donec pede Testantwort 4',
    date = '2020-10-17 13:36:07'
WHERE answer_id = 1;
