/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.event;

import de.thm.kim.tc.app.auth.service.AuthMetricsService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationFailureDisabledEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

@Log
@Component
class AuthenticationEventListener {

    private final AuthMetricsService service;

    @Autowired
    public AuthenticationEventListener(AuthMetricsService service) {
        this.service = service;
    }

    @EventListener
    public void authenticationSuccessEventListener(AuthenticationSuccessEvent event) {
        if (event.getSource() instanceof UsernamePasswordAuthenticationToken) {
            log.info("Successful login (Fetch Token): " + service.countSuccessFetch());
        } else if (event.getSource() instanceof OAuth2Authentication) {
            log.info("Successful login (Use Token): " + service.countSuccessUse());
        }

        String visitor = event.getAuthentication().getName();
        service.countUserLogin(visitor);
    }

    @EventListener
    public void authenticationFailureDisabledEventListener(AuthenticationFailureDisabledEvent event) {
        log.info("Failed login (Disabled): " + service.countFailedDisabled());
    }

    @EventListener
    public void authenticationFailureBadCredentialsEventListener(AuthenticationFailureBadCredentialsEvent event) {
        if (event.getSource() instanceof UsernamePasswordAuthenticationToken) {
            log.info("Failed login (Bad Credentials): " + service.countFailedBadCredentials());
        } else if (event.getSource() instanceof PreAuthenticatedAuthenticationToken) {
            log.info("Failed login (Bad Token): " + service.countFailedBadToken());
        }
    }
}
