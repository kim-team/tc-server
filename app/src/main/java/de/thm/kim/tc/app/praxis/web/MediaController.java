/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpMedia;
import de.thm.kim.tc.app.praxis.data.entity.PpUnit;
import de.thm.kim.tc.app.praxis.service.PpMediaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about praxis media", tags = {"Praxis - Admin - Media"})
@RestController
@RequestMapping(path = "/praxis")
class MediaController {

    private final PpMediaService ppMediaService;

    @Autowired
    MediaController(PpMediaService ppMediaService) {
        this.ppMediaService = ppMediaService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all media for unit", tags = {"Praxis - Admin - Media"})
    @GetMapping(path = "/units/{unitId}/media")
    public List<MediaDto> getMediaForUnit(@PathVariable("unitId") Long unitId) {
        return ppMediaService.getMediaForUnit(unitId).stream()
            .map(MediaDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all media", tags = {"Praxis - Admin - Media"})
    @GetMapping(path = "/media")
    public List<MediaDto> getAllMedia() {
        return ppMediaService.getAllMedia().stream()
            .map(MediaDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new media", tags = {"Praxis - Admin - Media"})
    @PostMapping(path = "/media")
    @ResponseStatus(HttpStatus.CREATED)
    public MediaDto saveMedia(@RequestBody PpMedia media) {
        PpMedia m = ppMediaService.createOrUpdateMedia(media);

        return new MediaDto(m);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update a media", tags = {"Praxis - Admin - Media"})
    @PutMapping(path = "/media/{mediaId}")
    public MediaDto updateMedia(@PathVariable("mediaId") Long mediaId, @RequestBody PpMedia media) {
        if (media.getMediaId() == null || !media.getMediaId().equals(mediaId))
            throw new IllegalArgumentException("Invalid mediaId in body");

        ppMediaService.getMediaById(mediaId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpMedia for ID: " + mediaId));

        PpMedia m = ppMediaService.createOrUpdateMedia(media);

        return new MediaDto(m);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete a media", tags = {"Praxis - Admin - Media"})
    @DeleteMapping(path = "/media/{mediaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMedia(@PathVariable("mediaId") Long mediaId) {
        ppMediaService.getMediaById(mediaId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpMedia for ID: " + mediaId));

        ppMediaService.deleteMediaById(mediaId);
    }

    public static List<MediaDto> getMediaDtosForUnit(PpMediaService service, PpUnit unit) {
        return service.getMediaForUnit(unit.getUnitId()).stream()
            .map(MediaDto::new)
            .collect(Collectors.toList());
    }
}
