/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.data.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "departments")
public class Department {

    /**
     * Organizer-ID ("departmentID")
     **/
    @Id
    @Column(name = "department_id")
    private Long id;

    /**
     * Official number (e.g. "06" for FB 06, MNI)
     **/
    private String number;

    /**
     * Official shorthand (e.g. "MNI")
     */
    private String name;

    /**
     * Full official name (e.g. "Mathematik, Naturwissenschaften und Informatik")
     */
    private String longName;
}
