/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.dashboard;

import de.thm.kim.tc.app.auth.data.entity.AuthMetrics;
import de.thm.kim.tc.app.auth.service.AuthMetricsService;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.service.AnswerService;
import de.thm.kim.tc.app.qanda.service.HelpfulAnswerService;
import de.thm.kim.tc.app.qanda.service.QuestionService;
import de.thm.kim.tc.app.thinkbig.data.entity.Comment;
import de.thm.kim.tc.app.thinkbig.service.CommentService;
import de.thm.kim.tc.app.thinkbig.service.EventService;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.SourceService;
import de.thm.kim.tc.app.timeline.service.TimelinePinService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import lombok.extern.java.Log;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log
@Controller
public class TimelineDashboardController implements WebMvcConfigurer {

    private final HelpfulAnswerService helpfulAnswerService;
    private final SourceService sourceService;
    private final TimelineService timelineService;
    private final TimelinePinService pinService;
    private final EventService eventService;
    private final CommentService commentService;
    private final AnswerService answerService;
    private final QuestionService questionService;
    private final AuthMetricsService authMetricsService;

    @Autowired
    public TimelineDashboardController(HelpfulAnswerService helpfulAnswerService,
                                       SourceService sourceService,
                                       TimelineService timelineService,
                                       TimelinePinService pinService,
                                       EventService eventService,
                                       CommentService commentService,
                                       AnswerService answerService,
                                       QuestionService questionService,
                                       AuthMetricsService authMetricsService) {
        this.helpfulAnswerService = helpfulAnswerService;
        this.sourceService = sourceService;
        this.timelineService = timelineService;
        this.pinService = pinService;
        this.eventService = eventService;
        this.commentService = commentService;
        this.answerService = answerService;
        this.questionService = questionService;
        this.authMetricsService = authMetricsService;
    }


    @GetMapping("/dashboard")
    public String dashboard(Model model) {
        List<TimelineEntry> entries = timelineService.getAllTimelineEntriesAsList();

        model.addAttribute("title", "Timeline");
        model.addAttribute("entries", entries);
        model.addAttribute("classActiveDashboard", "active");
        return "dashboard";
    }

    @PostMapping("/dashboard")
    public String deleteTimelineEntry(Long id) {
        val entry = timelineService.getTimelineEntryById(id);

        log.info(entry.toString());
        timelineService.deleteTimelineEntry(entry);

        return "redirect:/dashboard";
    }


    @GetMapping("/form")
    public String showForm(Model model, TimelineEntryForm timelineEntryForm) {
        model.addAttribute("title", "New Timeline Entry");
        model.addAttribute("sources", buildSourceDropDown());
        model.addAttribute("classActiveForm", "active");
        return "form";
    }

    @PostMapping("/form")
    public String createTimelineEntry(@Valid TimelineEntryForm timelineEntryForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("title", "New Timeline Entry");
            model.addAttribute("sources", buildSourceDropDown());
            model.addAttribute("classActiveForm", "active");
            return "form";
        }

        Source source = sourceService.getSourceById(timelineEntryForm.getSourceId());
        String contentArgs = ""; // TODO contentArgs
        val entry = timelineService.createNewTimelineEntry(source, timelineEntryForm.getText(), contentArgs);
        log.info(entry.toString());

        if (timelineEntryForm.getIsPinned())
            log.info(pinService.pinTimelineEntry(entry).toString());

        return "redirect:/dashboard";
    }

    @GetMapping("/tb-events")
    public String showEvents(Model model) {
        model.addAttribute("title", "Events");
        model.addAttribute("events", eventService.getAllEvents());
        model.addAttribute("classActiveEvents", "active");
        return "tb-events";
    }

    @GetMapping("/event-feedback")
    public String showFeedback(Model model) {
        model.addAttribute("title", "Feedback");
        model.addAttribute("feedback", eventService.getFeedback());
        model.addAttribute("classActiveFeedback", "active");
        return "feedback";
    }

    @GetMapping("/comments")
    public String showComments(Model model) {
        List<Comment> comments = commentService.getAllComments();

        model.addAttribute("title", "Comments");
        model.addAttribute("comments", comments);
        model.addAttribute("classActiveComments", "active");
        return "comments";
    }

    @PostMapping("/comments")
    public String deleteComment(Long id) {
        val comment = commentService.getCommentById(id);

        log.info(comment.toString());
        commentService.deleteComment(comment);

        return "redirect:/comments";
    }

    @GetMapping("/qanda")
    public String showQuestionsAndAnswers(Model model) {
        List<Question> questions = questionService.getAllQuestions();
        List<Answer> answers = answerService.getAllAnswers();

        model.addAttribute("title", "Questions");
        model.addAttribute("questions", questions);
        model.addAttribute("answers", answers);
        model.addAttribute("classActiveQuestions", "active");
        return "qanda";
    }

    @PostMapping("/qanda/questions")
    public String deleteQuestion(Long id) {
        val question = questionService.getQuestionById(id);

        // log.info(question.toString());
        helpfulAnswerService.removeHelpfulAnswerForQuestion(question);
        question.getAnswers().forEach(answerService::deleteAnswer);
        questionService.deleteQuestion(question);

        return "redirect:/qanda";
    }

    @PostMapping("/qanda/answers")
    public String deleteAnswer(Long id) {
        val answer = answerService.getAnswerById(id);

        // log.info(answer.toString());
        helpfulAnswerService.removeHelpfulAnswerForAnswer(answer);
        answerService.deleteAnswer(answer);

        return "redirect:/qanda";
    }

    @GetMapping("/dashboard/metrics")
    public String showMetrics(Model model) {
        List<AuthMetrics> metrics = authMetricsService.getAllAuthMetrics();

        model.addAttribute("title", "Metrics");
        model.addAttribute("metrics", metrics);
        model.addAttribute("classActiveMetrics", "active");
        return "metrics";
    }


    /**
     * Build map of Source IDs and names including names of parent Sources
     * <p>
     * e.g. 1 -> "Parent > Child"
     */
    private Map<Long, String> buildSourceDropDown() {
        return sourceService.getAllSources()
            .stream()
            .filter(s -> s.getParent() != null)
            .collect(Collectors.toMap(
                Source::getId,
                v -> {
                    StringBuilder tmp = new StringBuilder(v.getDefaultName());
                    v.getAllParents().forEach(p -> tmp.insert(0, p.getDefaultName() + " > "));
                    return tmp.toString();
                }));
    }
}
