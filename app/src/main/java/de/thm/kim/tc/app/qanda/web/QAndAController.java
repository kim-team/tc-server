/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaLikeService;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.qanda.data.entity.*;
import de.thm.kim.tc.app.qanda.service.*;
import de.thm.kim.tc.app.thinkbig.service.ThinkBigLikeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/questions")
public class QAndAController {

    private final QuestionService questionService;
    private final AnswerService answerService;
    private final CafeteriaLikeService cafeteriaLikeService;
    private final LikeService likeService;
    private final HelpfulAnswerService helpfulAnswerService;
    private final UserService userService;
    private final QuestionCategoryService questionCategoryService;
    private final BadgeService badgeService;
    private final ViewService viewService;
    private final ThinkBigLikeService thinkBigLikeService;
    private final ReportService<Answer> answerReportService;
    private final ReportService<Question> questionReportService;
    private final FlairService flairService;

    @Autowired
    public QAndAController(QuestionService questionService,
                           AnswerService answerService,
                           CafeteriaLikeService cafeteriaLikeService,
                           LikeService likeService,
                           HelpfulAnswerService helpfulAnswerService,
                           UserService userService,
                           QuestionCategoryService questionCategoryService,
                           BadgeService badgeService,
                           ViewService viewService,
                           ThinkBigLikeService thinkBigLikeService,
                           ReportService<Answer> answerReportService,
                           ReportService<Question> questionReportService,
                           FlairService flairService) {
        this.questionService = questionService;
        this.answerService = answerService;
        this.cafeteriaLikeService = cafeteriaLikeService;
        this.likeService = likeService;
        this.helpfulAnswerService = helpfulAnswerService;
        this.userService = userService;
        this.questionCategoryService = questionCategoryService;
        this.badgeService = badgeService;
        this.viewService = viewService;
        this.thinkBigLikeService = thinkBigLikeService;
        this.answerReportService = answerReportService;
        this.questionReportService = questionReportService;
        this.flairService = flairService;
    }

    @ApiOperation(value = "Get all questions", tags = {"Questions & Answers"})
    @GetMapping
    public Collection<QuestionOutputDto> allQuestions(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        return convertToQuestionDtos(questionService.getAllQuestionsForUser(user), user);
    }

    private List<QuestionOutputDto> convertToQuestionDtos(Collection<Question> questions, Optional<User> user) {
        return questions.stream()
            .map(q -> convertToQuestionDto(q, user))
            .collect(Collectors.toList());
    }

    private QuestionOutputDto convertToQuestionDto(Question question, Optional<User> user) {
        Collection<QuestionLike> likes = likeService.getQuestionLikesForQuestion(question);
        AuthorDto author = new AuthorDto(question.getAuthor(),
            flairService.getAllFlairsForUser(question.getAuthor()));

        return new QuestionOutputDto(
            question,
            author,
            viewService.getViewCountForQuestion(question),
            getHelpfulAnswerAsDto(question, user),
            answerService.countAnswersForQuestion(question),
            likes.size(),
            user.isPresent() && likes.stream().anyMatch(l -> l.getUser().equals(user.get())),
            user.isPresent() && question.getAuthor().equals(user.get()),
            questionReportService.isReportedByUser(question, user)
        );
    }

    private AnswerOutputDto getHelpfulAnswerAsDto(Question question, Optional<User> user) {
        Optional<HelpfulAnswer> h = helpfulAnswerService.getHelpfulAnswerForQuestion(question);

        return h.map(helpfulAnswer -> convertToAnswerOutputDto(helpfulAnswer.getAnswer(), user)).orElse(null);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Create a new question", tags = {"Questions & Answers"})
    @PostMapping
    public QuestionOutputDto createQuestion(@Valid @RequestBody QuestionInputDto input, Principal principal) {
        // FIXME check principal for null
        User user = userService.getUserByUsername(principal.getName());
        QuestionCategory questionCategory = questionCategoryService.getQuestionCategoryById(input.getCategoryId());
        Question question = new Question(questionCategory, user, input.getText());

        Question savedQuestion = questionService.createOrUpdateQuestion(question);
        AuthorDto authorDto = new AuthorDto(savedQuestion.getAuthor(),
            flairService.getAllFlairsForUser(savedQuestion.getAuthor()));

        Badge badge;
        Long questionCount = questionService.getQuestionCountByUser(user);

        if (questionCount >= 25) {
            badge = badgeService.getBadgeById(3L);
            badgeService.awardBadgeToUser(badge, user);
        } else if (questionCount >= 5) {
            badge = badgeService.getBadgeById(2L);
            badgeService.awardBadgeToUser(badge, user);
        } else if (questionCount >= 1) {
            badge = badgeService.getBadgeById(1L);
            badgeService.awardBadgeToUser(badge, user);
        }

        return new QuestionOutputDto(savedQuestion, authorDto);
    }

    @Transactional
    @ApiOperation(value = "Find question with answers by question ID", tags = {"Questions & Answers"})
    @GetMapping(value = "/{questionId}")
    public QuestionOutputDto getQuestion(@PathVariable("questionId") Long questionId, Principal principal) {
        Question question = questionService.getQuestionById(questionId);
        Optional<User> user = userService.getUserByPrincipal(principal);

        // Register new view and fetch views for question
        user.ifPresent(u -> viewService.registerView(question, u));
        int views = viewService.getViewCountForQuestion(question);

        AuthorDto author = new AuthorDto(question.getAuthor(), flairService.getAllFlairsForUser(question.getAuthor()));

        // Fetch answers
        List<AnswerOutputDto> answers = convertToAnswerOutputDtos(answerService.getAnswersForQuestion(question, user), user);

        // Fetch likes for question
        Collection<QuestionLike> likes = likeService.getQuestionLikesForQuestion(question);
        boolean isLiked = (user.isPresent() && likes.stream().anyMatch(l -> l.getUser().equals(user.get())));
        boolean canEdit = (user.isPresent() && question.getAuthor().equals(user.get()));
        boolean isReported = questionReportService.isReportedByUser(question, user);

        AnswerOutputDto helpfulAnswer = helpfulAnswerService.getHelpfulAnswerForQuestion(question)
            .map(a -> convertToAnswerOutputDto(a.getAnswer(), user))
            .orElse(null);

        return new QuestionOutputDto(question, author, views, helpfulAnswer, answers, likes.size(), isLiked, canEdit, isReported);
    }

    @ApiOperation(value = "Find all questions by the current user", tags = {"Questions & Answers"})
    @GetMapping(value = "/my-questions")
    public List<QuestionOutputDto> getOwnQuestions(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return Collections.emptyList();

        List<Question> questions = questionService.getAllQuestionsByAuthor(user.get());

        return convertToQuestionDtos(questions, user);
    }

    @ApiOperation(value = "Find all questions the current user has replied to. Questions with multiple answers are included multiple times.", tags = {"Questions & Answers"})
    @GetMapping(value = "/my-replies")
    public List<QuestionOutputDto> getAllQuestionsWithAnswersByUser(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return Collections.emptyList();

        List<Answer> answers = answerService.getAllAnswersByUser(user.get());

        return answers.stream()
            .map(a -> {
                Question q = a.getQuestion();
                AuthorDto author = new AuthorDto(q.getAuthor(), flairService.getAllFlairsForUser(q.getAuthor()));
                int views = viewService.getViewCountForQuestion(q);
                AnswerOutputDto helpfulAnswer = getHelpfulAnswerAsDto(q, user);
                List<AnswerOutputDto> answerOutputDtos = convertToAnswerOutputDtos(List.of(a), user);
                Collection<QuestionLike> likes = likeService.getQuestionLikesForQuestion(q);
                boolean isLiked = likes.stream().anyMatch(l -> l.getUser().equals(user.get()));
                boolean canEdit = q.getAuthor().equals(user.get());
                boolean isReported = questionReportService.isReportedByUser(q, user);

                return new QuestionOutputDto(q, author, views, helpfulAnswer, answerOutputDtos, likes.size(), isLiked, canEdit, isReported);
            })
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Edit question by question ID (only the question's author can edit)", tags = {"Questions & Answers"})
    @PatchMapping(value = "/{questionId}")
    public ResponseEntity<QuestionOutputDto> editQuestion(@Valid @RequestBody QuestionUpdateDto input,
                                                          @PathVariable("questionId") Long questionId,
                                                          Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Question question = questionService.getQuestionById(questionId);

        if (!question.getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        question.setText(input.getText());
        Question updatedQuestion = questionService
            .createOrUpdateQuestion(question);

        QuestionOutputDto response = convertToQuestionDto(updatedQuestion, user);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Secured("ROLE_USER")
    @Transactional
    @ApiOperation(value = "Remove question by question ID (only the question's author can remove)", tags = {"Questions & Answers"})
    @DeleteMapping(value = "/{questionId}")
    public ResponseEntity<?> deleteQuestion(@PathVariable("questionId") Long questionId, Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Question question = questionService.getQuestionById(questionId);

        if (!question.getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        helpfulAnswerService.removeHelpfulAnswerForQuestion(question);
        question.getAnswers().forEach(answerService::deleteAnswer);
        questionService.deleteQuestion(question);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Transactional
    @ApiOperation(value = "Find answers for question ID", tags = {"Questions & Answers"})
    @GetMapping(value = "/{questionId}/answers")
    public List<AnswerOutputDto> getAnswersForQuestion(@PathVariable("questionId") Long questionId, Principal principal) {
        Question question = questionService.getQuestionById(questionId);
        Optional<User> user = userService.getUserByPrincipal(principal);
        List<Answer> answer = answerService.getAnswersForQuestion(question, user);

        // Register new view
        user.ifPresent(u -> viewService.registerView(question, u));

        return convertToAnswerOutputDtos(answer, user);
    }

    private List<AnswerOutputDto> convertToAnswerOutputDtos(Collection<Answer> answers, Optional<User> user) {
        return answers.stream().map(a -> convertToAnswerOutputDto(a, user)).collect(Collectors.toList());
    }

    private AnswerOutputDto convertToAnswerOutputDto(Answer answer, Optional<User> user) {
        Collection<AnswerLike> likes = likeService.getAnswerLikesForAnswer(answer);
        AuthorDto author = new AuthorDto(answer.getAuthor(),
            flairService.getAllFlairsForUser(answer.getAuthor()));

        return new AnswerOutputDto(answer,
            author,
            likes.size(),
            user.isPresent() && likes.stream().anyMatch(l -> l.getUser().equals(user.get())),
            user.isPresent() && answer.getAuthor().equals(user.get()),
            helpfulAnswerService.answerIsHelpful(answer),
            answerReportService.isReportedByUser(answer, user)
        );
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Create new answer for question ID", tags = {"Questions & Answers"})
    @PostMapping(value = "/{questionId}/answers")
    @ResponseStatus(HttpStatus.CREATED)
    public AnswerOutputDto createAnswerForQuestion(@Valid @RequestBody AnswerInputDto input,
                                                   @PathVariable("questionId") Long id,
                                                   Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        Question question = questionService.getQuestionById(id);
        Answer answer = new Answer(question, user, input.getText());

        Answer savedAnswer = answerService.createOrUpdateAnswer(answer);
        AuthorDto authorDto = new AuthorDto(savedAnswer.getAuthor(),
            flairService.getAllFlairsForUser(savedAnswer.getAuthor()));

        Badge badge;
        Long answerCount = answerService.getAnswerCountByUser(user);

        if (answerCount >= 25) {
            badge = badgeService.getBadgeById(6L);
            badgeService.awardBadgeToUser(badge, user);
        } else if (answerCount >= 5) {
            badge = badgeService.getBadgeById(5L);
            badgeService.awardBadgeToUser(badge, user);
        } else if (answerCount >= 1) {
            badge = badgeService.getBadgeById(4L);
            badgeService.awardBadgeToUser(badge, user);
        }

        return new AnswerOutputDto(savedAnswer, authorDto);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Edit answer by answer ID (only the answer's author can edit)", tags = {"Questions & Answers"})
    @PatchMapping(value = "/answers/{answerId}")
    public ResponseEntity<AnswerOutputDto> editAnswer(@Valid @RequestBody AnswerInputDto input,
                                                      @PathVariable("answerId") Long answerId,
                                                      Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Answer answer = answerService.getAnswerById(answerId);

        if (!answer.getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        answer.setText(input.getText());
        AnswerOutputDto response = convertToAnswerOutputDto(
            answerService.createOrUpdateAnswer(answer), user);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Secured("ROLE_USER")
    @Transactional
    @ApiOperation(value = "Remove answer by answer ID (only the answer's author can remove)", tags = {"Questions & Answers"})
    @DeleteMapping(value = "/answers/{answerId}")
    public ResponseEntity<?> deleteAnswer(@PathVariable("answerId") Long answerId, Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Answer answer = answerService.getAnswerById(answerId);

        if (!answer.getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        helpfulAnswerService.removeHelpfulAnswerForAnswer(answer);
        answerService.deleteAnswer(answer);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Secured("ROLE_USER")
    @Transactional
    @ApiOperation(value = "Mark answer as helpful", tags = {"Questions & Answers"})
    @PostMapping(value = "/answers/{answerId}/helpful")
    public ResponseEntity<?> markAnswerAsHelpful(@PathVariable("answerId") Long answerId, Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Answer answer = answerService.getAnswerById(answerId);

        if (!answer.getQuestion().getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        helpfulAnswerService.createHelpfulAnswer(answer);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Secured("ROLE_USER")
    @Transactional
    @ApiOperation(value = "Unmark answer as helpful", tags = {"Questions & Answers"})
    @DeleteMapping(value = "/answers/{answerId}/helpful")
    public ResponseEntity<?> unmarkAnswerAsHelpful(@PathVariable("answerId") Long answerId, Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        Answer answer = answerService.getAnswerById(answerId);

        if (!answer.getQuestion().getAuthor().equals(user.get()))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        helpfulAnswerService.removeHelpfulAnswerForAnswer(answer);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Like question by ID", tags = {"Questions & Answers - Likes"})
    @PostMapping(value = "/{questionId}/like")
    public void likeQuestion(@PathVariable("questionId") Long questionId, Principal principal) {
        Question question = questionService.getQuestionById(questionId);
        User user = userService.getUserByUsername(principal.getName());

        likeService.createNewQuestionLike(question, user);

        Badge badge;
        int likeCount = checkUserLikeCount(question.getAuthor());

        if (likeCount >= 100) {
            badge = badgeService.getBadgeById(9L);
            badgeService.awardBadgeToUser(badge, question.getAuthor());
        } else if (likeCount >= 25) {
            badge = badgeService.getBadgeById(8L);
            badgeService.awardBadgeToUser(badge, question.getAuthor());
        } else if (likeCount >= 5) {
            badge = badgeService.getBadgeById(7L);
            badgeService.awardBadgeToUser(badge, question.getAuthor());
        }
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unlike question by ID", tags = {"Questions & Answers - Likes"})
    @PostMapping(value = "/{questionId}/unlike")
    public void unlikeQuestion(@PathVariable("questionId") Long questionId, Principal principal) {
        Question question = questionService.getQuestionById(questionId);
        User user = userService.getUserByUsername(principal.getName());

        likeService.removeQuestionLike(question, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Like answer by ID", tags = {"Questions & Answers - Likes"})
    @PostMapping(value = "/answers/{answerId}/like")
    public void likeAnswer(@PathVariable("answerId") Long answerId, Principal principal) {
        Answer answer = answerService.getAnswerById(answerId);
        User user = userService.getUserByUsername(principal.getName());

        likeService.createNewAnswerLike(answer, user);

        Badge badge;
        int likeCount = checkUserLikeCount(answer.getAuthor());

        if (likeCount >= 100) {
            badge = badgeService.getBadgeById(9L);
            badgeService.awardBadgeToUser(badge, answer.getAuthor());
        } else if (likeCount >= 25) {
            badge = badgeService.getBadgeById(8L);
            badgeService.awardBadgeToUser(badge, answer.getAuthor());
        } else if (likeCount >= 5) {
            badge = badgeService.getBadgeById(7L);
            badgeService.awardBadgeToUser(badge, answer.getAuthor());
        }
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unlike answer by ID", tags = {"Questions & Answers - Likes"})
    @PostMapping(value = "/answers/{answerId}/unlike")
    public void unlikeAnswer(@PathVariable("answerId") Long answerId, Principal principal) {
        Answer answer = answerService.getAnswerById(answerId);
        User user = userService.getUserByUsername(principal.getName());

        likeService.removeAnswerLike(answer, user);
    }

    @ApiOperation(value = "Get number of activities for your questions and all question categories", tags = {"Questions & Answers - Categories"})
    @GetMapping(value = "/overview")
    public OverviewDto getOverview(Principal principal) {
        final List<QuestionCategory> questionCategories = questionCategoryService.getAllQuestionCategories();
        final List<QuestionCategory> subscribedCategories = principal != null
            ? questionCategoryService.getSubscribedQuestionCategoriesForUser(userService.getUserByUsername(principal.getName()))
            : new ArrayList<>();
        final int newReplies = (principal != null)
            ? answerService.getReplyActivityForUser(userService.getUserByUsername(principal.getName()))
            : 0;

        return new OverviewDto(newReplies, questionCategories
            .stream()
            .filter(qc -> qc.getParentId() == null)
            .map(qc -> new QuestionCategoryOverviewDto(
                new QuestionCategoryDto(qc, subscribedCategories.contains(qc), (questionService.getQuestionActivitiesForCategory(qc)
                    + answerService.getAnswerActivitiesForCategory(qc))),
                subscribedCategories.contains(qc) || subscribedCategories.stream().anyMatch(sc -> qc.getId().equals(sc.getParentId())),
                questionCategories
                    .stream()
                    .filter(sc -> qc.getId().equals(sc.getParentId()))
                    .map(sc -> new QuestionCategoryDto(
                        sc,
                        subscribedCategories.contains(sc),
                        (questionService.getQuestionActivitiesForCategory(sc)
                            + answerService.getAnswerActivitiesForCategory(sc))))
                    .collect(Collectors.toList())
            ))
            .collect(Collectors.toList()));
    }

    @Deprecated
    @ApiOperation(value = "Get all question categories", tags = {"Questions & Answers - Categories"})
    @GetMapping(value = "/categories")
    public Collection<QuestionCategoryOverviewDto> getQuestionCategories(Principal principal) {
        final List<QuestionCategory> questionCategories = questionCategoryService.getAllQuestionCategories();
        final List<QuestionCategory> subscribedCategories = principal == null
            ? new ArrayList<>()
            : questionCategoryService.getSubscribedQuestionCategoriesForUser(userService.getUserByUsername(principal.getName()));

        return questionCategories
            .stream()
            .filter(qc -> qc.getParentId() == null)
            .map(qc -> new QuestionCategoryOverviewDto(
                new QuestionCategoryDto(qc, subscribedCategories.contains(qc), (questionService.getQuestionActivitiesForCategory(qc)
                    + answerService.getAnswerActivitiesForCategory(qc))),
                subscribedCategories.contains(qc) || subscribedCategories.stream().anyMatch(sc -> qc.getId().equals(sc.getParentId())),
                questionCategories
                    .stream()
                    .filter(sc -> qc.getId().equals(sc.getParentId()))
                    .map(sc -> new QuestionCategoryDto(
                        sc,
                        subscribedCategories.contains(sc),
                        (questionService.getQuestionActivitiesForCategory(sc)
                            + answerService.getAnswerActivitiesForCategory(sc))))
                    .collect(Collectors.toList())
            ))
            .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get all questions including answers for a question category", tags = {"Questions & Answers"})
    @GetMapping(value = "/categories/{categoryId}")
    public List<QuestionOutputDto> getAllQuestionsForACategory(@PathVariable("categoryId") Long categoryId, Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);
        List<Question> questionsForCategory = questionService.getAllQuestionsByQuestionCategory(categoryId, user);

        return convertToQuestionDtos(questionsForCategory, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Reset/unsubscribe a QuestionCategory subscription for the current user", tags = {"Questions & Answers - Categories"})
    @DeleteMapping(path = "/categories/{categoryId}")
    public void deleteUserQuestionCategory(Principal principal,
                                           @PathVariable("categoryId") Long categoryId) {
        User user = userService.getUserByUsername(principal.getName());
        QuestionCategory category = questionCategoryService.getQuestionCategoryById(categoryId);

        questionCategoryService.deleteUserQuestionCategory(category, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Subscribe to a QuestionCategory as the current user", tags = {"Questions & Answers - Categories"})
    @PostMapping(path = "/categories/{categoryId}")
    public void subscribeUserToQuestionCategory(Principal principal,
                                                @PathVariable("categoryId") Long categoryId) {
        User user = userService.getUserByUsername(principal.getName());
        QuestionCategory category = questionCategoryService.getQuestionCategoryById(categoryId);

        questionCategoryService.createOrUpdateUserQuestionCategory(category, user);
    }

    @Deprecated
    @Secured("ROLE_USER")
    @ApiOperation(value = "Get number of received likes for the current user", tags = {"Questions & Answers - Likes"})
    @GetMapping(value = "/user/likes") // TODO Change to more sensible route
    public int getAllLikesReceivedByUser(Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        return checkUserLikeCount(user);
    }

    private int checkUserLikeCount(User user) {
        // FIXME ProfileController has a redundant method now
        return likeService.getAnswerLikesForAnswerAuthor(user).size()
            + likeService.getQuestionLikesForQuestionAuthor(user).size()
            + thinkBigLikeService.getCommentLikesForUser(user).size()
            + cafeteriaLikeService.getCafeteriaLikesForUser(user).intValue();
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Report question by ID", tags = {"Questions & Answers"})
    @PostMapping(path = "/{questionId}/report")
    @ResponseStatus(HttpStatus.CREATED)
    public void reportQuestion(@PathVariable("questionId") Long questionId,
                               Principal principal) {
        Question question = questionService.getQuestionById(questionId);
        User user = userService.getUserByUsername(principal.getName());

        questionReportService.report(question, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Report answer by ID", tags = {"Questions & Answers"})
    @PostMapping(path = "/answers/{answerId}/report")
    @ResponseStatus(HttpStatus.CREATED)
    public void reportAnswer(@PathVariable("answerId") Long answerId,
                             Principal principal) {
        Answer answer = answerService.getAnswerById(answerId);
        User user = userService.getUserByUsername(principal.getName());

        answerReportService.report(answer, user);
    }
}
