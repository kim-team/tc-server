/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.web;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import de.thm.kim.tc.app.qanda.data.entity.QuestionCategory;
import lombok.Value;

@Value
class QuestionCategoryDto {

    @JsonUnwrapped
    QuestionCategory questionCategory;
    boolean isSubscribed;
    int countActivities;

    QuestionCategoryDto(QuestionCategory questionCategory, boolean isSubscribed, int countActivities) {
        this.questionCategory = questionCategory;
        this.isSubscribed = isSubscribed;
        this.countActivities = countActivities;
    }
}
