CREATE TABLE timeline_questions (
    question_id       BIGINT NOT NULL,
    timeline_entry_id BIGINT NOT NULL,
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    FOREIGN KEY (timeline_entry_id) REFERENCES timeline_entries (timeline_entry_id),
    PRIMARY KEY (question_id, timeline_entry_id)
);
