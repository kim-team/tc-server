INSERT INTO categories (category_id, title, text) VALUES
(1, 'Help',                  'Hilfe bei allgemeinen Fragen'),
(2, 'Connect',               'Verabredungen zum Essen, Austausch, Networking etc.'),
(3, 'Ask tinyCampus',        'Fragen an die tinyCampus-Redaktion'),
(4, 'Offtopic',              'Alles was sonst nirgendwo reinpasst'),
(5, 'Lost and Found',        'Fundsachen und Vermisstes'),
(6, 'Vorträge und Workshop', 'Fragen zu Vorträgen und Workshops der Konferenz');

INSERT INTO badges (badge_id, title, description, asset) VALUES
(1, 'Neugierig', 'Stelle Deine erste Frage', 'badge_default'),
(2, 'Neugieriger', 'Stelle Deine dritte Frage', 'badge_default'),
(3, 'Am Neugierigsten', 'Stelle Deine fünfte Frage', 'badge_default'),
(4, 'Hilfsbereit', 'Verfasse Deine erste Antwort', 'badge_default'),
(5, 'Weise', 'Verfasse Deine dritte Antwort', 'badge_default'),
(6, 'Wikipedia-Person', 'Verfasse Deine fünfte Antwort', 'badge_default'),
(7, 'Herzliche Umarmung', 'Erhalte 3 Herzen', 'badge_default'),
(8, 'Beliebt', 'Erhalte 5 Herzen', 'badge_default'),
(9, 'Queen/King of Hearts', 'Erhalte 10 Herzen', 'badge_default'),
(10, 'Lehrling', 'Erreiche Stufe 3', 'badge_default'),
(11, 'Geselle', 'Erreiche Stufe 5', 'badge_default'),
(12, 'Meister', 'Erreiche Stufe 10', 'badge_default');
