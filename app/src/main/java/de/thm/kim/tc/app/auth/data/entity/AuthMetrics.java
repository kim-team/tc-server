/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.data.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "authentication_metrics")
public class AuthMetrics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auth_metrics_id")
    Long id;

    @CreatedDate
    @Column(name = "date")
    private Date date;

    @Column(name = "unique_daily_users")
    private Integer uniqueDailyUsers;

    @Column(name = "successful_logins_fetch_token")
    private Long successfulLoginsFetchToken;

    @Column(name = "successful_logins_use_token")
    private Long successfulLoginsUseToken;

    @Column(name = "failed_logins_disabled")
    private Long failedLoginsDisabled;

    @Column(name = "failed_logins_bad_credentials")
    private Long failedLoginsBadCredentials;

    @Column(name = "failed_logins_bad_token")
    private Long failedLoginsBadToken;

    public AuthMetrics(Integer uniqueDailyUsers,
                       Long successfulLoginsFetchToken,
                       Long successfulLoginsUseToken,
                       Long failedLoginsDisabled,
                       Long failedLoginsBadCredentials,
                       Long failedLoginsBadToken) {
        this.date = new Date();
        this.uniqueDailyUsers = uniqueDailyUsers;
        this.successfulLoginsFetchToken = successfulLoginsFetchToken;
        this.successfulLoginsUseToken = successfulLoginsUseToken;
        this.failedLoginsDisabled = failedLoginsDisabled;
        this.failedLoginsBadCredentials = failedLoginsBadCredentials;
        this.failedLoginsBadToken = failedLoginsBadToken;
    }
}
