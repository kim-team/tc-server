/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.service;

import de.thm.kim.tc.app.account.data.entity.Homescreen;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.HomescreenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HomescreenServiceTest {

    private static final List<Integer> NEW_CARDS_ORDER = List.of(1, 2, 3);

    private HomescreenService service;

    @Mock
    private HomescreenRepository homescreenRepositoryMock;

    @Mock
    private Homescreen homescreenMock;

    @Mock
    private User userMock;

    @BeforeEach
    public void setUp() {
        service = new HomescreenService(homescreenRepositoryMock);

        when(homescreenRepositoryMock.findByUser(userMock))
            .thenReturn(Optional.of(homescreenMock));
    }

    @Test
    public void get_homescreen() {
        assertThat(service.getHomescreen(userMock), is(Optional.of(homescreenMock)));
    }

    @Test
    public void update_homescreen() {
        when(homescreenRepositoryMock.save(any(Homescreen.class)))
            .thenReturn(homescreenMock);

        assertThat(service.updateHomescreen(NEW_CARDS_ORDER, userMock), is(homescreenMock));
    }
}
