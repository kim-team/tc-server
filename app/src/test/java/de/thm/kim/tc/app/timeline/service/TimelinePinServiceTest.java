/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.timeline.data.entity.TimelinePin;
import de.thm.kim.tc.app.timeline.data.repository.TimelinePinRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TimelinePinServiceTest {

    private static final Long USER_ID = 1L;

    @InjectMocks
    private TimelinePinService service;
    @Mock
    private TimelinePinRepository timelinePinRepositoryMock;
    @Mock
    private Collection<TimelinePin> timelinePinsMock;

    @Mock
    private UserService userService;
    @Mock
    private User user;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);

        when(userService.getUserById(USER_ID))
            .thenReturn(user);
    }

    @Test
    public void get_all_timeline_pins() {
        assertThat(service.getAllTimelinePins(), is(
            timelinePinsMock
                .stream()
                .map(TimelinePin::getTimelineEntry)
                .collect(Collectors.toList())
        ));
    }

    @Test
    public void get_all_timeline_pins_by_user_id() {
        User user = userService.getUserById(USER_ID);

        assertThat(service.getAllTimelinePinsByUser(user), is(
            timelinePinsMock
                .stream()
                .map(TimelinePin::getTimelineEntry)
                .collect(Collectors.toList())
        ));
    }
}
