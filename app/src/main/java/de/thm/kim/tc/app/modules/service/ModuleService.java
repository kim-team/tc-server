/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.modules.service;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.modules.data.entity.Module;
import de.thm.kim.tc.app.modules.data.repository.ModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class ModuleService implements HealthIndicator {

    private final ModuleRepository moduleRepository;

    @Autowired
    public ModuleService(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    @Override
    public Health health() {
        if (moduleRepository.count() <= 0)
            return Health.down().build();

        return Health.up().build();
    }

    public Collection<Module> getAllModules() {
        return moduleRepository.findAll();
    }

    public Collection<Module> getPublishedModules() {
        return moduleRepository.findAllByPublishedIsTrue();
    }

    public Collection<Module> getAllUnpublishedModules() {
        return moduleRepository.findAllByPublishedIsFalse();
    }

    public Optional<Module> getModule(String moduleId) {
        return moduleRepository.findById(moduleId);
    }

    public void publishModule(String moduleId) {
        Module module = moduleRepository.findById(moduleId).orElseThrow(
            () -> new ResourceNotFoundException("No Module found for Module ID: " + moduleId));
        module.setPublished(true);
        moduleRepository.save(module);
    }

    public void unpublishModule(String moduleId) {
        Module module = moduleRepository.findById(moduleId).orElseThrow(
            () -> new ResourceNotFoundException("No Module found for Module ID: " + moduleId));
        module.setPublished(false);
        moduleRepository.save(module);
    }

    public boolean isCafeteriaPublished() {
        return isModulePublished("cafeteria");
    }

    public boolean isQAndAPublished() {
        return isModulePublished("qanda");
    }

    public boolean isThinkBigPublished() {
        return isModulePublished("thinkbig");
    }

    private boolean isModulePublished(String moduleId) {
        return getPublishedModules().stream().anyMatch(m -> moduleId.equals(m.getId()));
    }
}
