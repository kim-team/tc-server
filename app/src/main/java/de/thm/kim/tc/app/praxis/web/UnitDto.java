/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.praxis.data.entity.PpUnit;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
class UnitDto {

    Long unitId;
    Long groupId;
    Map<String, String> title;
    Map<String, String> subtitle;
    Map<String, String> markdown;
    String image;
    Map<String, String> imageAlt;

    Long order;
    List<MediaDto> medias;
    List<EntryDto> entries;
    List<QuestionDto> questions;

    UnitDto(PpUnit unit, List<MediaDto> medias, List<EntryDto> entries, List<QuestionDto> questions) {
        this.unitId = unit.getUnitId();
        this.groupId = unit.getGroupId();
        this.title = unit.getTitleMap();
        this.subtitle = unit.getSubtitleMap();
        this.markdown = unit.getMarkdownMap();
        this.image = unit.getImage();
        this.imageAlt = unit.getImageAltMap();
        this.order = unit.getOrder();
        this.medias = medias;
        this.entries = entries;
        this.questions = questions;
    }
}
