/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TimelineEntryRepository extends PagingAndSortingRepository<TimelineEntry, Long> {
    Page<TimelineEntry> findAllByIdIsNotIn(Collection<Long> blacklist, Pageable pageable);
    List<TimelineEntry> findAllByOrderByIdDesc();

    /*
        Cave: findAllBySourceInAndIdIsNotIn() silently fails on empty hideIds collection,
              use findAllBySourceIn() for these cases instead!
    */
    Page<TimelineEntry> findAllBySourceIn(Collection<Source> sources, Pageable pageable);

    Page<TimelineEntry> findAllBySourceInAndIdIsNotIn(Collection<Source> sources, Collection<Long> hideIds, Pageable pageable);
}
