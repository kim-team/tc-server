/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import de.thm.kim.tc.app.timeline.service.UserTimelineHideService;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HiddenEntryControllerTest {

    private static final String HIDDEN_ENTRIES_URL = "/timeline/hidden";

    private static final Long ENTRY_ID = 1L;
    private static final Long INVALID_ENTRY_ID = 999L;

    private static final String VALID_USERNAME = "admin";
    private static final String INVALID_USERNAME = "i_dont_exist";

    @SpyBean
    private TimelineService timelineService;

    @SpyBean
    private UserService userService;

    @SpyBean
    private UserTimelineHideService userTimelineHideService;

    @Autowired
    private MockMvc mockMvc;

    @WithMockUser(VALID_USERNAME)
    @Test
    public void hide_timeline_entry_for_user() throws Exception {
        User user = userService.getUserByUsername(VALID_USERNAME);
        TimelineEntry entry = timelineService.getTimelineEntryById(ENTRY_ID);

        mockMvc.perform(post(HIDDEN_ENTRIES_URL + "/" + ENTRY_ID))
            .andExpect(status().isOk());

        verify(userService, times(2))
            .getUserByUsername(VALID_USERNAME);
        verify(timelineService, times(2))
            .getTimelineEntryById(ENTRY_ID);
        verify(userTimelineHideService).addHideTimelineEntry(entry, user);
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_hide_timeline_entry_for_invalid_user() throws Exception {
        mockMvc.perform(post(HIDDEN_ENTRIES_URL + "/" + ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(timelineService, never())
            .getTimelineEntryById(ENTRY_ID);
        verify(userTimelineHideService, never())
            .addHideTimelineEntry(any(TimelineEntry.class), any(User.class));

        verify(userService).getUserByUsername(INVALID_USERNAME);
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void should_fail_to_hide_invalid_timeline_entry() throws Exception {

        mockMvc.perform(post(HIDDEN_ENTRIES_URL + "/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No TimelineEntry found for ID: " + INVALID_ENTRY_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineHideService, never())
            .addHideTimelineEntry(any(TimelineEntry.class), any(User.class));

        verify(userService).getUserByUsername(VALID_USERNAME);
        verify(timelineService).getTimelineEntryById(INVALID_ENTRY_ID);
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_hide_invalid_timeline_entry_for_invalid_user() throws Exception {
        mockMvc.perform(post(HIDDEN_ENTRIES_URL + "/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(timelineService, never())
            .getTimelineEntryById(INVALID_ENTRY_ID);
        verify(userTimelineHideService, never())
            .addHideTimelineEntry(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void restore_all_timeline_entries_for_user() throws Exception {
        User user = userService.getUserByUsername(VALID_USERNAME);

        mockMvc.perform(delete(HIDDEN_ENTRIES_URL))
            .andExpect(status().isOk());

        verify(userService, times(2))
            .getUserByUsername(VALID_USERNAME);
        verify(userTimelineHideService)
            .restoreAllHiddenTimelineEntriesForUser(user);
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_restore_all_timeline_entries_for_invalid_user() throws Exception {
        mockMvc.perform(delete(HIDDEN_ENTRIES_URL))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineHideService, never())
            .restoreAllHiddenTimelineEntriesForUser(any(User.class));

        verify(userService).getUserByUsername(INVALID_USERNAME);
    }
}
