/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.HomescreenService;
import de.thm.kim.tc.app.account.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HomescreenControllerTest {

    private static final String URL = "/homescreen";
    private static final String VALID_USERNAME = "admin";
    private static final String INVALID_USERNAME = "i_dont_exist";

    @Autowired
    private WebApplicationContext context;

    @SpyBean
    private HomescreenService homescreenService;

    @SpyBean
    private UserService userService;

    private MockMvc mockMvc;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void get_homescreen_for_valid_user() throws Exception {
        User user = userService.getUserByUsername(VALID_USERNAME);

        mockMvc.perform(get(URL)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.userId", is(user.getId().intValue())))
            .andExpect(jsonPath("$.order").isArray())
            .andExpect(content().json("{ \"userId\":1, \"order\":[1,2,3] }", true)); // Cave: Do not remove 'true'

        verify(userService).getUserByPrincipal(any(Principal.class));
        verify(homescreenService).getHomescreen(user);
    }

    // TODO Move to service test
//    @WithMockUser(INVALID_USERNAME)
//    @Test(expected = ResourceNotFoundException.class)
    public void should_fail_to_return_homescreen_for_invalid_user_id() throws Exception {
        mockMvc.perform(get(URL));
        verify(userService).getUserByUsername(INVALID_USERNAME);

        verify(homescreenService, never()).getHomescreen(any());
    }

    @Transactional
    @WithMockUser(VALID_USERNAME)
    @Test
    public void update_homescreen_for_valid_user() throws Exception {
        List<Integer> newOrder = List.of(6, 5, 4, 3, 2, 1);
        User user = userService.getUserByUsername(VALID_USERNAME);

        mockMvc.perform(post(URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonMapper.writeValueAsString(newOrder)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.userId", is(user.getId().intValue())))
            .andExpect(jsonPath("$.order").isArray())
            .andExpect(content().json("{ \"userId\":1, \"order\":[6,5,4,3,2,1] }", true)); // Cave: Do not remove 'true'

        verify(userService, times(2)).getUserByUsername(VALID_USERNAME);
        verify(homescreenService).updateHomescreen(newOrder, user);
    }

    @Transactional
    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_update_homescreen_for_invalid_user() throws Exception {
        List<Integer> newOrder = List.of(6, 5, 4, 3, 2, 1);

        mockMvc.perform(post(URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonMapper.writeValueAsString(newOrder)))
            .andExpect(status().isNotFound());

        verify(userService).getUserByUsername(INVALID_USERNAME);

        verify(homescreenService, never()).updateHomescreen(any(), any(User.class));
    }

    @Transactional
    @Test
    public void should_fail_to_update_homescreen_for_guest_user() throws Exception {
        List<Integer> newOrder = List.of(6, 5, 4, 3, 2, 1);

        mockMvc.perform(post(URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonMapper.writeValueAsString(newOrder)))
            .andExpect(status().isUnauthorized());

        verify(userService, never()).getUserByUsername(VALID_USERNAME);
        verify(homescreenService, never()).updateHomescreen(any(), any(User.class));
    }
}
