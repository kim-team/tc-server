/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.qanda.data.entity.QuestionCategory;
import de.thm.kim.tc.app.qanda.data.entity.UserQuestionCategory;
import de.thm.kim.tc.app.qanda.data.repository.QuestionCategoryRepository;
import de.thm.kim.tc.app.qanda.data.repository.UserQuestionCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestionCategoryService implements HealthIndicator {

    private final AnswerService answerService;
    private final HelpfulAnswerService helpfulAnswerService;
    private final QuestionService questionService;
    private final QuestionCategoryRepository questionCategoryRepository;
    private final UserQuestionCategoryRepository userQuestionCategoryRepository;

    @Autowired
    public QuestionCategoryService(AnswerService answerService,
                                   HelpfulAnswerService helpfulAnswerService,
                                   QuestionService questionService,
                                   QuestionCategoryRepository questionCategoryRepository,
                                   UserQuestionCategoryRepository userQuestionCategoryRepository) {
        this.answerService = answerService;
        this.helpfulAnswerService = helpfulAnswerService;
        this.questionService = questionService;
        this.questionCategoryRepository = questionCategoryRepository;
        this.userQuestionCategoryRepository = userQuestionCategoryRepository;
    }

    @Override
    public Health health() {
        if (questionCategoryRepository.count() <= 0)
            return Health.down().build();

        return Health.up().build();
    }

    public List<QuestionCategory> getAllQuestionCategories() {
        return questionCategoryRepository.findAll();
    }

    public QuestionCategory getQuestionCategoryById(Long id) {
        return questionCategoryRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No QuestionCategory found for ID: " + id));
    }

    public List<QuestionCategory> getSubscribedQuestionCategoriesForUser(User user) {
        return userQuestionCategoryRepository.findAllByUser(user)
            .stream()
            .map(UserQuestionCategory::getQuestionCategory)
            .collect(Collectors.toList());
    }

    public UserQuestionCategory createOrUpdateUserQuestionCategory(QuestionCategory questionCategory, User user) {
        return userQuestionCategoryRepository.save(getUserQuestionCategoryByQuestionCategoryAndUserOrCreate(questionCategory, user));
    }

    private UserQuestionCategory getUserQuestionCategoryByQuestionCategoryAndUserOrCreate(QuestionCategory questionCategory, User user) {
        return userQuestionCategoryRepository.findByQuestionCategoryAndUser(questionCategory, user)
            .orElse(new UserQuestionCategory(questionCategory, user));
    }

    public void deleteUserQuestionCategory(QuestionCategory questionCategory, User user) {
        userQuestionCategoryRepository.delete(getUserQuestionCategoryByQuestionCategoryAndUser(questionCategory, user));
    }

    private UserQuestionCategory getUserQuestionCategoryByQuestionCategoryAndUser(QuestionCategory questionCategory, User user) {
        return userQuestionCategoryRepository.findByQuestionCategoryAndUser(questionCategory, user)
            .orElseThrow(() -> new ResourceNotFoundException("No UserQuestionCategory found for QuestionCategory ID: " + questionCategory.getId() + " and User ID: " + user.getId()));
    }

    @Transactional
    public void deleteAllUserCategoriesByUser(User user) {
        userQuestionCategoryRepository.deleteAllByUser(user);
    }

    @Transactional
    public QuestionCategory createPraxisQuestionCategory(String title) {
        // ID of the "Praxisphase" parent category
        final Long parentId = 180L;
        QuestionCategory newCategory = new QuestionCategory(null, title, "", parentId, "");

        return questionCategoryRepository.save(newCategory);
    }

    public void updateCategoryTitle(Long categoryId, String newTitle) {
        questionCategoryRepository.findById(categoryId)
            .ifPresent(c -> {
                c.setTitle(newTitle);
                questionCategoryRepository.save(c);
            });
    }

    @Transactional
    public void deleteCategoryById(Long categoryId) {
        questionService.getAllQuestionsByQuestionCategory(categoryId, Optional.empty())
            .forEach(q -> {
                helpfulAnswerService.removeHelpfulAnswerForQuestion(q);
                q.getAnswers()
                    .forEach(answerService::deleteAnswer);
                questionService.deleteQuestion(q);
            });
        userQuestionCategoryRepository.deleteAllByQuestionCategoryId(categoryId);
        questionCategoryRepository.deleteById(categoryId);
    }
}
