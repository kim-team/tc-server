/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelineLike;
import de.thm.kim.tc.app.timeline.service.TimelineReportService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import de.thm.kim.tc.app.timeline.service.UserTimelineFaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Api(description = "Everything about timeline entries", tags = {"Timeline - Entries"})
@RestController
@RequestMapping(path = "/timeline")
public class TimelineController {

    private final UserService userService;
    private final TimelineService timelineService;
    private final TimelineReportService timelineReportService;
    private final UserTimelineFaveService userTimelineFaveService;

    @Autowired
    public TimelineController(UserService userService,
                              TimelineService timelineService,
                              TimelineReportService timelineReportService,
                              UserTimelineFaveService userTimelineFaveService) {
        this.userService = userService;
        this.timelineService = timelineService;
        this.timelineReportService = timelineReportService;
        this.userTimelineFaveService = userTimelineFaveService;
    }

    @ApiOperation(value = "Get all timeline entries")
    @GetMapping
    public Page<TimelineEntryDto> timeline(Principal principal,
                                           @SortDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        if (user.isEmpty())
            return getAllTimelineEntries(pageable);

        return getUserTimelineEntries(user.get(), pageable);
    }

    @ApiOperation(value = "Get timeline entry by ID")
    @GetMapping(path = "/{entryId}")
    public TimelineEntryDto getTimelineEntryById(Principal principal,
                                                 @PathVariable("entryId") Long entryId) {
        val user = userService.getUserByPrincipal(principal);
        val entry = timelineService.getTimelineEntryById(entryId);
        val faves = user
            .map(u -> userTimelineFaveService.getTimelineFaveByTimelineEntriesAndUser(Set.of(entry), u))
            .orElse(Collections.emptySet());

        return getTimelineEntryDtoForUser(user, entry, faves);
    }

    private Page<TimelineEntryDto> getAllTimelineEntries(Pageable pageable) {
        Page<TimelineEntry> page = timelineService.getAllTimelineEntries(pageable);

        List<TimelineEntryDto> timelineEntryDtos = page.getContent()
            .stream()
            .map(timelineEntry -> {
                Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                return new TimelineEntryDto(timelineEntry, false, false, likes.size(), false, false);
            })
            .collect(Collectors.toList());

        return new PageImpl<>(timelineEntryDtos, pageable, page.getTotalElements());
    }

    private Page<TimelineEntryDto> getUserTimelineEntries(User user, Pageable pageable) {
        Page<TimelineEntry> page = timelineService.getAllTimelineEntriesByUser(user, pageable);
        Collection<TimelineEntry> timelineEntries = page.getContent();
        Collection<TimelineEntry> faves = userTimelineFaveService.getTimelineFaveByTimelineEntriesAndUser(timelineEntries, user);

        List<TimelineEntryDto> timelineEntryDtos = timelineEntries
            .stream()
            .map(tle -> getTimelineEntryDtoForUser(Optional.of(user), tle, faves))
            .collect(Collectors.toList());

        return new PageImpl<>(timelineEntryDtos, pageable, page.getTotalElements());
    }

    private TimelineEntryDto getTimelineEntryDtoForUser(Optional<User> user, TimelineEntry timelineEntry, Collection<TimelineEntry> faves) {
        Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

        return new TimelineEntryDto(timelineEntry,
            faves.contains(timelineEntry),
            false,
            likes.size(),
            user.map(u -> likes.stream().anyMatch(value -> value.getUser().equals(u)))
                .orElse(false),
            timelineReportService.isReportedByUser(timelineEntry, user));
    }

    @Secured("ROLE_USER")
    @PostMapping(path = "/{entryId}/like")
    public void likeTimelineEntry(@PathVariable("entryId") Long entryId,
                                  Principal principal) {
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);
        User user = userService.getUserByUsername(principal.getName());

        timelineService.createNewTimelineLike(entry, user);
    }

    @Secured("ROLE_USER")
    @PostMapping(path = "/{entryId}/unlike")
    public void unlikeTimelineEntry(@PathVariable("entryId") Long entryId,
                                    Principal principal) {
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);
        User user = userService.getUserByUsername(principal.getName());

        timelineService.removeTimelineLike(entry, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Report timeline entry by ID")
    @PostMapping(path = "/{entryId}/report")
    @ResponseStatus(HttpStatus.CREATED)
    public void reportTimelineEntry(@PathVariable("entryId") Long entryId,
                                    Principal principal) {
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);
        User user = userService.getUserByUsername(principal.getName());

        timelineReportService.report(entry, user);
    }
}
