ALTER TABLE badges
ADD category VARCHAR(255);

UPDATE badges SET category = 'badges.categories.qanda'   WHERE badge_id = 1;
UPDATE badges SET category = 'badges.categories.qanda'   WHERE badge_id = 2;
UPDATE badges SET category = 'badges.categories.qanda'   WHERE badge_id = 3;
UPDATE badges SET category = 'badges.categories.qanda'   WHERE badge_id = 4;
UPDATE badges SET category = 'badges.categories.qanda'   WHERE badge_id = 5;
UPDATE badges SET category = 'badges.categories.qanda'   WHERE badge_id = 6;
UPDATE badges SET category = 'badges.categories.general' WHERE badge_id = 7;
UPDATE badges SET category = 'badges.categories.general' WHERE badge_id = 8;
UPDATE badges SET category = 'badges.categories.general' WHERE badge_id = 9;
UPDATE badges SET category = 'badges.categories.general' WHERE badge_id = 10;
UPDATE badges SET category = 'badges.categories.general' WHERE badge_id = 11;
UPDATE badges SET category = 'badges.categories.general' WHERE badge_id = 12;
