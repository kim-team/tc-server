/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.entity;

import de.thm.kim.tc.app.account.data.entity.Campus;
import de.thm.kim.tc.app.account.data.entity.Department;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sources")
public class Source implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "source_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "campus_id")
    private Campus campus;

    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToOne
    @JoinColumn(name = "parent_id")
    private Source parent;

    @Column(name = "name")
    private String name;

    @Column(name = "default_name")
    private String defaultName;

    @Column(name = "description")
    private String description;

    @Column(name = "logo")
    private String logo;

    @Column(name = "subscription")
    @Enumerated(EnumType.STRING)
    private SubscriptionTypes subscriptionType;

    @Column(name = "content_root")
    private String contentRoot;

    /**
     * Recursively finds all parent sources for the object
     *
     * @return The object's parent source and all their parent sources all
     * the way to the top or an empty collection for top-level sources
     */
    public List<Source> getAllParents() {
        List<Source> result = new ArrayList<>();
        Source element = this;

        while (element.getParent() != null) {
            result.add(element.getParent());
            element = element.getParent();
        }

        return result;
    }
}
