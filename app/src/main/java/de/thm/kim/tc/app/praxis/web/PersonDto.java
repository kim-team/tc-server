/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.praxis.data.entity.PpPerson;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
class PersonDto {

    Long personId;
    Long phaseId;
    Map<String, String> fullName;
    String image;
    Map<String, String> imageAlt;
    Map<String, String> description;
    List<AddressDto> addresses;

    public PersonDto(PpPerson person, List<AddressDto> addresses) {
        this.personId = person.getPersonId();
        this.phaseId = person.getPhaseId();
        this.fullName = person.getFullNameMap();
        this.image = person.getImage();
        this.imageAlt = person.getImageAltMap();
        this.description = person.getDescriptionMap();
        this.addresses = addresses;
    }
}
