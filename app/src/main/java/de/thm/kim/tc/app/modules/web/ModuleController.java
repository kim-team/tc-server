/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.modules.web;

import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.modules.data.entity.Module;
import de.thm.kim.tc.app.modules.data.entity.UserModule;
import de.thm.kim.tc.app.modules.service.ModuleService;
import de.thm.kim.tc.app.modules.service.UserModuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Api(description = "Everything about modules and homescreens", tags = {"Modules"})
@RestController
@RequestMapping(path = "/modules")
public class ModuleController {

    private final ModuleService moduleService;
    private final UserModuleService userModuleService;
    private final UserService userService;

    @Autowired
    public ModuleController(ModuleService moduleService, UserModuleService userModuleService, UserService userService) {
        this.moduleService = moduleService;
        this.userModuleService = userModuleService;
        this.userService = userService;
    }

    @ApiOperation(value = "Get homescreen for current user (includes only active modules)")
    @GetMapping
    public List<ModuleDto> getHomescreen(Principal principal) {
        val user = userService.getUserByPrincipal(principal);
        val modules = moduleService.getPublishedModules();

        if (user.isEmpty())
            return modules.stream()
                .map(ModuleDto::new)
                .sorted(Comparator.comparing(ModuleDto::getPriority))
                .collect(Collectors.toList());

        val userModules = userModuleService.getModulesByUser(user.get())
            .stream()
            .filter(u -> u.getModule().isPublished())
            .collect(Collectors.toSet());

        modules.removeAll(userModules.stream().map(UserModule::getModule).collect(Collectors.toSet()));

        val dtos = modules.stream()
            .map(ModuleDto::new)
            .collect(Collectors.toList());
        dtos.addAll(userModules
            .stream()
            .map(ModuleDto::new)
            .collect(Collectors.toList()));
        dtos.sort(Comparator.comparing(ModuleDto::getPriority));

        return dtos;
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Update homescreen for current user")
    @PostMapping
    public void saveHomescreen(@RequestBody List<ModuleDto> modules, Principal principal) {
        val user = userService.getUserByUsername(principal.getName());
        val publishedModules = moduleService.getPublishedModules().stream()
            .collect(Collectors.toMap(Module::getId, Function.identity()));

        val userModules = modules.stream()
            // Drop unpublished modules
            .filter(m -> publishedModules.containsKey(m.getId()))
            // Drop visible modules with unchanged priority
            .filter(m ->
                (!m.isVisible()) || publishedModules.get(m.getId()).getPriority() != m.getPriority())
            .map(m -> new UserModule(publishedModules.get(m.getId()), user, m.isVisible(), m.getPriority()))
            .collect(Collectors.toList());

        userModuleService.saveModules(userModules);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Activate a module (Admins only)")
    @PostMapping(path = "/{moduleId}")
    public void publishModule(@PathVariable String moduleId) {
        moduleService.publishModule(moduleId);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Deactivate a module (Admins only)")
    @DeleteMapping(path = "/{moduleId}")
    public void unpublishModule(@PathVariable String moduleId) {
        moduleService.unpublishModule(moduleId);
    }
}
