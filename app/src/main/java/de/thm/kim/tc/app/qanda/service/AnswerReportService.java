/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.reports.Report;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.ReportedAnswer;
import de.thm.kim.tc.app.qanda.data.repository.ReportedAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AnswerReportService implements ReportService<Answer> {

    private final ReportedAnswerRepository repository;

    @Autowired
    public AnswerReportService(ReportedAnswerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Collection<ReportedAnswer> getAllReports() {
        return repository.findAllByOrderByReportedItemDesc();
    }

    @Override
    public Map<Answer, List<Report<Answer>>> getAllReportsByItem() {
        return repository.findAll().stream()
            .collect(Collectors.groupingBy(Report::getReportedItem));
    }

    @Override
    public boolean isReportedByUser(Answer answer, Optional<User> user) {
        if (user.isEmpty())
            return false;

        return repository.existsByReportedItemAndReportingUser(answer, user.get());
    }

    @Override
    public void report(Answer answer, User user) {
        if (!answer.getAuthor().equals(user))
            repository.save(new ReportedAnswer(answer, user));
    }

    @Override
    @Transactional
    public void removeAllReportsById(Long id) {
        repository.deleteAllByReportedItemId(id);
    }

    @Override
    @Transactional
    public void removeAllReports(Answer answer) {
        repository.deleteAllByReportedItem(answer);
    }

    @Override
    @Transactional
    public void removeAllReportsByUser(User user) {
        repository.deleteAllByReportingUser(user);
    }

    @Override
    @Transactional
    public void removeReport(Answer answer, User user) {
        if (!answer.getAuthor().equals(user))
            repository.deleteByReportedItemAndReportingUser(answer, user);
    }
}
