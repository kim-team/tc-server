/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.account.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Api(description = "Everything about blocking users", tags = {"User blocking"})
@RestController
@RequestMapping(value = "/users")
public class BlockController {

    private final BlockService service;
    private final UserService userService;

    @Autowired
    public BlockController(BlockService service, UserService userService) {
        this.service = service;
        this.userService = userService;
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Block user by ID")
    @PostMapping(value = "/{userId}/block")
    @ResponseStatus(HttpStatus.CREATED)
    public void blockUser(@PathVariable("userId") Long id, Principal principal) {
        val user = userService.getUserByUsername(principal.getName());

        if (!user.getId().equals(id)) {
            val blockedUser = userService.getUserById(id);
            service.blockUser(user, blockedUser);
        }
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Unblock user by ID")
    @DeleteMapping(value = "/{userId}/block")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void unblockUser(@PathVariable("userId") Long id, Principal principal) {
        val user = userService.getUserByUsername(principal.getName());

        if (!user.getId().equals(id)) {
            val blockedUser = userService.getUserById(id);
            service.unblockUser(user, blockedUser);
        }
    }
}
