/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.auth.data.entity.Permission;
import de.thm.kim.tc.app.auth.data.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PermissionService {

    private final PermissionRepository permissionRepository;

    @Autowired
    PermissionService(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    public Set<GrantedAuthority> getAuthorities(String username) {
        return permissionRepository
            .findByUsername(username)
            .stream()
            .map(permission -> new SimpleGrantedAuthority("ROLE_" + permission.getRole().toUpperCase()))
            .collect(Collectors.toSet());
    }

    public void addPermission(String username, String role) {
        Permission permission = new Permission(username, role.toUpperCase());

        if (permissionRepository.findByUsername(username).contains(permission))
            return;

        permissionRepository.save(permission);
    }
}
