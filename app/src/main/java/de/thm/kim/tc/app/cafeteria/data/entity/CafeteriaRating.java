/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.data.entity;

import de.thm.kim.tc.app.account.data.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "cafeteria_rating")
public class CafeteriaRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cafeteria_rating_id")
    private Long id;

    @Column(name = "cafeteria_item")
    private String cafeteriaItem;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

    @Column(name = "rating")
    private int rating;

    @CreatedDate
    @Column(name = "created_at")
    private Date createdAt;

    public CafeteriaRating(String cafeteriaItem, User author, int rating) {
        this.cafeteriaItem = cafeteriaItem;
        this.rating = rating;
        this.author = author;
        this.createdAt = new Date();
    }

}
