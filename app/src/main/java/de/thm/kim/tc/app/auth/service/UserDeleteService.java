/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.account.service.HomescreenService;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.data.repository.PermissionRepository;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaCommentService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaLikeService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaRatingService;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.modules.service.UserModuleService;
import de.thm.kim.tc.app.praxis.service.PpCompletionService;
import de.thm.kim.tc.app.praxis.service.PpProgressService;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.service.*;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.UserSourceService;
import de.thm.kim.tc.app.timeline.service.UserTimelineFaveService;
import de.thm.kim.tc.app.timeline.service.UserTimelineHideService;
import de.thm.kim.tc.app.timeline.service.UserTimelineUnpinService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDeleteService {

    private final AnswerService answerService;
    private final AuthUserRepository authUserRepository;
    private final AuthUserVerificationService authUserVerificationService;
    private final BadgeService badgeService;
    private final BlockService blockService;
    private final CafeteriaCommentService cafeteriaCommentService;
    private final CafeteriaLikeService cafeteriaLikeService;
    private final CafeteriaRatingService cafeteriaRatingService;
    private final FlairService flairService;
    private final HelpfulAnswerService helpfulAnswerService;
    private final HomescreenService homescreenService;
    private final LikeService likeService;
    private final PermissionRepository permissionRepository;
    private final PpCompletionService ppCompletionService;
    private final PpProgressService ppProgressService;
    private final QuestionCategoryService questionCategoryService;
    private final QuestionService questionService;
    private final ReportService<Answer> answerReportService;
    private final ReportService<CafeteriaComment> cafeteriaCommentReportService;
    private final ReportService<Question> questionReportService;
    private final ReportService<TimelineEntry> timelineEntryReportService;
    private final UserModuleService userModuleService;
    private final UserService userService;
    private final UserSourceService userSourceService;
    private final UserTimelineFaveService userTimelineFaveService;
    private final UserTimelineHideService userTimelineHideService;
    private final UserTimelineUnpinService userTimelineUnpinService;
    private final ViewService viewService;

    @Autowired
    public UserDeleteService(AnswerService answerService,
                             AuthUserRepository authUserRepository,
                             AuthUserVerificationService authUserVerificationService,
                             BadgeService badgeService,
                             BlockService blockService,
                             CafeteriaCommentService cafeteriaCommentService,
                             CafeteriaLikeService cafeteriaLikeService,
                             CafeteriaRatingService cafeteriaRatingService,
                             FlairService flairService,
                             HelpfulAnswerService helpfulAnswerService,
                             HomescreenService homescreenService,
                             LikeService likeService,
                             PermissionRepository permissionRepository,
                             PpCompletionService ppCompletionService,
                             PpProgressService ppProgressService,
                             QuestionCategoryService questionCategoryService,
                             QuestionService questionService,
                             ReportService<Answer> answerReportService,
                             ReportService<CafeteriaComment> cafeteriaCommentReportService,
                             ReportService<Question> questionReportService,
                             ReportService<TimelineEntry> timelineEntryReportService,
                             UserModuleService userModuleService,
                             UserService userService,
                             UserSourceService userSourceService,
                             UserTimelineFaveService userTimelineFaveService,
                             UserTimelineHideService userTimelineHideService,
                             UserTimelineUnpinService userTimelineUnpinService,
                             ViewService viewService) {
        this.answerReportService = answerReportService;
        this.answerService = answerService;
        this.authUserRepository = authUserRepository;
        this.authUserVerificationService = authUserVerificationService;
        this.badgeService = badgeService;
        this.blockService = blockService;
        this.cafeteriaCommentReportService = cafeteriaCommentReportService;
        this.cafeteriaCommentService = cafeteriaCommentService;
        this.cafeteriaLikeService = cafeteriaLikeService;
        this.cafeteriaRatingService = cafeteriaRatingService;
        this.flairService = flairService;
        this.helpfulAnswerService = helpfulAnswerService;
        this.homescreenService = homescreenService;
        this.likeService = likeService;
        this.permissionRepository = permissionRepository;
        this.ppCompletionService = ppCompletionService;
        this.ppProgressService = ppProgressService;
        this.questionCategoryService = questionCategoryService;
        this.questionReportService = questionReportService;
        this.questionService = questionService;
        this.timelineEntryReportService = timelineEntryReportService;
        this.userModuleService = userModuleService;
        this.userService = userService;
        this.userSourceService = userSourceService;
        this.userTimelineFaveService = userTimelineFaveService;
        this.userTimelineHideService = userTimelineHideService;
        this.userTimelineUnpinService = userTimelineUnpinService;
        this.viewService = viewService;
    }


    public void deleteUser(User user) {
        deleteQAndADataByUser(user);
        deleteCafeteriaDataByUser(user);
        deleteTimelineDataByUser(user);
        deletePraxisDataByUser(user);

        badgeService.deleteBadgesByUser(user);

        homescreenService.deleteByUser(user);
        userModuleService.deleteAllByUser(user);

        blockService.deleteBlockedUsersByUser(user);
        blockService.unblockUserForAllUsers(user);

        deleteAuthDataByUsername(user.getUsername());

        userService.deleteUserByUsername(user.getUsername());
    }

    private void deleteQAndADataByUser(User user) {
        viewService.deleteAllByUser(user);
        likeService.deleteAllLikesByUser(user);
        questionCategoryService.deleteAllUserCategoriesByUser(user);

        answerReportService.removeAllReportsByUser(user);
        questionReportService.removeAllReportsByUser(user);

        val answers = answerService.getAllAnswersByUser(user);
        answers.forEach(answerService::deleteAnswer);

        val questions = questionService.getAllQuestionsByAuthor(user);
        questions.forEach(q -> {
            helpfulAnswerService.removeHelpfulAnswerForQuestion(q);
            q.getAnswers().forEach(answerService::deleteAnswer);
            questionService.deleteQuestion(q);
        });

        flairService.setFlairs(new ArrayList<>(), user);
    }

    private void deleteCafeteriaDataByUser(User user) {
        cafeteriaRatingService.deleteRatingsByUser(user);

        cafeteriaLikeService.deleteAllCafeteriaCommentLikesForUser(user);

        cafeteriaCommentReportService.removeAllReportsByUser(user);

        val comments = cafeteriaCommentService.getAllCafeteriaCommentsByAuthor(user);
        comments.forEach(cafeteriaCommentService::deleteComment);
    }

    private void deleteTimelineDataByUser(User user) {
        userSourceService.deleteAllByUser(user);

        userTimelineFaveService.deleteAllByUser(user);

        userTimelineHideService.restoreAllHiddenTimelineEntriesForUser(user);

        userTimelineUnpinService.deleteAllByUser(user);

        timelineEntryReportService.removeAllReportsByUser(user);
    }

    private void deletePraxisDataByUser(User user) {
        ppProgressService.deleteProgressForUser(user);

        ppCompletionService.deleteCompletionForUser(user);
    }

    private void deleteAuthDataByUsername(String userName) {
        // TODO logout: DELETE * FROM "oauth_access_token" WHERE user_name = :username
        permissionRepository.deleteAllByUsername(userName);

        authUserVerificationService.deleteVerificationByUsername(userName);

//        TODO
//        authUserRecoveryService
//        reset tokens

        authUserRepository.deleteByUsername(userName);
    }
}
