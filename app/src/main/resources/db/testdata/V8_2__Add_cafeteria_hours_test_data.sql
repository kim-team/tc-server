INSERT INTO cafeteria_hours (cafeteria, day_of_week, open, close) VALUES
('Campustor', 0, '07:00:00', '19:00:00'),
('Campustor', 1, '07:00:00', '19:00:00'),
('Campustor', 2, '07:00:00', '19:00:00'),
('Campustor', 3, '07:00:00', '19:00:00'),
('Campustor', 4, '07:00:00', '19:00:00'),
('Pastaria', 0, '11:00:00', '14:00:00'),
('Pastaria', 1, '11:00:00', '14:00:00'),
('Pastaria', 2, '11:00:00', '14:00:00'),
('Pastaria', 3, '11:00:00', '14:00:00'),
('Pastaria', 4, '11:00:00', '14:00:00');

INSERT INTO cafeteria_closures (cafeteria_closure_id, cafeteria, closure_from, closure_to) VALUES
(1, 'Campustor', '2020-06-15', '2020-06-16'),
(2, 'Campustor', '2020-06-17', '2020-06-18'),
(3, 'Pastaria', '2020-06-15', '2020-06-16'),
(4, 'Pastaria', '2020-06-17', '2020-06-18');
