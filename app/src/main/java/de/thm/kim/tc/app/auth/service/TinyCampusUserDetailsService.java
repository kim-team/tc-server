/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.entity.AuthUserVerification;
import de.thm.kim.tc.app.auth.data.entity.Permission;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserVerificationRepository;
import de.thm.kim.tc.app.auth.data.repository.PermissionRepository;
import de.thm.kim.tc.app.common.exceptions.InvalidLoginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TinyCampusUserDetailsService implements UserDetailsService {

    private final AuthUserRepository authUserRepository;
    private final PermissionRepository permissionRepository;
    private final AuthUserVerificationRepository authUserVerificationRepository;

    @Autowired
    public TinyCampusUserDetailsService(AuthUserRepository authUserRepository,
                                        PermissionRepository permissionRepository,
                                        AuthUserVerificationRepository authUserVerificationRepository) {
        super();

        this.authUserRepository = authUserRepository;
        this.permissionRepository = permissionRepository;
        this.authUserVerificationRepository = authUserVerificationRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String email = username.toLowerCase();

        List<AuthUserVerification> authUserVerification = authUserVerificationRepository.findAllByIdEmail(email);
        Optional<AuthUser> authUser = authUserRepository.findByUsernameOrEmail(username, email);

        if (!authUserVerification.isEmpty() && authUser.isEmpty()) {
            throw new InvalidLoginException("email_not_verified");
        }

        AuthUser user = authUser.orElseThrow(() -> new UsernameNotFoundException("No User found for username: " + username));
        List<Permission> permissions = this.permissionRepository.findByUsername(user.getUsername());

        return user.isVerified() ?
            new TinyCampusUserPrincipal(user, permissions) :
            new TinyCampusGuestPrincipal(user, permissions);
    }
}
