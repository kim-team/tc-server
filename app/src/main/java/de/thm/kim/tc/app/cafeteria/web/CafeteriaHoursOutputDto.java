/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.web;

import de.thm.kim.tc.app.cafeteria.data.entity.Cafeteria;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.time.DayOfWeek;
import java.time.OffsetTime;
import java.util.*;

@Data
@NoArgsConstructor
class CafeteriaHoursOutputDto {

    private Long cafeteriaId;
    private String name;
    private String news;
    private String description;
    private String image;
    private String latitude;
    private String longitude;
    private Map<String, Collection<CafeteriaOpenCloseDto>> openingHours = new HashMap<>();
    private Map<String, Collection<CafeteriaOpenCloseDto>> eatingHours = new HashMap<>();
    private Collection<CafeteriaFromToDto> closures = new ArrayList<>();

    public CafeteriaHoursOutputDto(Cafeteria cafeteria) {
        this.cafeteriaId = cafeteria.getId();
        this.name = cafeteria.getName();
        this.news = cafeteria.getNews();
        this.description = cafeteria.getDescription();
        this.image = cafeteria.getImage();
        this.latitude = cafeteria.getLatitude();
        this.longitude = cafeteria.getLongitude();
    }

    public void addOpeningHours(DayOfWeek dayOfWeek, OffsetTime open, OffsetTime close) {
        openingHours.putIfAbsent(dayOfWeek.toString().toLowerCase(), new HashSet<>());
        openingHours.get(dayOfWeek.toString().toLowerCase()).add(new CafeteriaOpenCloseDto(open, close));
    }

    public void addEatingHours(DayOfWeek dayOfWeek, OffsetTime open, OffsetTime close) {
        eatingHours.putIfAbsent(dayOfWeek.toString().toLowerCase(), new HashSet<>());
        eatingHours.get(dayOfWeek.toString().toLowerCase()).add(new CafeteriaOpenCloseDto(open, close));
    }

    public void addClosure(Date from, Date to) {
        closures.add(new CafeteriaFromToDto(from, to));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class CafeteriaFromToDto {
        private Date from;
        private Date to;
    }

    @Data
    @AllArgsConstructor
    private static class CafeteriaOpenCloseDto {
        private final OffsetTime open;
        private final OffsetTime close;
    }
}
