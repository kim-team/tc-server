INSERT INTO pp_phase (title_de, visible) VALUES
('Erste Praxis-Phase', true),
('Phase zwei',         true);

INSERT INTO pp_person (phase_id, full_name_de, full_name_en, image, image_alt_de, image_alt_en, description_de, description_en) VALUES
(1, 'Person 1', 'English person 1', 'broken image url, boom', 'Alt 1', 'Alt 1', 'Beschreibung 1', 'Description 1'),
(1, 'Person 2', 'English person 2', 'broken image url, boom', 'Alt 2', 'Alt 2', 'Beschreibung 2', 'Description 2'),
(1, 'Person 3', 'English person 3', 'broken image url, boom', 'Alt 3', 'Alt 3', 'Beschreibung 3', 'Description 3');

INSERT INTO pp_address (person_id, street, city, postal_code, phone, email, latitude, longitude) VALUES
(1, 'Wiesenstr. 14', 'Gießen', '35390', '+491234567890', 'nobody@nowhere.tld', '50.58715813883787', '8.683499898289938');

INSERT INTO pp_group (phase_id, title_de) VALUES
(1, 'Erste Gruppe'),
(1, 'Zweite Gruppe'),
(2, 'Dritte Gruppe');

INSERT INTO pp_unit (group_id, title_de) VALUES
(1, 'Unit 1'),
(1, 'Unit 2'),
(2, 'Unit 3'),
(2, 'Unit 4'),
(3, 'Unit 5');

INSERT INTO pp_media (unit_id, title_de, title_en, media_type, url) VALUES
(1, 'Media 1', 'The Media 1', 0, 'www.tinycampus.de'),
(1, 'Media 2', 'The Media 2', 1, 'www.tinycampus.de'),
(1, 'Media 3', 'The Media 3', 2, 'www.tinycampus.de'),
(2, 'Media 4', 'The Media 4', 1, 'www.tinycampus.de'),
(3, 'Media 5', 'The Media 5', 2, 'www.tinycampus.de');

INSERT INTO pp_entry (unit_id, title_de, title_en, body_de, body_en) VALUES
(1, 'Entry 1', 'The Entry 1', 'Deutscher Body 1', 'English body 1'),
(2, 'Entry 2', 'The Entry 2', 'Deutscher Body 2', 'English body 2'),
(3, 'Entry 3', 'The Entry 3', 'Deutscher Body 3', 'English body 3');

INSERT INTO pp_question (unit_id, body_de, body_en, body_correct_de, body_correct_en, body_wrong_de, body_wrong_en, image, image_alt_de, image_alt_en) VALUES
(1, 'Frage 1?', 'Question 1?', 'Deutscher Body 1', 'English body 1', 'Falsch 1', 'Wrong 1', 'https://tinycampus.de/img/mockup_transparent.png', 'Bild 1', 'Image 1'),
(2, 'Frage 2?', 'Question 2?', 'Deutscher Body 2', 'English body 2', 'Falsch 2', 'Wrong 2', 'https://tinycampus.de/img/mockup_transparent.png', 'Bild 2', 'Image 2'),
(3, 'Frage 3?', 'Question 3?', 'Deutscher Body 3', 'English body 3', 'Falsch 2', 'Wrong 3', 'https://tinycampus.de/img/mockup_transparent.png', 'Bild 3', 'Image 3');

INSERT INTO pp_answer (question_id, body_de, body_en) VALUES
(1, 'Antwort 1.1', 'Answer 1.1'),
(1, 'Antwort 1.2', 'Answer 1.2'),
(1, 'Antwort 1.3', 'Answer 1.3'),
(1, 'Antwort 1.4', 'Answer 1.4'),
(2, 'Antwort 2.1', 'Answer 2.1'),
(2, 'Antwort 2.2', 'Answer 2.2'),
(2, 'Antwort 2.3', 'Answer 2.3'),
(2, 'Antwort 2.4', 'Answer 2.4'),
(3, 'Antwort 3.1', 'Answer 3.1'),
(3, 'Antwort 3.2', 'Answer 3.2'),
(3, 'Antwort 3.3', 'Answer 3.3');
