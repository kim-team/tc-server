/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.entity.AuthUserVerification;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRecoveryCodeRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserVerificationRepository;
import de.thm.kim.tc.app.auth.event.OnUserMailVerificationEvent;
import de.thm.kim.tc.app.common.exceptions.SendMailException;
import de.thm.kim.tc.app.common.exceptions.VerificationException;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Log
@Service
public class AuthUserVerificationService {

    @Value("${tinycampus.auth.verification-token-expiration-time}")
    private long tokenExpirationTime;

    private final ApplicationEventPublisher eventPublisher;
    private final PermissionService permissionService;
    private final AuthUserRepository authUserRepository;
    private final AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository;
    private final AuthUserVerificationRepository authUserVerificationRepository;
    private final UserService userService;

    @Autowired
    public AuthUserVerificationService(final ApplicationEventPublisher eventPublisher,
                                       final PermissionService permissionService,
                                       final AuthUserRepository authUserRepository,
                                       final AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository,
                                       final AuthUserVerificationRepository authUserVerificationRepository,
                                       final UserService userService) {
        this.eventPublisher = eventPublisher;
        this.permissionService = permissionService;
        this.authUserRepository = authUserRepository;
        this.authUserRecoveryCodeRepository = authUserRecoveryCodeRepository;
        this.authUserVerificationRepository = authUserVerificationRepository;
        this.userService = userService;
    }

    public void sendVerificationMail(String username, String email) {
        if (authUserRepository.findByUsername(username).isEmpty()) {
            throw new UsernameNotFoundException("There is no user with that username.");
        }

        User user = userService.getUserByUsername(username);

        try {
            eventPublisher.publishEvent(new OnUserMailVerificationEvent(user.getName(), username, email));
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new SendMailException("They couldn't send your registration mail!");
        }
    }

    /**
     * Returns {@code true} if a verification email has been sent for user
     * {@code username} to any email address.
     */
    public boolean checkMailStatus(String username) {
        return authUserVerificationRepository.existsByIdUsername(username);
    }

    public String createVerificationToken(String username, String email) {
        String token = UUID.randomUUID().toString();
        while (authUserVerificationRepository.existsByToken(token)) {
            token = UUID.randomUUID().toString();
        }

        AuthUserVerification authUserVerification = authUserVerificationRepository
            .findByIdUsernameAndIdEmail(username, email)
            .orElse(authUserVerificationRepository.save(new AuthUserVerification(
                username,
                email,
                token
            )));

        authUserVerification.setCreatedAt(new Date());
        authUserVerificationRepository.save(authUserVerification);

        return authUserVerification.getToken();
    }

    @Transactional
    public void verifyEmail(String email, String token) {
        AuthUserVerification authUserVerification =
            authUserVerificationRepository
                .findByIdEmailAndToken(email, token)
                // n.b. German error messages are displayed on the website
                .orElseThrow(() -> new UsernameNotFoundException("Ungültige URL"));
        String username = authUserVerification.getId().getUsername();

        Optional<AuthUser> authUserFindByEmail = authUserRepository.findByEmail(email);
        if (authUserFindByEmail.isPresent()) {
            if (authUserFindByEmail.get().getUsername().equals(username))
                throw new IllegalArgumentException("E-Mail-Adresse wurde bereits bestätigt");

            throw new IllegalArgumentException("E-Mail wurde bereits von einem anderen User registriert");
        }

        AuthUser authUser = authUserRepository
            .findByUsername(username)
            .orElseThrow(() -> new VerificationException("User not found"));
        authUser.setEmail(email);
        if (!authUser.isVerified())
            permissionService.addPermission(username, "VERIFIED");
        authUserRepository.save(authUser);
        authUserVerificationRepository.deleteAllByIdUsername(username);
        authUserRecoveryCodeRepository.deleteAllByUsername(username);
    }

    @Transactional
    public void deleteVerificationByUsername(String username) {
        authUserVerificationRepository.deleteAllByIdUsername(username);
    }

    @Transactional
    @Scheduled(fixedRateString = "${tinycampus.auth.verification-token-expiration-time}")
    public void cleanUpVerificationTokens() {
        long n = authUserVerificationRepository
            .deleteAllByCreatedAtBefore(
                new Date(System.currentTimeMillis() - tokenExpirationTime));

        if (n > 0)
            log.info(String.format("Deleted %d verification token(s)", n));
    }
}
