/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.web;

import de.thm.kim.tc.app.thinkbig.data.entity.Comment;
import lombok.Value;

import java.util.Date;

@Value
class CommentOutputDto {

    Long id;
    String author;
    String text;
    Date date;
    int likes;
    boolean isLiked;

    CommentOutputDto(Comment comment, int likes, boolean isLiked) {
        this.id = comment.getId();
        this.author = comment.getAuthor().getName();
        this.text = comment.getText();
        this.date = comment.getDate();
        this.likes = likes;
        this.isLiked = isLiked;
    }
}
