/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.UserRepository;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineHide;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UserTimelineHideRepositoryTest {

    private static final Long USER_ID = 1L;
    private static final Long USER2_ID = 2L;
    private static final Long TIMELINE_ENTRY_ID = 6L;
    private static final Long TIMELINE_ENTRY_ID_NOT_EXISTS = 1L;

    @Autowired
    private UserTimelineHideRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TimelineEntryRepository timelineEntryRepository;

    @Test
    public void find_all_by_user() {
        User user = userRepository.findById(USER_ID).orElseThrow();

        Collection<UserTimelineHide> userTimelineHides = repository.findAllByUser(user);
        assertFalse(userTimelineHides.isEmpty());

        User user2 = userRepository.findById(USER2_ID).orElseThrow();
        Collection<UserTimelineHide> user2TimelineHides = repository.findAllByUser(user2);

        assertNotEquals(userTimelineHides, user2TimelineHides);
    }

    @Test
    public void find_by_user_and_timeline_entry() {
        User user = userRepository.findById(USER_ID).orElseThrow();
        TimelineEntry timelineEntry = timelineEntryRepository.findById(TIMELINE_ENTRY_ID).orElseThrow();

        Optional<UserTimelineHide> userTimelineHide = repository.findByTimelineEntryAndUser(timelineEntry, user);
        assertFalse(userTimelineHide.isEmpty());

        /* FIXME
        TimelineEntry timelineEntryNotExists = timelineEntryRepository.findById(TIMELINE_ENTRY_ID_NOT_EXISTS).orElseThrow();

        Optional<UserTimelineHide> userTimelineHideNotExists = repository.findByTimelineEntryAndUser(timelineEntryNotExists, user);
        assertTrue(userTimelineHideNotExists.isEmpty());
        */
    }

    @Test
    @Transactional
    public void delete_all_by_user() {
        User user = userRepository.findById(USER_ID).orElseThrow();

        Collection<UserTimelineHide> userTimelineHide = repository.findAllByUser(user);
        assertFalse(userTimelineHide.isEmpty());

        repository.deleteAllByUser(user);

        Collection<UserTimelineHide> userTimelineHideAfterDelete = repository.findAllByUser(user);
        assertTrue(userTimelineHideAfterDelete.isEmpty());
    }
}
