/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.HomescreenService;
import de.thm.kim.tc.app.account.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Api(description = "Everything about homescreens", tags = {"Homescreen"})
@RestController
@RequestMapping(path = "/homescreen")
public class HomescreenController {

    private final HomescreenService homescreenService;
    private final UserService userService;

    @Autowired
    public HomescreenController(HomescreenService homescreenService, UserService userService) {
        this.homescreenService = homescreenService;
        this.userService = userService;
    }

    @ApiOperation(value = "Get homescreen for current user")
    @GetMapping
    public ResponseEntity<?> getHomescreen(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);

        // This should never happen since the app only supports authenticated users
        if (user.isEmpty())
            return new ResponseEntity<>(
                new HomescreenDto(-2L, List.of(1, 5, 4, 8, 11, 12, 13)),
                HttpStatus.OK);

        return homescreenService.getHomescreen(user.get())
            .map(h -> new ResponseEntity<>(new HomescreenDto(h), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Update homescreen for current user")
    @PostMapping
    public ResponseEntity<?> updateHomescreen(Principal principal,
                                              @ApiParam(value = "New homescreen order")
//, example = "[3,1,2]") // FIXME Array examples don't work with Swagger 2
                                              @RequestBody List<Integer> order
    ) {
        User user = userService.getUserByUsername(principal.getName());

        return new ResponseEntity<>(
            new HomescreenDto(homescreenService.updateHomescreen(order, user)),
            HttpStatus.CREATED);
    }
}
