CREATE TABLE authentication_user (
    username VARCHAR(128) PRIMARY KEY,
    password VARCHAR(256) NOT NULL
);

CREATE TABLE authentication_role (
    role VARCHAR(128) PRIMARY KEY
);

CREATE TABLE permissions (
    username  VARCHAR(128),
    role VARCHAR(128),
    FOREIGN KEY (username) REFERENCES authentication_user (username),
    FOREIGN KEY (role) REFERENCES authentication_role (role),
    PRIMARY KEY (username, role)
);

CREATE TABLE oauth_access_token (
    token_id                VARCHAR(255),
    token                   bytea,
    authentication_id       VARCHAR(255)        PRIMARY KEY,
    user_name               VARCHAR(255),
    client_id               VARCHAR(255),
    authentication          bytea,
    refresh_token           VARCHAR(255)
);

CREATE TABLE oauth_client_details (
    client_id               VARCHAR(256) PRIMARY KEY,
    resource_ids            VARCHAR(256),
    client_secret           VARCHAR(256),
    scope                   VARCHAR(256),
    authorized_grant_types  VARCHAR(256),
    web_server_redirect_uri VARCHAR(256),
    authorities             VARCHAR(256),
    access_token_validity   INTEGER,
    refresh_token_validity  INTEGER,
    additional_information  VARCHAR(4096),
    autoapprove             VARCHAR(256)
);

CREATE TABLE oauth_client_token (
    token_id                VARCHAR(255),
    token                   bytea,
    authentication_id       VARCHAR(255),
    user_name               VARCHAR(255),
    client_id               VARCHAR(255)
);

CREATE TABLE oauth_code (
    code                    VARCHAR(255),
    authentication          bytea
);

CREATE TABLE oauth_refresh_token (
    token_id                VARCHAR(255),
    token                   bytea,
    authentication          bytea
);
