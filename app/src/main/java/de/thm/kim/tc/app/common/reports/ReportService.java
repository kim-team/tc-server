/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.common.reports;

import de.thm.kim.tc.app.account.data.entity.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ReportService<T extends Reportable> {

    Collection<? extends Report<T>> getAllReports();

    Map<T, List<Report<T>>> getAllReportsByItem();

    boolean isReportedByUser(T entity, Optional<User> user);

    void report(T entity, User user);

    @Transactional
    void removeAllReportsById(Long id);

    @Transactional
    void removeAllReports(T entity);

    @Transactional
    void removeAllReportsByUser(User user);

    @Transactional
    void removeReport(T entity, User user);
}
