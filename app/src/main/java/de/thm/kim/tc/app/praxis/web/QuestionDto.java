/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.praxis.data.entity.PpQuestion;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
class QuestionDto {

    Long questionId;
    Long unitId;
    Long correctAnswerId;
    Map<String, String> body;
    Map<String, String> bodyCorrect;
    Map<String, String> bodyWrong;
    String image;
    Map<String, String> imageAlt;
    List<AnswerDto> answers;

    QuestionDto(PpQuestion question, List<AnswerDto> answers) {
        this.questionId = question.getQuestionId();
        this.unitId = question.getUnitId();
        this.correctAnswerId = question.getCorrectAnswerId();
        this.body = question.getBodyMap();
        this.bodyCorrect = question.getBodyCorrectMap();
        this.bodyWrong = question.getBodyWrongMap();
        this.image = question.getImage();
        this.imageAlt = question.getImageAltMap();
        this.answers = answers;
    }
}
