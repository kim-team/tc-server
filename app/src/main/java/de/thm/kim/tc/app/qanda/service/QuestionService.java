/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.data.entity.QuestionCategory;
import de.thm.kim.tc.app.qanda.data.repository.QuestionRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static de.thm.kim.tc.app.qanda.constants.Constants.ACTIVITY_TIMEFRAME;

@Service
public class QuestionService {

    private final BlockService blockService;
    private final QuestionRepository questionRepository;
    private final LikeService likeService;
    private final ReportService<Question> questionReportService;
    private final TimelineQuestionService timelineQuestionService;
    private final ViewService viewService;

    @Autowired
    public QuestionService(BlockService blockService,
                           QuestionRepository questionRepository,
                           LikeService likeService,
                           ReportService<Question> questionReportService,
                           TimelineQuestionService timelineQuestionService,
                           ViewService viewService) {
        this.blockService = blockService;
        this.questionRepository = questionRepository;
        this.likeService = likeService;
        this.questionReportService = questionReportService;
        this.timelineQuestionService = timelineQuestionService;
        this.viewService = viewService;
    }

    public List<Question> getAllQuestions() {
        return questionRepository.findAllByOrderByDateDesc();
    }

    public List<Question> getAllQuestionsForUser(Optional<User> user) {
        val blockedUsers = user
            .map(blockService::getBlockedUsers)
            .orElse(Collections.emptySet());

        return questionRepository.findAllByOrderByDateDesc()
            .stream()
            .filter(q -> user.isPresent() && q.getAuthor().equals(user.get())
                || !blockedUsers.contains(q.getAuthor()))
            .collect(Collectors.toList());
    }

    public List<Question> getAllQuestionsByAuthor(User user) {
        return questionRepository.findAllByAuthorOrderByDateDesc(user);
    }

    public List<Question> getAllQuestionsByQuestionCategory(Long categoryId, Optional<User> user) {
        val blockedUsers = user
            .map(blockService::getBlockedUsers)
            .orElse(Collections.emptySet());

        return questionRepository.findAllByCategoryIdOrderByDateDesc(categoryId)
            .stream()
            .filter(q -> user.isPresent() && q.getAuthor().equals(user.get())
                || !blockedUsers.contains(q.getAuthor()))
            .collect(Collectors.toList());
    }

    public Question getQuestionById(Long id) {
        return questionRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No Question found for ID: " + id));
    }

    public Question createOrUpdateQuestion(Question question) {
        val q = questionRepository.save(question);
        timelineQuestionService.createOrUpdateTimelineEntryForQuestion(q);
        return q;
    }

    public Long getQuestionCountByUser(User user) {
        return questionRepository.countAllByAuthor(user);
    }

    @Transactional
    public void deleteQuestion(Question question) {
        questionReportService.removeAllReports(question);
        likeService.deleteAllQuestionLikesForQuestion(question);
        viewService.deleteAllQuestionViewsForQuestion(question);
        timelineQuestionService.deleteTimelineEntryForQuestion(question);
        questionRepository.delete(question);
    }

    public int getQuestionActivitiesForCategory(QuestionCategory questionCategory) {
        Date date = new Date(System.currentTimeMillis() - ACTIVITY_TIMEFRAME);
        return questionRepository.countByCategoryAndDateAfter(questionCategory, date);
    }
}
