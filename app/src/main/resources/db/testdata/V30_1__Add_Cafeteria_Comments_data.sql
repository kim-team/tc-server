-- The column 'cafeteria_item' is Base64 encoded
INSERT INTO cafeteria_comment (cafeteria_comment_id, cafeteria_item, user_id, text) VALUES
-- 'THM MensaXXXAusgabe 1XXXSchnitzel'
(5, 'VEhNIE1lbnNhWFhYQXVzZ2FiZSAxWFhYU2Nobml0emVs',             1, 'Wie bei Omma'),

-- 'Cafeteria Campus TorXXXAusgabe 1XXXSalat'
(6, 'Q2FmZXRlcmlhIENhbXB1cyBUb3JYWFhBdXNnYWJlIDFYWFhTYWxhdA==', 2, 'Superlecker'),

-- 'THM MensaXXXAusgabe 2XXXPommes'
(7, 'VEhNIE1lbnNhWFhYQXVzZ2FiZSAyWFhYUG9tbWVz',                 2, 'etwas matschig'),

-- 'Café am KunstwegXXXAusgabe 1XXXBurger'
(8, 'Q2Fmw6kgYW0gS3Vuc3R3ZWdYWFhBdXNnYWJlIDFYWFhCdXJnZXI=',     1, 'Lecker');
