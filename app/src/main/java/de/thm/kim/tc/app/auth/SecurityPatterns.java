/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth;

class SecurityPatterns {

    static final String[] PERMIT_ALL_PATTERN = {
        "/",
        "/actuator/health/**",
        "/favicon.ico",
        "/oauth/recovery/*",
        "/oauth/signup",
        "/oauth/upgrade",
        "/bouncyball/highscore",
        "/events/**",
        "/homescreen",
        "/modules/**",
        "/questions/**",
        "/timeline/**",
        "/cafeteria/**",
        "/oauth/reset",
        "/oauth/reset/*",

        // Dashboard, public account pages and Swagger UI
        "/images/**",
        "/oauth/verify",
        "/reset/**",
        "/webjars/**",
    };

    static final String[] USER_PATTERN = {
        "/praxis/**",
        "/profile",
        "/proxy/**",
        "/users/*/block",
        "/oauth/delete",
    };

    static final String[] ADMIN_PATTERN = {
        // Dashboard
        "/dashboard/**",
        "/form",
        "/tb-events/**",
        "/participants/**",
        "/event-feedback/**",
        "/comments/**",
        "/qanda/**",
        "/mensa/**",
        "/cafeteria/comments",
        "/dashboard/cafeteria/**",
        "/dashboard/metrics",
        "/dashboard/reports",

        "/actuator/**",     // n.b. /actuator/health/** is public
    };

    static final String[] DEV_PATTERN = {
        "/h2-console/**",
    };

    static final String[] SWAGGER_PATTERN = {
        "/swagger-resources/**",
        "/swagger-ui.html",
        "/v2/api-docs/**",
    };
}
