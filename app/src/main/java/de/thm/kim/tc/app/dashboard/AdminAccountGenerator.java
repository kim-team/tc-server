/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.dashboard;

import de.thm.kim.tc.app.auth.service.RegistrationService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Log
@Profile("!dev")
@Component
public class AdminAccountGenerator implements CommandLineRunner {

    @Value("${tinycampus.admin.username}")
    private String adminUsername;

    @Value("${tinycampus.admin.email}")
    private String adminEmail;

    @Value("${tinycampus.admin.password}")
    private String adminPassword;

    private final RegistrationService service;

    @Autowired
    public AdminAccountGenerator(RegistrationService service) {
        this.service = service;
    }

    @Override
    public void run(String... args) {
        if (adminUsername.isBlank()) {
            log.warning("Could not create admin user: No username specified");
            return;
        }

        if (adminPassword.isBlank()) {
            log.warning("Could not create admin user: No password specified");
            return;
        }

        if (service.registerNewAdminUser(adminUsername, "tinyCampus-Redaktion", adminEmail, adminPassword).isEmpty())
            log.info("Admin user '" + adminUsername + "' already exists");
        else
            log.info("Admin user created, Username: " + adminUsername + " Password: " + adminPassword);
    }
}
