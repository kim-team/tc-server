var currentURL = window.location.href;
var sending = false;

var State = {
    WAITING: 1,
    SENDING: 2,
    SERVERERROR: 3,
    SUCCESS: 4,
};
var currentState = State.WAITING;

function setState(newState) {
    currentState = newState;

    switch (newState) {
        case State.WAITING:
            endAnimation();
            break;
        case State.SENDING:
            loading();
            resetErrorMessages();
            break;
        case State.SERVERERROR:
            endAnimation();
            enableButton();
            break;
        case State.SUCCESS:
            showSuccessScreen();
            break;
        default:
            break;
    }
}

function handleErrors(xhr) {
    var statusCode = xhr.status;
    var errorMsg = 'Fehler';

    if (xhr.responseJSON && xhr.responseJSON.message)
        errorMsg = xhr.responseJSON.message;

    switch (statusCode) {
        // Fallback for successful requests with no response body
        case 200:
        case 201:
            $("#serverError").hide();
            $("#serverErrorDescription").hide();
            break;
        case 400:
        case 500:
            setErrorMessages(statusCode, errorMsg);
            break;
        case 401:
        case 403:
            setErrorMessages(statusCode, "Fehlende Berechtigung");
            break;
        case 404:
            setErrorMessages(statusCode, "Schnittstelle nicht gefunden");
            break;
        default:
            setErrorMessages("Error", "Fehler");
            break;
    }
}

function setErrorMessages(statusCode, errorMsg) {
    $("#serverError").text(statusCode);
    $("#serverErrorDescription").text(errorMsg);
}

function resetErrorMessages() {
    setErrorMessages("", "");
}

function showSuccessScreen() {
    console.log('showSuccessScreen() was called');
    $("#data").hide();
    $("#header-logo").fadeOut(560, function () {

        $(".noDisplay").fadeIn(260);
        $("#data").slideDown(520);

    });
    $("form").slideUp(460);
    $("#headline").text("Passwort geändert");


}

function getParams(getKey) {
    var url = window.location.href;
    var params = url.split('?')[1].split('&');
    for (var i = 0; i < params.length; i++) {
        var temp = params[i].split('=');
        var key = temp[0];
        var value = temp[1];
        if (getKey == key) return value;
    }
}

function sendData() {
    if (currentState == State.WAITING || currentState == State.SERVERERROR) {
        $(':focus').blur();

        setState(State.SENDING);
        $.ajax({
            url: '/oauth/reset',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                email: decodeURIComponent(getParams("email")),
                token: getParams("token"),
                password: $("#pwd").val(),
            }),
            success: function (msg) {
                setState(State.SUCCESS);
                $("#data").text(JSON.stringify(msg["form"]));
            },
            error: function (xhr) {
                setState(State.SERVERERROR);
                handleErrors(xhr);
            },
        });
    }
}

function send() {
    if (validateInputs()) {
        sendData();
    }
    return false;
}

function loading() {
    console.log("loading");
    startAnimation();
    disableButton();
}

function done() {
    console.log("done");
    endAnimation();
}

function disableButton() {
    $('#sendButton').addClass("disabled");
    $("#sendButton").removeAttr("disabled");
}

function enableButton() {
    $('#sendButton').removeClass("disabled");
    $("#sendButton").prop("disabled", false);
}

function validateInputs() {
    resetErrorMessages();
    var same = $('#pwd').val() == $('#pwd-confirm').val();
    var pwdEmpty = $('#pwd').val() == "";
    var repeatEmpty = $('#pwd-confirm').val() == "";
    var validPw = validatePassword();
    if (validPw) {
        $("#pwd, #pwd-confirm").removeClass("validation_error");
        $("#pwd, #pwd-confirm").addClass("validation_correct");
        if (currentState == State.WAITING || currentState == State.SERVERERROR) {
            enableButton();
        }
    } else {
        $("#pwd, #pwd-confirm").removeClass("validation_correct");
        disableButton();
    }

    return validPw && same;
}

$('#pwd, #pwd-confirm').on('keyup', function () {
    validateInputs();
});

function validatePassword() {
    var p = $('#pwd').val();
    $(".valInfo").removeClass("valTrue");
    errors = [];
    if (p.length < 8 || p.length > 64) {
        errors.push("Dein Passwort muss länger als 8 und kleiner als 64 Zeichen lang sein");
        // $("#valMinMax").addClass("valFalse");
    } else {
        $("#valMinMax").addClass("valTrue");
    }
    if (p.search(/[a-zA-Z]/i) < 0) {
        errors.push("Dein Passwort muss mindestens einen Buchstaben enthalten");
    } else {
        $("#valCharacter").addClass("valTrue");
    }
    if (p.search(/[0-9]/) < 0) {
        errors.push("Dein Passwort muss mindestens eine Zahl enthalten");
    } else {
        $("#valNumber").addClass("valTrue");
    }
    const same = $('#pwd').val() == $('#pwd-confirm').val();
    if (!same) {
        errors.push("Die Wiederholung ist nicht korrekt");
    } else {
        if ($('#pwd').val() != "") {
            $("#valSame").addClass("valTrue");
        }
    }

    return errors.length <= 0;
}


function startAnimation() {
    $("#header-logo path").addClass('path-loading');
    $("#logo_single path").addClass('path-loading');
    $("#logo path").addClass('path-loading');
    $("#font path").addClass('path-loading');

}

function endAnimation() {
    $("#header-logo path").removeClass('path-loading');
    $("#logo_single path").removeClass('path-loading');
    $("#logo path").removeClass('path-loading');
    $("#font path").removeClass('path-loading');
}


$(document).ready(function () {
    var input = $('input');

    input.on("focusin", function () {
        $(this).parent().find(".valInfo").addClass("valFocused");
    });
    input.on("focusout", function () {
        $(".valInfo").removeClass("valFocused");
    });
});
