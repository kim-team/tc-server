/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.web;

import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.thinkbig.service.CommentService;
import de.thm.kim.tc.app.thinkbig.service.EventService;
import de.thm.kim.tc.app.thinkbig.service.ThinkBigLikeService;
import de.thm.kim.tc.app.timeline.service.SourceService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class EventControllerTest {

    private static final String URL = "/events/";
    private static final int NUMBER_OF_EVENTS = 37;
    private static final String VALID_USERNAME = "jane_doe";

    @SpyBean
    private EventService eventService;
    @SpyBean
    private CommentService commentService;
    @SpyBean
    private UserService userService;
    @SpyBean
    private SourceService sourceService;
    @SpyBean
    private ThinkBigLikeService thinkBigLikeService;
    @SpyBean
    private TimelineService timelineService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(new EventController(
                eventService,
                commentService,
                userService,
                sourceService,
                thinkBigLikeService,
                timelineService))
            .build();
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void get_all_events() throws Exception {
        mockMvc.perform(get(URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(NUMBER_OF_EVENTS)));

        verify(eventService).getAllEvents();
        verify(thinkBigLikeService, times(NUMBER_OF_EVENTS)).getEventLikesForEvent(any());
        verify(commentService, times(NUMBER_OF_EVENTS)).getNumberOfCommentsForEvent(any());
        verify(eventService, times(NUMBER_OF_EVENTS)).getAverageRatingForEvent(any());
    }
}
