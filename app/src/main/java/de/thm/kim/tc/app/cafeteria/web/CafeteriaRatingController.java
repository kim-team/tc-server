/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaRating;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaRatingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Api(description = "Everything about cafeteria ratings", tags = {"Cafeteria - Ratings"})
@RestController
@RequestMapping(path = "/cafeteria")
public class CafeteriaRatingController {

    final private CafeteriaRatingService cafeteriaRatingService;
    final private UserService userService;

    @Autowired
    public CafeteriaRatingController(CafeteriaRatingService cafeteriaRatingService, UserService userService) {
        this.cafeteriaRatingService = cafeteriaRatingService;
        this.userService = userService;
    }

    @ApiOperation(value = "Get rating values for cafeteria item (User rating of -1 indicates no recent rating)")
    @GetMapping(path = "/{cafeteriaItem}/ratings")
    public CafeteriaRatingOutputDto getRatingsForCafeteriaItem(Principal principal,
                                                               @PathVariable("cafeteriaItem") String cafeteriaItem) {
        float avg = cafeteriaRatingService.getAverageRatingForCafeteriaItem(cafeteriaItem);
        int userRating = -1;
        int numberOfRatings = cafeteriaRatingService.getNumberOfRatingsForCafeteriaItem(cafeteriaItem);
        val user = userService.getUserByPrincipal(principal);

        if (user.isPresent()) {
            val rating = cafeteriaRatingService.getRecentRatingForUser(cafeteriaItem, user.get());

            if (rating.isPresent()) {
                userRating = rating.get().getRating();
            }
        }

        return new CafeteriaRatingOutputDto(avg, userRating, numberOfRatings);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Create new or update existing rating for a cafeteria item and return updated")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/{cafeteriaItem}/ratings")
    public CafeteriaRatingOutputDto postRating(Principal principal,
                                               @ApiParam("Rating must be between 1 and 5 (inclusive), comment length must be 255 characters or less")
                                               @Valid @RequestBody CafeteriaRatingInputDto input,
                                               @PathVariable("cafeteriaItem") String cafeteriaItem) {
        User user = userService.getUserByUsername(principal.getName());
        float avg = cafeteriaRatingService.getAverageRatingForCafeteriaItem(cafeteriaItem);
        CafeteriaRating userRating = cafeteriaRatingService.createOrUpdateRating(cafeteriaItem, user, input.getRating());
        int numberOfRatings = cafeteriaRatingService.getNumberOfRatingsForCafeteriaItem(cafeteriaItem);

        return new CafeteriaRatingOutputDto(avg, userRating.getRating(), numberOfRatings);
    }
}
