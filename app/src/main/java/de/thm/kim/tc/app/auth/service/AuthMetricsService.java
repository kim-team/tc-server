/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.auth.data.entity.AuthMetrics;
import de.thm.kim.tc.app.auth.data.repository.AuthMetricsRepository;
import lombok.extern.java.Log;
import lombok.val;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@Log
@Service
public class AuthMetricsService implements DisposableBean {

    private final AuthMetricsRepository repo;

    private final AtomicLong counterSuccessFetch = new AtomicLong();
    private final AtomicLong counterSuccessUse = new AtomicLong();
    private final AtomicLong counterFailedDisabled = new AtomicLong();
    private final AtomicLong counterFailedBadCredentials = new AtomicLong();
    private final AtomicLong counterFailedBadToken = new AtomicLong();
    private final Set<String> logins = new HashSet<>();

    @Autowired
    public AuthMetricsService(AuthMetricsRepository repo) {
        this.repo = repo;
    }

    public List<AuthMetrics> getAllAuthMetrics() {
        val res = repo.findAllByOrderByDateDesc();
        val cur = getCurrentMetrics();
        cur.setDate(new Date());
        res.add(0, cur);

        return res;
    }

    public Long countSuccessFetch() {
        return counterSuccessFetch.incrementAndGet();
    }

    public Long countSuccessUse() {
        return counterSuccessUse.incrementAndGet();
    }

    public Long countFailedDisabled() {
        return counterFailedDisabled.incrementAndGet();
    }

    public Long countFailedBadCredentials() {
        return counterFailedBadCredentials.incrementAndGet();
    }

    public Long countFailedBadToken() {
        return counterFailedBadToken.incrementAndGet();
    }

    public void countUserLogin(String name) {
        logins.add(name);
    }

    // TODO timezone, server is off by 2 h
    @Scheduled(cron = "30 59 23 * * *")
    private void saveDailyStats() {
        AuthMetrics m = repo.save(getCurrentMetrics());
        log.info("Saved daily usage stats: " + m.getDate());

        logins.clear();
        counterSuccessFetch.set(0L);
        counterSuccessUse.set(0L);
        counterFailedDisabled.set(0L);
        counterFailedBadCredentials.set(0L);
        counterFailedBadToken.set(0L);
    }

    private AuthMetrics getCurrentMetrics() {
        return new AuthMetrics(this.logins.size(),
            this.counterSuccessFetch.get(),
            this.counterSuccessUse.get(),
            this.counterFailedDisabled.get(),
            this.counterFailedBadCredentials.get(),
            this.counterFailedBadToken.get());
    }

    @Override
    public void destroy() throws Exception {
        saveDailyStats();
    }
}
