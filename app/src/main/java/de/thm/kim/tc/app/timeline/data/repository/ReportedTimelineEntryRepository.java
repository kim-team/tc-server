/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.ReportedTimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.ReportedTimelineEntryId;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ReportedTimelineEntryRepository extends CrudRepository<ReportedTimelineEntry, ReportedTimelineEntryId> {

    boolean existsByReportedItemAndReportingUser(TimelineEntry entry, User user);

    void deleteByReportedItemAndReportingUser(TimelineEntry entry, User user);

    void deleteAllByReportedItemId(Long id);

    void deleteAllByReportedItem(TimelineEntry entry);

    Collection<ReportedTimelineEntry> findAll();

    List<ReportedTimelineEntry> findAllByOrderByReportedItemDesc();

    void deleteByReportingUser(User user);
}
