/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.qanda.data.entity.Badge;
import de.thm.kim.tc.app.qanda.data.entity.UserBadge;
import lombok.Value;

import java.util.Date;
import java.util.List;

@Value
class BadgeDto {

    Long badgeId;

    String title;

    String description;

    String category;

    String asset;

    String animationLevel;

    Date unlockDate;

    List<BadgeCondition> conditions;

    public BadgeDto(Badge badge, List<BadgeCondition> conditions) {
        this.badgeId = badge.getId();
        this.title = badge.getTitle();
        this.description = badge.getDescription();
        this.category = badge.getCategory();
        this.asset = badge.getAsset();
        this.animationLevel = badge.getAnimationLevel();
        this.unlockDate = null;
        this.conditions = conditions;
    }

    public BadgeDto(UserBadge userBadge, List<BadgeCondition> conditions) {
        Badge badge = userBadge.getBadge();

        this.badgeId = badge.getId();
        this.title = badge.getTitle();
        this.description = badge.getDescription();
        this.category = badge.getCategory();
        this.asset = badge.getAsset();
        this.animationLevel = badge.getAnimationLevel();

        this.unlockDate = userBadge.getDate();
        this.conditions = conditions;
    }
}
