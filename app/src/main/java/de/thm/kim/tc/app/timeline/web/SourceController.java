/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.modules.service.ModuleService;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import de.thm.kim.tc.app.timeline.service.SourceService;
import de.thm.kim.tc.app.timeline.service.UserSourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Api(description = "Everything about timeline sources", tags = {"Timeline - Sources"})
@RestController
@RequestMapping(value = "/timeline/sources")
public class SourceController {

    private final ModuleService moduleService;
    private final SourceService sourceService;
    private final UserService userService;
    private final UserSourceService userSourceService;

    @Autowired
    public SourceController(ModuleService moduleService,
                            SourceService sourceService,
                            UserService userService,
                            UserSourceService userSourceService) {
        this.moduleService = moduleService;
        this.sourceService = sourceService;
        this.userService = userService;
        this.userSourceService = userSourceService;
    }

    @ApiOperation(value = "Get info for the timeline settings menu for a user")
    @GetMapping(path = "/settings")
    public List<SourceDto> getTimelineSettings(Principal principal) {
        Optional<User> user = userService.getUserByPrincipal(principal);
        if (user.isEmpty())
            return getPublicTimelineSettings();

        return getUserTimelineSettings(user.get());
    }

    private boolean isActiveModule(Source source) {
        switch (source.getId().intValue()) {
            // Redaktion
            case 1:
            case 2:
            case 3:
            case 4:
                return true;

            // Q & A
            case 5:
                return moduleService.isQAndAPublished();

            // Think Big
            case 6:
            case 7:
                return moduleService.isThinkBigPublished();
            default:
                return false;
        }
    }

    private List<SourceDto> getPublicTimelineSettings() {
        Collection<Source> topLevelSources = sourceService.getAllTopLevelSources();

        return topLevelSources.stream()
            .filter(this::isActiveModule)
            .map(SourceDto::new)
            .collect(Collectors.toList());
    }

    private List<SourceDto> getUserTimelineSettings(User user) {
        Collection<Source> topLevelSources = sourceService.getAllTopLevelSources();
        Collection<Source> userSources = sourceService.getAllSubscribedSourcesByUser(user);

        return topLevelSources.stream()
            .filter(this::isActiveModule)
            .map(s -> new SourceDto(s, userSources.contains(s)))
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Toggle a source subscription for a user")
    @PutMapping(path = "/{sourceId}")
    public void toggleUserSubscription(Principal principal,
                                       @PathVariable("sourceId") Long sourceId) {
        User user = userService.getUserByUsername(principal.getName());
        Source source = sourceService.getSourceById(sourceId);

        userSourceService.createOrToggleUserSource(source, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Update a source subscription for a user")
    @PutMapping(path = "/{sourceId}/{subscriptionType}")
    public void subscribeUserToSource(Principal principal,
                                      @PathVariable("sourceId") Long sourceId,
                                      @PathVariable("subscriptionType") SubscriptionTypes subscriptionType) {
        User user = userService.getUserByUsername(principal.getName());
        Source source = sourceService.getSourceById(sourceId);

        userSourceService.createOrUpdateUserSource(source, user, subscriptionType);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Reset a source subscription for a user")
    @DeleteMapping(path = "/{sourceId}")
    public void deleteUserSource(Principal principal,
                                 @PathVariable("sourceId") Long sourceId) {
        User user = userService.getUserByUsername(principal.getName());
        Source source = sourceService.getSourceById(sourceId);

        userSourceService.deleteUserSource(source, user);
    }
}
