-- I re-added the wrong PKs in V29_0__Re-create_Q_and_A_and_timeline_schema.sql
-- so here we go recreating the schema once more ...

-- Third digit in version is because V29_0__Re-create_Q_and_A_and_timeline_schema.sql
-- already contained a reference to V29_3__Add_blocks_and_reports_test_data.sql.

-- Same migration as V28_1__Fix_primary_keys.sql below this line:

-------------------------------------------------------------------------

-- Proper solution to change existing PK to composite key (see https://gist.github.com/scaryguy/6269293)
-- didn't work so here's a quick and dirty fix.

DROP TABLE reported_timeline_entries;
DROP TABLE reported_answers;
DROP TABLE reported_questions;
DROP TABLE reported_cafeteria_comments;

CREATE TABLE reported_timeline_entries (
    timeline_entry_id   BIGINT NOT NULL,
    user_id             BIGINT NOT NULL,
    date                TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (timeline_entry_id) REFERENCES timeline_entries (timeline_entry_id),
    PRIMARY KEY (timeline_entry_id, user_id)
);

CREATE TABLE reported_answers (
    answer_id   BIGINT NOT NULL,
    user_id     BIGINT NOT NULL,
    date        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (answer_id) REFERENCES answers (answer_id),
    PRIMARY KEY (answer_id, user_id)
);

CREATE TABLE reported_questions (
    question_id   BIGINT NOT NULL,
    user_id       BIGINT NOT NULL,
    date          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    PRIMARY KEY (question_id, user_id)
);

CREATE TABLE reported_cafeteria_comments (
    cafeteria_comment_id   BIGINT NOT NULL,
    user_id                BIGINT NOT NULL,
    date                   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (cafeteria_comment_id) REFERENCES cafeteria_comment (cafeteria_comment_id),
    PRIMARY KEY (cafeteria_comment_id, user_id)
);
