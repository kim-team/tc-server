ALTER TABLE sources ADD COLUMN default_name VARCHAR(255) NOT NULL DEFAULT '';
UPDATE sources SET default_name = 'tinyCampus-Redaktion'   WHERE source_id = 1;
UPDATE sources SET default_name = 'Allgemein'              WHERE source_id = 2;
UPDATE sources SET default_name = 'Wichtige Mitteilungen'  WHERE source_id = 3;
UPDATE sources SET default_name = 'Veranstaltungen'        WHERE source_id = 4;
UPDATE sources SET default_name = 'Fragen & Antworten'     WHERE source_id = 5;
UPDATE sources SET default_name = 'Think Big'              WHERE source_id = 6;
UPDATE sources SET default_name = 'Think Big - Kommentare' WHERE source_id = 7;
