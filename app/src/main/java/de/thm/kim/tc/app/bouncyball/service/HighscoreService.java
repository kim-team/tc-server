/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.bouncyball.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.bouncyball.data.entity.Highscore;
import de.thm.kim.tc.app.bouncyball.data.repository.HighscoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static de.thm.kim.tc.app.bouncyball.constants.HighscoreConstants.RECENT_HIGHSCORE_TIMEFRAME;

@Service
public class HighscoreService {

    private final HighscoreRepository repo;

    @Autowired
    public HighscoreService(HighscoreRepository repo) {
        this.repo = repo;
    }

    public List<Highscore> getGlobalHighscores() {
        return repo.findTop10ByOrderByScoreDescCreatedAtAsc();
    }

    public List<Highscore> getRecentHighscores() {
        Date recent = new Date(System.currentTimeMillis() - RECENT_HIGHSCORE_TIMEFRAME);

        return repo.findTop10ByCreatedAtAfterOrderByScoreDescCreatedAtAsc(recent);
    }

    public void submitHighscore(User user, String name, Long score) {
        repo.save(new Highscore(user.getId(), name, score));
    }

    public Optional<Highscore> getPersonalHighscore(User user) {
        return repo.findTopByUserIdOrderByScoreDescCreatedAtAsc(user.getId());
    }
}
