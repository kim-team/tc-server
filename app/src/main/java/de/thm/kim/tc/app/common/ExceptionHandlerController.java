/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.common;

import de.thm.kim.tc.app.common.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeParseException;

@RestControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(ResourceNotFoundException.class)
    public void handleResourceNotFoundException(ResourceNotFoundException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public void handleResourceAlreadyExistsException(ResourceAlreadyExistsException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public void handleOtherException(Exception e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public void onValidationError(MethodArgumentNotValidException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public void onValidationError(UserAlreadyExistsException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public void handleAccessDeniedException(AccessDeniedException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();

        // FIXME Aber warum?
        if (e.getMessage().toLowerCase().contains("access is denied"))
            res.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
        else
            res.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }

    @ExceptionHandler(DateTimeParseException.class)
    public void onValidationError(DateTimeParseException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(InvalidRecoveryException.class)
    public void handleInvalidRecoveryException(InvalidRecoveryException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(InvalidUpgradeException.class)
    public void handleInvalidUpgradeException(InvalidUpgradeException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(InvalidLoginException.class)
    public void handleInvalidLoginException(InvalidLoginException e, HttpServletResponse res) throws IOException {
        e.printStackTrace();
        res.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }
}
