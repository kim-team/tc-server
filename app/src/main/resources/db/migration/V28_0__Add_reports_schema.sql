CREATE TABLE reported_timeline_entries (
    timeline_entry_id   BIGINT NOT NULL,
    user_id             BIGINT NOT NULL,
    date                TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (timeline_entry_id) REFERENCES timeline_entries (timeline_entry_id),
    PRIMARY KEY (user_id)
);

CREATE TABLE reported_answers (
    answer_id   BIGINT NOT NULL,
    user_id     BIGINT NOT NULL,
    date        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (answer_id) REFERENCES answers (answer_id),
    PRIMARY KEY (user_id)
);

CREATE TABLE reported_questions (
    question_id   BIGINT NOT NULL,
    user_id       BIGINT NOT NULL,
    date          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    PRIMARY KEY (user_id)
);

CREATE TABLE reported_cafeteria_comments (
    cafeteria_comment_id   BIGINT NOT NULL,
    user_id                BIGINT NOT NULL,
    date                   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    FOREIGN KEY (cafeteria_comment_id) REFERENCES cafeteria_comment (cafeteria_comment_id),
    PRIMARY KEY (user_id)
);
