/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.web;

import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.service.AuthUserService;
import de.thm.kim.tc.app.auth.service.AuthUserVerificationService;
import de.thm.kim.tc.app.auth.service.UserDeleteService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping(path = "/oauth")
public class AccountController {

    private final AuthUserVerificationService authUserVerificationService;
    private final AuthUserService authUserService;
    private final UserDeleteService userDeleteService;
    private final UserService userService;

    @Autowired
    public AccountController(final AuthUserVerificationService authUserVerificationService,
                             final AuthUserService authUserService,
                             final UserDeleteService userDeleteService,
                             final UserService userService) {
        this.authUserVerificationService = authUserVerificationService;
        this.authUserService = authUserService;
        this.userDeleteService = userDeleteService;
        this.userService = userService;
    }

    @Secured("ROLE_VERIFIED")
    @PostMapping("/change/password/{password}")
    public void changePassword(Principal principal, @PathVariable String password) {
        authUserService.changePassword(principal.getName(), password);
    }

    @Secured("ROLE_VERIFIED")
    @PostMapping("/change/email/{email}")
    public void changeEmail(Principal principal, @PathVariable String email) {
        authUserVerificationService.sendVerificationMail(principal.getName(), email);
    }

    @Secured("ROLE_USER")
    @Transactional
    @PostMapping("/delete")
    public void deleteUser(Principal principal, @RequestParam Optional<String> name) {
        val user = userService.getUserByUsername(principal.getName());

        if (name.isEmpty() || !user.getName().equals(name.get().trim()))
            throw new IllegalArgumentException("Cannot delete account: Invalid name: " + name.orElse(""));

        userDeleteService.deleteUser(user);
    }
}
