INSERT INTO departments (department_id, number, name, long_name) VALUES
(1,  '01', 'BAU', 'Bauwesen'),
(2,  '02', 'EI',  'Elektro- und Informationstechnik'),
(3,  '03', 'ME',  'Maschinenbau und Energietechnik'),
(4,  '04', 'LSE', 'Life Science Engineering'),
(6,  '05', 'GES', 'Gesundheit'),
(14, '06', 'MNI', 'Mathematik, Naturwissenschaften und Informatik'),
(7,  '07', 'W',   'THM Business School'),
(15, '11', 'IEM', 'Informationstechnik-Elektrotechnik-Mechatronik'),
(16, '12', 'M',   'Maschinenbau, Mechatronik, Materialtechnologie'),
(17, '13', 'MND', 'Mathematik, Naturwissenschaften und Datenverarbeitung'),
(18, '14', 'WI',  'Wirtschaftsingenieurwesen'),
(8,  '21', 'MuK', 'Management und Kommunikation');

-- FB 01 - BAU
INSERT INTO programs (program_id, name, department_id) VALUES
(28,  'Architektur (B.Eng. 2012)',        1),
(361, 'Architektur (B.Eng. 2018)',        1),
(29,  'Architektur (M.Eng. 2012)',        1),
(362, 'Architektur (M.Eng. 2018)',        1),
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (32,  'Bahningenieurwesen (B.Eng. 2014)', 1);
(30,  'Bauingenieurwesen (B.Eng. 2012)',  1),
(357, 'Bauingenieurwesen (B.Eng., 2018)', 1),
(31,  'Bauingenieurwesen (M.Eng. 2012)',  1),
(363, 'Bauingeniuerwesen (M.Eng. 2018)',  1),
(34,  'Infrastrukturmanagement (M.Eng.)', 1),
-- FB 02 - EI
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (364, 'Control, Computer and Communications Engineering (M.Sc. 2018)',              2);
(190, 'Elektrische Energietechnik für Regenerative Energiesysteme (B.Eng. 2015)', 2),
(191, 'Elektro- und Informationstechnik (B.Eng. 2014)',                           2),
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (192, 'Elektro- und Informationstechnik (M.Sc. 2017)',                            2);
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (193, 'Information and Communications Engineering (M.Sc. 2010)',                  2);
-- FB 03 - ME
(40,  'Energiesysteme (B.Eng. 2015)',                                       3),
(394, 'Energietechnik (M.Sc. 2018)',                                        3),
(39,  'Energiewirtschaft und Energiemanagement (B.Eng. 2016)',              3),
(41,  'Grundstudium Maschinenbau und Energiesysteme Studiengänge (B.Eng.)', 3),
(43,  'Maschinenbau (B.Eng. 2015)',                                         3),
(395, 'Maschinenbau (M.Sc. 2018)',                                          3),
(44,  'Maschinenbau und Energiesysteme (M.Sc. 2015)',                       3),
-- FB 04 - LSE
(63,  'Biomedizinische Technik (B.Sc. 2016)',                         4),
(62,  'Biomedizinische Technik (M.Sc. 2015)',                         4),
(65,  'Biotechnologie / Biopharmazeutische Technologie (B.Sc. 2016)', 4),
(66,  'Biotechnologie / Biopharmazeutische Technologie (M.Sc. 2016)', 4),
(75,  'Grundstudium LSE Wiederholungsveranstaltungen (B.Sc.)',        4),
(68,  'Krankenhaushygiene (B.Sc. 2014)',                              4),
(70,  'KrankenhausPlanungTechnik (B.Eng. 2016)',                      4),
(69,  'KrankenhausPlanungTechnik (M.Eng. 2014)',                      4),
(72,  'Medizinische Physik (M.Sc. 2016)',                             4),
(177, 'Medizinische Physik & Strahlenschutz (B.Sc. 2015)',            4),
(73,  'Umwelt-, Hygiene- & Sicherheitsingenieurwesen (B.Sc. 2016)',   4),
(74,  'Umwelt-, Hygiene- & Sicherheitsingenieurwesen (M.Sc. 2016)',   4),
-- FB 05 - GES
(18, 'Medizinische Informatik (B.Sc. 2016)',  6),
(19, 'Medizinische Informatik (M.Sc. 2014)',  6),
(20, 'Medizinisches Management (B.Sc. 2016)', 6),
-- FB 06 - MNI
(9,   'Bioinformatik (B.Sc. 2012)',                                  14),
(10,  'Bioinformatik & Systembiologie (M.Sc. 2012)',                 14),
(12,  'Informatik (B.Sc. 2010)',                                     14),
(13,  'Informatik (M.Sc. 2010)',                                     14),
(14,  'Ingenieur-Informatik (B.Sc. 2010)',                           14),
(15,  'Ingenieur-Informatik (M.Sc. 2017)',                           14),
(22,  'Social Media Systems (B.Sc. 2016)',                           14),
(24,  'Technische Redaktion & Multimedia Dokumentation (M.A. 2015)', 14),
(352, 'Vakuumingenieurwesen (M.Sc. 2017)',                           14),

-- FB 07 - W
(50,  'Betriebswirtschaft (B.A. 2009)',      7),
(51,  'Betriebswirtschaft (B.A. 2015)',      7),
(46,  'International Marketing (M.A. 2010)', 7),
(53,  'International Marketing (M.A. 2016)', 7),
(354, 'Personalmanagement (M.A., 2017)',     7),
(48,  'Unternehmensführung (M.A. 2010)',     7),
(49,  'Unternehmenssteuerung (M.A. 2017)',   7),
-- FB 11 - IEM
(366, 'Allgemeine Elektrotechnik (B.Eng. 2014)',                     15),
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (32,  'Bahningenieurwesen (B.Eng. 2014)',                            15);
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (364, 'Control, Computer and Communications Engineering (M.Sc. 2018)', 15);
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (192, 'Elektro- und Informationstechnik (M.Sc. 2017)',               15);
-- FIXME
--INSERT INTO programs (program_id, name, department_id) VALUES (193, 'Information and Communications Engineering (M.Sc. 2010)',     15);
(377, 'Medieninformatik (B.Sc. 2010)',                               15),
(378, 'Medieninformatik (B.Sc. 2018)',                               15),
(379, 'Medieninformatik (M.Sc. 2010)',                               15),
(381, 'Nachrichtentechnik und Computernetze (B.Eng. 2015)',          15),
(385, 'Technische Informatik (B.Eng. 2010)',                         15),
(386, 'Technische Informatik (B.Eng. 2018)',                         15),
-- FB 12 - M
(374, 'Maschinenbau (B.Sc.)',                  16),
(380, 'Maschinenbau Mechatronik (M.Sc. 2016)', 16),
(375, 'Mechatronik (B.Sc. 2016)',              16),
-- FB 13 - MND
-- FIXME
--(32,  'Bahningenieurwesen (B.Eng. 2014)',                                    17);
(376, 'Mathematik für Finanzen, Versicherungen und Management (M.Sc. 2017)', 17),
(382, 'Optotechnik und Bildverarbeitung (M.Sc.)',                            17),
(383, 'Physikalische Technik (B.Sc. 2014)',                                  17),
(388, 'Wirtschaftsinformatik (B.Sc. 2010)',                                  17),
(390, 'Wirtschaftsinformatik (M.Sc. 2018)',                                  17),
(391, 'Wirtschaftsmathematik (B.Sc. 2015)',                                  17),
-- FB 14 - WI
(370, 'Wirtschaftsingenieurwesen - Immobilien (B.Sc. 2015)', 18),
(371, 'Wirtschaftsingenieurwesen - Industrie (B.Sc. 2015)',  18),
(387, 'Wirtschaftsingenieurwesen (M.Sc. 2016)',              18),
-- FB 21 - MuK
(182, 'Eventmanagement und -technik (B.Sc. 2017)',                                        8),
(373, 'Logistikmanagement (B.Sc. 2013)',                                                  8),
(355, 'Methoden und Didaktik in angewandten Wissenschaften_Higher Education (M.A. 2017)', 8),
(384, 'Supply Chain Management (M.Sc. 2012)',                                             8);

-- CAMPUSES
INSERT INTO campuses (campus_id, name) VALUES
(1, 'Gießen'),
(2, 'Friedberg'),
(3, 'Wetzlar');
