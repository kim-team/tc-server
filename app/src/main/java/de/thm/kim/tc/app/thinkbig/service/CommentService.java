/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.thinkbig.data.entity.Comment;
import de.thm.kim.tc.app.thinkbig.data.entity.Event;
import de.thm.kim.tc.app.thinkbig.data.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final ThinkBigLikeService thinkBigLikeService;

    @Autowired
    public CommentService(CommentRepository commentRepository, ThinkBigLikeService thinkBigLikeService) {
        this.commentRepository = commentRepository;
        this.thinkBigLikeService = thinkBigLikeService;
    }

    public Collection<Comment> getCommentsForEvent(Event event) {
        return commentRepository.findAllByEventOrderByDateDesc(event);
    }

    public Comment createNewComment(Event event, User user, String text) {
        return commentRepository.save(new Comment(event, user, text));
    }

    public Comment getCommentById(Long commentId) {
        return commentRepository.findById(commentId)
            .orElseThrow(() -> new ResourceNotFoundException("No Comment found for ID: " + commentId));
    }

    public int getNumberOfCommentsForEvent(Event event) {
        return commentRepository.countByEvent(event);
    }

    public List<Comment> getAllComments() {
        return commentRepository.findAllByOrderByDateDesc();
    }

    public void deleteComment(Comment comment) {
        thinkBigLikeService.deleteAllCommentLikesForComment(comment);
        commentRepository.delete(comment);
    }
}
