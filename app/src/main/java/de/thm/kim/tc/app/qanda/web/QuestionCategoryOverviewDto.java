/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.web;

import lombok.Value;

import java.util.List;

@Value
class QuestionCategoryOverviewDto {

    Long id;
    String title;
    String description;
    String logo;
    boolean isSubscribed;
    int countActivities;

    List<QuestionCategoryDto> subcategories;

    QuestionCategoryOverviewDto(QuestionCategoryDto parentCategory, boolean isSubscribed, List<QuestionCategoryDto> subcategories) {
        this.id = parentCategory.getQuestionCategory().getId();
        this.title = parentCategory.getQuestionCategory().getTitle();
        this.description = parentCategory.getQuestionCategory().getDescription();
        this.logo = parentCategory.getQuestionCategory().getLogo();
        this.isSubscribed = isSubscribed;

        this.subcategories = subcategories;
        this.countActivities = parentCategory.getCountActivities() +
            subcategories.stream().map(QuestionCategoryDto::getCountActivities).mapToInt(Integer::intValue).sum();
    }
}
