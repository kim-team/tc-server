/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.praxis.data.entity.PpCompletion;
import de.thm.kim.tc.app.praxis.data.repository.PpCompletionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PpCompletionService {

    private final PpCompletionRepository ppCompletionRepository;

    @Autowired
    public PpCompletionService(PpCompletionRepository ppCompletionRepository) {
        this.ppCompletionRepository = ppCompletionRepository;
    }

    public List<PpCompletion> getAllCompletions() {
        return ppCompletionRepository.findAll();
    }

    public Optional<PpCompletion> getCompletionById(Long completionId) {
        return ppCompletionRepository.findById(completionId);
    }

    public List<PpCompletion> getCompletionsByPhaseId(Long phaseId) {
        return ppCompletionRepository.findAllByPhaseId(phaseId);
    }

    public PpCompletion saveOrUpdateCompletion(PpCompletion completion) {
        return ppCompletionRepository.save(completion);
    }

    @Transactional
    public void deleteCompletionById(Long completionId) {
        ppCompletionRepository.deleteById(completionId);
    }

    @Transactional
    public void deleteCompletionForUser(User user) {
        ppCompletionRepository.deleteAllByUserId(user.getId());
    }
}
