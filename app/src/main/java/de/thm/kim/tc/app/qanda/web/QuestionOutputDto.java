/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.web;

import de.thm.kim.tc.app.qanda.data.entity.Question;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
class QuestionOutputDto {

    private Long id;

    private String category;

    private AuthorDto author;

    private String text;

    private Date createdAt;

    private Date modifiedAt;

    private int numberOfAnswers;

    private AnswerOutputDto helpfulAnswer;

    private List<AnswerOutputDto> answers;

    private int views;

    private int likes;

    private boolean isLiked;

    private boolean canEdit;

    boolean isReported;

    /**
     * Constructor for newly created {@link Question} objects with no views,
     * {@link de.thm.kim.tc.app.qanda.data.entity.Answer}s or likes
     *
     * @param question {@link Question} in question
     * @param author   question author and {@link de.thm.kim.tc.app.qanda.data.entity.Flair}s
     */
    QuestionOutputDto(Question question, AuthorDto author) {
        this(question, author, 0, null, 0, 0, false, true, false);
    }

    /**
     * Constructor for {@link Question} overviews that includes the helpful
     * {@link de.thm.kim.tc.app.qanda.data.entity.Answer}, number of answers, views and likes
     *
     * <p>N.B.: This constructor takes {@code numberOfAnswers} but no {@code answers} parameter, allowing states where
     * {@code numberOfAnswers != answers.size()}. It is meant for the 'my questions' route and overviews only!</p>
     *
     * @param question        {@link Question} in question
     * @param author          question author and {@link de.thm.kim.tc.app.qanda.data.entity.Flair}s
     * @param views           number of unique views
     * @param helpfulAnswer   helpful {@link de.thm.kim.tc.app.qanda.data.entity.Answer} as {@link AnswerOutputDto}
     * @param numberOfAnswers number of answers
     * @param likes           number of likes
     * @param isLiked         {@code true} if the current user has liked the {@link Question}
     * @param canEdit         {@code true} if the current user is allowed to edit the {@link Question}
     */
    QuestionOutputDto(Question question, AuthorDto author, int views, AnswerOutputDto helpfulAnswer, int numberOfAnswers, int likes, boolean isLiked, boolean canEdit, boolean isReported) {
        this(question, author, views, helpfulAnswer, new ArrayList<>(), likes, isLiked, canEdit, isReported);
        this.numberOfAnswers = numberOfAnswers;
    }

    /**
     * Constructor for individual {@link Question} objects with all {@link de.thm.kim.tc.app.qanda.data.entity.Answer}s
     * (including the helpful answer), views and likes
     *
     * @param question      {@link Question} in question
     * @param author        question author and {@link de.thm.kim.tc.app.qanda.data.entity.Flair}s
     * @param views         number of unique views
     * @param helpfulAnswer helpful {@link de.thm.kim.tc.app.qanda.data.entity.Answer} as {@link AnswerOutputDto}
     * @param answers       all answers to the {@link Question}, sorted from newest to oldest
     * @param likes         number of likes
     * @param isLiked       {@code true} if the current user has liked the {@link Question}
     * @param canEdit       {@code true} if the current user is allowed to edit the {@link Question}
     */
    QuestionOutputDto(Question question, AuthorDto author, int views, AnswerOutputDto helpfulAnswer, List<AnswerOutputDto> answers, int likes, boolean isLiked, boolean canEdit, boolean isReported) {
        this.id = question.getId();
        this.category = question.getCategory().getTitle();
        this.author = author;
        this.text = question.getText();
        this.createdAt = question.getDate();
        this.modifiedAt = question.getModifiedAt();
        this.numberOfAnswers = answers.size();
        this.helpfulAnswer = helpfulAnswer;
        this.answers = answers;
        this.views = views;
        this.likes = likes;
        this.isLiked = isLiked;
        this.canEdit = canEdit;
        this.isReported = isReported;
    }
}
