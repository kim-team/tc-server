/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.service;

import de.thm.kim.tc.app.cafeteria.data.entity.Cafeteria;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaClosureRepository;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaEatingHoursRepository;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaOpeningHoursRepository;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaRepository;
import de.thm.kim.tc.app.common.exceptions.ResourceAlreadyExistsException;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CafeteriaService implements HealthIndicator {

    private final CafeteriaRepository cafeteriaRepository;
    private final CafeteriaOpeningHoursRepository cafeteriaOpeningHoursRepository;
    private final CafeteriaEatingHoursRepository cafeteriaEatingHoursRepository;
    private final CafeteriaClosureRepository cafeteriaClosureRepository;

    @Autowired
    public CafeteriaService(CafeteriaRepository cafeteriaRepository,
                            CafeteriaOpeningHoursRepository cafeteriaOpeningHoursRepository,
                            CafeteriaEatingHoursRepository cafeteriaEatingHoursRepository,
                            CafeteriaClosureRepository cafeteriaClosureRepository) {
        this.cafeteriaRepository = cafeteriaRepository;
        this.cafeteriaOpeningHoursRepository = cafeteriaOpeningHoursRepository;
        this.cafeteriaEatingHoursRepository = cafeteriaEatingHoursRepository;
        this.cafeteriaClosureRepository = cafeteriaClosureRepository;
    }

    @Override
    public Health health() {
        Map<String, String> details = new HashMap<>();

        if (cafeteriaOpeningHoursRepository.count() <= 0)
            details.put("openingHours", "No opening hours available");

        if (cafeteriaEatingHoursRepository.count() <= 0)
            details.put("eatingHours", "No eating hours available");

        if (cafeteriaClosureRepository.count() <= 0)
            details.put("closures", "No closures available");

        if (cafeteriaRepository.count() <= 0) {
            details.put("cafeterias", "No cafeterias available");
            return Health.down().withDetails(details).build();
        }

        return Health.up().withDetails(details).build();
    }

    public List<Cafeteria> getAllCafeterias() {
        return cafeteriaRepository.findAll();
    }

    public Cafeteria getAllCafeterias(Long cafeteriaId) {
        return cafeteriaRepository.findById(cafeteriaId)
            .orElseThrow(() -> new ResourceNotFoundException("No Cafeteria found for Cafeteria name: " + cafeteriaId));
    }

    public void createNewCafeteria(String name, String news, String description, String image, String latitude, String longitude) {
        if (cafeteriaRepository.findByName(name).isPresent()) {
            throw new ResourceAlreadyExistsException("There is already a Cafeteria called: " + name);
        }
        cafeteriaRepository.save(new Cafeteria(name, news, description, image, latitude, longitude));
    }

    public void updateCafeteria(Long id, String name, String news, String description, String image, String latitude, String longitude) {
        Cafeteria cafeteria = cafeteriaRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No Cafeteria found for Cafeteria name: " + id));
        cafeteria.setName(name);
        cafeteria.setNews(news);
        cafeteria.setDescription(description);
        cafeteria.setImage(image);
        cafeteria.setLatitude(latitude);
        cafeteria.setLongitude(longitude);
        cafeteriaRepository.save(cafeteria);
    }

    @Transactional
    public void deleteCafeteria(Long cafeteriaId) {
        cafeteriaOpeningHoursRepository.deleteAllByCafeteriaId(cafeteriaId);
        cafeteriaEatingHoursRepository.deleteAllByCafeteriaId(cafeteriaId);
        cafeteriaClosureRepository.deleteAllByCafeteriaId(cafeteriaId);
        cafeteriaRepository.deleteById(cafeteriaId);
    }
}
