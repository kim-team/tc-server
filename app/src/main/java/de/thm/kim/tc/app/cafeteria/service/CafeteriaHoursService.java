/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.service;

import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaClosure;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaEatingHours;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaOpeningHours;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaClosureRepository;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaEatingHoursRepository;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaOpeningHoursRepository;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaRepository;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.DayOfWeek;
import java.time.OffsetTime;

@Service
@Transactional
public class CafeteriaHoursService {

    private final CafeteriaRepository cafeteriaRepository;
    private final CafeteriaOpeningHoursRepository cafeteriaOpeningHoursRepository;
    private final CafeteriaEatingHoursRepository cafeteriaEatingHoursRepository;
    private final CafeteriaClosureRepository cafeteriaClosureRepository;

    @Autowired
    public CafeteriaHoursService(CafeteriaRepository cafeteriaRepository,
                                 CafeteriaOpeningHoursRepository cafeteriaOpeningHoursRepository,
                                 CafeteriaEatingHoursRepository cafeteriaEatingHoursRepository,
                                 CafeteriaClosureRepository cafeteriaClosureRepository) {
        this.cafeteriaRepository = cafeteriaRepository;
        this.cafeteriaOpeningHoursRepository = cafeteriaOpeningHoursRepository;
        this.cafeteriaEatingHoursRepository = cafeteriaEatingHoursRepository;
        this.cafeteriaClosureRepository = cafeteriaClosureRepository;
    }

    /**
     * Opening
     */
    public Iterable<CafeteriaOpeningHours> getCafeteriaOpeningHours(Long cafeteriaId) {
        return cafeteriaOpeningHoursRepository.findAllByCafeteriaId(cafeteriaId);
    }

    public void addCafeteriaOpeningHours(Long cafeteriaId, DayOfWeek dayOfWeek, OffsetTime open, OffsetTime close) {
        if (cafeteriaRepository.findById(cafeteriaId).isEmpty()) {
            throw new ResourceNotFoundException("No Cafeteria found for Cafeteria ID: " + cafeteriaId);
        }
        cafeteriaOpeningHoursRepository.save(new CafeteriaOpeningHours(cafeteriaId, dayOfWeek, open, close));
    }

    public void updateCafeteriaOpeningHours(Long id, DayOfWeek dayOfWeek, OffsetTime open, OffsetTime close) {
        CafeteriaOpeningHours cafeteriaOpeningHours = cafeteriaOpeningHoursRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No CafeteriaOpeningHours found for CafeteriaOpeningHours ID: " + id));
        cafeteriaOpeningHours.setDayOfWeek(dayOfWeek);
        cafeteriaOpeningHours.setOpen(open);
        cafeteriaOpeningHours.setClose(close);
        cafeteriaOpeningHoursRepository.save(cafeteriaOpeningHours);
    }

    public void deleteCafeteriaOpeningHours(Long id) {
        cafeteriaOpeningHoursRepository.deleteById(id);
    }

    /**
     * Eating
     */
    public Iterable<CafeteriaEatingHours> getCafeteriaEatingHours(Long cafeteriaId) {
        return cafeteriaEatingHoursRepository.findAllByCafeteriaId(cafeteriaId);
    }

    public void addCafeteriaEatingHours(Long cafeteriaId, DayOfWeek dayOfWeek, OffsetTime open, OffsetTime close) {
        if (cafeteriaRepository.findById(cafeteriaId).isEmpty()) {
            throw new ResourceNotFoundException("No Cafeteria found for Cafeteria ID: " + cafeteriaId);
        }
        cafeteriaEatingHoursRepository.save(new CafeteriaEatingHours(cafeteriaId, dayOfWeek, open, close));
    }

    public void updateCafeteriaEatingHours(Long id, DayOfWeek dayOfWeek, OffsetTime open, OffsetTime close) {
        CafeteriaEatingHours cafeteriaEatingHours = cafeteriaEatingHoursRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No CafeteriaEatingHours found for CafeteriaEatingHours ID: " + id));
        cafeteriaEatingHours.setDayOfWeek(dayOfWeek);
        cafeteriaEatingHours.setOpen(open);
        cafeteriaEatingHours.setClose(close);
        cafeteriaEatingHoursRepository.save(cafeteriaEatingHours);
    }

    public void deleteCafeteriaEatingHours(Long id) {
        cafeteriaEatingHoursRepository.deleteById(id);
    }

    /**
     * Closure
     */
    public Iterable<CafeteriaClosure> getAllCafeteriaClosures(Long cafeteriaId) {
        return cafeteriaClosureRepository.findAllByCafeteriaId(cafeteriaId);
    }

    public void addCafeteriaClosure(Long cafeteriaId, Date from, Date to) {
        if (cafeteriaRepository.findById(cafeteriaId).isEmpty()) {
            throw new ResourceNotFoundException("No Cafeteria found for Cafeteria ID: " + cafeteriaId);
        }
        cafeteriaClosureRepository.save(new CafeteriaClosure(cafeteriaId, from, to));
    }

    public void updateCafeteriaClosure(Long id, Date from, Date to) {
        CafeteriaClosure cafeteriaClosure = cafeteriaClosureRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("No CafeteriaClosure found for CafeteriaClosure ID: " + id));
        cafeteriaClosure.setFrom(from);
        cafeteriaClosure.setTo(to);
        cafeteriaClosureRepository.save(cafeteriaClosure);
    }

    public void deleteCafeteriaClosure(Long id) {
        cafeteriaClosureRepository.deleteById(id);
    }
}
