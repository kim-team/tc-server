INSERT INTO permissions (username, role) VALUES ('admin', 'VERIFIED');

UPDATE authentication_user SET email = 'admin'                  where username = 'admin';
UPDATE authentication_user SET email = 'user'                   where username = 'user';
UPDATE authentication_user SET email = 'jane_doe'               where username = 'jane_doe';
UPDATE authentication_user SET email = 'otto_normalverbraucher' where username = 'otto_normalverbraucher';
