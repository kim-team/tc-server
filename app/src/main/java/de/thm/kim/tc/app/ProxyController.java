/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app;

import lombok.extern.java.Log;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;


@Log
@RestController
public class ProxyController implements HealthIndicator {

    @Value("${tinycampus.proxy.organizer-query-parameter-fix}")
    private String queryParamFix;

    @Value("${tinycampus.proxy.target-url:}")
    private String proxyUrl;

    @Override
    public Health health() {
        if (proxyUrl.isBlank())
            return Health.down().build();

        return Health.up()
            .withDetail("url", proxyUrl)
            .build();
    }

    /*
     * Hacky proxy route for GET requests
     *
     * Usage:
     *   GET /proxy/path?query returns GET $PROXY_URL/path?query
     *
     * Supported:
     *   - GET requests
     *   - Query parameters
     *
     * Not supported:
     *   - Non-GET requests
     *   - HTTP headers
     *   - HTTP status codes
     *   - Fragments (internal links with #)
     */
    @GetMapping("/proxy/**")
    public ResponseEntity<String> proxyOrganizer(HttpServletRequest request) {

        if (proxyUrl.isBlank())
            return new ResponseEntity<>("Service unavailable",
                HttpStatus.SERVICE_UNAVAILABLE);

        val uri = getUri(request);
        val restTemplate = new RestTemplate();
        val response = restTemplate
            .exchange(uri, HttpMethod.GET, null, String.class);

        log.info(String.format("Proxy request performed for URL '%s'",
            uri.toString()));

        return new ResponseEntity<>(response.getBody(),
            response.getStatusCode());
    }

    private URI getUri(HttpServletRequest request) {
        val path = (String) request.getAttribute(
            HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        val bestMatchPattern = (String) request.getAttribute(
            HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        val apm = new AntPathMatcher();
        val finalPath = apm.extractPathWithinPattern(bestMatchPattern, path);

        val prefix = proxyUrl.endsWith("/") ? proxyUrl : proxyUrl + "/";
        var queryString = request.getQueryString();

        if (request.getQueryString() == null) {
            return URI.create(prefix + finalPath);
        }

        // FIXME Fix in app
        // Add additional query parameter to organizer requests, e.g.
        // '&date=2021-10-04' needed by new API as a temporary workaround
        if (!queryParamFix.isBlank()) {
            if (queryString.toLowerCase().contains("view=category_units"))
                queryString += "&" + queryParamFix;
        }

        return URI.create(prefix + finalPath + "?" + queryString);
    }
}
