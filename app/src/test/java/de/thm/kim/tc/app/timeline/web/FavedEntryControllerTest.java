/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelineLike;
import de.thm.kim.tc.app.timeline.service.TimelinePinService;
import de.thm.kim.tc.app.timeline.service.TimelineReportService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import de.thm.kim.tc.app.timeline.service.UserTimelineFaveService;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class FavedEntryControllerTest {
    private static final String FAVED_ENTRIES_URL = "/timeline/faved";

    private static final Long USER_ID = 1L;
    private static final Long ENTITY_ID = 1L;
    private static final Long INVALID_ENTRY_ID = 99L;

    private static final int PAGE_NUMBER = 0;
    private static final int PAGE_SIZE = 20;

    private final Pageable pageRequest = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);

    private static final String VALID_USERNAME = "admin";

    @SpyBean
    private TimelinePinService timelinePinService;

    @SpyBean
    private TimelineService timelineService;

    @SpyBean
    private UserService userService;

    @SpyBean
    private TimelineReportService timelineReportService;

    @SpyBean
    private UserTimelineFaveService userTimelineFaveService;

    @Autowired
    private MockMvc mockMvc;

    @WithMockUser(VALID_USERNAME)
    @Test
    public void faves_by_user() throws Exception {
        User user = userService.getUserById(USER_ID);
        Page<TimelineEntry> page = userTimelineFaveService.getFavesForUserAsPage(user, pageRequest);
        Collection<TimelineEntry> pins = timelinePinService.getAllTimelinePinsByUser(user);

        List<TimelineEntryDto> timelineEntries = page.getContent()
            .stream()
            .map(timelineEntry -> {
                Collection<TimelineLike> likes = timelineService.getTimelineLikesForTimelineEntry(timelineEntry);

                return new TimelineEntryDto(timelineEntry,
                    true,
                    pins.contains(timelineEntry),
                    likes.size(),
                    likes.stream().anyMatch(value -> value.getUser().equals(user)),
                    false);
            })
            .collect(Collectors.toList());

        Page<TimelineEntryDto> timelineEntriesDto = new PageImpl<>(timelineEntries, pageRequest, page.getTotalElements());

        mockMvc.perform(get(FAVED_ENTRIES_URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.number", is(PAGE_NUMBER)))
            .andExpect(jsonPath("$.size", is(PAGE_SIZE)))
            .andExpect(jsonPath("$.totalElements", is((int) timelineEntriesDto.getTotalElements())));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void fave_timeline_entry() throws Exception {
        mockMvc.perform(post(FAVED_ENTRIES_URL + "/" + ENTITY_ID))
            .andExpect(status().isOk());
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_to_fave_invalid_entry() throws Exception {
        mockMvc.perform(post(FAVED_ENTRIES_URL + "/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No TimelineEntry found for ID: " + INVALID_ENTRY_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void delete_timeline_faves() throws Exception {
        mockMvc.perform(delete(FAVED_ENTRIES_URL + "/" + ENTITY_ID))
            .andExpect(status().isOk());

        // HTTP 404 on second call
        mockMvc.perform(delete(FAVED_ENTRIES_URL + "/" + ENTITY_ID))
            .andExpect(status().isNotFound());
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void should_fail_on_invalid_entry() throws Exception {
        mockMvc.perform(delete(FAVED_ENTRIES_URL + "/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No TimelineEntry found for ID: " + INVALID_ENTRY_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));
    }
}
