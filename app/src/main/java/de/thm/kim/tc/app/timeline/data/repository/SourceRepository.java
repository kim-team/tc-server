/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface SourceRepository extends CrudRepository<Source, Long> {
    Collection<Source> findAll();

    Collection<Source> findAllByParentIsNull();

    @Query(value = "SELECT source FROM Source source \n" +
        "LEFT JOIN UserSource userSource ON ( source.id = userSource.source.id AND userSource.id.userId = :#{#user.id} ) \n" +
        "WHERE ( \n" +
        "   source.campus = :#{#user.campus} OR \n" +                   // Default Campus subscribed sources
        "   source.department = :#{#user.program.department} OR \n" +   // Default Department subscribed sources
        "   source.subscriptionType = 'SUBSCRIBED' OR \n" +             // Default subscribed sources
        "   userSource.subscriptionType = 'SUBSCRIBED' \n" +            // Custom User subscribed sources (whitelist)
        ") AND ( \n" +
        "   userSource.subscriptionType = 'SUBSCRIBED' OR \n" +         // Custom User unsubscribed sources (blacklist)
        "   userSource.subscriptionType IS NULL \n" +
        ")")
    Collection<Source> findAllByUser(@Param("user") User user);

    Optional<Source> findByName(String name);
}
