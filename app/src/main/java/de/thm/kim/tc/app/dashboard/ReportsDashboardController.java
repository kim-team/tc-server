/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.dashboard;

import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.common.reports.Report;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.common.reports.Reportable;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping(value = "/dashboard/reports")
public class ReportsDashboardController implements WebMvcConfigurer {

    private final ReportService<Answer> answerReportService;
    private final ReportService<CafeteriaComment> cafeteriaCommentReportService;
    private final ReportService<Question> questionReportService;
    private final ReportService<TimelineEntry> timelineReportService;

    @Autowired
    public ReportsDashboardController(ReportService<Answer> answerReportService,
                                      ReportService<CafeteriaComment> cafeteriaCommentReportService,
                                      ReportService<Question> questionReportService,
                                      ReportService<TimelineEntry> timelineReportService) {
        this.answerReportService = answerReportService;
        this.cafeteriaCommentReportService = cafeteriaCommentReportService;
        this.questionReportService = questionReportService;
        this.timelineReportService = timelineReportService;
    }

    private <T extends Reportable> List<ReportDto<?, ?>> convertReports(Map<T, List<Report<T>>> reports) {
        return reports.keySet().stream()
            .map(k -> {
                val first = reports.get(k).stream()
                    // FIXME optional?
                    .min(Comparator.comparing(Report::getReportDate)).get();

                return new ReportDto<>(
                    first,
                    k,
                    k.getClass().getSimpleName(),
                    reports.get(k).size(),
                    first.getReportDate());
            })
            .collect(Collectors.toList());
    }

    private <T extends Reportable> List<ReportDto<?, ?>> getReports(ReportService<T> service) {
        return convertReports(service.getAllReportsByItem());
    }

    @GetMapping
    public String showReportedContents(Model model) {

        List<ReportDto<?, ?>> reportedItems = Stream
            .of(getReports(answerReportService),
                getReports(cafeteriaCommentReportService),
                getReports(questionReportService),
                getReports(timelineReportService))
            .flatMap(Collection::stream)
            .sorted(Comparator.comparing(
                ReportDto::getFirstReported,
                Comparator.reverseOrder()))
            .collect(Collectors.toList());

        model.addAttribute("title", "Reports");
        model.addAttribute("reports", reportedItems);
        model.addAttribute("classActiveReports", "active");

        return "reports";
    }

    @PostMapping
    public String deleteReports(String type, Long id) {
        switch (type) {
            case "Answer":
                answerReportService.removeAllReportsById(id);
                break;
            case "CafeteriaComment":
                cafeteriaCommentReportService.removeAllReportsById(id);
                break;
            case "Question":
                questionReportService.removeAllReportsById(id);
                break;
            case "TimelineEntry":
                timelineReportService.removeAllReportsById(id);
                break;
            default:
                throw new IllegalArgumentException("Invalid report type: " + type);
        }

        return "redirect:/dashboard/reports";
    }
}
