/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.repository.SourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class SourceServiceTest {

    private static final Long SOURCE_ID = 1L;
    private static final Long USER_ID = 1L;

    @MockBean
    private SourceRepository sourceRepositoryMock;

    private SourceService service;

    @SpyBean
    private UserService userService;

    @Mock
    private Collection<Source> sourcesMock;

    @Mock
    private Source sourceMock;

    @BeforeEach
    public void setUp() {
        service = new SourceService(sourceRepositoryMock);
    }

    @Test
    public void get_all_sources_by_user_id() {
        User user = userService.getUserById(USER_ID);

        when(sourceRepositoryMock.findAllByUser(user)).thenReturn(sourcesMock);

        assertThat(service.getAllSourcesByUser(user), is(sourcesMock));
    }

    @Test
    public void get_all_subscribed_sources_by_user_id() {
        User user = userService.getUserById(USER_ID);
        List<Source> sourceMocks = List.of(sourceMock);

        when(sourceRepositoryMock.findAllByUser(user)).thenReturn((sourceMocks));
        when(sourceMock.getParent()).thenReturn(sourceMock);
        when(sourceMock.getAllParents()).thenReturn(sourceMocks);

        assertThat(service.getAllSubscribedSourcesByUser(user), is(List.of(sourceMock)));
        verify(sourceMock).getParent();
        verify(sourceMock).getAllParents();
    }


    @Test
    public void get_source_by_id() {
        when(sourceRepositoryMock.findById(SOURCE_ID)).thenReturn(Optional.of(sourceMock));

        assertThat(service.getSourceById(SOURCE_ID), is(sourceMock));
    }
}
