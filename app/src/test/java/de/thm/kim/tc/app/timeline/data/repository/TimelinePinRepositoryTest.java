/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.UserRepository;
import de.thm.kim.tc.app.timeline.data.entity.TimelinePin;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TimelinePinRepositoryTest {

    private static final Long USER_ID = 1L;
    private static final Long USER2_ID = 2L;

    @Autowired
    private TimelinePinRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void find_all() {
        Collection<TimelinePin> timelinePins = repository.findAll();
        assertFalse(timelinePins.isEmpty());
    }

    @Test
    public void find_all_by_user() {
        User user = userRepository.findById(USER_ID).orElseThrow();

        Collection<TimelinePin> timelinePins = repository.findAllByUser(user);
        assertFalse(timelinePins.isEmpty());

        User user2 = userRepository.findById(USER2_ID).orElseThrow();
        Collection<TimelinePin> timelinePins2 = repository.findAllByUser(user2);

        assertNotEquals(timelinePins, timelinePins2);
    }
}
