/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.UserRepository;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class SourceRepositoryTest {

    private static final Long USER1_ID = 1L;
    private static final Long USER2_ID = 2L;

    @Autowired
    private SourceRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void find_all() {
        Collection<Source> sources = repository.findAll();
        assertFalse(sources.isEmpty());
    }

    @Test
    public void find_all_by_user() {
        Optional<User> user1 = userRepository.findById(USER1_ID);
        Optional<User> user2 = userRepository.findById(USER2_ID);

        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());

        Collection<Source> sourcesUser1 = repository.findAllByUser(user1.get());
        assertFalse(sourcesUser1.isEmpty());

        Collection<Source> sourcesUser2 = repository.findAllByUser(user2.get());
        assertFalse(sourcesUser2.isEmpty());
    }
}
