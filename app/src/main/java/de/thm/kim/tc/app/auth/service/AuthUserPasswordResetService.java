/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.entity.AuthUserResetPassword;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserResetPasswordRepository;
import de.thm.kim.tc.app.auth.event.OnUserPasswordResetEvent;
import de.thm.kim.tc.app.common.exceptions.InvalidRecoveryException;
import de.thm.kim.tc.app.common.exceptions.SendMailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
public class AuthUserPasswordResetService {

    private final ApplicationEventPublisher eventPublisher;
    private final AuthUserRepository authUserRepository;
    private final AuthUserResetPasswordRepository authUserResetPasswordRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    @Autowired
    public AuthUserPasswordResetService(final ApplicationEventPublisher eventPublisher,
                                        final AuthUserRepository authUserRepository,
                                        final AuthUserResetPasswordRepository authUserResetPasswordRepository,
                                        final PasswordEncoder passwordEncoder,
                                        final UserService userService) {
        this.eventPublisher = eventPublisher;
        this.authUserRepository = authUserRepository;
        this.authUserResetPasswordRepository = authUserResetPasswordRepository;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    private void sendResetPasswordMail(String displayName, String email) {
        try {
            eventPublisher.publishEvent(new OnUserPasswordResetEvent(displayName, email));
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new SendMailException("They couldn't send your registration mail!");
        }
    }

    public String createPasswordResetToken(String email) {
        Date createdAt = new Date();
        String token = UUID.randomUUID().toString();

        AuthUserResetPassword authUserResetPassword = authUserResetPasswordRepository
            .findById(email)
            .orElse(new AuthUserResetPassword(email, token, createdAt));

        authUserResetPassword.setCreatedAt(createdAt);
        authUserResetPassword.setToken(token);
        authUserResetPasswordRepository.save(authUserResetPassword);

        return token;
    }

    public void createResetPasswordRequest(String email) {
        AuthUser authUser = authUserRepository.findByEmail(email)
            .orElseThrow(() -> new UsernameNotFoundException("There is no user with that email!"));
        User user = userService.getUserByUsername(authUser.getUsername());

        sendResetPasswordMail(user.getName(), email);
    }

    @Transactional
    public void resetPassword(String email, String token, String newPassword) {
        // n.b. German error messages are displayed on the website
        AuthUser authUser = authUserRepository.findByEmail(email)
            .orElseThrow(() -> new IllegalArgumentException("Ungültige E-Mail-Adresse"));

        if (!authUserResetPasswordRepository.existsByEmailAndToken(email, token))
            throw new InvalidRecoveryException("Ungültige Passwort-Reset-URL");

        // TODO Change AuthUserService.changePassword
        authUser.setPassword(passwordEncoder.encode(newPassword));
        authUserRepository.save(authUser);

        authUserResetPasswordRepository.deleteById(authUser.getEmail());
    }
}
