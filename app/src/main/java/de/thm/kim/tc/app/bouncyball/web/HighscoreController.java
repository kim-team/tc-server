/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.bouncyball.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.bouncyball.data.entity.Highscore;
import de.thm.kim.tc.app.bouncyball.service.HighscoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Api(description = "Everything about Bouncy Ball highscores", tags = {"Bouncy Ball - Highscores"})
@RestController
@RequestMapping(path = "/bouncyball/highscore")
public class HighscoreController {

    private static final HighscoreOutputDto BLANK_HIGHSCORE = new HighscoreOutputDto("BLANK", -1L, false);

    private final HighscoreService highscoreService;
    private final UserService userService;

    @Autowired
    public HighscoreController(HighscoreService highscoreService,
                               UserService userService) {
        this.highscoreService = highscoreService;
        this.userService = userService;
    }

    @ApiOperation(value = "Get global, recent (last 24 h) and personal highscores sorted by score. " +
        "Personal 'name' and 'score' are set to 'BLANK' and -1 if no personal best is available. " +
        "The 'isSelf' flag marks highscores submitted by the current user.",
        tags = {"Bouncy Ball - Highscores"})
    @GetMapping
    public HighscoreOverviewDto getHighscores(Principal principal) {
        val user = userService.getUserByPrincipal(principal);
        val global = highscoreService.getGlobalHighscores().stream()
            .map(h -> new HighscoreOutputDto(h, user))
            .collect(Collectors.toList());
        val recent = highscoreService.getRecentHighscores().stream()
            .map(h -> new HighscoreOutputDto(h, user))
            .collect(Collectors.toList());
        val personal = user
            .map(u -> highscoreService.getPersonalHighscore(u)
                .map(h -> new HighscoreOutputDto(h, user))
                .orElse(BLANK_HIGHSCORE))
            .orElse(BLANK_HIGHSCORE);

        return new HighscoreOverviewDto(global, recent, personal);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Submit new highscore for current user. 'name' must be between 1 and 5 characters.",
        tags = {"Bouncy Ball - Highscores"})
    @PostMapping
    public void submitHighscore(@Valid @RequestBody HighscoreInputDto input, Principal principal) {
        val user = userService.getUserByUsername(principal.getName());

        highscoreService.submitHighscore(user, input.getName().toUpperCase(), input.getScore());
    }

    @Value
    private static class HighscoreOverviewDto {

        List<HighscoreOutputDto> global;

        List<HighscoreOutputDto> recent;

        HighscoreOutputDto personal;
    }

    @NoArgsConstructor
    @Getter
    private static class HighscoreInputDto {

        @NotBlank
        @Size(min = 1, max = 5)
        private String name;

        @NotNull
        private Long score;
    }

    @AllArgsConstructor
    @Value
    private static class HighscoreOutputDto {

        String name;

        Long score;

        Boolean isSelf;

        public HighscoreOutputDto(Highscore highscore, Optional<User> user) {
            this.name = highscore.getName();
            this.score = highscore.getScore();
            this.isSelf = user
                .map(u -> u.getId().equals(highscore.getUserId()))
                .orElse(false);
        }
    }
}
