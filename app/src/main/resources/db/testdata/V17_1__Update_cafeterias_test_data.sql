update cafeterias
set description = 'Beschreibung Mensa OBS',
    image = '',
    latitude = 50.576856,
    longitude = 8.6880931
where cafeteria_id = 1;

update cafeterias
set description = 'Beschreibung Mensa THM Gießen',
    image = '',
    latitude = 50.587008,
    longitude = 8.6812831
where cafeteria_id = 2;

update cafeterias
set description = 'Beschreibung Mensa THM Friedberg',
    image = '',
    latitude = 50.3300201,
    longitude = 8.7559177
where cafeteria_id = 3;

update cafeterias
set description = 'Beschreibung Hochschule Fulda',
    image = '',
    latitude = 50.5653916,
    longitude = 9.6848248
where cafeteria_id = 4;
