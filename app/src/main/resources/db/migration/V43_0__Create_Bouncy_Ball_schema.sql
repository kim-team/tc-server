CREATE TABLE highscores (
    highscore_id BIGINT      GENERATED BY DEFAULT AS IDENTITY,
    user_id      BIGINT      NOT NULL,
    name         VARCHAR(20) NOT NULL,
    score        BIGINT      NOT NULL,
    created_at   TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE,
    PRIMARY KEY (highscore_id)
)
