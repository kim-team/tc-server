/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.account.data.entity.Homescreen;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Value
@AllArgsConstructor
class HomescreenDto {

    Long userId;
    List<Integer> order;

    HomescreenDto(Homescreen homescreen) {
        userId = homescreen.getUser().getId();

        order = Arrays
            .stream(homescreen.getCardsOrder()
                .substring(1, homescreen.getCardsOrder().length() - 1)
                .split(","))
            .map(s -> Integer.parseInt(s.trim()))
            .collect(Collectors.toList());
    }
}
