/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpPerson;
import de.thm.kim.tc.app.praxis.data.repository.PpPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PpPersonService {

    private final PpAddressService ppAddressService;
    private final PpPersonRepository ppPersonRepository;

    @Autowired
    public PpPersonService(PpAddressService ppAddressService,
                           PpPersonRepository ppPersonRepository) {
        this.ppAddressService = ppAddressService;
        this.ppPersonRepository = ppPersonRepository;
    }

    public List<PpPerson> getPersonsForPhaseId(Long phaseId) {
        return ppPersonRepository.findAllByPhaseId(phaseId);
    }

    public List<PpPerson> getAllPersons() {
        return ppPersonRepository.findAll();
    }

    public Optional<PpPerson> getPersonById(Long id) {
        return ppPersonRepository.findById(id);
    }

    @Transactional
    public PpPerson createOrUpdatePerson(PpPerson person) {
        return ppPersonRepository.save(person);
    }

    @Transactional
    public void deletePersonById(Long personId) {
        if (ppPersonRepository.existsById(personId)) {
            ppAddressService.getAddressesForPersonId(personId)
                .forEach(a -> ppAddressService.deleteAddressById(a.getAddressId()));
            ppPersonRepository.deleteById(personId);
        } else {
            throw new ResourceNotFoundException("No Person found for ID: " + personId);
        }
    }
}
