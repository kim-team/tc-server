/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.data.repository;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelinePin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TimelinePinRepository extends CrudRepository<TimelinePin, Long> {
    Collection<TimelinePin> findAll();

    @Query(value = "SELECT timelinePin FROM TimelinePin timelinePin \n" +
        "LEFT JOIN TimelineEntry timelineEntry ON timelinePin.timelineEntry.id = timelineEntry.id \n" +
        "LEFT JOIN UserSource userSource ON ( timelineEntry.source.id = userSource.source.id AND userSource.id.userId = :#{#user.id} ) \n" +
        "LEFT JOIN UserTimelineUnpin userTimelineUnpin ON ( userTimelineUnpin.id.userId = :#{#user.id} AND userTimelineUnpin.id.timelineEntryId = timelineEntry.id ) \n" +
        "LEFT JOIN UserTimelineHide userTimelineHide ON ( userTimelineHide.id.userId = :#{#user.id} AND userTimelineHide.id.timelineEntryId = timelineEntry.id ) \n" +
        "WHERE ( \n" +
        "   timelineEntry.source.campus = :#{#user.campus} OR \n" +                     // Default Campus subscribed sources
        "   timelineEntry.source.department = :#{#user.program.department} OR \n" +     // Default Department subscribed sources
        "   timelineEntry.source.subscriptionType = 'SUBSCRIBED' OR \n" +               // Default subscribed sources
        "   userSource.subscriptionType = 'SUBSCRIBED' \n" +                            // Custom User subscribed sources (whitelist)
        ") AND ( \n" +
        "   userSource.subscriptionType = 'SUBSCRIBED' OR \n" +                         // Custom User unsubscribed sources (blacklist)
        "   userSource.subscriptionType IS NULL \n" +
        ") AND ( \n" +
        "   userTimelineUnpin.timelineEntry != timelineEntry OR \n" +                   // Custom User unpinned (blacklist)
        "   userTimelineUnpin.timelineEntry IS NULL" +
        ") AND ( \n" +
        "   userTimelineHide.timelineEntry != timelineEntry OR \n" +                    // Custom User hide (blacklist)
        "   userTimelineHide.timelineEntry IS NULL" +
        ")")
    Collection<TimelinePin> findAllByUser(@Param("user") User user);

    void deleteByTimelineEntry(TimelineEntry entry);
}
