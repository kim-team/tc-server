/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.web;

import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.service.AuthUserService;
import de.thm.kim.tc.app.auth.service.AuthUserVerificationService;
import de.thm.kim.tc.app.common.exceptions.InvalidUpgradeException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping(path = "/oauth")
public class AccountVerificationController {

    private final AuthUserVerificationService authUserVerificationService;
    private final AuthUserRepository authUserRepository;
    private final AuthUserService authUserService;

    @Autowired
    public AccountVerificationController(final AuthUserVerificationService authUserVerificationService,
                                         final AuthUserRepository authUserRepository,
                                         final AuthUserService authUserService) {
        this.authUserVerificationService = authUserVerificationService;
        this.authUserRepository = authUserRepository;
        this.authUserService = authUserService;
    }

    @Secured("ROLE_USER")
    @ResponseBody
    @PostMapping(path = "/upgrade")
    public void upgrade(Principal principal, @Valid @RequestBody UpgradeAccountDto data) {
        String email = data.getEmail().toLowerCase();

        AuthUser authUser = authUserRepository
            .findByUsername(principal.getName())
            .orElseThrow(() -> new UsernameNotFoundException("User not found!"));

        if (authUserRepository.findByEmail(email).isPresent())
            throw new InvalidUpgradeException("email_already_in_use");

        if (authUser.isVerified())
            throw new InvalidUpgradeException("User is already verified!");

        authUserVerificationService.sendVerificationMail(authUser.getUsername(), email);
        authUserService.changePassword(authUser.getUsername(), data.getPassword());
    }

    @NoArgsConstructor
    @Getter
    private static class UpgradeAccountDto {

        @Email
        @NotBlank
        @Size(max = 128)
        private String email;

        @Pattern(regexp = "^(?=.*\\d)(?=.*[a-zA-Z]).{8,64}$")
        @NotBlank
        private String password;
    }

    @GetMapping(path = "/verify")
    public String verify(@RequestParam Optional<String> email, @RequestParam Optional<String> token, Model model) {
        String title = "E-Mail-Adresse bestätigt";
        String message = "Registrierung erfolgreich abgeschlossen. Du kannst nun den Tab schließen und zur App zurückkehren.";
        String messageType = "serverMessage";

        if (email.isEmpty() || token.isEmpty()) {
            title = "Fehler";
            message = "Ungültiger Token oder E-Mail-Adresse";
            messageType = "serverError";
        } else {
            try {
                authUserVerificationService.verifyEmail(email.get().toLowerCase(), token.get());
            } catch (Exception e) {
                e.printStackTrace();
                title = "Fehler";
                message = e.getMessage();
                messageType = "serverError";
            }
        }

        model.addAttribute("title", title);
        model.addAttribute("message", message);
        model.addAttribute("messageType", messageType);

        return "verify";
    }
}
