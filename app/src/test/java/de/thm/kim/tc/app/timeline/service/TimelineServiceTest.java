/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.BlockService;
import de.thm.kim.tc.app.qanda.data.repository.TimelineQuestionRepository;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import de.thm.kim.tc.app.timeline.data.repository.TimelineEntryRepository;
import de.thm.kim.tc.app.timeline.data.repository.TimelineLikeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TimelineServiceTest {

    private static final Long TIMELINE_ENTRY_ID = 1L;

    private final static Set<Source> SOURCES = Set.of(
        new Source(7L, null, null, null, "", "", "", "", SubscriptionTypes.SUBSCRIBED, "")
    );
    private final static Set<TimelineEntry> HIDDEN_TIMELINE_ENTRIES = Set.of(
        new TimelineEntry(8L, null, "", "", null, null)
    );
    private final static Set<TimelineEntry> PINNED_TIMELINE_ENTRIES = Set.of(
        new TimelineEntry(9L, null, "", "", null, null)
    );

    private static final int PAGE_NUMBER = 0;
    private static final int PAGE_SIZE = 2;

    private TimelineService service;
    @Mock
    private Page<TimelineEntry> pageTimelineEntriesMock;
    @Mock
    private Page<TimelineEntry> pageTimelineEntriesHideMock;
    @Mock
    private Page<TimelineEntry> pageTimelineEntriesPinnedMock;
    @Mock
    private Page<TimelineEntry> pageTimelineEntriesHideAndPinnedMock;
    @Mock
    private TimelineEntry timelineEntryMock;

    @Mock
    private BlockService blockService;
    @Mock
    private SourceService sourceServiceMock;
    @Mock
    private TimelineEntryRepository timelineEntryRepositoryMock;
    @Mock
    private TimelinePinService timelinePinServiceMock;
    @Mock
    private TimelineQuestionRepository timelineQuestionRepository;
    @Mock
    private UserTimelineFaveService userTimelineFaveService;
    @Mock
    private UserTimelineHideService userTimelineHideServiceMock;
    @Mock
    private UserTimelineUnpinService userTimelineUnpinService;
    @Mock
    private TimelineLikeRepository timelineLikeRepository;

    @Mock
    private User user;
    @Mock
    private User userWithPinned;
    @Mock
    private User userWithHidden;
    @Mock
    private User userWithPinnedAndHidden;

    private final Pageable pageRequest = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);

    @BeforeEach
    public void setUp() {
        service = new TimelineService(
            blockService,
            sourceServiceMock,
            timelineEntryRepositoryMock,
            timelinePinServiceMock,
            timelineQuestionRepository,
            userTimelineFaveService,
            userTimelineHideServiceMock,
            userTimelineUnpinService,
            timelineLikeRepository);

    }

    @Test
    public void get_all_timeline_entries() {
        when(timelineEntryRepositoryMock.findAllByIdIsNotIn(anyCollection(), any(Pageable.class)))
            .thenReturn(pageTimelineEntriesMock);
        when(timelinePinServiceMock.getAllTimelinePins())
            .thenReturn(Collections.emptySet());

        assertThat(service.getAllTimelineEntries(pageRequest), is(pageTimelineEntriesMock));
    }

    @Test
    public void get_all_timeline_entries_by_user() {
        when(sourceServiceMock.getAllSourcesByUser(any()))
            .thenReturn(SOURCES);
        when(userTimelineHideServiceMock.getAllHiddenTimelineEntriesByUser(user))
            .thenReturn(Collections.emptySet());
        when(timelinePinServiceMock.getAllTimelinePinsByUser(user))
            .thenReturn(Collections.emptySet());
        when(timelineEntryRepositoryMock.findAllBySourceIn(SOURCES, pageRequest))
            .thenReturn(pageTimelineEntriesMock);

        assertThat(service.getAllTimelineEntriesByUser(user, pageRequest), is(pageTimelineEntriesMock));
    }

    @Test
    public void get_all_timeline_entries_by_user_with_hidden() {
        when(userTimelineHideServiceMock.getAllHiddenTimelineEntriesByUser(userWithHidden))
            .thenReturn(HIDDEN_TIMELINE_ENTRIES);
        when(timelineEntryRepositoryMock.findAllBySourceInAndIdIsNotIn(
            anyCollection(),
            eq(HIDDEN_TIMELINE_ENTRIES.stream()
                .map(TimelineEntry::getId)
                .collect(Collectors.toSet())),
            any(Pageable.class)))
            .thenReturn(pageTimelineEntriesHideMock);

        assertThat(service.getAllTimelineEntriesByUser(userWithHidden, pageRequest),
            is(pageTimelineEntriesHideMock));
    }

    @Test
    public void get_all_timeline_entries_by_user_with_pinned() {
        when(sourceServiceMock.getAllSourcesByUser(any()))
            .thenReturn(SOURCES);
        when(userTimelineHideServiceMock.getAllHiddenTimelineEntriesByUser(userWithPinned))
            .thenReturn(PINNED_TIMELINE_ENTRIES);
        when(timelineEntryRepositoryMock.findAllBySourceInAndIdIsNotIn(
            anyCollection(),
            eq(PINNED_TIMELINE_ENTRIES.stream()
                .map(TimelineEntry::getId)
                .collect(Collectors.toSet())),
            any(Pageable.class)))
            .thenReturn(pageTimelineEntriesPinnedMock);

        assertThat(service.getAllTimelineEntriesByUser(userWithPinned, pageRequest),
            is(pageTimelineEntriesPinnedMock));
    }

    @Test
    public void get_all_timeline_entries_by_user_with_hidden_and_pinned() {
        when(sourceServiceMock.getAllSourcesByUser(any()))
            .thenReturn(SOURCES);
        when(userTimelineHideServiceMock
            .getAllHiddenTimelineEntriesByUser(userWithPinnedAndHidden))
            .thenReturn(Stream.concat(
                HIDDEN_TIMELINE_ENTRIES.stream(),
                PINNED_TIMELINE_ENTRIES.stream())
                .collect(Collectors.toSet()));
        when(timelineEntryRepositoryMock
            .findAllBySourceInAndIdIsNotIn(
                anyCollection(),
                eq(Stream.concat(
                    HIDDEN_TIMELINE_ENTRIES.stream(),
                    PINNED_TIMELINE_ENTRIES.stream())
                    .map(TimelineEntry::getId)
                    .collect(Collectors.toSet())),
                any(Pageable.class)))
            .thenReturn(pageTimelineEntriesHideAndPinnedMock);

        assertThat(service.getAllTimelineEntriesByUser(userWithPinnedAndHidden, pageRequest),
            is(pageTimelineEntriesHideAndPinnedMock));
    }

    @Test
    public void get_timeline_entry_by_id() {
        when(timelineEntryRepositoryMock.findById(TIMELINE_ENTRY_ID))
            .thenReturn(Optional.of(timelineEntryMock));

        assertThat(service.getTimelineEntryById(TIMELINE_ENTRY_ID), is(timelineEntryMock));
    }
}
