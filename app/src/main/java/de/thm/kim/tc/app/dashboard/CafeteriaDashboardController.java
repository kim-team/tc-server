/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.dashboard;

import de.thm.kim.tc.app.cafeteria.data.entity.Cafeteria;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaCommentService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaHoursService;
import de.thm.kim.tc.app.cafeteria.service.CafeteriaService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.sql.Date;
import java.time.DayOfWeek;
import java.time.OffsetTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping(value = "/dashboard")
public class CafeteriaDashboardController implements WebMvcConfigurer {

    private final CafeteriaService cafeteriaService;
    private final CafeteriaHoursService cafeteriaHoursService;
    private final CafeteriaCommentService cafeteriaCommentService;

    @Autowired
    public CafeteriaDashboardController(CafeteriaService cafeteriaService,
                                        CafeteriaHoursService cafeteriaHoursService,
                                        CafeteriaCommentService cafeteriaCommentService) {
        this.cafeteriaService = cafeteriaService;
        this.cafeteriaHoursService = cafeteriaHoursService;
        this.cafeteriaCommentService = cafeteriaCommentService;
    }

    @GetMapping("/cafeteria")
    public String showMensaHours(Model model) {
        Iterable<Cafeteria> cafeterias = cafeteriaService.getAllCafeterias();
        Map<Long, Iterable<CafeteriaDashboardHoursDto>> cafeteriaOpeningHours = new HashMap<>();
        Map<Long, Iterable<CafeteriaDashboardHoursDto>> cafeteriaEatingHours = new HashMap<>();
        Map<Long, Iterable<CafeteriaDashboardClosureDto>> cafeteriaClosures = new HashMap<>();

        for (Cafeteria cafeteria : cafeterias) {
            cafeteriaOpeningHours.putIfAbsent(cafeteria.getId(), convertToOpeningHoursDomain(cafeteria.getId()));
            cafeteriaEatingHours.putIfAbsent(cafeteria.getId(), convertToEatingHoursDomain(cafeteria.getId()));
            cafeteriaClosures.putIfAbsent(cafeteria.getId(), convertToClosureDomain(cafeteria.getId()));
        }

        model.addAttribute("title", "Mensa Hours");
        model.addAttribute("cafeterias", cafeterias);
        model.addAttribute("cafeteriaOpeningHours", cafeteriaOpeningHours);
        model.addAttribute("cafeteriaEatingHours", cafeteriaEatingHours);
        model.addAttribute("cafeteriaClosures", cafeteriaClosures);
        model.addAttribute("classActiveMensaHours", "active");
        return "cafeteria";
    }

    private Iterable<CafeteriaDashboardHoursDto> convertToOpeningHoursDomain(Long cafeteriaId) {
        return StreamSupport
            .stream(cafeteriaHoursService.getCafeteriaOpeningHours(cafeteriaId).spliterator(), false)
            .map(c -> new CafeteriaDashboardHoursDto(
                    c.getId(),
                    c.getDayOfWeek(),
                    c.getOpen(),
                    c.getClose()
                )
            )
            .sorted(Comparator.comparing(CafeteriaDashboardHoursDto::getDayOfWeek))
            .collect(Collectors.toList());
    }

    private Iterable<CafeteriaDashboardHoursDto> convertToEatingHoursDomain(Long cafeteriaId) {
        return StreamSupport
            .stream(cafeteriaHoursService.getCafeteriaEatingHours(cafeteriaId).spliterator(), false)
            .map(c -> new CafeteriaDashboardHoursDto(
                    c.getId(),
                    c.getDayOfWeek(),
                    c.getOpen(),
                    c.getClose()
                )
            )
            .sorted(Comparator.comparing(CafeteriaDashboardHoursDto::getDayOfWeek))
            .collect(Collectors.toList());
    }

    private Iterable<CafeteriaDashboardClosureDto> convertToClosureDomain(Long cafeteriaId) {
        return StreamSupport
            .stream(cafeteriaHoursService.getAllCafeteriaClosures(cafeteriaId).spliterator(), false)
            .map(c -> new CafeteriaDashboardClosureDto(
                    c.getId(),
                    c.getFrom(),
                    c.getTo()
                )
            )
            .sorted(Comparator.comparing(CafeteriaDashboardClosureDto::getFrom))
            .collect(Collectors.toList());
    }

    @Transactional
    @PostMapping("/cafeteria")
    public String updateCafeteriaHours(@RequestParam Map<String, String> requestParams) {
        for (String key : requestParams.keySet()) {
            if (key.equals("create_cafeteria")) {
                cafeteriaService.createNewCafeteria(
                    getFieldString(requestParams, "create_cafeteria_name"),
                    getFieldString(requestParams, "create_cafeteria_news"),
                    getFieldString(requestParams, "create_cafeteria_description"),
                    getFieldString(requestParams, "create_cafeteria_image"),
                    getFieldString(requestParams, "create_cafeteria_latitude"),
                    getFieldString(requestParams, "create_cafeteria_longitude")
                );
            } else if (key.startsWith("update_cafeteria_") && requestParams.get(key).equals("Save")) {
                cafeteriaService.updateCafeteria(
                    getFieldLong(key, "update_cafeteria_"),
                    getFieldString(key, requestParams, "update_cafeteria_", "name_"),
                    getFieldString(key, requestParams, "update_cafeteria_", "news_"),
                    getFieldString(key, requestParams, "update_cafeteria_", "description_"),
                    getFieldString(key, requestParams, "update_cafeteria_", "image_"),
                    getFieldString(key, requestParams, "update_cafeteria_", "latitude_"),
                    getFieldString(key, requestParams, "update_cafeteria_", "longitude_")
                );
            } else if (key.startsWith("delete_cafeteria_") && requestParams.get(key).equals("Delete")) {
                cafeteriaService.deleteCafeteria(getFieldLong(key, "delete_cafeteria_"));
            } else if (key.equals("create_opening_hours")) {
                cafeteriaHoursService.addCafeteriaOpeningHours(
                    getCafeteriaId(requestParams),
                    getFieldDayOfWeek(requestParams, "create_opening_hours_day_of_week"),
                    getFieldOffsetTime(requestParams, "create_opening_hours_open"),
                    getFieldOffsetTime(requestParams, "create_opening_hours_close")
                );
            } else if (key.startsWith("update_opening_hours_") && requestParams.get(key).equals("Save")) {
                cafeteriaHoursService.updateCafeteriaOpeningHours(
                    getFieldLong(key, "update_opening_hours_"),
                    getFieldDayOfWeek(key, requestParams, "update_opening_hours_", "day_of_week_"),
                    getFieldOffsetTime(key, requestParams, "update_opening_hours_", "open_"),
                    getFieldOffsetTime(key, requestParams, "update_opening_hours_", "close_")
                );
            } else if (key.startsWith("delete_opening_hours_") && requestParams.get(key).equals("Delete")) {
                cafeteriaHoursService.deleteCafeteriaOpeningHours(getFieldLong(key, "delete_opening_hours_"));
            } else if (key.equals("create_eating_hours")) {
                cafeteriaHoursService.addCafeteriaEatingHours(
                    getCafeteriaId(requestParams),
                    getFieldDayOfWeek(requestParams, "create_eating_hours_day_of_week"),
                    getFieldOffsetTime(requestParams, "create_eating_hours_open"),
                    getFieldOffsetTime(requestParams, "create_eating_hours_close")
                );
            } else if (key.startsWith("update_eating_hours_") && requestParams.get(key).equals("Save")) {
                cafeteriaHoursService.updateCafeteriaEatingHours(
                    getFieldLong(key, "update_eating_hours_"),
                    getFieldDayOfWeek(key, requestParams, "update_eating_hours_", "day_of_week_"),
                    getFieldOffsetTime(key, requestParams, "update_eating_hours_", "open_"),
                    getFieldOffsetTime(key, requestParams, "update_eating_hours_", "close_")
                );
            } else if (key.startsWith("delete_eating_hours_") && requestParams.get(key).equals("Delete")) {
                cafeteriaHoursService.deleteCafeteriaEatingHours(getFieldLong(key, "delete_eating_hours_"));
            } else if (key.equals("create_closure")) {
                cafeteriaHoursService.addCafeteriaClosure(
                    getCafeteriaId(requestParams),
                    getFieldDate(requestParams, "create_closure_from"),
                    getFieldDate(requestParams, "create_closure_to")
                );
            } else if (key.startsWith("update_closure_") && requestParams.get(key).equals("Save")) {
                cafeteriaHoursService.updateCafeteriaClosure(
                    getFieldLong(key, "update_closure_"),
                    getFieldDate(key, requestParams, "update_closure_", "from_"),
                    getFieldDate(key, requestParams, "update_closure_", "to_")
                );
            } else if (key.startsWith("delete_closure_") && requestParams.get(key).equals("Delete")) {
                cafeteriaHoursService.deleteCafeteriaClosure(getFieldLong(key, "delete_closure_"));
            }
        }
        return "redirect:/dashboard/cafeteria";
    }

    private String getKey(String key, String keyPattern) {
        return key.substring(keyPattern.length());
    }

    private String getFieldString(String key, Map<String, String> requestParams, String keyPattern, String fieldPattern) {
        return getFieldString(
            requestParams,
            requestParams.keySet()
                .stream()
                .filter(i -> i.startsWith(keyPattern + fieldPattern + getKey(key, keyPattern)))
                .findFirst()
                .orElseThrow()
        );
    }

    private String getFieldString(Map<String, String> requestParams, String fieldName) {
        return requestParams.get(fieldName);
    }

    private Long getCafeteriaId(Map<String, String> requestParams) {
        return Long.valueOf(getFieldString(requestParams, "cafeteria_id"));
    }

    private Long getFieldLong(String key, String keyPattern) {
        return Long.valueOf(getKey(key, keyPattern));
    }

    private DayOfWeek getFieldDayOfWeek(String key, Map<String, String> requestParams, String keyPattern, String fieldPattern) {
        return DayOfWeek.valueOf(getFieldString(key, requestParams, keyPattern, fieldPattern).toUpperCase());
    }

    private DayOfWeek getFieldDayOfWeek(Map<String, String> requestParams, String fieldPattern) {
        return DayOfWeek.valueOf(getFieldString(requestParams, fieldPattern).toUpperCase());
    }

    private OffsetTime getFieldOffsetTime(String key, Map<String, String> requestParams, String keyPattern, String fieldPattern) {
        return OffsetTime.parse(getFieldString(key, requestParams, keyPattern, fieldPattern) + ":00+02:00");
    }

    private OffsetTime getFieldOffsetTime(Map<String, String> requestParams, String fieldPattern) {
        return OffsetTime.parse(getFieldString(requestParams, fieldPattern) + ":00+02:00");
    }

    private Date getFieldDate(String key, Map<String, String> requestParams, String keyPattern, String fieldPattern) {
        return Date.valueOf(getFieldString(key, requestParams, keyPattern, fieldPattern));
    }

    private Date getFieldDate(Map<String, String> requestParams, String fieldPattern) {
        return Date.valueOf(getFieldString(requestParams, fieldPattern));
    }

    @GetMapping("/cafeteria/comments")
    public String showCafeteriaComments(Model model) {
        val comments = cafeteriaCommentService.getAllCafeteriaComments().stream()
            .map(CafeteriaCommentDto::new)
            .collect(Collectors.toList());

        model.addAttribute("title", "CafeteriaComments");
        model.addAttribute("cafeteriaComments", comments);
        model.addAttribute("classActiveCafeteriaComments", "active");
        return "cafeteria-comments";
    }

    @Transactional
    @PostMapping("/cafeteria/comments")
    public String deleteCafeteriaComment(Long id) {
        CafeteriaComment cafeteriaComment = cafeteriaCommentService.getCafeteriaCommentById(id);

        cafeteriaCommentService.deleteComment(cafeteriaComment);

        return "redirect:/dashboard/cafeteria/comments";
    }
}
