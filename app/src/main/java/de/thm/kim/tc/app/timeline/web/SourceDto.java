/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.timeline.data.entity.Source;
import lombok.Value;

@Value
class SourceDto {

    Long id;
    String name;
    String description;
    String logo;
    boolean subscribed;

    SourceDto(Source source) {
        this(source, true);
    }

    SourceDto(Source source, boolean subscribed) {
        id = source.getId();
        name = source.getName();
        description = source.getDescription();
        logo = source.getLogo();
        this.subscribed = subscribed;
    }
}
