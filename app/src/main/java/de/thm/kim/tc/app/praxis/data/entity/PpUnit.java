/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@Table(name = "pp_unit")
@Entity
public class PpUnit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long unitId;
    private Long groupId;

    private String titleDe;
    private String titleEn;
    private String subtitleDe;
    private String subtitleEn;
    private String markdownDe;
    private String markdownEn;
    private String image;
    private String imageAltDe;
    private String imageAltEn;
    @Column(name = "order_by")
    private Long order;

    @JsonIgnore
    public Map<String, String> getTitleMap() {
        Map<String, String> title = new HashMap<>();

        if (!this.getTitleDe().isBlank())
            title.put("de", this.getTitleDe());

        if (!this.getTitleEn().isBlank())
            title.put("en", this.getTitleEn());

        return title;
    }

    @JsonIgnore
    public Map<String, String> getSubtitleMap() {
        Map<String, String> subtitle = new HashMap<>();

        if (!this.getSubtitleDe().isBlank())
            subtitle.put("de", this.getSubtitleDe());

        if (!this.getSubtitleEn().isBlank())
            subtitle.put("en", this.getSubtitleEn());

        return subtitle;
    }

    @JsonIgnore
    public Map<String, String> getMarkdownMap() {
        Map<String, String> markdown = new HashMap<>();

        if (!this.getMarkdownDe().isBlank())
            markdown.put("de", this.getMarkdownDe());

        if (!this.getMarkdownEn().isBlank())
            markdown.put("en", this.getMarkdownEn());

        return markdown;
    }

    @JsonIgnore
    public Map<String, String> getImageAltMap() {
        Map<String, String> imageAlt = new HashMap<>();

        if (!this.getImageAltDe().isBlank())
            imageAlt.put("de", this.getImageAltDe());

        if (!this.getImageAltEn().isBlank())
            imageAlt.put("en", this.getImageAltEn());

        return imageAlt;
    }
}
