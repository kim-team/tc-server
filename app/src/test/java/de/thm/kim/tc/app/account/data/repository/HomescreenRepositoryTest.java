/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.data.repository;

import de.thm.kim.tc.app.account.data.entity.Homescreen;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;


@DataJpaTest
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HomescreenRepositoryTest {

    @Autowired
    private HomescreenRepository repository;

    @SpyBean
    private UserService userService;

    @Test
    public void return_homescreen_for_valid_user() {
        User user = userService.getUserById(1L);
        Optional<Homescreen> homescreen = repository.findByUser(user);
        assertTrue(homescreen.isPresent());
    }

    @Test
    public void should_fail_to_return_homescreen_for_invalid_user() {
        User user = new User(999L, "bad_guy", "The Bad Guy", null, null, null);
        Optional<Homescreen> homescreen = repository.findByUser(user);
        assertTrue(homescreen.isEmpty());
    }
}
