/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@Table(name = "pp_question")
@Entity
public class PpQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long questionId;
    private Long unitId;

    private Long correctAnswerId;
    private String bodyDe;
    private String bodyEn;
    private String bodyCorrectDe;
    private String bodyCorrectEn;
    private String bodyWrongDe;
    private String bodyWrongEn;
    private String image;
    private String imageAltDe;
    private String imageAltEn;

    @JsonIgnore
    public Map<String, String> getBodyMap() {
        Map<String, String> body = new HashMap<>();

        if (!this.getBodyDe().isBlank())
            body.put("de", this.getBodyDe());

        if (!this.getBodyEn().isBlank())
            body.put("en", this.getBodyEn());

        return body;
    }

    @JsonIgnore
    public Map<String, String> getBodyCorrectMap() {
        Map<String, String> bodyCorrect = new HashMap<>();

        if (!this.getBodyCorrectDe().isBlank())
            bodyCorrect.put("de", this.getBodyCorrectDe());

        if (!this.getBodyCorrectEn().isBlank())
            bodyCorrect.put("en", this.getBodyCorrectEn());

        return bodyCorrect;
    }

    @JsonIgnore
    public Map<String, String> getBodyWrongMap() {
        Map<String, String> bodyWrong = new HashMap<>();

        if (!this.getBodyWrongDe().isBlank())
            bodyWrong.put("de", this.getBodyWrongDe());

        if (!this.getBodyWrongEn().isBlank())
            bodyWrong.put("en", this.getBodyWrongEn());

        return bodyWrong;
    }

    @JsonIgnore
    public Map<String, String> getImageAltMap() {
        Map<String, String> imageAlt = new HashMap<>();

        if (!this.getImageAltDe().isBlank())
            imageAlt.put("de", this.getImageAltDe());

        if (!this.getImageAltEn().isBlank())
            imageAlt.put("en", this.getImageAltEn());

        return imageAlt;
    }
}
