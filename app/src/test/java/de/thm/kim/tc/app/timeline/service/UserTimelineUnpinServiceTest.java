/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineUnpin;
import de.thm.kim.tc.app.timeline.data.repository.UserTimelineUnpinRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UserTimelineUnpinServiceTest {

    private UserTimelineUnpinService service;

    @Mock
    private UserTimelineUnpinRepository userTimelineUnpinRepositoryMock;

    @Mock
    private TimelineEntry timelineEntryMock;
    @Mock
    private TimelineEntry timelineEntryNotExistsMock;
    @Mock
    private User userMock;
    @Mock
    private Collection<UserTimelineUnpin> userTimelineUnpinsMock;
    @Mock
    private UserTimelineUnpin userTimelineUnpinMock;

    @BeforeEach
    public void setUp() {
        service = new UserTimelineUnpinService(userTimelineUnpinRepositoryMock);
    }

    @Test
    public void get_all_unpinned_timeline_entries_by_user_id() {
        when(userTimelineUnpinRepositoryMock.findAllByUser(userMock))
            .thenReturn(userTimelineUnpinsMock);

        assertThat(service.getAllUnpinnedTimelineEntriesByUser(userMock),
            is(userTimelineUnpinsMock.stream().map(UserTimelineUnpin::getTimelineEntry).collect(Collectors.toList())));
    }

    @Test
    public void add_unpin_timeline_entry() {
        when(userTimelineUnpinRepositoryMock.findByTimelineEntryAndUser(timelineEntryNotExistsMock, userMock))
            .thenReturn(Optional.empty());

        service.addUnpinTimelineEntry(timelineEntryNotExistsMock, userMock);

        verify(userTimelineUnpinRepositoryMock).save(any(UserTimelineUnpin.class));
    }

    @Test
    public void delete_unpin_timeline_entry() {
        when(userTimelineUnpinRepositoryMock.findByTimelineEntryAndUser(timelineEntryMock, userMock))
            .thenReturn(Optional.of(userTimelineUnpinMock));

        service.deleteUnpinTimelineEntryForUser(timelineEntryMock, userMock);

        verify(userTimelineUnpinRepositoryMock).delete(any(UserTimelineUnpin.class));
    }
}
