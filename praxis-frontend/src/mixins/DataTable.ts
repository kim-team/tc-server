import {Vue, Watch} from 'vue-property-decorator'
import {Mixin} from 'vue-mixin-decorator';

import axios from "axios";

@Mixin
export default class DataTable extends Vue {

  /**
   * Editable variables
   */
  pathGet = ""
  pathCreate = ""
  pathUpdate = ""
  pathDelete = ""

  newItemText = "New Item"
  editItemText = "Edit Item"
  itemSuccessfullyCreatedText = "Item successfully created."
  itemSuccessfullyUpdatedText = "Item successfully updated."
  itemSuccessfullyDeletedText = "Item successfully deleted."

  keyIdentifier = ""

  /**
   * Internal variables
   */
    // Table specific
  search = ""
  pagination = {}
  // Dialogs
  dialog = false
  dialogDeleteItem = false
  dialogDeleteItems = false

  // Snackbar
  snack = false
  snackColor = ""
  snackText = ""
  items = [] as any

  editedIndex = -1
  editedItem = {} as any
  defaultItem = this.editedItem

  selected = [] as any

  expanded = []

  deleteItemId = 0;

  addEmptyAlertClass = (item: any) =>
    item === undefined || item === "" ? "emptyAlert" : "";

  /**
   * Styling
   */
  footerProps = {
    showFirstLastPage: true,
    firstIcon: "mdi-arrow-collapse-left",
    lastIcon: "mdi-arrow-collapse-right",
    prevIcon: "mdi-arrow-left",
    nextIcon: "mdi-arrow-right"
  }

  // Axios config
  config = process.env.NODE_ENV == 'production' ? {
    headers: {
      'Content-Type': 'application/json'
    }
  } : {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${process.env.VUE_APP_TOKEN}`
    }
  }

  // API endpoint
  endpoint = process.env.NODE_ENV == 'production'
    ? window.location.origin
    : process.env.VUE_APP_ENDPOINT;

  // Render dialog title
  formTitle() {
    return this.editedIndex >= 0 ? this.editItemText : this.newItemText;
  }

  // Dialogs
  @Watch('dialog')
  onDialogChanged(val: string) {
    val || this.closeDialog();
  }

  @Watch('dialogDelete')
  onDialogDeleteChanged(val: string) {
    val || this.closeDialog();
  }

  // Snackbar
  errorHandling(error: any) {
    this.snack = true;
    this.snackColor = "error";
    this.snackText = error.toString();
    throw error;
  }

  // Fetch items on page load
  created() {
    this.getItems().then((response: any) => {
      this.items = response.data;
    });
  }

  /**
   * CRUD-Requests
   */
  getItems() {
    return axios.get(this.endpoint + this.pathGet, this.config).catch(this.errorHandling);
  }

  createItem(item: any) {
    return axios
      .post(this.endpoint + this.pathCreate, item, this.config)
      .then(response => {
        this.snack = true;
        this.snackColor = "success";
        this.snackText = this.itemSuccessfullyCreatedText;
        return response;
      })
      .catch(this.errorHandling);
  }

  updateItem(id: number, item: any) {
    return axios
      .put(`${this.endpoint}${this.pathUpdate}/${id}`, item, this.config)
      .then(response => {
        this.snack = true;
        this.snackColor = "success";
        this.snackText = this.itemSuccessfullyUpdatedText;
        this.expanded.splice(this.items.indexOf(item), 1);
        return response;
      })
      .catch(this.errorHandling);
  }

  deleteItem(id: any) {
    return axios
      .delete(`${this.endpoint}${this.pathDelete}/${id}`, this.config)
      .then(() => {
        this.snack = true;
        this.snackColor = "success";
        this.snackText = this.itemSuccessfullyDeletedText;
      })
      .catch(this.errorHandling);
  }

  /**
   * Dialogs
   */
  openItemDialog(item: any) {
    this.editedIndex = this.items.indexOf(item);
    this.editedItem = Object.assign({}, item);
    this.dialog = true;
  }

  deleteItemDialog(item: any) {
    this.deleteItemId = item[this.keyIdentifier];
    this.editedItem = Object.assign({}, item);
    this.dialogDeleteItem = true;
  }

  deleteItemConfirm() {
    this.deleteItem(this.deleteItemId).then(() => {
      for (let i = 0; i < this.selected.length; i++) {
        if (this.selected[i][this.keyIdentifier] == this.deleteItemId) {
          this.selected.splice(i, 1);
        }
      }
      for (let i = 0; i < this.items.length; i++) {
        if (this.items[i][this.keyIdentifier] == this.deleteItemId) {
          this.items.splice(i, 1);
        }
      }
      this.closeDialog();
    });
  }

  deleteItemsDialog() {
    this.dialogDeleteItems = true;
  }

  async deleteItemsConfirm() {
    this.closeDialog();
    for (let i = 0; i < this.selected.length; i++) {
      await this.deleteItem(this.selected[i][this.keyIdentifier]).then(() => {
        this.items.splice(this.items.indexOf(this.selected[i]), 1);
      });
    }
    this.selected = [];
  }

  saveItem(id: number, item: any) {
    if (id >= 0) {
      this.updateItem(id, item).then((response: any) => {
        Object.assign(this.items[this.editedIndex], response.data);
        this.closeDialog();
      });
    } else {
      this.createItem(item).then((response: any) => {
        this.items.push(response.data);
        this.closeDialog();
      });
    }
  }

  closeDialog() {
    this.dialog = false;
    this.dialogDeleteItem = false;
    this.dialogDeleteItems = false;
    this.$nextTick(() => {
      this.editedItem = Object.assign({}, this.defaultItem);
      this.editedIndex = -1;
    });
  }
}
