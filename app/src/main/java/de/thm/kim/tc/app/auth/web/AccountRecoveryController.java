/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.web;

import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.service.AuthUserPasswordResetService;
import de.thm.kim.tc.app.auth.service.AuthUserService;
import de.thm.kim.tc.app.auth.service.PermissionService;
import de.thm.kim.tc.app.common.exceptions.InvalidRecoveryException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "/oauth")
public class AccountRecoveryController {

    @Value("${security.oauth2.client.client-id}")
    private String CLIENT_ID;

    private final AuthorizationServerTokenServices tokenServices;
    private final PermissionService permissionService;
    private final AuthUserPasswordResetService authUserPasswordResetService;
    private final AuthUserService authUserService;

    @Autowired
    public AccountRecoveryController(final AuthorizationServerTokenServices tokenServices,
                                     final PermissionService permissionService,
                                     final AuthUserPasswordResetService authUserPasswordResetService,
                                     final AuthUserService authUserService) {
        this.tokenServices = tokenServices;
        this.permissionService = permissionService;
        this.authUserPasswordResetService = authUserPasswordResetService;
        this.authUserService = authUserService;
    }

    @PostMapping(path = "/recovery/{usernameAndRecoveryCode}")
    public OAuth2AccessToken recovery(@PathVariable String usernameAndRecoveryCode) {
        var fields = usernameAndRecoveryCode.split(":");
        if (fields.length != 2) {
            throw new InvalidRecoveryException("The recovery token is invalid!");
        }
        AuthUser authUser = authUserService.getAuthUser(fields[0], fields[1]);

        if (authUser.isVerified())
            throw new InvalidRecoveryException("User is already verified! Recovery token is not allowed!");

        Set<GrantedAuthority> permissions = permissionService.getAuthorities(authUser.getUsername());

        Set<String> scopes = new HashSet<>();
        scopes.add("read");
        scopes.add("write");

        Set<String> responseTypes = new HashSet<>();
        responseTypes.add("code");

        return tokenServices.createAccessToken(new OAuth2Authentication(
            new OAuth2Request(
                new HashMap<>(),
                CLIENT_ID,
                permissions,
                true,
                scopes,
                new HashSet<>(),
                null,
                responseTypes,
                new HashMap<>()
            ),
            new UsernamePasswordAuthenticationToken(
                new User(
                    authUser.getUsername(),
                    authUser.getPassword(),
                    true,
                    true,
                    true,
                    true,
                    permissions
                ),
                null,
                permissions
            )
        ));
    }

    @PostMapping(path = "/reset/{email}")
    public void sendResetPasswordMail(@PathVariable String email) {
        authUserPasswordResetService.createResetPasswordRequest(email.toLowerCase());
    }

    @PostMapping(path = "/reset")
    public void changePassword(@Valid @RequestBody ResetPasswordDto data) {
        authUserPasswordResetService.resetPassword(data.getEmail().toLowerCase(), data.getToken(), data.getPassword());
    }

    @NoArgsConstructor
    @Getter
    private static class ResetPasswordDto {

        @NotBlank
        private String email;

        @NotBlank
        private String token;

        @NotBlank
        private String password;
    }
}
