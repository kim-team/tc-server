/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import de.thm.kim.tc.app.timeline.service.UserTimelineHideService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Api(description = "Everything about hiding and restoring timeline entries", tags = {"Timeline - Hidden entries"})
@RestController
@RequestMapping(value = "/timeline/hidden")
public class HiddenEntryController {

    private final UserService userService;
    private final TimelineService timelineService;
    private final UserTimelineHideService userTimelineHideService;

    @Autowired
    public HiddenEntryController(UserService userService,
                                 TimelineService timelineService,
                                 UserTimelineHideService userTimelineHideService) {
        this.userService = userService;
        this.timelineService = timelineService;
        this.userTimelineHideService = userTimelineHideService;
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Hide a timeline entry for a user")
    @PostMapping(path = "/{entryId}")
    public void hideTimelineEntry(Principal principal,
                                  @PathVariable("entryId") Long entryId) {
        User user = userService.getUserByUsername(principal.getName());
        TimelineEntry entry = timelineService.getTimelineEntryById(entryId);

        userTimelineHideService.addHideTimelineEntry(entry, user);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Restore all hidden timeline entries for a user")
    @DeleteMapping
    public void restoreAllTimelineEntries(Principal principal) {
        User user = userService.getUserByUsername(principal.getName());

        userTimelineHideService.restoreAllHiddenTimelineEntriesForUser(user);
    }
}
