/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.praxis.data.entity.PpGroup;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
class GroupDto {

    Long phaseId;
    Long groupId;
    Map<String, String> title;
    Long order;
    Long dependsOn;
    List<UnitDto> units;

    GroupDto(PpGroup group, List<UnitDto> units) {
        this.phaseId = group.getPhaseId();
        this.groupId = group.getGroupId();
        this.title = group.getTitleMap();
        this.order = group.getOrder();
        this.dependsOn = group.getDependsOn();
        this.units = units;
    }
}
