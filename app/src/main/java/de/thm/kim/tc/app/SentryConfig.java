/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Profile("prod")
@Configuration
public class SentryConfig implements HealthIndicator {

    @Value("${sentry.dsn:#{null}}")
    private String sentryDsn;

    private final BuildProperties buildProperties;
    private final Environment env;

    @Autowired
    public SentryConfig(BuildProperties buildProperties, Environment env) {
        this.buildProperties = buildProperties;
        this.env = env;
    }

    @Override
    public Health health() {
        if (sentryDsn == null)
            return Health.down().build();

        return Health.up().build();
    }

    @PostConstruct
    private void initializeSentry() {
        if (sentryDsn != null) {
            SentryClient client = Sentry.init(sentryDsn);

            client.addTag("isCute", "true");
            client.addTag("springProfiles", Arrays.toString(env.getActiveProfiles()));
            client.setRelease(buildProperties.getVersion());
        }
    }

    @Bean
    public HandlerExceptionResolver sentryExceptionResolver() {
        return new io.sentry.spring.SentryExceptionResolver();
    }

    @Bean
    public ServletContextInitializer sentryServletContextInitializer() {
        return new io.sentry.spring.SentryServletContextInitializer();
    }
}
