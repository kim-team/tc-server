/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.data.entity;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.reports.Report;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "reported_cafeteria_comments")
public class ReportedCafeteriaComment implements Report<CafeteriaComment> {

    @EmbeddedId
    private ReportedCafeteriaCommentId id;

    @ManyToOne
    @MapsId("commentId")
    @JoinColumn(name = "cafeteria_comment_id")
    private CafeteriaComment reportedItem;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User reportingUser;

    @CreatedDate
    @Column(name = "date")
    private Date reportDate;

    public ReportedCafeteriaComment(CafeteriaComment reportedItem, User reportingUser) {
        this.id = new ReportedCafeteriaCommentId(reportedItem.getId(), reportingUser.getId());
        this.reportedItem = reportedItem;
        this.reportingUser = reportingUser;
        this.reportDate = new Date();
    }
}
