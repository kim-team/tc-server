-- Dummy auth users
INSERT INTO authentication_user (username, password) VALUES
('admin',                  '$2a$10$I0VVsOnIzYL.gLHyHSt2ne/plyrxzGpnDfDkY6h817KDbTPk1ypoi'), -- Password: admin
('user',                   '$2a$10$HxlOSUQJBpuCpCgybRFkX.vtLJdGn01n23Ymo9UF3VuQqCCIpkH8.'), -- Password: user
('anonymous',              '$2a$10$SIx3MO1ooDfGVy19g3vGl.wTgSts5FH.j2BbbTUpUcp5EJECP0BYy'), -- Password: anonymous
('jane_doe',               '$2a$10$8s29EgiS9AG0xZhqN07kNud0AzobwJrR2ImDP0FAacx.Ck2IGqPcS'), -- Password: jane_doe
('otto_normalverbraucher', '$2a$10$I0VVsOnIzYL.gLHyHSt2ne/plyrxzGpnDfDkY6h817KDbTPk1ypoi');  -- Password: admin

INSERT INTO permissions (username, role) VALUES
('admin',                  'USER'),
('admin',                  'ADMIN'),
('user',                   'USER'),
('anonymous',              'USER'),
('jane_doe',               'USER'),
('otto_normalverbraucher', 'USER'),
('otto_normalverbraucher', 'ADMIN');
