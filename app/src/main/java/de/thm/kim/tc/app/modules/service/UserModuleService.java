/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.modules.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.modules.data.entity.UserModule;
import de.thm.kim.tc.app.modules.data.repository.UserModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class UserModuleService {

    private final UserModuleRepository userModuleRepository;

    @Autowired
    public UserModuleService(UserModuleRepository userModuleRepository) {
        this.userModuleRepository = userModuleRepository;
    }

    public Collection<UserModule> getModulesByUser(User user) {
        return userModuleRepository.findAllByUser(user);
    }

    public Iterable<UserModule> saveModules(Iterable<UserModule> userModules) {
        return userModuleRepository.saveAll(userModules);
    }

    @Transactional
    public void deleteAllByUser(User user) {
        userModuleRepository.deleteAllByUser(user);
    }
}
