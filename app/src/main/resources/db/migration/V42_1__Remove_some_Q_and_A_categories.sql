-- Clean up category subscriptions
DELETE FROM users_categories AS u
WHERE u.category_id IN
    ((
        SELECT c.category_id
        FROM categories AS c
          INNER JOIN users_categories AS u
          ON c.category_id = u.category_id
        WHERE c.parent_id IN (2, 3, 4, 7, 8, 9, 10, 176, 177)
    ) UNION (
        SELECT u.category_id
        FROM users_categories AS u
        WHERE u.category_id IN (2, 3, 4, 7, 8, 9, 10, 176, 177)
    ));

-- 'Studienstart (Bachelor)'
DELETE FROM categories WHERE parent_id   = 2;
DELETE FROM categories WHERE category_id = 2;

-- 'Studienstart (Master)'
DELETE FROM categories WHERE parent_id   = 3;
DELETE FROM categories WHERE category_id = 3;

-- 'Studienstart (Duales Studium)'
DELETE FROM categories WHERE parent_id   = 4;
DELETE FROM categories WHERE category_id = 4;

-- 'Allgemein THM' --> 'Campus TALK'
UPDATE categories SET title = 'Campus TALK' WHERE category_id = 6;

-- 'Corona Info'
DELETE FROM categories WHERE category_id = 7;

-- 'Campus THM'
DELETE FROM categories WHERE parent_id   = 8;
DELETE FROM categories WHERE category_id = 8;

-- 'Fachschaften'
DELETE FROM categories WHERE category_id = 9;

-- 'Smalltalk'
DELETE FROM categories WHERE category_id = 10;

-- 'Events'
DELETE FROM categories WHERE category_id = 176;

-- 'Lost and Found'
DELETE FROM categories WHERE parent_id   = 177;
DELETE FROM categories WHERE category_id = 177;
