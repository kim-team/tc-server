/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.qanda.data.entity.Answer;
import de.thm.kim.tc.app.qanda.data.entity.AnswerLike;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.data.entity.QuestionLike;
import de.thm.kim.tc.app.qanda.data.repository.AnswerLikeRepository;
import de.thm.kim.tc.app.qanda.data.repository.QuestionLikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class LikeService {

    private final AnswerLikeRepository answerLikeRepository;
    private final QuestionLikeRepository questionLikeRepository;

    @Autowired
    public LikeService(AnswerLikeRepository answerLikeRepository, QuestionLikeRepository questionLikeRepository) {
        this.answerLikeRepository = answerLikeRepository;
        this.questionLikeRepository = questionLikeRepository;
    }

    // Answer Likes

    public Collection<AnswerLike> getAnswerLikesForAnswer(Answer answer) {
        return answerLikeRepository.findAllByAnswer(answer);
    }

    public Collection<AnswerLike> getAnswerLikesForAnswerAuthor(User user) {
        return answerLikeRepository.findAllByAnswer_Author(user);
    }

    public AnswerLike createNewAnswerLike(Answer answer, User user) {
        return answerLikeRepository.findByAnswerAndUser(answer, user)
            .orElse(answerLikeRepository.save(new AnswerLike(answer, user)));
    }

    public AnswerLike removeAnswerLike(Answer answer, User user) {
        AnswerLike answerLike = answerLikeRepository.findByAnswerAndUser(answer, user)
            .orElseThrow(() -> new ResourceNotFoundException("No AnswerLike found for Answer ID: " + answer.getId() + " and User ID: " + user.getId()));

        answerLikeRepository.delete(answerLike);
        return answerLike;
    }

    public void deleteAllAnswerLikesForAnswer(Answer answer) {
        answerLikeRepository.deleteAllByAnswer(answer);
    }

    // Question Likes

    public Collection<QuestionLike> getQuestionLikesForQuestion(Question question) {
        return questionLikeRepository.findAllByQuestion(question);
    }

    public Collection<QuestionLike> getQuestionLikesForQuestionAuthor(User user) {
        return questionLikeRepository.findAllByQuestion_Author(user);
    }

    public QuestionLike createNewQuestionLike(Question question, User user) {
        return questionLikeRepository.findByQuestionAndUser(question, user)
            .orElse(questionLikeRepository.save(new QuestionLike(question, user)));
    }

    public QuestionLike removeQuestionLike(Question question, User user) {
        QuestionLike questionLike = questionLikeRepository.findByQuestionAndUser(question, user)
            .orElseThrow(() -> new ResourceNotFoundException("No QuestionLike found for Question ID: " + question.getId() + " and User ID: " + user.getId()));

        questionLikeRepository.delete(questionLike);
        return questionLike;
    }

    public void deleteAllQuestionLikesForQuestion(Question question) {
        questionLikeRepository.deleteAllByQuestion(question);
    }

    @Transactional
    public void deleteAllLikesByUser(User user) {
        answerLikeRepository.deleteAllByUser(user);
        questionLikeRepository.deleteAllByUser(user);
    }
}
