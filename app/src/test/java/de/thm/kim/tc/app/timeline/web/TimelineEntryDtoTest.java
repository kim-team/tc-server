/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TimelineEntryDtoTest {

    private static final Long ID = 1L;
    private static final String AVATAR = "cafeteria.png";
    private static final String TEXT = "Die Kueche spricht!";
    private static final String CONTENT_ARGS = "1";
    private static final Date MODIFIED = Date.from(Instant.now());

    @Test
    public void test_all() {
        Source source = new Source();
        source.setLogo(AVATAR);
        Date created = Date.from(Instant.now());
        TimelineEntry timelineEntry = new TimelineEntry(ID, source, TEXT, CONTENT_ARGS, created, MODIFIED);
        TimelineEntryDto dto = new TimelineEntryDto(timelineEntry, false, true, 0, false, false);
        assertThat(dto.getId(), is(ID));
        assertThat(dto.getAvatar(), is(AVATAR));
        assertThat(dto.getText(), is(TEXT));
        assertThat(dto.getRoute(), is(source.getContentRoot()));
        assertThat(dto.getRouteArgs(), is(CONTENT_ARGS));
        assertThat(dto.getDate(), is(MODIFIED));
        assertThat(dto.isPinned(), is(true));
        assertThat(dto.isFaved(), is(false));
        dto = new TimelineEntryDto(timelineEntry);
        assertThat(dto.getId(), is(ID));
        assertThat(dto.getAvatar(), is(AVATAR));
        assertThat(dto.getText(), is(TEXT));
        assertThat(dto.getRoute(), is(source.getContentRoot()));
        assertThat(dto.getRouteArgs(), is(CONTENT_ARGS));
        assertThat(dto.getDate(), is(MODIFIED));
        assertThat(dto.isPinned(), is(false));
        assertThat(dto.isFaved(), is(false));
        dto = new TimelineEntryDto(timelineEntry, true, true, 0, false, false);
        assertThat(dto.getId(), is(ID));
        assertThat(dto.getAvatar(), is(AVATAR));
        assertThat(dto.getText(), is(TEXT));
        assertThat(dto.getRoute(), is(source.getContentRoot()));
        assertThat(dto.getRouteArgs(), is(CONTENT_ARGS));
        assertThat(dto.getDate(), is(MODIFIED));
        assertThat(dto.isFaved(), is(true));
        assertThat(dto.isPinned(), is(true));
        dto = new TimelineEntryDto(timelineEntry, true, false, 0, false, false);
        assertThat(dto.getId(), is(ID));
        assertThat(dto.getAvatar(), is(AVATAR));
        assertThat(dto.getText(), is(TEXT));
        assertThat(dto.getRoute(), is(source.getContentRoot()));
        assertThat(dto.getRouteArgs(), is(CONTENT_ARGS));
        assertThat(dto.getDate(), is(MODIFIED));
        assertThat(dto.isFaved(), is(true));
        assertThat(dto.isPinned(), is(false));
    }
}
