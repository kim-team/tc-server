/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.web;

import com.maximeroussy.invitrode.WordGenerator;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRecoveryCodeRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.auth.service.RegistrationService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@RestController
@RequestMapping(path = "/oauth")
public class RegistrationController {

    private final AuthUserRepository authUserRepository;
    private final AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository;
    private final RegistrationService registrationService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(AuthUserRepository authUserRepository,
                                  AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository,
                                  RegistrationService registrationService,
                                  PasswordEncoder passwordEncoder) {
        this.authUserRepository = authUserRepository;
        this.authUserRecoveryCodeRepository = authUserRecoveryCodeRepository;
        this.registrationService = registrationService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(path = "/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public RegistrationOutputDto signup(@Valid @RequestBody RegistrationController.RegistrationInputDto data) {
        String uuid = UUID.randomUUID().toString();
        while (authUserRepository.existsByUsername(uuid)) {
            uuid = UUID.randomUUID().toString();
        }

        String recoveryCode = generateRecoveryCode(new WordGenerator());
        var hashedRecoveryCode = passwordEncoder.encode(recoveryCode);

        registrationService.registerNewUser(uuid, data.getDisplayName(), hashedRecoveryCode).orElseThrow();

        return new RegistrationOutputDto(uuid, uuid + ":" + recoveryCode);
    }

    private String generateRecoveryCode(WordGenerator generator) {
        return String.format(
            "%s-%s-%s-%s-%s-%s",
            generator.newWord(7),
            generator.newWord(7),
            generator.newWord(7),
            generator.newWord(7),
            generator.newWord(7),
            generator.newWord(7)
        ).toLowerCase();
    }

    @NoArgsConstructor
    @Getter
    private static class RegistrationInputDto {

        @NotBlank
        @Size(max = 128)
        private String displayName;
    }

    @Value
    private static class RegistrationOutputDto {

        String userId;

        String recoveryCode;
    }
}
