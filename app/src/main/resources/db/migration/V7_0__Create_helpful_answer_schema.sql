CREATE TABLE helpful_answers (
    question_id     BIGINT  NOT NULL,
    answer_id       BIGINT  NOT NULL,
    date            TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (question_id) REFERENCES questions (question_id),
    FOREIGN KEY (answer_id) REFERENCES answers (answer_id),
    PRIMARY KEY (answer_id, question_id)
);
