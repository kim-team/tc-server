/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpCompletion;
import de.thm.kim.tc.app.praxis.data.entity.PpPhase;
import de.thm.kim.tc.app.praxis.data.entity.PpProgress;
import de.thm.kim.tc.app.praxis.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Value;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(description = "All praxis routes for the app", tags = {"Praxis - App"})
@RestController
@RequestMapping(path = "/praxis")
class PraxisController {

    private final PpPhaseService ppPhaseService;
    private final PpPersonService ppPersonService;
    private final PpAddressService ppAddressService;
    private final PpGroupService ppGroupService;
    private final PpUnitService ppUnitService;
    private final PpMediaService ppMediaService;
    private final PpEntryService ppEntryService;
    private final PpQuestionService ppQuestionService;
    private final PpAnswerService ppAnswerService;
    private final PpProgressService ppProgressService;
    private final PpCompletionService ppCompletionService;
    private final UserService userService;

    @Autowired
    PraxisController(PpPhaseService ppPhaseService,
                     PpPersonService ppPersonService,
                     PpAddressService ppAddressService,
                     PpGroupService ppGroupService,
                     PpUnitService ppUnitService,
                     PpMediaService ppMediaService,
                     PpEntryService ppEntryService,
                     PpQuestionService ppQuestionService,
                     PpAnswerService ppAnswerService,
                     PpProgressService ppProgressService,
                     PpCompletionService ppCompletionService,
                     UserService userService) {
        this.ppPhaseService = ppPhaseService;
        this.ppPersonService = ppPersonService;
        this.ppAddressService = ppAddressService;
        this.ppGroupService = ppGroupService;
        this.ppUnitService = ppUnitService;
        this.ppMediaService = ppMediaService;
        this.ppEntryService = ppEntryService;
        this.ppQuestionService = ppQuestionService;
        this.ppAnswerService = ppAnswerService;
        this.ppProgressService = ppProgressService;
        this.ppCompletionService = ppCompletionService;
        this.userService = userService;
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Overview over the available praxis phases", tags = {"Praxis - App"})
    @GetMapping
    public List<OverviewPhaseDto> getPhaseOverview() {
        return ppPhaseService.getAllVisiblePhases().stream()
            .map(OverviewPhaseDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Full content for specified praxis phase", tags = {"Praxis - App"})
    @GetMapping(path = "/{phaseId}")
    public VisiblePhaseDto getVisiblePhase(@PathVariable("phaseId") Long phaseId) {
        return ppPhaseService.getVisiblePhaseById(phaseId)
            .map(p -> new VisiblePhaseDto(p,
                GroupController.getGroupDtosForPhase(
                    ppGroupService,
                    ppUnitService,
                    ppMediaService,
                    ppEntryService,
                    ppQuestionService,
                    ppAnswerService,
                    p),
                PersonController.getPersonDtosForPhase(
                    ppPersonService,
                    ppAddressService,
                    p
                )))
            .orElseThrow(() -> new ResourceNotFoundException("No Phase found for ID: " + phaseId));
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Get progress for current user", tags = {"Praxis - App"})
    @GetMapping(path = "/progress")
    public ResponseEntity<String> getProgress(Principal principal) {
        val user = userService.getUserByPrincipal(principal);
        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(ppProgressService.getProgressForUser(user.get().getId())
            .map(PpProgress::getData)
            .orElse(""), HttpStatus.OK);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Save progress for current user", tags = {"Praxis - App"})
    @PostMapping(path = "/progress")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> saveProgress(@RequestBody String data, Principal principal) {
        val user = userService.getUserByPrincipal(principal);
        if (user.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(
            ppProgressService.saveOrUpdateProgressForUser(user.get().getId(), data).getData(),
            HttpStatus.CREATED);
    }

    @Secured("ROLE_USER")
    @ApiOperation(value = "Submit finished praxis phase", tags = {"Praxis - App"})
    @PostMapping(path = "/completions")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveCompletion(@RequestBody CompletionDto input, Principal principal) {
        val user = userService.getUserByPrincipal(principal)
            // This should never happen
            // See de.thm.kim.tc.app.common.ExceptionHandlerController for string
            .orElseThrow(() -> new AccessDeniedException("access is denied"));

        val completion = new PpCompletion(user.getId(), input.getPhaseId(), input.getMatNumber(), input.getLastName());

        ppCompletionService.saveOrUpdateCompletion(completion);
    }

    @Value
    static class OverviewPhaseDto {

        Long phaseId;
        Map<String, String> title;

        OverviewPhaseDto(PpPhase phase) {
            this.phaseId = phase.getPhaseId();
            this.title = phase.getTitleMap();
        }
    }

    /**
     * This DTO is only for praxis phases visible to the app,
     * see {@link de.thm.kim.tc.app.praxis.web.PhaseController.PhaseDto} for
     * the admin API DTO.
     */
    @Value
    static class VisiblePhaseDto {

        Long phaseId;
        Long categoryId;
        String faqUrl;
        Map<String, String> description;
        Map<String, String> title;
        List<GroupDto> groups;
        List<PersonDto> persons;

        VisiblePhaseDto(PpPhase phase, List<GroupDto> groups, List<PersonDto> persons) {
            this.phaseId = phase.getPhaseId();
            this.categoryId = phase.getCategoryId();
            this.faqUrl = phase.getFaqUrl();
            this.description = phase.getDescriptionMap();
            this.title = phase.getTitleMap();
            this.groups = groups;
            this.persons = persons;
        }
    }

}
