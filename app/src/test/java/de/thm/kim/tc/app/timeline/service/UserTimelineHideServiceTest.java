/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineHide;
import de.thm.kim.tc.app.timeline.data.repository.UserTimelineHideRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UserTimelineHideServiceTest {

    private UserTimelineHideService service;

    @Mock
    private UserTimelineHideRepository userTimelineHideRepositoryMock;

    @Mock
    private UserTimelineHide userTimelineHideUpdateMock;
    @Mock
    private TimelineEntry timelineEntryNotExistsMock;
    @Mock
    private User userMock;
    @Mock
    private Collection<UserTimelineHide> userTimelineHidesMock;

    @BeforeEach
    public void setUp() {
        service = new UserTimelineHideService(userTimelineHideRepositoryMock);
    }

    @Test
    public void get_all_hidden_timeline_entries_by_user_id() {
        when(userTimelineHideRepositoryMock.findAllByUser(userMock))
            .thenReturn(userTimelineHidesMock);

        assertThat(service.getAllHiddenTimelineEntriesByUser(userMock),
            is(userTimelineHidesMock.stream().map(UserTimelineHide::getTimelineEntry).collect(Collectors.toList())));

        verify(userTimelineHideRepositoryMock).findAllByUser(any(User.class));
    }

    @Test
    public void hide_timeline_entry() {
        when(userTimelineHideRepositoryMock.save(any(UserTimelineHide.class)))
            .thenReturn(userTimelineHideUpdateMock);

        service.addHideTimelineEntry(timelineEntryNotExistsMock, userMock);

        verify(userTimelineHideRepositoryMock).findByTimelineEntryAndUser(any(TimelineEntry.class), any(User.class));
        verify(userTimelineHideRepositoryMock).save(any(UserTimelineHide.class));
    }

    @Test
    public void restore_all_hidden_timeline_entries_for_user() {
        doNothing().when(userTimelineHideRepositoryMock).deleteAllByUser(userMock);

        service.restoreAllHiddenTimelineEntriesForUser(userMock);

        verify(userTimelineHideRepositoryMock).deleteAllByUser(userMock);
    }
}
