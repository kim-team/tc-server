/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.cafeteria.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaRating;
import de.thm.kim.tc.app.cafeteria.data.repository.CafeteriaRatingRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CafeteriaRatingService {

    // Rating delay in milliseconds
    @Value("#{${tinycampus.cafeteria.rating.delay} * 1000 * 60 * 60}")
    private long ratingDelay;

    private final CafeteriaRatingRepository cafeteriaRatingRepository;

    @Autowired
    public CafeteriaRatingService(CafeteriaRatingRepository cafeteriaRatingRepository) {
        this.cafeteriaRatingRepository = cafeteriaRatingRepository;
    }

    public Optional<CafeteriaRating> getRecentRatingForUser(String cafeteriaItem, User user) {
        val rating = cafeteriaRatingRepository.findTopByAuthorAndCafeteriaItemOrderByCreatedAtDesc(user, cafeteriaItem);

        // New ratings are possible 12 hours (default) after the last rating
        if (rating.isEmpty() || (new Date().getTime() - rating.get().getCreatedAt().getTime()) >= ratingDelay)
            return Optional.empty();

        return rating;
    }

    public CafeteriaRating createOrUpdateRating(String cafeteriaItem, User user, int rating) {
        Optional<CafeteriaRating> cafeteriaRating = getRecentRatingForUser(cafeteriaItem, user);

        // Update existing rating
        if (cafeteriaRating.isPresent()) {
            val r = cafeteriaRating.get();
            r.setRating(rating);
            r.setCreatedAt(new Date());
            return cafeteriaRatingRepository.save(r);
        }

        // Create new rating
        return cafeteriaRatingRepository.save(new CafeteriaRating(cafeteriaItem, user, rating));
    }

    public List<CafeteriaRating> getCafeteriaRatingForCafeteriaItem(String cafeteriaItem) {
        return cafeteriaRatingRepository.findAllByOrderByCreatedAtDesc();
    }

    public float getAverageRatingForCafeteriaItem(String cafeteriaItem) {
        return cafeteriaRatingRepository.avgRatingByCafeteriaItem(cafeteriaItem);
    }

    public int getNumberOfRatingsForCafeteriaItem(String cafeteriaItem) {
        return cafeteriaRatingRepository.countByCafeteriaItem(cafeteriaItem);
    }

    @Transactional
    public void deleteRatingsByUser(User user) {
        cafeteriaRatingRepository.deleteAllByAuthor(user);
    }
}
