/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.enums.SubscriptionTypes;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class SourceDtoTest {

    private static final Long ID = 1L;
    private static final String NAME = "Test";
    private static final String DESCRIPTION = "Testiii";
    private static final String LOGO = "logo.png";
    private static final boolean SUBSCRIBED = true;
    private static final String CONTENT_ROOT = "";

    private static final Source source = new Source(
        ID,
        null,
        null,
        null,
        NAME,
        NAME,
        DESCRIPTION,
        LOGO,
        SubscriptionTypes.SUBSCRIBED,
        CONTENT_ROOT
    );
    private static final SourceDto sourceDto = new SourceDto(
        source,
        SUBSCRIBED
    );

    @Test
    public void get_id() {
        assertEquals(ID, sourceDto.getId());
    }

    @Test
    public void get_name() {
        assertEquals(NAME, sourceDto.getName());
    }

    @Test
    public void get_description() {
        assertEquals(DESCRIPTION, sourceDto.getDescription());
    }

    @Test
    public void get_logo() {
        assertEquals(LOGO, sourceDto.getLogo());
    }

    @Test
    public void is_subscribed() {
        assertEquals(SUBSCRIBED, sourceDto.isSubscribed());
    }
}
