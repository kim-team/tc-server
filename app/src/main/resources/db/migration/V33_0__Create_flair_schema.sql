CREATE TABLE flairs (
    flair_id    BIGINT GENERATED BY DEFAULT AS IDENTITY,
    title       VARCHAR(255) NOT NULL,
    PRIMARY KEY (flair_id)
);

CREATE TABLE user_flairs (
    flair_id BIGINT NOT NULL,
    user_id  BIGINT NOT NULL,
    FOREIGN KEY (flair_id) REFERENCES flairs (flair_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (flair_id, user_id)
)
