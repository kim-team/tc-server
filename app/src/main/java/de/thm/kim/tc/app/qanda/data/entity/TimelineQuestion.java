/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.data.entity;

import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "timeline_questions")
public class TimelineQuestion {

    @EmbeddedId
    private TimelineQuestionId id;

    @OneToOne
    @MapsId("questionId")
    @JoinColumn(name = "question_id")
    private Question question;

    @OneToOne
    @MapsId("timelineEntryId")
    @JoinColumn(name = "timeline_entry_id")
    private TimelineEntry timelineEntry;

    public TimelineQuestion(Question question, TimelineEntry timelineEntry) {
        this.id = new TimelineQuestionId(question.getId(), timelineEntry.getId());
        this.question = question;
        this.timelineEntry = timelineEntry;
    }
}
