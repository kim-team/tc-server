/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.web;

import de.thm.kim.tc.app.thinkbig.data.entity.Event;
import lombok.Value;

@Value
class EventDto {

    Long eventId;
    String title;
    String logo;
    String topic;
    String type;
    String speaker;
    String shortDescription;
    String description;
    String tools;
    String targetGroup;
    String location;
    String timeslot;
    int comments;
    int likes;
    boolean isLiked;
    float avgRating;
    int ratings;


    EventDto(Event event, int comments, int likes, boolean isLiked, float avgRating, int ratings) {
        this.eventId = event.getId();
        this.title = event.getTitle();
        this.logo = event.getLogo();
        this.topic = event.getTopic().name();
        this.type = event.getType().name();
        this.speaker = event.getSpeaker();
        this.shortDescription = event.getShortDescription();
        this.description = event.getDescription();
        this.tools = event.getTools();
        this.targetGroup = event.getTargetGroup();
        this.location = event.getLocation();
        this.timeslot = event.getTimeslot();
        this.comments = comments;
        this.likes = likes;
        this.isLiked = isLiked;
        this.avgRating = avgRating;
        this.ratings = ratings;
    }
}
