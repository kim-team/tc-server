/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.service;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpAddress;
import de.thm.kim.tc.app.praxis.data.repository.PpAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PpAddressService {

    private final PpAddressRepository ppAddressRepository;

    @Autowired
    public PpAddressService(PpAddressRepository ppAddressRepository) {
        this.ppAddressRepository = ppAddressRepository;
    }

    public List<PpAddress> getAddressesForPersonId(Long personId) {
        return ppAddressRepository.findAllByPersonId(personId);
    }

    public List<PpAddress> getAllAddresses() {
        return ppAddressRepository.findAll();
    }

    public Optional<PpAddress> getAddressById(Long id) {
        return ppAddressRepository.findById(id);
    }

    @Transactional
    public PpAddress createOrUpdateAddress(PpAddress address) {
        return ppAddressRepository.save(address);
    }

    @Transactional
    public void deleteAddressById(Long addressId) {
        if (ppAddressRepository.existsById(addressId)) {
            ppAddressRepository.deleteById(addressId);
        } else {
            throw new ResourceNotFoundException("No Address found for ID: " + addressId);
        }
    }
}
