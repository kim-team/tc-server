INSERT INTO authentication_role (role) VALUES ('VERIFIED');

CREATE TABLE authentication_user_verification (
    username   VARCHAR(128) NOT NULL,
    email      VARCHAR(256) NOT NULL,
    token      VARCHAR(256) NOT NULL,
    created_at TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (username, email)
);

CREATE TABLE authentication_user_recovery_code (
    username VARCHAR(128) NOT NULL,
    recovery VARCHAR(256) NOT NULL,
    PRIMARY KEY (username)
);

CREATE TABLE authentication_user_reset_password (
    email      VARCHAR(128) NOT NULL,
    token      VARCHAR(256) NOT NULL,
    created_at TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (email)
);

ALTER TABLE authentication_user ADD email    VARCHAR(256) NOT NULL DEFAUlT '';
ALTER TABLE authentication_user ADD recovery VARCHAR(256) NOT NULL DEFAUlT '';
