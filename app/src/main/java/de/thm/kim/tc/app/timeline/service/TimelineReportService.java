/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.reports.Report;
import de.thm.kim.tc.app.common.reports.ReportService;
import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.service.TimelineQuestionService;
import de.thm.kim.tc.app.timeline.data.entity.ReportedTimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.repository.ReportedTimelineEntryRepository;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TimelineReportService implements ReportService<TimelineEntry> {

    private final ReportService<Question> reportService;
    private final ReportedTimelineEntryRepository repository;
    private final TimelineQuestionService timelineQuestionService;

    @Autowired
    public TimelineReportService(ReportService<Question> reportService,
                                 ReportedTimelineEntryRepository repository,
                                 TimelineQuestionService timelineQuestionService) {
        this.reportService = reportService;
        this.repository = repository;
        this.timelineQuestionService = timelineQuestionService;
    }

    @Override
    public Collection<ReportedTimelineEntry> getAllReports() {
        return repository.findAllByOrderByReportedItemDesc();
    }

    @Override
    public Map<TimelineEntry, List<Report<TimelineEntry>>> getAllReportsByItem() {
        return repository.findAll().stream()
            .collect(Collectors.groupingBy(Report::getReportedItem));
    }

    @Override
    public boolean isReportedByUser(TimelineEntry entry, Optional<User> user) {
        if (user.isEmpty())
            return false;

        val q = timelineQuestionService.getLinkedQuestion(entry);

        return q.map(question -> reportService.isReportedByUser(question, user))
            .orElseGet(() -> repository.existsByReportedItemAndReportingUser(entry, user.get()));
    }

    @Override
    public void report(TimelineEntry entry, User user) {
        timelineQuestionService.getLinkedQuestion(entry).ifPresentOrElse(
            (q) -> reportService.report(q, user),
            () -> repository.save(new ReportedTimelineEntry(entry, user)));
    }

    @Override
    @Transactional
    public void removeAllReportsById(Long id) {
        repository.deleteAllByReportedItemId(id);
    }

    @Override
    @Transactional
    public void removeAllReports(TimelineEntry entry) {
        repository.deleteAllByReportedItem(entry);
    }

    @Override
    @Transactional
    public void removeAllReportsByUser(User user) {
        repository.deleteByReportingUser(user);
    }

    @Override
    @Transactional
    public void removeReport(TimelineEntry entry, User user) {
        timelineQuestionService.getLinkedQuestion(entry).ifPresentOrElse(
            (q) -> reportService.removeReport(q, user),
            () -> repository.deleteByReportedItemAndReportingUser(entry, user));
    }
}
