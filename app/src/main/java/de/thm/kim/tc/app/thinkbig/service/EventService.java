/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.thinkbig.service;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.thinkbig.data.entity.Event;
import de.thm.kim.tc.app.thinkbig.data.entity.Feedback;
import de.thm.kim.tc.app.thinkbig.data.entity.enums.EventTopic;
import de.thm.kim.tc.app.thinkbig.data.entity.enums.EventType;
import de.thm.kim.tc.app.thinkbig.data.repository.EventRepository;
import de.thm.kim.tc.app.thinkbig.data.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class EventService implements HealthIndicator {

    private final EventRepository eventRepository;
    private final FeedbackRepository feedbackRepository;

    @Autowired
    public EventService(EventRepository eventRepository, FeedbackRepository feedbackRepository) {
        this.eventRepository = eventRepository;
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public Health health() {
        if (eventRepository.count() <= 0)
            return Health.down().build();

        return Health.up().build();
    }

    public Collection<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    public Event getEvenBytId(Long eventId) {
        return eventRepository.findById(eventId)
            .orElseThrow(() -> new ResourceNotFoundException("No Event found for ID: " + eventId));
    }

    public Collection<Event> getEventsPerTopic(EventTopic topic) {
        return eventRepository.findAllByTopic(topic);
    }

    public Long countEventsByTopicAndType(EventTopic topic, EventType type) {
        return eventRepository.countByTopicAndType(topic, type);
    }

    public void createNewFeedback(Event event, int rating, String comment) {
        feedbackRepository.save(new Feedback(event, rating, comment));
    }

    public List<Feedback> getFeedback() {
        return feedbackRepository.findAllByOrderByCreatedAtDesc();
    }

    public float getAverageRatingForEvent(Event event) {
        return feedbackRepository.avgRatingByEventId(event.getId());
    }

    public int getNumberOfRatingsForEvent(Event event) {
        return feedbackRepository.countByEvent(event);
    }
}
