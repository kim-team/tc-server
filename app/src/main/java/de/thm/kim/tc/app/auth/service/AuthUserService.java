/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.auth.service;

import de.thm.kim.tc.app.auth.data.entity.AuthUser;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRecoveryCodeRepository;
import de.thm.kim.tc.app.auth.data.repository.AuthUserRepository;
import de.thm.kim.tc.app.common.exceptions.InvalidRecoveryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthUserService {

    private final AuthUserRepository authUserRepository;
    private final AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthUserService(final AuthUserRepository authUserRepository,
                           final AuthUserRecoveryCodeRepository authUserRecoveryCodeRepository,
                           final PasswordEncoder passwordEncoder) {
        this.authUserRepository = authUserRepository;
        this.authUserRecoveryCodeRepository = authUserRecoveryCodeRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<AuthUser> getAll() {
        return authUserRepository.findAll();
    }

    public void changePassword(String username, String newPassword) {
        AuthUser authUser = getAuthUser(username);
        authUser.setPassword(passwordEncoder.encode(newPassword));
        authUserRepository.save(authUser);
    }

    public AuthUser getAuthUser(String username) {
        return authUserRepository
            .findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("There is no user with that username!"));
    }

    public AuthUser getAuthUser(String username, String recoveryCode) {
        var authUserRecoveryCode = authUserRecoveryCodeRepository
            .findByUsername(username)
            .orElseThrow(() -> new InvalidRecoveryException("There is no user!"));

        if (!passwordEncoder.matches(recoveryCode, authUserRecoveryCode.getRecovery())) {
            throw new InvalidRecoveryException("The recovery code is wrong!");
        }

        return authUserRepository.findByUsername(authUserRecoveryCode.getUsername())
            .orElseThrow(() -> new InvalidRecoveryException("The is no user with that recovery code!"));
    }
}
