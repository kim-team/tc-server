/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.service;

import de.thm.kim.tc.app.account.data.entity.Homescreen;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.HomescreenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class HomescreenService {

    private final HomescreenRepository homescreenRepository;

    @Autowired
    public HomescreenService(HomescreenRepository homescreenRepository) {
        this.homescreenRepository = homescreenRepository;
    }

    public Optional<Homescreen> getHomescreen(User user) {
        return homescreenRepository.findByUser(user);
    }

    public Homescreen updateHomescreen(List<Integer> order, User user) {
        Homescreen hs = homescreenRepository.findByUser(user)
            .orElse(new Homescreen(user));
        hs.setCardsOrder(order.toString());

        return homescreenRepository.save(hs);
    }

    @Transactional
    public void deleteByUser(User user) {
        homescreenRepository.deleteByUser(user);
    }
}
