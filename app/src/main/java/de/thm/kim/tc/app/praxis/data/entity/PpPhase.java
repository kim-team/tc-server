/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@Table(name = "pp_phase")
@Entity
public class PpPhase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long phaseId;
    private Long categoryId;

    // TODO validation
    private String faqUrl;
    private String descriptionDe;
    private String descriptionEn;
    private String titleDe;
    private String titleEn;
    private boolean visible;

    @JsonIgnore
    public Map<String, String> getDescriptionMap() {
        Map<String, String> description = new HashMap<>();

        if (!this.getDescriptionDe().isBlank())
            description.put("de", this.getDescriptionDe());

        if (!this.getDescriptionEn().isBlank())
            description.put("en", this.getDescriptionEn());

        return description;
    }

    @JsonIgnore
    public Map<String, String> getTitleMap() {
        Map<String, String> title = new HashMap<>();

        if (!this.getTitleDe().isBlank())
            title.put("de", this.getTitleDe());

        if (!this.getTitleEn().isBlank())
            title.put("en", this.getTitleEn());

        return title;
    }
}
