/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.service;

import de.thm.kim.tc.app.account.data.entity.BlockedUser;
import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.data.repository.BlockedUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class BlockService {

    private final BlockedUserRepository repository;

    @Autowired
    public BlockService(BlockedUserRepository repository) {
        this.repository = repository;
    }

    public BlockedUser blockUser(User user, User blockedUser) {
        return repository.findByUserAndBlockedUser(user, blockedUser)
            .orElse(repository.save(new BlockedUser(user, blockedUser)));
    }

    public Collection<User> getBlockedUsers(User user) {
        return repository.findAllByUser(user).stream()
            .map(BlockedUser::getBlockedUser)
            .collect(Collectors.toSet());
    }

    @Transactional
    public void unblockUser(User user, User blockedUser) {
        repository.deleteByUserAndBlockedUser(user, blockedUser);
    }


    @Transactional
    public void unblockUserForAllUsers(User blockedUser) {
        repository.deleteAllByBlockedUser(blockedUser);
    }

    @Transactional
    public void deleteBlockedUsersByUser(User user) {
        repository.deleteAllByUser(user);
    }
}
