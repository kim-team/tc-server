/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.account.web;

import de.thm.kim.tc.app.account.data.entity.Homescreen;
import de.thm.kim.tc.app.account.data.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HomescreenDtoTest {

    private static final Long USER_ID = 999L;
    private static final List<Integer> ORDER = List.of(1337, 42);

    private static HomescreenDto homescreenDto;

    @BeforeEach
    public void setUp() {
        Homescreen homescreen = new Homescreen();
        homescreen.setId(111L);
        homescreen.setUser(new User(USER_ID, "tester", "The Tester", null, null, null));
        homescreen.setCardsOrder(ORDER.toString());
        homescreenDto = new HomescreenDto(homescreen);
    }

    @Test
    public void get_user_id() {
        assertEquals(USER_ID, homescreenDto.getUserId());
    }

    @Test
    public void get_order() {
        assertEquals(ORDER, homescreenDto.getOrder());
    }
}
