-- Add new test user who's blocked by 'admin'
INSERT INTO users (user_id, username, name, program_id, campus_id) VALUES
(6, 'blocked', 'Blocked User', 12, 1); -- Informatik BSc.

INSERT INTO authentication_user (username, password) VALUES
('blocked', '$2a$10$IzvxIC38qqnOp7SuO/cPEedCc1o1I0eRUa2ypK/lDmJg2etn84E2y'); -- Password: blocked

INSERT INTO permissions (username, role) VALUES
('blocked', 'USER'),
('blocked', 'ADMIN');

INSERT INTO blocked_users (user_id, blocked_user_id) VALUES
(1, 6);


-- New questions and answers
INSERT INTO questions (question_id, user_id, category_id, text) VALUES
(4, 4, 1, 'Phasellus viverra nulla ut metus varius laoreet.'),
(5, 5, 1, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),
(6, 6, 1, 'Lorem ipsum Testfrage');

INSERT INTO timeline_entries (timeline_entry_id, source_id, content_args, text) VALUES
(54, 5, '4', 'Powerful User fragt: Phasellus viverra nulla ut metus varius laoreet.'),
(55, 5, '5', 'Powerful User fragt: Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'),
(56, 5, '6', 'Blocked User fragt: Lorem ipsum Testfrage');

INSERT INTO timeline_questions (question_id, timeline_entry_id) VALUES
(4, 54),
(5, 55),
(6, 56);

INSERT INTO answers (answer_id, question_id, user_id, text) VALUES
(4, 2, 3, 'Lorom novis lunenarum'),
(5, 2, 6, 'This answer was posted by a blocked user.'),
(6, 2, 3, 'This answer should be visible'),
(7, 6, 2, 'Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue.');


-- Reported questions
INSERT INTO reported_questions (question_id, user_id) VALUES
(2, 1),
(3, 1),
(3, 2),
(3, 3);

-- Reported answers
INSERT INTO reported_answers (answer_id, user_id) VALUES
(2, 1),
(2, 5),
(4, 1);

-- Reported timeline entries (not linked to any questions)
INSERT INTO reported_timeline_entries (timeline_entry_id, user_id) VALUES
(8, 2),
(8, 3),
(50, 1),
(51, 1),
(51, 3),
(53, 1),
(53, 2),
(53, 4);


-- New cafeteria comments
-- The column 'cafeteria_item' is Base64 encoded
INSERT INTO cafeteria_comment (cafeteria_comment_id, cafeteria_item, user_id, text) VALUES
-- 'Die beste MensaXXXAusgabe 14XXXLabskausiXXXhaha ein feld zu viel'
(1, 'RGllIGJlc3RlIE1lbnNhWFhYQXVzZ2FiZSAxNFhYWExhYnNrYXVzaVhYWGhhaGEgZWluIGZlbGQgenUgdmllbA==', 1, 'Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.'),
-- 'Die zweitbeste MensaXXXAusgabe 35bXXXXXL-Schnitzel'
(2, 'RGllIHp3ZWl0YmVzdGUgTWVuc2FYWFhBdXNnYWJlIDM1YlhYWFhYTC1TY2huaXR6ZWw=',                     2, 'Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt.'),
-- 'Die allerbeste MensaXXXAusgabe 35bXXXXXXL-Schnitzel'
(3, 'RGllIGFsbGVyYmVzdGUgTWVuc2FYWFhBdXNnYWJlIDM1YlhYWFhYWEwtU2Nobml0emVs',                     6, 'Aenean imperdiet. Etiam ultricies nisi vel augue.'),
-- 'XXXXXXXXXXXXXXX'
(4, 'WFhYWFhYWFhYWFhYWFhY',                                                                     4, 'Nur X im String aber passt.');

-- Reported cafeteria comment
INSERT INTO reported_cafeteria_comments (cafeteria_comment_id, user_id) VALUES
(1, 3),
(1, 4),
(2, 1),
(2, 2);
