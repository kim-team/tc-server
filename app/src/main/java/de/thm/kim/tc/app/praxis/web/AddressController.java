/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.praxis.web;

import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.praxis.data.entity.PpAddress;
import de.thm.kim.tc.app.praxis.data.entity.PpPerson;
import de.thm.kim.tc.app.praxis.service.PpAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(description = "Everything about addresses", tags = {"Praxis - Admin - Addresses"})
@RestController
@RequestMapping(path = "/praxis")
class AddressController {

    private final PpAddressService ppAddressService;

    @Autowired
    AddressController(PpAddressService ppAddressService) {
        this.ppAddressService = ppAddressService;
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all addresses for person", tags = {"Praxis - Admin - Addresses"})
    @GetMapping(path = "/persons/{personId}/addresses")
    public List<AddressDto> getAddressesForPerson(@PathVariable("personId") Long personId) {
        return ppAddressService.getAddressesForPersonId(personId).stream()
            .map(AddressDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get all addresses", tags = {"Praxis - Admin - Addresses"})
    @GetMapping(path = "/addresses")
    public List<AddressDto> getAllAddresses() {
        return ppAddressService.getAllAddresses().stream()
            .map(AddressDto::new)
            .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Get address by ID", tags = {"Praxis - Admin - Addresses"})
    @GetMapping(path = "/addresses/{addressId}")
    public AddressDto getAddress(@PathVariable("addressId") Long addressId) {
        return ppAddressService.getAddressById(addressId)
            .map(AddressDto::new)
            .orElseThrow(() -> new ResourceNotFoundException("No Address found for ID: " + addressId));
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Create new address", tags = {"Praxis - Admin - Addresses"})
    @PostMapping(path = "/addresses")
    @ResponseStatus(HttpStatus.CREATED)
    public AddressDto saveAddress(@RequestBody PpAddress address) {
        PpAddress a = ppAddressService.createOrUpdateAddress(address);

        return new AddressDto(a);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Update an address", tags = {"Praxis - Admin - Addresses"})
    @PutMapping(path = "/addresses/{addressId}")
    public AddressDto updateAddress(@PathVariable("addressId") Long addressId, @RequestBody PpAddress address) {
        if (address.getAddressId() == null || !address.getAddressId().equals(addressId))
            throw new IllegalArgumentException("Invalid addressId in body");

        ppAddressService.getAddressById(addressId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpAddress for ID: " + addressId));

        PpAddress a = ppAddressService.createOrUpdateAddress(address);

        return new AddressDto(a);
    }

    @Secured("ROLE_ADMIN")
    @ApiOperation(value = "Delete an address", tags = {"Praxis - Admin - Addresses"})
    @DeleteMapping(path = "/addresses/{addressId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAddress(@PathVariable("addressId") Long addressId) {
        ppAddressService.getAddressById(addressId)
            .orElseThrow(() -> new ResourceNotFoundException("No PpAddress for ID: " + addressId));

        ppAddressService.deleteAddressById(addressId);
    }

    public static List<AddressDto> getAddressDtosForPerson(PpAddressService addressService,
                                                           PpPerson person) {
        return addressService.getAddressesForPersonId(person.getPersonId()).stream()
            .map(AddressDto::new)
            .collect(Collectors.toList());
    }
}
