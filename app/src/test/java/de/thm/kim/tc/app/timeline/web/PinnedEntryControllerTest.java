/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.web;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.account.service.UserService;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.*;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Objects;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PinnedEntryControllerTest {

    private static final String PINNED_ENTRIES_URL = "/timeline/pinned";

    private static final Long ENTRY_ID = 1L;
    private static final Long UNPINNED_ENTRY_ID = 4L;
    private static final Long INVALID_ENTRY_ID = 999L;

    private static final String VALID_USERNAME = "admin";
    private static final String INVALID_USERNAME = "i_dont_exist";

    @SpyBean
    private TimelinePinService timelinePinService;

    @SpyBean
    private TimelineReportService timelineReportService;

    @SpyBean
    private TimelineService timelineService;

    @SpyBean
    private UserService userService;

    @SpyBean
    private UserTimelineFaveService userTimelineFaveService;

    @SpyBean
    private UserTimelineUnpinService userTimelineUnpinService;

    @Autowired
    private MockMvc mockMvc;

    // FIXME Remove once deprecated route is gone
    @Test
    public void get_all_timeline_pins() throws Exception {
        // Number of pinned entries
        final int n = 5;

        mockMvc.perform(get(PINNED_ENTRIES_URL + "/all"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(n)))
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[1].id", is(4)))
            .andExpect(jsonPath("$[2].id", is(5)))
            .andExpect(jsonPath("$[3].id", is(35)))
            .andExpect(jsonPath("$[4].id", is(52)));

        verify(timelinePinService)
            .getAllTimelinePins();
        verify(timelineService, times(n))
            .getTimelineLikesForTimelineEntry(any(TimelineEntry.class));
    }

    // FIXME Test randomly fails with 3 pinned entries, regardless of running order
    //@WithMockUser(VALID_USERNAME)
    //@Test
    public void get_pinned_timeline_entries_for_user() throws Exception {
        // Number of pinned entries
        final int n = 4;

        mockMvc.perform(get(PINNED_ENTRIES_URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(n)))
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[1].id", is(5)))
            .andExpect(jsonPath("$[2].id", is(35)))
            .andExpect(jsonPath("$[3].id", is(52)));

        verify(userService)
            .getUserByPrincipal(any(Principal.class));
        verify(timelinePinService)
            .getAllTimelinePinsByUser(any(User.class));
        verify(userTimelineFaveService)
            .getTimelineFaveByTimelineEntriesAndUser(
                anyCollection(), any(User.class));
        verify(timelineService, times(n))
            .getTimelineLikesForTimelineEntry(any(TimelineEntry.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void get_all_pinned_timeline_entries_for_invalid_user() throws Exception {
        // Number of pinned entries
        final int n = 5;

        mockMvc.perform(get(PINNED_ENTRIES_URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(n)))
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[1].id", is(4)))
            .andExpect(jsonPath("$[2].id", is(5)))
            .andExpect(jsonPath("$[3].id", is(35)))
            .andExpect(jsonPath("$[4].id", is(52)));

        verify(userService)
            .getUserByPrincipal(any(Principal.class));
        verify(timelinePinService)
            .getAllTimelinePins();
        verify(timelineService, times(n))
            .getTimelineLikesForTimelineEntry(any(TimelineEntry.class));

        verify(timelinePinService, never())
            .getAllTimelinePinsByUser(any(User.class));
        verify(userTimelineFaveService, never())
            .getTimelineFaveByTimelineEntriesAndUser(
                anyCollection(), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void get_unpinned_timeline_entries_for_user() throws Exception {

        mockMvc.perform(get(PINNED_ENTRIES_URL + "/unpin"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id", is(4)));

        verify(userService).getUserByUsername(VALID_USERNAME);
        verify(userTimelineUnpinService)
            .getAllUnpinnedTimelineEntriesByUser(any(User.class));
        verify(timelineService)
            .getTimelineLikesForTimelineEntry(any(TimelineEntry.class));
        verify(timelineReportService)
            .isReportedByUser(any(TimelineEntry.class), any());
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_get_all_unpinned_timeline_entries_for_user_not_exists() throws Exception {

        mockMvc.perform(get(PINNED_ENTRIES_URL + "/unpin"))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineUnpinService, never())
            .getAllUnpinnedTimelineEntriesByUser(any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void unpin_timeline_entry_for_user_and_entity() throws Exception {

        mockMvc.perform(post(PINNED_ENTRIES_URL + "/unpin/" + ENTRY_ID))
            .andExpect(status().isOk());

        verify(userTimelineUnpinService)
            .addUnpinTimelineEntry(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_unpin_timeline_entry_for_user_not_exists() throws Exception {

        mockMvc.perform(post(PINNED_ENTRIES_URL + "/unpin/" + ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineUnpinService, never())
            .addUnpinTimelineEntry(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void should_fail_to_unpin_timeline_entry_for_entity_not_exists() throws Exception {

        mockMvc.perform(post(PINNED_ENTRIES_URL + "/unpin/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound());

        verify(userTimelineUnpinService, never())
            .addUnpinTimelineEntry(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_unpin_timeline_entry_for_user_and_entity_not_exists() throws Exception {

        mockMvc.perform(post(PINNED_ENTRIES_URL + "/unpin/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineUnpinService, never())
            .addUnpinTimelineEntry(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Transactional
    @Test
    public void unpin_timeline_delete_entry_for_user_and_entity() throws Exception {

        mockMvc.perform(delete(PINNED_ENTRIES_URL + "/unpin/" +
            UNPINNED_ENTRY_ID))
            .andExpect(status().isOk());

        verify(userTimelineUnpinService)
            .deleteUnpinTimelineEntryForUser(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_delete_unpin_timeline_entry_for_user_not_exists() throws Exception {

        mockMvc.perform(delete(PINNED_ENTRIES_URL + "/unpin/" + ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineUnpinService, never())
            .deleteUnpinTimelineEntryForUser(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(VALID_USERNAME)
    @Test
    public void should_fail_to_delete_unpin_timeline_entry_for_entity_not_exists() throws Exception {

        mockMvc.perform(delete(PINNED_ENTRIES_URL + "/unpin/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No TimelineEntry found for ID: " + INVALID_ENTRY_ID,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userTimelineUnpinService, never())
            .deleteUnpinTimelineEntryForUser(any(TimelineEntry.class), any(User.class));
    }

    @WithMockUser(INVALID_USERNAME)
    @Test
    public void should_fail_to_delete_unpin_timeline_entry_for_user_and_entity_not_exists() throws Exception {

        mockMvc.perform(delete(PINNED_ENTRIES_URL + "/unpin/" + INVALID_ENTRY_ID))
            .andExpect(status().isNotFound())
            .andExpect(result ->
                assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
            .andExpect(result ->
                assertEquals("No User found for username: " + INVALID_USERNAME,
                    Objects.requireNonNull(result.getResolvedException()).getMessage()));

        verify(userService).getUserByUsername(INVALID_USERNAME);

        verify(timelineService, never())
            .getTimelineEntryById(INVALID_ENTRY_ID);
        verify(userTimelineUnpinService, never())
            .deleteUnpinTimelineEntryForUser(any(TimelineEntry.class), any(User.class));
    }
}
