CREATE TABLE blocked_users (
    user_id         BIGINT NOT NULL,
    blocked_user_id BIGINT NOT NULL,
    date            TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (blocked_user_id) REFERENCES users (user_id),
    FOREIGN KEY (user_id) REFERENCES users (user_id),
    PRIMARY KEY (user_id, blocked_user_id)
);
