-- Re-add and update Q & A test data from V4_2__Add_Q_and_A_test_data.sql
INSERT INTO questions (question_id, user_id, category_id, text) VALUES
(1, 1, 1, 'Lorem ipsum Testfrage'),
(2, 2, 2, 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.'),
(3, 1, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');

INSERT INTO answers (answer_id, question_id, user_id, text) VALUES
(1, 1, 1, 'Donec pede Testantwort 1'),
(2, 1, 2, 'Aenean leo Testantwort 2'),
(3, 1, 2, 'Aliquam lorem ante Testantwort 3');

INSERT INTO question_likes (question_id, user_id) VALUES
(1, 1),
(1, 2),
(2, 1);


-- Re-add and update Q & A test data from V12_1__Add_Q_and_A_test_subscriptions.sql
INSERT INTO users_categories (user_id, category_id) VALUES
(1, 1),
(1, 10),
(2, 8);


-- Re-add and update Q & A test data from V2_2__Add_timeline_test_data.sql
INSERT INTO timeline_entries (timeline_entry_id, source_id, text, content_args) VALUES
( 1, 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', ''),
( 2, 1, 'Aenean commodo ligula eget dolor', ''),
( 3, 4, 'Aenean massa', ''),
( 4, 1, 'Cum sociis natoque penatibus et magnis dis parturient montes, nasce', ''),
( 5, 4, 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem', ''),
( 6, 3, 'Nulla consequat massa quis enim', ''),
( 7, 4, 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu' , ''),
( 8, 2, 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo', ''),
( 9, 5, 'Powerful User fragt: Lorem ipsum Testfrage', '1'),
(10, 3, 'At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles.', ''),
(11, 4, 'Cras dapibus', ''),
(12, 2, 'Vivamus elementum semper nisi', ''),
(13, 3, 'Aenean vulputate eleifend tellus', ''),
(14, 1, 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim', ''),
(15, 2, 'Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus', ''),
(16, 2, 'Phasellus viverra nulla ut metus varius laoreet', ''),
(17, 2, 'Quisque rutrum', ''),
(18, 2, 'Aenean imperdiet', ''),
(19, 4, 'Etiam ultricies nisi vel augue', ''),
(20, 5, 'Friendly User fragt: Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.', '2'),
(21, 3, 'Nam eget dui', ''),
(22, 2, 'Etiam rhoncus', ''),
(23, 1, 'Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper', ''),
(24, 3, 'Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem', ''),
(25, 4, 'Maecenas nec odio et ante tincidunt tempus', ''),
(26, 2, 'Donec vitae sapien ut libero venenatis faucibus', ''),
(27, 5, 'Powerful User fragt: Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '3'),
(28, 4, 'Etiam sit amet orci eget eros faucibus tincidunt', ''),
(29, 2, 'Duis leo', ''),
(30, 3, 'Sed fringilla mauris sit amet nibh', ''),
(31, 1, 'Donec sodales sagittis magna', ''),
(32, 1, 'Sed consequat, leo eget bibendum sodales, augue velit cursus nunc', ''),
(33, 3, 'Li Europan lingues es membres del sam familie', ''),
(34, 4, 'Lor separat existentie es un myth', ''),
(35, 3, 'Por scientie, musica, sport etc, litot Europa usa li sam vocabular', ''),
(36, 2, 'Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules', ''),
(37, 4, 'Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar cust', ''),
(38, 3, 'At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles', ''),
(39, 3, 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regular', ''),
(40, 3, 'Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues', ''),
(41, 3, 'It va esser tam simplic quam Occidental in fact, it va esser Occidental', ''),
(42, 3, 'A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit m', ''),
(43, 4, 'Lor separat existentie es un myth', ''),
(44, 2, 'Por scientie, musica, sport etc, litot Europa usa li sam vocabular', ''),
(45, 2, 'Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules', ''),
(46, 1, 'Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar cust', ''),
(47, 4, 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu', ''),
(48, 3, 'Aenean commodo ligula eget dolor', ''),
(49, 2, 'Aenean massa', ''),
(50, 1, 'Cum sociis natoque penatibus et magnis dis parturient montes, nasce', ''),
(51, 1, 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem', ''),
(52, 2, 'Nulla consequat massa quis enim', ''),
(53, 1, 'Lorem ipsum dolor sit amet', '');

INSERT INTO timeline_questions(timeline_entry_id, question_id) VALUES
( 9, 1),
(20, 2),
(27, 3);

INSERT INTO timeline_pins (timeline_entry_id) VALUES
(1),
(4),
(5),
(35),
(52);

INSERT INTO users_timeline_entries_faves (user_id, timeline_entry_id) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 8),
(1, 9);

INSERT INTO users_timeline_entries_hides (user_id, timeline_entry_id) VALUES (1, 6);

INSERT INTO users_timeline_entries_unpins (user_id, timeline_entry_id) VALUES (1, 4);
