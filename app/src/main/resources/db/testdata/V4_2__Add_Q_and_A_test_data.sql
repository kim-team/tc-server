-- Q & A
-- Test Data for table rewards in schema V1_2__Add_core_test_data.sql

INSERT INTO questions (user_id, category_id, text) VALUES
(1, 1, 'Lorem ipsum Testfrage'),
(2, 2, 'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.');

INSERT INTO answers (question_id, user_id, text) VALUES
(1, 1, 'Donec pede Testantwort 1'),
(1, 2, 'Aenean leo Testantwort 2'),
(1, 2, 'Aliquam lorem ante Testantwort 3');

INSERT INTO question_likes (question_id, user_id) VALUES
(1, 1),
(1, 2),
(2, 1);
