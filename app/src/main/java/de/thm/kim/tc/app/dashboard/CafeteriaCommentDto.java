/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.dashboard;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.cafeteria.data.entity.CafeteriaComment;
import lombok.Value;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.Base64;
import java.util.Date;

@Log
@Value
class CafeteriaCommentDto {
    Long id;
    String cafeteriaName;

    /**
     * The serving desk in the cafeteria
     */
    String category;

    /**
     * the descriptionClean of the item
     */
    String descriptionClean;
    User author;
    String text;
    Date createdAt;

    /**
     * Converts a {@link CafeteriaComment} to a {@link CafeteriaCommentDto}.
     * <p>
     * Base64 decoding errors are silently caught and logged. Missing fields
     * are padded with empty strings.
     *
     * @param comment The {@link CafeteriaComment} to convert
     */
    CafeteriaCommentDto(CafeteriaComment comment) {
        String[] cafeteriaFields = {"", "", ""};
        try {
            String decodedCafeteriaItem = new String(Base64.getDecoder().decode(comment.getCafeteriaItem()));
            cafeteriaFields = decodedCafeteriaItem.split("XXX", 3);
        } catch (Exception e) {
            log.warning(e.toString());
        }

        cafeteriaFields = Arrays.copyOf(cafeteriaFields, 3);
        this.id = comment.getId();
        this.cafeteriaName = cafeteriaFields[0];
        this.category = cafeteriaFields[1];
        this.descriptionClean = cafeteriaFields[2];
        this.author = comment.getAuthor();
        this.text = comment.getText();
        this.createdAt = comment.getCreatedAt();
    }
}
