/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.timeline.service;

import de.thm.kim.tc.app.account.data.entity.User;
import de.thm.kim.tc.app.common.exceptions.ResourceNotFoundException;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.data.entity.UserTimelineUnpin;
import de.thm.kim.tc.app.timeline.data.repository.UserTimelineUnpinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserTimelineUnpinService {

    private final UserTimelineUnpinRepository userTimelineUnpinRepository;

    @Autowired
    public UserTimelineUnpinService(UserTimelineUnpinRepository userTimelineUnpinRepository) {
        this.userTimelineUnpinRepository = userTimelineUnpinRepository;
    }

    public Collection<TimelineEntry> getAllUnpinnedTimelineEntriesByUser(User user) {
        return userTimelineUnpinRepository.findAllByUser(user)
            .stream()
            .map(UserTimelineUnpin::getTimelineEntry)
            .collect(Collectors.toList());
    }

    public void addUnpinTimelineEntry(TimelineEntry timelineEntry, User user) {
        if (userTimelineUnpinRepository.findByTimelineEntryAndUser(timelineEntry, user).isEmpty())
            userTimelineUnpinRepository.save(new UserTimelineUnpin(timelineEntry, user));
    }

    public void deleteUnpinTimelineEntryForUser(TimelineEntry timelineEntry, User user) {
        userTimelineUnpinRepository.findByTimelineEntryAndUser(timelineEntry, user)
            .ifPresentOrElse(userTimelineUnpinRepository::delete,
                () -> {
                    throw new ResourceNotFoundException("No UserTimelineUnpin found for TimelineEntry ID: " + timelineEntry.getId() + " and User ID: " + user.getId());
                });
    }

    public void deleteUnpinTimelineEntry(TimelineEntry timelineEntry) {
        userTimelineUnpinRepository.deleteAllByTimelineEntry(timelineEntry);
    }

    @Transactional
    public void deleteAllByUser(User user) {
        userTimelineUnpinRepository.deleteAllByUser(user);
    }
}
