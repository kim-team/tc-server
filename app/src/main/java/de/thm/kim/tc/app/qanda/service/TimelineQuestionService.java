/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package de.thm.kim.tc.app.qanda.service;

import de.thm.kim.tc.app.qanda.data.entity.Question;
import de.thm.kim.tc.app.qanda.data.entity.TimelineQuestion;
import de.thm.kim.tc.app.qanda.data.repository.TimelineQuestionRepository;
import de.thm.kim.tc.app.timeline.data.entity.Source;
import de.thm.kim.tc.app.timeline.data.entity.TimelineEntry;
import de.thm.kim.tc.app.timeline.service.SourceService;
import de.thm.kim.tc.app.timeline.service.TimelineService;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class TimelineQuestionService {

    private final SourceService sourceService;
    private final TimelineService timelineService;
    private final TimelineQuestionRepository repo;

    public TimelineQuestionService(SourceService sourceService,
                                   TimelineService timelineService,
                                   TimelineQuestionRepository repo) {
        this.sourceService = sourceService;
        this.timelineService = timelineService;
        this.repo = repo;
    }

    @Transactional
    public void deleteTimelineEntryForQuestion(Question question) {
        val entry = repo.findByQuestion(question);

        if (entry.isPresent()) {
            repo.deleteByQuestion(question);
            timelineService.deleteTimelineEntry(entry.get().getTimelineEntry());
        }
    }

    public void createOrUpdateTimelineEntryForQuestion(Question question) {
        val t = getLinkedTimelineEntry(question);
        Source source = sourceService.getSourceByName("timeline.sources.qanda.title");
        // TODO i18n
        String text = question.getAuthor().getName() + " fragt: " + question.getText();
        String contentArgs = question.getId().toString();

        if (t.isPresent()) {
            TimelineEntry entry = t.get();
            entry.setSource(source);
            entry.setText(text);
            entry.setContentArgs(contentArgs);
            timelineService.updateTimelineEntry(t.get());
        } else {
            // Post new question to timeline
            val entry = timelineService.createNewTimelineEntry(source, text, contentArgs);

            repo.save(new TimelineQuestion(question, entry));
        }
    }

    public Optional<TimelineEntry> getLinkedTimelineEntry(Question question) {
        return repo.findByQuestion(question).map(TimelineQuestion::getTimelineEntry);
    }

    public Optional<Question> getLinkedQuestion(TimelineEntry entry) {
        return repo.findByTimelineEntry(entry).map(TimelineQuestion::getQuestion);
    }
}
