-- Move test questions and user_categories before deleting categories,
-- see db/migration/V42_1__Remove_some_Q_and_A_categories.sql

-- 'Studienstart (Bachelor)'
UPDATE questions        SET category_id = 6 WHERE category_id = 2;

-- 'Campus THM'
UPDATE users_categories SET category_id = 6 WHERE category_id = 8;

-- 'Smalltalk'
UPDATE users_categories SET category_id = 6 WHERE category_id = 10;
